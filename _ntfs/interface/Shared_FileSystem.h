#ifndef _SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED
#define _SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED
/*
	Created by Tech_dog (VToropov) on 28-Nov-2014 at 11:42:03pm, GMT+3, Taganrog, Friday;
	This is Shared Lite Library File System Wrapper class declaration file.
*/
#include "Shared_FS_CommonDefs.h"

namespace shared { namespace ntfs
{
	using shared::lite::data::CRawData;
	using shared::lite::common::CSysError;

	class CGenericDriveType
	{
	public:
		enum _e{
			eUnknown   = DRIVE_UNKNOWN    ,
			eNoRoot    = DRIVE_NO_ROOT_DIR,
			eRemovable = DRIVE_REMOVABLE  ,
			eFixed     = DRIVE_FIXED      ,
			eRemote    = DRIVE_REMOTE     ,
			eOptical   = DRIVE_CDROM      ,
			eMemory    = DRIVE_RAMDISK    ,
		};
	};

	class CGenericDrive
	{
	private:
		mutable
		CSysError         m_error;
	public:
		CGenericDrive(void);
	public:
		TDriveList        Enumerate(const CGenericDriveType::_e);
		TErrorRef         Error(void)const;
	};

	class CGenericFile
	{
	private:
		mutable
		CSysError         m_error;
		CAtlString        m_path;
		CAtlString        m_name;
	public:
		CGenericFile(LPCTSTR pszPath);
		~CGenericFile(void);
	public:
		HRESULT           Delete  (VOID);
		TErrorRef         Error   (VOID)const;
		CAtlString        Name    (VOID)const;
		HRESULT           Read    (CRawData&)const;
		LONGLONG          Size    (VOID)const;
	};

	class CGenericFolder
	{
	private:
		mutable
		CSysError         m_error;
		CAtlString        m_path;
	public:
		CGenericFolder(LPCTSTR pszPath);
		~CGenericFolder(void);
	public:
		HRESULT           Create  (VOID);
		HRESULT           Delete  (VOID);
		HRESULT           EnumerateFiles(TFileListSorted& _out, const bool bIncludeSubfolders = true, LPCTSTR pszMask = _T("*"));
		HRESULT           EnumerateItems(TFileList& _files, TFolderList& _subfolders, LPCTSTR pszMask = _T("*")); // enumerates items in specific folder only, no recursion
		TErrorRef         Error   (VOID)const;
		bool              IsFolder(VOID)const;
		bool              IsExist (VOID)const;
		CAtlString        Path    (VOID)const;
	};

	class CPathFinder
	{
	public:
		CPathFinder(LPCTSTR pszDesiredPath);
		~CPathFinder(void);
	};

	class CCurrentFolder
	{
	private:
		CSysError        m_error;
		CAtlString       m_restore;
	public:
		CCurrentFolder(LPCTSTR pszDesiredPath);
		~CCurrentFolder(void);
	public:
		TErrorRef        Error(void)const;
	};

	class CSpecialFolder
	{
	private:
		CSysError         m_error;
	public:
		CSpecialFolder(void);
	public:
		TErrorRef         Error(void)const;
		CAtlString        ProgramFiles  (void); // the program files folder;
		CAtlString        ProgramFiles86(void); // the program files folder for 32-bit programs on 64-bit platform;
		CAtlString        PublicUserData(void); // the file system directory that contains application data for all users;
	};
}}

#endif/*_SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED*/