#ifndef _SHAREDFILEENUMASYNC_H_98F9BB92_EBB2_4689_8249_18264181EE2B_INCLUDED
#define _SHAREDFILEENUMASYNC_H_98F9BB92_EBB2_4689_8249_18264181EE2B_INCLUDED
/*
	Created by Tech_dog (VToropov) on 28-Dec-2015 at 6:41:25am, GMT+7, Phuket, Rawai, Monday;
	This is shared NTFS library folder/file enumerator task class declaration file.
*/
#include "Shared_SystemThreadPool.h"
#include "Shared_FileSystem.h"

namespace shared { namespace ntfs
{
	using shared::lite::sys_core::CThreadBase;

	class CFileEnumAsync : public CThreadBase
	{
	private:
		IFileEnumCallback&  m_callback;
	public:
		CFileEnumAsync(IFileEnumCallback&);
		~CFileEnumAsync(void);
	private:
		VOID       ThreadFunction(VOID)override sealed;
	public:
		HRESULT    AppendFolder(LPCTSTR pszFolder);
		HRESULT    AppendFolderList(const TFolderList&);
	};
}}

#endif/*_SHAREDFILEENUMASYNC_H_98F9BB92_EBB2_4689_8249_18264181EE2B_INCLUDED*/	