#ifndef _SHAREDFSCOMMONDEFS_H_B57D89F2_9784_471f_B070_06D8EBCA84E5_INCLUDED
#define _SHAREDFSCOMMONDEFS_H_B57D89F2_9784_471f_B070_06D8EBCA84E5_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Mar-2016 at 11:35:39am, GMT+7, Phuket, Rawai, Thursday;
	This is shared NTFS library common definition declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_RawData.h"

#include <shlobj.h>
#include <shellapi.h>

#pragma comment(lib, "shell32.lib")

namespace shared { namespace ntfs
{
	typedef ::std::vector<CAtlString>          TDriveList;
	typedef ::std::vector<CAtlString>          TFileList;
	typedef ::std::map<CAtlString, CAtlString> TFileListSorted;
	typedef ::std::vector<CAtlString>          TFolderList;

	interface IFileEnumCallback
	{
		virtual VOID IFileEnum_OnBegin(VOID)                  PURE;
		virtual VOID IFileEnum_OnFileAppend(const TFileList&) PURE;
		virtual VOID IFileEnum_OnFinish(VOID)                 PURE;
		virtual BOOL IFileEnum_OnInterruptRequest(VOID)       PURE;
	};
}}

#endif/*_SHAREDFSCOMMONDEFS_H_B57D89F2_9784_471f_B070_06D8EBCA84E5_INCLUDED*/