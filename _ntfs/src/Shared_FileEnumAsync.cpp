/*
	Created by Tech_dog (VToropov) on 28-Dec-2015 at 7:11:12am, GMT+7, Phuket, Rawai, Monday;
	This is shared NTFS library folder/file enumerator task class implementation file.
*/
#include "StdAfx.h"
#include "Shared_FileEnumAsync.h"

using namespace shared::ntfs;

#include "Shared_SystemCore.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericEvent.h"

using namespace shared::lite::sys_core;
using namespace shared::lite::runnable;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace ntfs { namespace details
{
	class CFileEnumSafeQueue
	{
		typedef ::std::queue<TFolderList> TAwaitQueue;
	private:
		CGenericSyncObject m_sync_obj;
		TAwaitQueue        m_queue;
	public:
		CFileEnumSafeQueue(void){}
	public:
		HRESULT            AppendFolders(const TFolderList& _list)
		{
			if (_list.empty())
				return E_INVALIDARG;
			{
				SAFE_LOCK(m_sync_obj);
				try
				{
					m_queue.push(_list);
				}
				catch(::std::bad_alloc&)
				{
					return E_OUTOFMEMORY;
				}
			}
			return S_OK;
		}

		TFolderList        GetFolderList(void)
		{
			TFolderList list_;
			{
				SAFE_LOCK(m_sync_obj);
				if (!m_queue.empty())
				{
					list_ = m_queue.front();
					m_queue.pop();
				}
			}
			return list_;
		}

		bool               IsEmpty(void) const
		{
			bool bEmpty = true;
			{
				SAFE_LOCK(m_sync_obj);
				bEmpty = m_queue.empty();
			}
			return bEmpty;
		}
	};

	CFileEnumSafeQueue&  FileEnum_GetQueueObject(void)
	{
		static CFileEnumSafeQueue queue_;
		return queue_;
	}

	HRESULT FileEnum_InternalFunc(const TFolderList& _list, IFileEnumCallback& _callback)
	{
		HRESULT hr_ = S_OK;

		if (_list.empty())
			return hr_;

		const size_t count_ = _list.size();
		for ( size_t i_ = 0; i_ < count_; i_++)
		{
			CGenericFolder folder_(_list[i_]);
			if (!folder_.IsExist())
				continue;
			if (!folder_.IsFolder())
				continue;

			if (_callback.IFileEnum_OnInterruptRequest())
				break;

			TFileList files_;
			TFolderList sub_folders_;

			HRESULT hr_ = folder_.EnumerateItems(files_, sub_folders_);
			if (FAILED(hr_))
				continue;
			if (!files_.empty())
				_callback.IFileEnum_OnFileAppend(files_);
			if (!sub_folders_.empty())
			{
				hr_ = FileEnum_InternalFunc(sub_folders_, _callback); // recursion
				if (FAILED(hr_))
					break;
			}
		}

		return hr_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CFileEnumAsync::CFileEnumAsync(IFileEnumCallback& _clbk):m_callback(_clbk)
{
	m_error.Source(_T("CFileEnumAsync"));
}

CFileEnumAsync::~CFileEnumAsync(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID       CFileEnumAsync::ThreadFunction(VOID)
{
	CCoInitializer com_core(false);

	CGenericWaitObject wait_(10, 1000);

	details::CFileEnumSafeQueue& queue_ = details::FileEnum_GetQueueObject();

	while (!m_crt.IsStopped())
	{
		wait_.Wait();
		if (wait_.IsElapsed())
			wait_.Reset();
		else
			continue;
		if (queue_.IsEmpty())
			continue;

		const TFolderList list_ = queue_.GetFolderList();
		if (list_.empty())
			continue;

		m_callback.IFileEnum_OnBegin();

		HRESULT hr_ = details::FileEnum_InternalFunc(list_, m_callback);
		if (FAILED(hr_))
		{
			CThreadBase::m_error.SetHresult(hr_);
			break;
		}
		else
			m_error = S_OK;

		m_callback.IFileEnum_OnFinish();
	}
	::SetEvent(m_crt.EventObject());
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CFileEnumAsync::AppendFolder(LPCTSTR pszFolder)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TFolderList list_;
	try
	{
		list_.push_back(CAtlString(pszFolder));
		this->AppendFolderList(list_);
	}
	catch(::std::bad_alloc&)
	{
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}

HRESULT    CFileEnumAsync::AppendFolderList(const TFolderList& _list)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (_list.empty())
		return (m_error = E_INVALIDARG);

	details::CFileEnumSafeQueue& queue_ = details::FileEnum_GetQueueObject();
	HRESULT hr_ = queue_.AppendFolders(_list);
	if (FAILED(hr_))
		m_error.SetState(
			hr_,
			_T("Cannot append folder list for file enumeration")
		);
	else if (!this->IsRunning())
		this->Start();

	return m_error;
}