#ifndef _SHAREDNTFSSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
#define _SHAREDNTFSSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 1-Mar-2016 at 10:21:33pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared NTFS Wrapper Library precompiled headers declaration file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0502  // this is for use Windows XP SP1 (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0502  // this is for use Windows XP SP1 (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0603  // this is for use IE 6 SP3 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>

#include <comdef.h>
#include <comutil.h>

#include <vector>
#include <map>
#include <queue>

#endif/*_SHAREDNTFSSTDAFX_H_D8061509_9F1B_4e4d_ABA3_B26B8082FFB6_INCLUDED*/