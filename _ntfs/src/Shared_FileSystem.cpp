/*
	Created by Tech_dog (VToropov) on 28-Nov-2014 at 11:42:03pm, GMT+3, Taganrog, Friday;
	This is Shared Lite Library File System Wrapper class implementation file.
*/
#include "StdAfx.h"
#include "Shared_FileSystem.h"
#include "Shared_GenericHandle.h"

using namespace shared::ntfs;
using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace ntfs { namespace details
{
	HRESULT  FileSystem_CreateLongPath(LPCTSTR pFilePath, ::ATL::CAtlString& cs_path)
	{
		if (!pFilePath || ::_tcslen(pFilePath) < _MAX_DRIVE)
			return E_INVALIDARG;
		if (!cs_path.IsEmpty())cs_path.Empty();
		cs_path += _T("\\\\?\\");
		cs_path += pFilePath;
		cs_path.Replace(_T('/'), _T('\\'));
		return S_OK;
	}

	HRESULT  FileSystem_GetAttributes(LPCTSTR pszPath, WIN32_FILE_ATTRIBUTE_DATA& _data)
	{
		::ATL::CAtlString cs_long_path;
		HRESULT hr_ = details::FileSystem_CreateLongPath(pszPath, cs_long_path);
		if (S_OK != hr_)
			return  hr_;

		const BOOL bResult = ::GetFileAttributesEx(cs_long_path, GetFileExInfoStandard, &_data);
		if (!bResult)
			hr_ = HRESULT_FROM_WIN32(::GetLastError());

		return hr_;
	}

	HRESULT  FileSystem_GetSpecialFolder(const INT _id, CAtlString& _path)
	{
		TCHAR szPath[MAX_PATH] = {0};
		HRESULT hr_ = ::SHGetFolderPath(
							NULL,
							_id|CSIDL_FLAG_CREATE, 
							NULL,
							0,
							szPath
						);
		if (SUCCEEDED(hr_))
			_path = szPath;

		return hr_;
	}

	VOID     FileSystem_NormalizeFolderPath(CAtlString& _path)
	{
		if (_path.Right(1) != _T("\\"))
			_path += _T("\\");
	}

	VOID     FileSystem_SplitFilePath(LPCTSTR pszFilePath, CAtlString& _name)
	{
		TCHAR name_[_MAX_FNAME] = {0};
		TCHAR ext_ [_MAX_EXT  ] = {0};

		::_tsplitpath_s(
				pszFilePath    ,
				NULL           ,
				0              ,
				NULL           ,
				0              ,
				name_          ,
				_countof(name_),
				ext_           ,
				_countof(ext_)
			);
		_name.Format(
				_T("%s%s"),
				name_,
				ext_
			);
	}

	VOID     FileSystem_SplitFolderPath(LPCTSTR pszFolderPath, CAtlString& _drive, CAtlString& _folder)
	{
		CAtlString cs_path(pszFolderPath);

		TCHAR drv_[_MAX_DRIVE] = {0};
		TCHAR dir_[_MAX_DIR  ] = {0};

		::_tsplitpath_s(
				cs_path       ,
				drv_          ,
				_countof(drv_),
				dir_          ,
				_countof(dir_),
				NULL          ,
				0             ,
				NULL          ,
				0
			);
		_drive = drv_;
		_folder = dir_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CGenericDrive::CGenericDrive(void)
{
	m_error.Source(_T("CGenericDrive"));
}

/////////////////////////////////////////////////////////////////////////////

TDriveList   CGenericDrive::Enumerate(const CGenericDriveType::_e _type)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TDriveList lst_;

	DWORD mask_ = ::GetLogicalDrives();
	if ( !mask_)
		m_error = ::GetLastError();
	else
	{
		TCHAR drive_[] = _T("a:\\");

		while (mask_)
		{
			if (mask_ & 1)
			{
				CAtlString cs_drive(drive_);
				const UINT uType = ::GetDriveType(cs_drive);
				if ((UINT) _type == uType)
				{
					try
					{
						lst_.push_back(cs_drive);
					}
					catch (::std::bad_alloc&)
					{
						m_error = E_OUTOFMEMORY;
						break;
					}
				}
			}
			++drive_[0];
			mask_ >>= 1;
		}
	}
	return lst_;
}

TErrorRef    CGenericDrive::Error(void)const
{
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CGenericFile::CGenericFile(LPCTSTR pszPath) : m_path(pszPath)
{
	m_error.Source(_T("CGenericFile"));
	if (m_path.IsEmpty())
		m_error.SetState(
				E_INVALIDARG,
				_T("Path cannot be empty")
			);
	else
	{
		details::FileSystem_SplitFilePath(pszPath, m_name);
	}
}

CGenericFile::~CGenericFile(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CGenericFile::Delete  (VOID)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const BOOL res_ = ::DeleteFile(
								m_path
							);
	if (!res_)
		m_error = ::GetLastError();

	return m_error;
}

TErrorRef    CGenericFile::Error   (VOID)const
{
	return m_error;
}

CAtlString   CGenericFile::Name    (VOID)const
{
	return m_name;
}

HRESULT      CGenericFile::Read    (CRawData& _data)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAutoHandle hFile = ::CreateFile(
				m_path,
				GENERIC_READ,
				FILE_SHARE_READ,
				NULL,
				OPEN_EXISTING,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);
	if (!hFile.IsValid())
		return (m_error = ::GetLastError());
	const DWORD dwSize  = ::GetFileSize(hFile, NULL);
	m_error = _data.Create(dwSize);
	if (m_error)
		return m_error;

	DWORD dwBytesRead = 0;
	const BOOL bResult = ::ReadFile(
				hFile,
				(LPVOID)
				_data.GetData(),
				_data.GetSize(),
				&dwBytesRead,
				NULL
			);
	if (!bResult)
		m_error = ::GetLastError();

	return m_error;
}

LONGLONG     CGenericFile::Size    (VOID)const
{
	m_error.Module(_T(__FUNCTION__));

	LARGE_INTEGER sz_ = {0};

	WIN32_FILE_ATTRIBUTE_DATA data_ = {0};
	m_error = details::FileSystem_GetAttributes(m_path, data_);

	if (!m_error)
	{
		if (0 != (FILE_ATTRIBUTE_DIRECTORY & data_.dwFileAttributes))
			m_error.SetState(
					DISP_E_TYPEMISMATCH,
					_T("Wrong type of file system object")
				);
		else
		{
			sz_.HighPart = data_.nFileSizeHigh;
			sz_.LowPart  = data_.nFileSizeLow;
		}
	}

	return sz_.QuadPart;
}

/////////////////////////////////////////////////////////////////////////////

CGenericFolder::CGenericFolder(LPCTSTR pszPath) : m_path(pszPath)
{
	m_error.Source(_T("CGenericFolder"));
	if (m_path.IsEmpty())
		m_error.SetState(
				E_INVALIDARG,
				_T("Path cannot be empty")
			);
	else
		details::FileSystem_NormalizeFolderPath(m_path);
}

CGenericFolder::~CGenericFolder(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CGenericFolder::Create  (VOID)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;
	
	if (m_path.IsEmpty())
		return (m_error = OLE_E_BLANK);

	TCHAR t_drv[_MAX_DRIVE] = {0};
	TCHAR t_dir[_MAX_DIR*9] = {0};
	{
		::ATL::CAtlString cs_path(m_path);
		cs_path.Replace(_T('/'), _T('\\'));
		if (_T('\\') != cs_path.Right(1))
			cs_path += _T('\\');
		::_tsplitpath(
				cs_path,
				t_drv  ,
				t_dir  ,
				NULL   ,
				NULL
			);
	}
	typedef ::std::vector<::ATL::CAtlString> TFolders;
	TFolders  folders;
	{
		::ATL::CAtlString cs_dir(t_dir);
		if (_T('\\') == cs_dir.Left(1))
			cs_dir = cs_dir.Right(cs_dir.GetLength() - 1);
		::ATL::CAtlString cs_folder;
		INT n_pos  = 0;
		cs_folder  = cs_dir.Tokenize(_T("\\"), n_pos);
		while (!cs_folder.IsEmpty())
		{
			try
			{
				folders.push_back(cs_folder);
			}
			catch (::std::bad_alloc&)
			{
				return (m_error = E_OUTOFMEMORY);
			}
			cs_folder = cs_dir.Tokenize(_T("\\"), n_pos);
		}
	}
	if (folders.empty())
		return (m_error = E_INVALIDARG);

	::ATL::CAtlString cs_long_path;
	cs_long_path += _T("\\\\?\\");
	cs_long_path += t_drv;

	for (size_t i_ = 0; i_ < folders.size(); i_++)
	{
		cs_long_path += _T("\\");
		cs_long_path += folders[i_];
		if (::PathFileExists(cs_long_path))
			continue;
		if (!::CreateDirectory(cs_long_path.GetString(), NULL))
			return (m_error = ::GetLastError());
	}

	return m_error;
}

HRESULT      CGenericFolder::Delete  (VOID)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	TCHAR szFolder[2048] = {0};
	if (_countof(szFolder) < m_path.GetLength() + 2) // must be terminated by double zeros
		return (m_error = (DWORD)ERROR_INSUFFICIENT_BUFFER);

	::_tcscpy(szFolder, m_path);

	SHFILEOPSTRUCT fo_ = {
			NULL,
			FO_DELETE,
			szFolder,
			NULL ,
			FOF_SILENT|FOF_NOCONFIRMATION|FOF_NOERRORUI|FOF_NOCONFIRMMKDIR,
			FALSE,
			NULL ,
			NULL
		};
	const INT result_ = ::SHFileOperation(&fo_);
	if (result_)
	{
		m_error.SetState(
				E_FAIL,
				_T("System error occurred while deleting the folder")
			);
	}
	return m_error;
}

HRESULT      CGenericFolder::EnumerateFiles(TFileListSorted& _out, const bool bIncludeSubfolders, LPCTSTR pszMask)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_path.IsEmpty())
		return (m_error = OLE_E_BLANK);

	CAtlString query_(this->m_path);
	query_ += pszMask;

	WIN32_FIND_DATA data_ = {0};
	HANDLE hFind = ::FindFirstFile(query_, &data_);
	if (INVALID_HANDLE_VALUE == hFind)
		return (m_error = ::GetLastError());
	do
	{
		const bool bIsFolder = (0 != (FILE_ATTRIBUTE_DIRECTORY & data_.dwFileAttributes));
		if (bIsFolder)
		{
			if (bIncludeSubfolders)
			{
				CAtlString cs_folder = data_.cFileName;
				if (cs_folder.GetAt(0) != _T('.')) // checks for special folder names (parent and root folder)
				{
					CAtlString cs_path = m_path;
					cs_path += cs_folder;

					CGenericFolder subfolder_(cs_path);
					HRESULT hr_ = subfolder_.EnumerateFiles(_out, bIncludeSubfolders, pszMask);
					if (FAILED(hr_))
						break;
				}
			}
		}
		else
		{
			CAtlString cs_path = m_path;
			CAtlString cs_file = data_.cFileName;
			cs_path += cs_file;
			try
			{
				_out[cs_path] = cs_file;
			}
			catch(::std::bad_alloc&)
			{
				return m_error = E_OUTOFMEMORY;
			}
		}
	} while (::FindNextFile(hFind, &data_));
	::FindClose(hFind);

	return m_error;
}

HRESULT      CGenericFolder::EnumerateItems(TFileList& _files, TFolderList& _subfolders, LPCTSTR pszMask)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_path.IsEmpty())
		return (m_error = OLE_E_BLANK);

	CAtlString query_(this->m_path);
	details::FileSystem_NormalizeFolderPath(query_);
	query_ += pszMask;

	WIN32_FIND_DATA data_ = {0};
	HANDLE hFind = ::FindFirstFile(query_, &data_);
	if (INVALID_HANDLE_VALUE == hFind)
		return (m_error = ::GetLastError());
	do
	{
		const bool bIsFolder = (0 != (FILE_ATTRIBUTE_DIRECTORY & data_.dwFileAttributes));
		if (bIsFolder)
		{
			CAtlString cs_folder = data_.cFileName;
			if (cs_folder.GetAt(0) != _T('.')) // checks for special folder names (parent and root folder)
			{
				try
				{
					CAtlString cs_path = m_path;
					cs_path += cs_folder;

					_subfolders.push_back(cs_path);
				}
				catch(::std::bad_alloc&)
				{
					return m_error = E_OUTOFMEMORY;
				}
			}
		}
		else
		{
			CAtlString cs_path = m_path;
			CAtlString cs_file = data_.cFileName;
			cs_path += cs_file;
			try
			{
				_files.push_back(cs_file);
			}
			catch(::std::bad_alloc&)
			{
				return m_error = E_OUTOFMEMORY;
			}
		}
	} while (::FindNextFile(hFind, &data_));
	::FindClose(hFind);

	return m_error;
}

TErrorRef    CGenericFolder::Error   (VOID)const
{
	return m_error;
}

bool         CGenericFolder::IsFolder(VOID)const
{
	m_error.Module(_T(__FUNCTION__));

	if (m_path.IsEmpty())
		return false;

	WIN32_FILE_ATTRIBUTE_DATA data_ = {0};

	m_error = details::FileSystem_GetAttributes(m_path, data_);
	if (m_error)
		return false;
	else
		return (0 != (FILE_ATTRIBUTE_DIRECTORY & data_.dwFileAttributes));
}

bool         CGenericFolder::IsExist (VOID)const
{
	m_error.Module(_T(__FUNCTION__));
	
	if (m_path.IsEmpty())
		return false;

	if (m_error)
		m_error.Clear();

	::ATL::CAtlString cs_path(m_path);
	cs_path.Replace(_T('/'), _T('\\'));

	if (!::PathFileExists(cs_path))
	{
		m_error = ::GetLastError();
		return false;
	}
	else
		return true;
}

CAtlString   CGenericFolder::Path(VOID)const
{
	return m_path;
}

/////////////////////////////////////////////////////////////////////////////

CPathFinder::CPathFinder(LPCTSTR pszDesiredPath)
{
	::SetDllDirectory(pszDesiredPath);
}

CPathFinder::~CPathFinder(void)
{
	::SetDllDirectory(NULL);
}

/////////////////////////////////////////////////////////////////////////////

CCurrentFolder::CCurrentFolder(LPCTSTR pszDesiredPath)
{
	m_error.Source(_T("CCurrentFolder"));
	m_error.Module(_T("{ctor}"));

	if (!::_tcslen(pszDesiredPath))
		m_error.SetState(
				E_INVALIDARG,
				_T("New current directory path cannot be empty")
			);
	else
	{
		TCHAR szRestore[_MAX_PATH] = {0};
		if (!::GetCurrentDirectory(_countof(szRestore), szRestore))
			m_error = ::GetLastError();
		else
		{
			m_restore = szRestore;
			if (!::SetCurrentDirectory(pszDesiredPath))
				m_error = ::GetLastError();
			else
				m_error = S_OK;
		}
	}
}

CCurrentFolder::~CCurrentFolder(void)
{
	if (!m_restore.IsEmpty())
		::SetCurrentDirectory(m_restore);
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CCurrentFolder::Error(void)const
{
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CSpecialFolder::CSpecialFolder(void)
{
	m_error.Source(_T("CSpecialFolder"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CSpecialFolder::Error(void)const
{
	return m_error;
}

CAtlString   CSpecialFolder::ProgramFiles (void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;
	HRESULT hr_ = details::FileSystem_GetSpecialFolder(CSIDL_PROGRAM_FILES, cs_path);
	if (FAILED(hr_))
		m_error = hr_;

	return cs_path;
}

CAtlString   CSpecialFolder::ProgramFiles86(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;
	HRESULT hr_ = details::FileSystem_GetSpecialFolder(CSIDL_PROGRAM_FILESX86 , cs_path);
	if (FAILED(hr_))
		m_error = hr_;

	return cs_path;
}

CAtlString   CSpecialFolder::PublicUserData(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	CAtlString cs_path;
	HRESULT hr_ = details::FileSystem_GetSpecialFolder(CSIDL_COMMON_APPDATA, cs_path);
	if (FAILED(hr_))
		m_error = hr_;

	return cs_path;
}