/*
	Created by Tech_dog (VToropov) on 17-Mar-2015 at 1:56:57pm, GMT+3, Taganrog, Tuesday;
	This is UIX library label control window class implementation file.
*/
#include "StdAfx.h"
#include "UIX_LabelWnd.h"

using namespace ex_ui;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::draw::common;
////////////////////////////////////////////////////////////////////////////

CLabelWnd::CLabelWnd(CControlCrt& crt_ref) :
	m_crt(crt_ref),
	m_fore(::GetSysColor(COLOR_WINDOWTEXT)),
	m_family(_T("verdana")),
	m_size(10),
	m_underline(false),
	m_horz_align(DT_LEFT)
{
}

CLabelWnd::~CLabelWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CLabelWnd::OnEraseBkgnd (UINT, WPARAM wParam, LPARAM, BOOL&)
{
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);
	const HDC hDC = reinterpret_cast<HDC>(wParam);
	CZBuffer dc_(hDC, rc_);
	// 1) draws parent background
	m_crt.CtrlParentRenderer_Ref().DrawParentBackground(TWindow::m_hWnd, dc_, rc_);
	// 2) draws the text
	{
		::ATL::CAtlString csText;
		TWindow::GetWindowText(csText);
		LPCTSTR pszText = csText.GetString();
		dc_.DrawTextExt(
				pszText,
				m_family,
				m_size,
				rc_,
				m_fore,
				m_horz_align|DT_VCENTER|DT_WORDBREAK|DT_NOCLIP|(m_underline ? eCreateFontOption::eUnderline : 0)
			);
	}
	return 0;
}

LRESULT CLabelWnd::OnLButtonDown(UINT, WPARAM, LPARAM, BOOL&)
{
	m_crt.CtrlEventSink_Ref().IControlNotify_OnClick(m_crt.CtrlId());
	return 0;
}

LRESULT CLabelWnd::OnPaint      (UINT, WPARAM, LPARAM, BOOL&)
{
	WTL::CPaintDC dc_(m_hWnd);
	return 0;
}

LRESULT CLabelWnd::OnTextChange (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CLabelWnd::_FontFamily(LPCTSTR pszFamily)
{
	if (!pszFamily || !::_tcslen(pszFamily))
		return E_INVALIDARG;

	m_family = pszFamily;

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT   CLabelWnd::_FontSize(const DWORD dwSize)
{
	if (dwSize < 6)
		return E_INVALIDARG;

	m_size = dwSize;

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT   CLabelWnd::_FontUnderline(const bool _set)
{
	m_underline = _set;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT   CLabelWnd::_ForeColor(const COLORREF _clr)
{
	const bool bChanged = (_clr != m_fore);
	if (bChanged)
		m_fore = _clr;
	return (bChanged ? S_OK : S_FALSE);
}

HRESULT   CLabelWnd::_HorzAlign(const DWORD _align)
{
	const bool bChanged = (_align != m_horz_align);
	if (bChanged)
		m_horz_align = _align;
	return (bChanged ? S_OK : S_FALSE);
}