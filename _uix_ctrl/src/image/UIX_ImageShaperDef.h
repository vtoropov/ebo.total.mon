#ifndef _UIXCTRLIMAGECONTROLSHAPER_H_242E0C29_11DD_4eef_902B_811613112ABD_INCLUDED
#define _UIXCTRLIMAGECONTROLSHAPER_H_242E0C29_11DD_4eef_902B_811613112ABD_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 7:17:20PM, GMT+3, Taganrog, Saturday;
	This is UIX library image control shaper class declaration file.
*/
#include "UIX_Image.h"
#include "UIX_PngWrap.h"

namespace ex_ui { namespace controls { namespace _impl
{
	using ex_ui::controls::CImage;
	using ex_ui::controls::eImageStyle;
	using ex_ui::draw::common::CPngBitmapPtr;

	class CImageShaperDefaultImpl
	{
	public:
		enum  eRectangleType
		{
			eRT_Entire = 0, 
			eRT_Image  = 1,
			eRT_Last   = eRT_Image,
		};
	private:
		CImage&          m_ctrl_ref;
		CPngBitmapPtr&   m_image_ref;
		RECT             m_rects[CImageShaperDefaultImpl::eRT_Last + 1];
	public:
		CImageShaperDefaultImpl(CImage&, CPngBitmapPtr&);
		~CImageShaperDefaultImpl(void);
	public:
		RECT             GetDefaultWindowRect(void) const;
		const RECT&      GetRectangle(const CImageShaperDefaultImpl::eRectangleType) const;
		HRESULT          RecalcLayout(void);
	private:
		bool            _AlignToImage(void) const;
		bool            _StretchImage(void) const;
	};
}}}

#endif/*_UIXCTRLIMAGECONTROLSHAPER_H_242E0C29_11DD_4eef_902B_811613112ABD_INCLUDED*/