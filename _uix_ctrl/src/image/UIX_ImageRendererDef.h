#ifndef _UIXCTRLIMAGECONTROLDEFAULTRENDERER_H_0AB4E1FF_AE63_45b1_99BA_44F1445A9430_INCLUDED
#define _UIXCTRLIMAGECONTROLDEFAULTRENDERER_H_0AB4E1FF_AE63_45b1_99BA_44F1445A9430_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 7:57:14pm, GMT+3, Taganrog, Saturday;
	This is UIX library image control default renderer class declaration file.
*/
#include "UIX_Image.h"
#include "UIX_ImageShaperDef.h"
#include "UIX_Renderer.h"

#include "UIX_GdiProvider.h"

namespace ex_ui { namespace controls { namespace _impl
{
	using ex_ui::draw::common::CColour;
	using ex_ui::draw::common::CPngBitmapPtr;
	using ex_ui::draw::renderers::CBackgroundRendererDefImpl;

	using ex_ui::draw::CZBuffer;

	class CImageRendererDefaultImpl :
		public  CBackgroundRendererDefImpl
	{
		typedef CBackgroundRendererDefImpl TBaseRenderer;
	private:
		RECT                       m_rect;           // stores rectangle drawing in order to detect draw area changing
		CColour                    m_bkg_clr;
		const CPngBitmapPtr&       m_img_ref;
		CImageShaperDefaultImpl&   m_shaper;
		CImage&                    m_ctrl_ref;
		IRenderer*                 m_prn_rnd;
	public:
		CImageRendererDefaultImpl(CImage&, const CPngBitmapPtr&, CImageShaperDefaultImpl&, CWindow&);
		~CImageRendererDefaultImpl(void);
	public:
		const
		CColour&     BackColor(void)const;
		VOID         BackColor(const COLORREF, const BYTE _alpha);
		HRESULT      Clear(void);
		HRESULT      Draw(CZBuffer&, const RECT& rcArea);
		IRenderer*   ParentRenderer(void);
		VOID         ParentRenderer(IRenderer*);
	private:
		bool        _IsTransparent(void) const;
		bool        _HasBorder(void) const;
	};
}}}

#endif/*_UIXCTRLIMAGECONTROLDEFAULTRENDERER_H_0AB4E1FF_AE63_45b1_99BA_44F1445A9430_INCLUDED*/