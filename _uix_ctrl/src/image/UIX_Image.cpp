/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 7:02:24pm, GMT+3, Taganrog, Saturday;
	This is UIX library PNG image control class implementation file.
*/
#include "StdAfx.h"
#include "UIX_Image.h"
#include "UIX_ImageWnd.h"
#include "UIX_ImageShaperDef.h"
#include "UIX_ImageRendererDef.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;

////////////////////////////////////////////////////////////////////////////

CImage::CImage(IRenderer* pParentRenderer):
	m_p_wnd(NULL),
	m_ctrlId(0),
	m_resId(0)
{
	try
	{
		m_p_wnd = new CImageWnd(*this);
		m_p_wnd->_Renderer().ParentRenderer(pParentRenderer);
	}
	catch(::std::bad_alloc&){}
}

CImage::~CImage(void)
{
	if (m_p_wnd)
	{
		delete m_p_wnd; m_p_wnd = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////

CColour    CImage::BackColor(void)const
{
	CColour clr_;
	if (m_p_wnd)
		clr_ = m_p_wnd->_Renderer().BackColor();
	return clr_;
}

VOID       CImage::BackColor(const COLORREF clr, const BYTE _alpha)
{
	if (m_p_wnd)
		m_p_wnd->_Renderer().BackColor(clr, _alpha);
}

HRESULT    CImage::Clear(void)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;

	m_p_wnd->_Clear();
	m_resId = 0;
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CImage::Create(const HWND hParent, const RECT& rcArea, const HWND hAfter, const UINT ctrlId)
{
	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;

	if (m_p_wnd == NULL)
		return OLE_E_BLANK;

	if (m_p_wnd->IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);

	if (::IsRectEmpty(&rcArea))
		return E_INVALIDARG;

	RECT rc_ = rcArea;
	DWORD dStyle = WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN;

	m_p_wnd->Create(hParent, rc_, NULL, dStyle);
	if (!m_p_wnd->IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());

	if (NULL == hAfter)
		m_p_wnd->SetWindowPos(NULL  , 0, 0, 0, 0, SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE|SWP_NOZORDER);
	else
		m_p_wnd->SetWindowPos(hAfter, 0, 0, 0, 0, SWP_NOACTIVATE|SWP_NOMOVE|SWP_NOSIZE);

	m_ctrlId = ctrlId;
	return S_OK;
}

HRESULT    CImage::Destroy(void)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;
	if (!m_p_wnd->IsWindow())
		return S_FALSE;

	m_p_wnd->SendMessage(WM_CLOSE);

	return S_OK;
}

UINT       CImage::GetIdentifier(void)const
{
	return m_ctrlId;
}

IRenderer* CImage::GetParentRendererPtr(void)const
{
	if (!m_p_wnd)
		return NULL;
	else
		return m_p_wnd->_Renderer().ParentRenderer();
}

IRenderer* CImage::GetRendererPtr(void)const
{
	if (!m_p_wnd)
		return NULL;
	else
		return &(m_p_wnd->_Renderer());
}

UINT       CImage::GetResourceId(void)const
{
	return m_resId;
}

HRESULT    CImage::GetSize(SIZE& size) const
{
	if (!m_p_wnd)
		return OLE_E_BLANK;

	HRESULT hr_ = m_p_wnd->_Renderer().GetImageSize(size);
	return  hr_;
}

DWORD      CImage::Style(void) const
{
	if (!m_p_wnd)
		return 0;
	else
		return m_p_wnd->m_dwStyle;
}

HRESULT    CImage::Style(const DWORD dwStyle)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;

	m_p_wnd->m_dwStyle = dwStyle;
	return S_OK;
}

CWindow&   CImage::GetWindow_Ref(void)
{
	static CWindow invalid_;
	if (!m_p_wnd)
		return invalid_;
	else
		return *m_p_wnd;
}

HRESULT    CImage::Refresh(void)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;

	if (!m_p_wnd->IsWindow())
		return OLE_E_BLANK;

	m_p_wnd->Invalidate(TRUE);
	return S_OK;
}

HRESULT    CImage::SetImage(const UINT nResId)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;
	if (!nResId)
		return E_INVALIDARG;
	if (nResId == m_resId)
		return S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();

	Gdiplus::Bitmap* pBitmap = NULL;
	HRESULT hr_ = CGdiPlusPngLoader::LoadResource(nResId, hInstance, pBitmap);
	if (S_OK != hr_)
		return  hr_;

	hr_ = m_p_wnd->m_image_ptr.Attach(pBitmap);
	hr_ = m_p_wnd->m_image_ptr.GetLastResult();

	if (S_OK == hr_ && m_p_wnd->IsWindow())
	{
		m_p_wnd->Invalidate();
		m_resId = nResId;
	}
	if (FAILED(hr_))
	{
		delete pBitmap;
		pBitmap = NULL;
	}
	return  hr_;
}

HRESULT    CImage::SetParentNotifyPtr(IControlNotify* p__)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;
	else
	{
		m_p_wnd->m_pNotify = p__;
		return S_OK;
	}
}

HRESULT    CImage::SetParentRendererPtr(IRenderer* p__)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;
	else
	{
		m_p_wnd->_Renderer().ParentRenderer(p__);
		return S_OK;
	}
}

HRESULT    CImage::UpdateLayout(void)
{
	if (!m_p_wnd)
		return OLE_E_BLANK;
	else
	{
		HRESULT hr_ = m_p_wnd->_UpdateLayout();
		return  hr_;
	}
}