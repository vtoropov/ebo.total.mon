#ifndef _UIXIMAGEWND_H_976E5CD4_E104_4a87_92B6_9437DF4B7F4A_INCLUDED
#define _UIXIMAGEWND_H_976E5CD4_E104_4a87_92B6_9437DF4B7F4A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 1-Dec-2015 at 8:44:19pm, GMT+7, Phuket, Rawai, Tuesday;
	This is UIX Image Control Window class declaration file.
*/
#include "UIX_CommonCtrlDefs.h"
#include "UIX_PngWrap.h"
#include "UIX_Image.h"
#include "UIX_ImageRendererDef.h"
#include "UIX_ImageShaperDef.h"

namespace ex_ui { namespace controls { namespace _impl
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::common::CPngBitmapPtr;

	class CImageWnd : 
			public  ::ATL::CWindowImpl<CImageWnd>
		{
			friend class CImage;
			typedef ::ATL::CWindowImpl<CImageWnd> TWindow;
		private:
			CPngBitmapPtr    m_image_ptr;
			HRESULT          m_hResult;       // initialisation result
			DWORD            m_dwStyle;
			CImage&          m_ctrl_ref;
			IControlNotify*  m_pNotify;
		private:
			CImageRendererDefaultImpl  m_renderer;
			CImageShaperDefaultImpl    m_shaper;
		public:
			CImageWnd(CImage&);
			~CImageWnd(void);
		public:
			DECLARE_WND_CLASS(_T("ex_ui::controls::Image::Window"));

			BEGIN_MSG_MAP(CImageWnd)
				MESSAGE_HANDLER(WM_CREATE     ,   OnCreate    )
				MESSAGE_HANDLER(WM_DESTROY    ,   OnDestroy   )
				MESSAGE_HANDLER(WM_ERASEBKGND ,   OnErase     )
				MESSAGE_HANDLER(WM_LBUTTONDOWN,   OnLButtonDn )
			END_MSG_MAP()
		private:
			LRESULT OnCreate    (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnLButtonDn (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnErase     (UINT, WPARAM, LPARAM, BOOL&);
		public:
			VOID     _Clear(void);
			CImageRendererDefaultImpl& _Renderer(void);
			HRESULT  _UpdateLayout(void);

		};
}}}

#endif/*_UIXIMAGEWND_H_976E5CD4_E104_4a87_92B6_9437DF4B7F4A_INCLUDED*/