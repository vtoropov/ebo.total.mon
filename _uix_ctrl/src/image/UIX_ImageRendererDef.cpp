/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 8:10:34pm, GMT+3, Taganrog, Saturday;
	This is UIX library image control default renderer class implementation file.
*/
#include "StdAfx.h"
#include "UIX_ImageRendererDef.h"
#include "UIX_PngWrap.h"

using namespace ex_ui;
using namespace ex_ui::draw::common;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;
using namespace ex_ui::draw::defs;

////////////////////////////////////////////////////////////////////////////

CImageRendererDefaultImpl::CImageRendererDefaultImpl(
		CImage& _ctrl,
		const CPngBitmapPtr& _img,
		CImageShaperDefaultImpl& _shaper,
		CWindow& _wnd
	):
	m_img_ref(_img),
	m_shaper(_shaper),
	m_prn_rnd(NULL),
	m_ctrl_ref(_ctrl),
	TBaseRenderer(_wnd),
	m_bkg_clr(RGB(0xff, 0xff, 0xff), 254)
{
	::SetRectEmpty(&m_rect);
}

CImageRendererDefaultImpl::~CImageRendererDefaultImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

const
CColour&  CImageRendererDefaultImpl::BackColor(void)const
{
	return m_bkg_clr;
}

VOID      CImageRendererDefaultImpl::BackColor(const COLORREF _clr, const BYTE _alpha)
{
	m_bkg_clr.SetColorFromRGBA(_clr, _alpha);
}

HRESULT   CImageRendererDefaultImpl::Clear(void)
{
	if (!TBaseRenderer::m_bkgnd_cache.IsValidObject())
		return S_FALSE;
	HRESULT hr__ = TBaseRenderer::m_bkgnd_cache.Destroy();
	return  hr__;
}

HRESULT   CImageRendererDefaultImpl::Draw(CZBuffer& dc_, const RECT& rcArea)
{
	HRESULT hr_ = S_OK;
	if (::IsRectEmpty(&rcArea)) return E_INVALIDARG;
	if (!::EqualRect(&rcArea, &m_rect) || true)
	{
		TBaseRenderer::m_bkgnd_cache.Destroy();
	}
	if (false == TBaseRenderer::m_bkgnd_cache.IsValidObject())
	{
		m_rect = rcArea;

		dc_.DrawSolidRect(m_rect, m_bkg_clr);
		if (this->_IsTransparent() && NULL != m_prn_rnd)
		{
			m_prn_rnd->DrawParentBackground(TBaseRenderer::m_owner_ref, dc_, m_rect);
		}

		if (m_img_ref.IsValidObject())
		{
			Gdiplus::Bitmap* pBitmap = m_img_ref.GetObjectRef().GetPtr();

			RECT draw_ = m_shaper.GetRectangle(CImageShaperDefaultImpl::eRT_Image);

			Gdiplus::Graphics gp_(dc_);
			gp_.DrawImage(pBitmap, draw_.left, draw_.top, __W(draw_), __H(draw_));
		}

		/*if (this->_HasBorder())
			dc_.Rectangle(&rc_, RGB(0x0f, 0x0f, 0x0f));*/

		HBITMAP hBitmap = NULL;
		hr_ = dc_.CopyTo(hBitmap);
		if (S_OK == hr_)
		{
			CGdiplusBitmapWrap wrap_(hBitmap);
			hr_ = TBaseRenderer::m_bkgnd_cache.Attach(wrap_.Detach());
			::DeleteObject((HGDIOBJ)hBitmap);
			hBitmap = NULL;
		}
	}
	if (!TBaseRenderer::m_bkgnd_cache.IsValidObject())
	{
		return TBaseRenderer::m_bkgnd_cache.GetLastResult();
	}
	else
	{
		const CPngBitmap* pPng = TBaseRenderer::m_bkgnd_cache.GetObject();
		if (NULL != pPng)
		{
			Gdiplus::Bitmap* pBitmap = pPng->GetPtr();
			if (pBitmap)
			{
				RECT draw_ = m_shaper.GetRectangle(CImageShaperDefaultImpl::eRT_Image);
				Gdiplus::Graphics gp_(dc_);
				gp_.DrawImage(pBitmap, draw_.left, draw_.top, __W(draw_), __H(draw_));
			}
		}
	}
	return hr_;
}

IRenderer*   CImageRendererDefaultImpl::ParentRenderer(void)
{
	return m_prn_rnd;
}

VOID         CImageRendererDefaultImpl::ParentRenderer(IRenderer* pRenderer)
{
	m_prn_rnd = pRenderer;
}

////////////////////////////////////////////////////////////////////////////

bool     CImageRendererDefaultImpl::_IsTransparent(void) const
{
	return (0 == (m_ctrl_ref.Style() & eImageStyle::eOpaque));
}

bool     CImageRendererDefaultImpl::_HasBorder(void) const
{
	return (0 != (m_ctrl_ref.Style() & eImageStyle::eBorder));
}