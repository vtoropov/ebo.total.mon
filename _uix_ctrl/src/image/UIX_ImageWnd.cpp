/*
	Created by Tech_dog (VToropov) on 1-Dec-2015 at 9:50:40pm, GMT+7, Phuket, Rawai, Tuesday;
	This is UIX Image Control Window class implementation file.
*/
#include "StdAfx.h"
#include "UIX_ImageWnd.h"

using namespace ex_ui;
using namespace ex_ui::controls;
using namespace ex_ui::controls::_impl;

////////////////////////////////////////////////////////////////////////////

CImageWnd::CImageWnd(CImage& ctrl_ref):
	m_dwStyle(eImageStyle::eNone),
	m_shaper(ctrl_ref, m_image_ptr),
	m_renderer(ctrl_ref, m_image_ptr, m_shaper, *this),
	m_ctrl_ref(ctrl_ref),
	m_pNotify(NULL)
{
}

CImageWnd::~CImageWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CImageWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

LRESULT CImageWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL&)
{
	m_image_ptr.Destroy();
	return 0;
}

LRESULT CImageWnd::OnErase  (UINT, WPARAM  wParam, LPARAM, BOOL&)
{
	RECT rc_ = {0};
	GetClientRect(&rc_);
	if (::IsRectEmpty(&rc_))
		return 1;
	CZBuffer dc_(reinterpret_cast<HDC>(wParam), rc_);
	
	m_renderer.Draw(dc_, rc_);

	return 0;
}

LRESULT CImageWnd::OnLButtonDn (UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	if (NULL != m_pNotify)
	{
		bHandled = TRUE;
		(*m_pNotify).IControlNotify_OnClick(m_ctrl_ref.GetIdentifier());
	}
	return 0;
}

//////////////////////////////////////////////////////////////////////////

VOID     CImageWnd::_Clear(void)
{
	m_image_ptr.Destroy();
	if (TWindow::IsWindow())
		TWindow::Invalidate();
}

CImageRendererDefaultImpl& CImageWnd::_Renderer(void)
{
	return m_renderer;
}

HRESULT CImageWnd::_UpdateLayout(void)
{	
	m_shaper.RecalcLayout();

	const RECT rcArea = m_shaper.GetRectangle(CImageShaperDefaultImpl::eRT_Entire);

	TWindow::SetWindowPos(
			NULL,
			&rcArea,
			SWP_NOZORDER|SWP_NOACTIVATE
		);

	return S_OK;
}