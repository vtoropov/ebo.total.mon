#ifndef _UIXCTRLIMAGECONTROL_H_C9D587B2_CD6B_4c21_B458_E66E9D4EA274_INCLUDED
#define _UIXCTRLIMAGECONTROL_H_C9D587B2_CD6B_4c21_B458_E66E9D4EA274_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Feb-2015 at 6:04:58pm, GMT+3, Taganrog, Saturday;
	This is UIX library PNG image control class declaration file.
*/
#include "UIX_CommonCtrlDefs.h"
#include "UIX_PngWrap.h"
#include "UIX_GdiObject.h"

namespace ex_ui { namespace controls { namespace _impl
{
	class CImageWnd;
}}}

namespace ex_ui { namespace controls
{
	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::draw::common::CColour;
	using ex_ui::draw::defs::IRenderer;

	using ex_ui::controls::_impl::CImageWnd;

	class eImageStyle
	{
	public:
		enum _e{
			eNone       = 0x0000,
			eBorder     = 0x0001,
			eStretch    = 0x0002,
			eOpaque     = 0x0004,
			eVCenter    = 0x0008,
		};
	};

	class CImage
	{
	private:
		CImageWnd*           m_p_wnd;
		UINT                 m_ctrlId;
		UINT                 m_resId;
	public:
		CImage(IRenderer* pParentRenderer = NULL);
		~CImage(void);
	public:
		CColour              BackColor(void)const;
		VOID                 BackColor(const COLORREF, const BYTE _alpha);
		HRESULT              Clear(void);
		HRESULT              Create(const HWND hParent, const RECT& rcArea, const HWND hAfter = NULL, const UINT ctrlId = 0);
		HRESULT              Destroy(void);
		UINT                 GetIdentifier(void)const;
		IRenderer*           GetParentRendererPtr(void)const;
		IRenderer*           GetRendererPtr(void)const;
		UINT                 GetResourceId(void)const;
		HRESULT              GetSize(SIZE&) const;
		::ATL::CWindow&      GetWindow_Ref(void);
		HRESULT              Refresh(void);
		HRESULT              SetImage(const UINT nResId);
		HRESULT              SetParentNotifyPtr(IControlNotify*);
		HRESULT              SetParentRendererPtr(IRenderer*);
		DWORD                Style(void) const;
		HRESULT              Style(const DWORD);
		HRESULT              UpdateLayout(void);
	private:
		CImage(const CImage&);
		CImage& operator= (const CImage&);
	};
}}

#endif/*_UIXCTRLIMAGECONTROL_H_C9D587B2_CD6B_4c21_B458_E66E9D4EA274_INCLUDED*/