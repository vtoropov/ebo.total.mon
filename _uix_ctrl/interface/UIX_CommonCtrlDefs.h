#ifndef _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
#define _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
/*
	Created by Tech_dog (VToropov) on 9-Feb-2015 at 7:23:30pm, GMT+3, Taganrog, Monday;
	This is UIX Control common definition/class declaration file.
*/
#include "UIX_GdiProvider.h"

namespace ex_ui { namespace controls { namespace defs
{
	class eControlState
	{
	public:
		enum _e {
			eUndefined   = 0x00,
			eNormal      = 0x01,
			eDisabled    = 0x02,
			eSelected    = 0x04,
			eHovered     = 0x08,
			ePressed     = 0x10,
		};
	};

	class eHorzAlign
	{
	public:
		enum _e {
			eLeft        = DT_LEFT  ,
			eCenter      = DT_CENTER,
			eRight       = DT_RIGHT ,
		};
	};

	interface IControlNotify
	{
		virtual   HRESULT  IControlNotify_OnClick(const UINT ctrlId) PURE;
		virtual   HRESULT  IControlNotify_OnClick(const UINT ctrlId, const LONG_PTR nData) { ctrlId; nData;  return E_NOTIMPL;}
	};
}}}

#endif/*_UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED*/