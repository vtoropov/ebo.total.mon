#ifndef _UIXCTRLLABELCONTROL_H_02422FCB_4FB2_4303_84E7_AD5F1867A8AA_INCLUDED
#define _UIXCTRLLABELCONTROL_H_02422FCB_4FB2_4303_84E7_AD5F1867A8AA_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Mar-2015 at 10:02:24pm, GMT+3, Taganrog, Monday;
	This is UIX library label control class declaration file.
*/
#include "UIX_ControlBase.h"

namespace ex_ui { namespace controls { namespace _impl
{
	class CLabelWnd;
}}}

namespace ex_ui { namespace controls
{
	using ex_ui::controls::_impl::CLabelWnd;
	class CLabel
	{
	protected:
		CLabelWnd*           m_wnd_ptr;
		CControlCrt          m_crt;
	public:
		CLabel(const CControlCrt);
		~CLabel(void);
	public:
		HRESULT              Create(const HWND hParent, const RECT& rcArea, LPCTSTR pszText);
		HRESULT              Destroy(void);
		HRESULT              FontFamily(LPCTSTR);
		HRESULT              FontSize(const DWORD);
		HRESULT              FontUnderline(const bool);
		HRESULT              ForeColor(const COLORREF);
		ATL::CWindow         GetWindow(void) const;
		HRESULT              HorzAlign(const DWORD);
		HRESULT              Text(LPCTSTR);
	private:
		CLabel(const CLabel&);
		CLabel& operator= (const CLabel&);
	};
}}

#endif/*_UIXCTRLLABELCONTROL_H_02422FCB_4FB2_4303_84E7_AD5F1867A8AA_INCLUDED*/