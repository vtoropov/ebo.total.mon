#ifndef _UIXMOUSEHANDLER_H_8C7E5305_3A29_4640_834C_A70DED87B111_INCLUDED
#define _UIXMOUSEHANDLER_H_8C7E5305_3A29_4640_834C_A70DED87B111_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 6:14:29pm, GMT+7, Phuket, Rawai, Saturday;
	This is UIX library mouse handler class declaration file.
*/
#include "Shared_SystemError.h"

namespace ex_ui { namespace frames
{
	using shared::lite::common::CSysError;

	class CMouseEvtSubscribers
	{
	private:
		enum {
			eStaticCacheSize = 10,
		};
	private:
		HWND         m_cache[CMouseEvtSubscribers::eStaticCacheSize];
	public:
		CMouseEvtSubscribers(void);
	public:
		INT          Count(void)const;
		bool         IsEmpty(void)const;
		bool         IsSubscribed(const HWND)const;
		const
		HWND&        Subscriber(const INT nIndex)const;
		HWND&        Subscriber(const INT nIndex);
	};

	class CMouseEvtHandler
	{
	protected:
		CSysError    m_error;
		CMouseEvtSubscribers m_subs;
	protected:
		CMouseEvtHandler(void);
	public:
		TErrorRef    Error(void)const;
		HRESULT      Subscribe(const HWND);
		HRESULT      Unsubscribe(const HWND);
	public:
		const
		CMouseEvtSubscribers& Subscribers(void)const;
	public:
		static CMouseEvtHandler& GetObjectRef(void);
	};
}}

#endif _UIXMOUSEHANDLER_H_8C7E5305_3A29_4640_834C_A70DED87B111_INCLUDED