#ifndef _UIXNOTIFYTRAYAREA_H_FC78667C_1725_4b96_8D6E_D8E80A16522C_INCLUDED
#define _UIXNOTIFYTRAYAREA_H_FC78667C_1725_4b96_8D6E_D8E80A16522C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 28-Sep-2013 at 5:57:21pm, GMT+3, Taganrog, Saturday;
	This is UIX library notify tray area wrapper class declaration file.
*/
#include <shellapi.h>

namespace ex_ui
{
	class CNotifyPopupType
	{
	public:
		enum _e{
			NPT_Information    = 0,   // default
			NPT_Warning        = 1,
			NPT_Error          = 2
		};
	};

	interface INotifyTrayAreaCallback
	{
		virtual HRESULT  NotifyTray_OnClickEvent(const UINT eventId)       { eventId; return E_NOTIMPL; } // a user clicks
		virtual HRESULT  NotifyTray_OnContextMenuEvent(const UINT eventId) { eventId; return E_NOTIMPL; } // requests to show context menu
		virtual HRESULT  NotifyTray_OnMouseMoveEvent(const UINT eventId)   { eventId; return E_NOTIMPL; } // notifies about mouse movement over the tray icon
	};

	class CNotifyTrayArea
	{
	public:
		enum {
			NTA_CallbackMessageId = WM_APP + 5
		};
	private:
		class CMessageHandler:
			public ATL::CWindowImpl<CMessageHandler>
		{
			typedef ATL::CWindowImpl<CMessageHandler> TWindow;
		private:
			INotifyTrayAreaCallback&   m_sink_ref;
			const UINT                 m_event_id;
		public:
			CMessageHandler(INotifyTrayAreaCallback&, const UINT eventId);
			~CMessageHandler(void);
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(CNotifyTrayArea::NTA_CallbackMessageId, OnNotify)
			END_MSG_MAP()
		private:
			LRESULT  OnNotify(UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		bool             m_bInitialized;
		bool             m_bIconIsShown;
		NOTIFYICONDATA   m_data;
		CMessageHandler  m_handler;
	public:
		CNotifyTrayArea(INotifyTrayAreaCallback&, const UINT eventId);
		~CNotifyTrayArea(void);
	public:
		HRESULT   HideIcon(void);
		HRESULT   Initialize(void);
		bool      IsIconShown(void)const;
		bool      IsInitialized(void)const;
		HRESULT   SetTooltip(LPCTSTR pszTooltip);
		HRESULT   ShowIcon(const UINT nDefIconResourceId, LPCTSTR pszTooltip);
		HRESULT   ShowPopup(LPCTSTR pTitle, LPCTSTR pText, const CNotifyPopupType::_e);
		HRESULT   Terminate(void);
	};
}

#endif/*_UIXNOTIFYTRAYAREA_H_FC78667C_1725_4b96_8D6E_D8E80A16522C_INCLUDED*/