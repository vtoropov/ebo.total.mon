/*
	Created by Tech_dog (VToropov) on 28-Sep-2013 at 6:11:38pm, GMT+3, Taganrog, Saturday;
	This is UIX library notify tray area wrapper class implementation file.
*/
#include "StdAfx.h"
#include "UIX_NotifyTrayArea.h"

using namespace ex_ui;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

#if !defined(NIF_SHOWTIP)
	#define  NIF_SHOWTIP     0x00000080
#endif
#if !defined(NOTIFYICON_VERSION_4)
	#define  NOTIFYICON_VERSION_4   4
#endif

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace details
{
	class __declspec(uuid("{34CCFCEF-AB28-45bc-B9A9-A29C8839414E}")) NotifyTrayArea_IconIdentifier;
}}

/////////////////////////////////////////////////////////////////////////////

CNotifyTrayArea::CMessageHandler::CMessageHandler(INotifyTrayAreaCallback& sink_ref, const UINT eventId):
	m_sink_ref(sink_ref),
	m_event_id(eventId)
{
}

CNotifyTrayArea::CMessageHandler::~CMessageHandler(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT  CNotifyTrayArea::CMessageHandler::OnNotify(UINT, WPARAM, LPARAM lParam, BOOL& bHandled)
{
	bHandled = TRUE;
	if (::WTL::RunTimeHelper::IsVista())
	{
		if (false){}
		else if (WM_CONTEXTMENU == LOWORD(lParam))
			m_sink_ref.NotifyTray_OnContextMenuEvent(m_event_id);
		else if (WM_MOUSEMOVE   == LOWORD(lParam))
			m_sink_ref.NotifyTray_OnMouseMoveEvent(m_event_id);
		else if (WM_LBUTTONDOWN == LOWORD(lParam))
			m_sink_ref.NotifyTray_OnClickEvent(m_event_id);
	}
	else
	{
		if (false){}
		else if (WM_RBUTTONDOWN == lParam)
			m_sink_ref.NotifyTray_OnContextMenuEvent(m_event_id);
		else if (WM_MOUSEMOVE   == lParam)
			m_sink_ref.NotifyTray_OnMouseMoveEvent(m_event_id);
		else if (WM_LBUTTONDOWN == lParam)
			m_sink_ref.NotifyTray_OnClickEvent(m_event_id);
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CNotifyTrayArea::CNotifyTrayArea(INotifyTrayAreaCallback& sink_ref, const UINT eventId): 
m_handler(sink_ref, eventId), m_bInitialized(false), m_bIconIsShown(false)
{
	::memset((void*)&m_data, 0, sizeof(NOTIFYICONDATA));
}

CNotifyTrayArea::~CNotifyTrayArea(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CNotifyTrayArea::Initialize(void)
{
	if (m_bInitialized)
		return S_OK;

	if (m_handler.IsWindow()!= TRUE)
		m_handler.Create(HWND_MESSAGE);

	HRESULT hr_ = m_handler.IsWindow() ? S_OK : HRESULT_FROM_WIN32(::GetLastError()); m_bInitialized = (S_OK == hr_);
	return  hr_;
}

bool      CNotifyTrayArea::IsIconShown(void)const
{
	return m_bIconIsShown;
}

bool      CNotifyTrayArea::IsInitialized(void)const
{
	return m_bInitialized;
}

HRESULT   CNotifyTrayArea::HideIcon(void)
{
	if (!this->IsIconShown())
		return S_FALSE;

	if (!::Shell_NotifyIcon(NIM_DELETE, &m_data))
		return HRESULT_FROM_WIN32(::GetLastError());
	else
	{
		m_bIconIsShown = false;
		return S_OK;
	}
}

HRESULT   CNotifyTrayArea::SetTooltip(LPCTSTR pszTooltip)
{
	if (!this->IsInitialized())
		return OLE_E_BLANK;
	if (!pszTooltip || !::_tcslen(pszTooltip))
		return E_INVALIDARG;

	NOTIFYICONDATA nid = {0};
	nid.cbSize         = sizeof(NOTIFYICONDATA);
	nid.hWnd           = m_handler;
	nid.uID            = 1;
	nid.uFlags         = NIF_TIP;
	errno_t err_ = ::_tcscpy_s(
				nid.szTip,
				ARRAYSIZE(m_data.szTip),
				pszTooltip
			);
	if (ERROR_SUCCESS != err_)
		return E_OUTOFMEMORY;
	if (!::Shell_NotifyIcon(NIM_MODIFY, &nid))
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT   CNotifyTrayArea::ShowIcon(const UINT nDefIconResourceId, LPCTSTR pszTooltip)
{
	if (this->IsIconShown())
		return S_FALSE;
	CApplicationIconLoader loader_(nDefIconResourceId);

	m_data.cbSize           = sizeof(NOTIFYICONDATA);
	m_data.hWnd             = m_handler;
	m_data.uID              = 1;
	m_data.uCallbackMessage = CNotifyTrayArea::NTA_CallbackMessageId;
	m_data.uFlags           = (NIF_ICON | NIF_MESSAGE | NIF_TIP);
	m_data.hIcon            = loader_.DetachSmallIcon();
	if (pszTooltip && ::_tcslen(pszTooltip))
	{
		::_tcscpy_s(m_data.szTip, ARRAYSIZE(m_data.szTip), pszTooltip);
	}

	HRESULT hr_ = S_OK;

	if (!::Shell_NotifyIcon(NIM_ADD, &m_data))
	{
		::DestroyIcon(m_data.hIcon);
		hr_ = HRESULT_FROM_WIN32(::GetLastError());
	}
	else
		m_bIconIsShown = true;
	return hr_;
}

HRESULT   CNotifyTrayArea::ShowPopup(LPCTSTR pTitle, LPCTSTR pText, const CNotifyPopupType::_e _type)
{
	NOTIFYICONDATA nid = {0};
	nid.cbSize      = sizeof(NOTIFYICONDATA);
	nid.hWnd        = m_handler;
	nid.uID         = 1;
	nid.uFlags      = NIF_INFO;
	nid.dwInfoFlags = (CNotifyPopupType::NPT_Error == _type ? NIIF_ERROR : 
	                  (CNotifyPopupType::NPT_Warning == _type ? NIIF_WARNING : NIIF_INFO));
	nid.uTimeout    = 11000;

	::_tcscpy_s(nid.szInfoTitle, ARRAYSIZE(nid.szInfoTitle), pTitle);
	::_tcscpy_s(nid.szInfo, ARRAYSIZE(nid.szInfo), pText);

	HRESULT hr_ = (::Shell_NotifyIcon(NIM_MODIFY, &nid) ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
	return  hr_;
}

HRESULT   CNotifyTrayArea::Terminate(void)
{
	if (!m_bInitialized)
		return S_FALSE;

	if (m_handler.IsWindow())
		m_handler.SendMessage(WM_CLOSE);

	m_bInitialized = false;

	return S_OK;
}