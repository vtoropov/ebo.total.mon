/*
	Created by Tech_dog (VToropov) on 9-Feb-2015 at 9:17:23pm, GMT+3, Taganrog, Monday;
	This is UIX Frame library panel base class implementation file.
*/
#include "StdAfx.h"
#include "UIX_PanelBase.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::frames;
using namespace ex_ui::draw;

////////////////////////////////////////////////////////////////////////////

CMessageHandlerDefImpl::CMessageHandlerDefImpl(void)
{
}

CMessageHandlerDefImpl::~CMessageHandlerDefImpl(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CMessageHandlerDefImpl::MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CPanelBase::CPanelBaseWnd::CPanelBaseWnd(const UINT nResId, IMessageHandler& snk_ref, CPanelBase& panel_ref):
	IDD(nResId),
	m_sink_ref(snk_ref),
	m_pane_ref(panel_ref),
	m_bkgnd_renderer(NULL),
	m_parent_renderer(NULL)
{
}

CPanelBase::CPanelBaseWnd::~CPanelBaseWnd(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT CPanelBase::CPanelBaseWnd::OnCreate  (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT CPanelBase::CPanelBaseWnd::OnDestroy (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	return 0;
}

LRESULT CPanelBase::CPanelBaseWnd::OnErase   (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);

	::WTL::CDCHandle dc = (HDC)wParam;
	if (NULL != m_bkgnd_renderer)
	{
		const HRESULT hr_ = m_bkgnd_renderer->DrawBackground(dc, rc_);
		if (FAILED(hr_))
		{}
		else
		{}
	}
	else
	if (NULL != m_parent_renderer)
	{
		CZBuffer dc_(dc, rc_);
		dc_.FillSolidRect(&rc_, RGB(0xee, 0xee, 0xee));
		const HRESULT hr_ = (*m_parent_renderer).DrawParentBackground(TWindow::m_hWnd, (HDC)dc_, rc_);
		if (FAILED(hr_))
		{}
		else
		{}
	}
	else
	{
		dc.FillSolidRect(&rc_, RGB(0xff, 0xff, 0xff));
	}
	return 1;
}

LRESULT CPanelBase::CPanelBaseWnd::OnShowHide(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	if (NULL != lParam)
	{
		LPWINDOWPOS const pPos = reinterpret_cast<LPWINDOWPOS>(lParam);
		if (NULL != pPos) // in order to invoke overriding functions of the derived class
		{
			//const bool flag_ = (0 != (SWP_SHOWWINDOW & (*pPos).flags));
			//m_pane_ref.Show(flag_);
		}
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CPanelBase::CPanelBase(const UINT nResId, IMessageHandler& snk_ref, const DWORD dwStyle):
	m_panel(nResId, snk_ref, *this),
	m_dwStyle(dwStyle)
{
}

CPanelBase::~CPanelBase(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT    CPanelBase::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	if (!::IsWindow(hParent))
		return OLE_E_INVALIDHWND;

	if (m_panel.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_EXISTS);

	m_panel.Create(hParent);

	if (!m_panel.IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());

	RECT rc_ = {0};
	m_panel.GetWindowRect(&rc_);
	::MapWindowPoints(HWND_DESKTOP, hParent, (LPPOINT)&rc_, 0x2);

	if (false){}
	else if (0 != (ePanelStyle::eAlignToRight & m_dwStyle))
	{
		const INT w_    = (rc_.right - rc_.left);
		const INT nLeft = rcArea.right - w_;
		const INT nTop  = rcArea.top;
		m_panel.SetWindowPos(NULL, nLeft, nTop, 0, 0, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
	}
	else if (0 != (ePanelStyle::eAlignToLeft & m_dwStyle))
	{
		const INT nLeft = rcArea.left;
		const INT nTop  = rcArea.top;
		m_panel.SetWindowPos(NULL, nLeft, nTop, 0, 0, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
	}
	else if (0 != (ePanelStyle::eUseOriginSize & m_dwStyle))
	{
		const INT nLeft = rcArea.left + ((rcArea.right - rcArea.left) - (rc_.right - rc_.left)) / 2;
		const INT nTop  = rcArea.top  + ((rcArea.bottom - rcArea.top) - (rc_.bottom - rc_.top)) / 2;
		m_panel.SetWindowPos(NULL, nLeft, nTop, 0, 0, SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOSIZE);
	}
	else if (0 != (ePanelStyle::eAlignToCenter & m_dwStyle))
	{
		const INT nLeft = rcArea.left + ((rcArea.right - rcArea.left) - (rc_.right - rc_.left)) / 2;
		::OffsetRect(&rc_, nLeft, 0);
		rc_.top = rcArea.top;
		rc_.bottom = rcArea.bottom;
		m_panel.SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	}
	else
	{
		m_panel.MoveWindow(&rcArea);
	}
	if (bVisible)
		m_panel.ShowWindow(SW_SHOW);
	else
		m_panel.ShowWindow(SW_HIDE);
	return S_OK;
}

HRESULT    CPanelBase::Destroy(void)
{
	if (!m_panel.IsWindow())
		return S_FALSE;
	if (!m_panel.DestroyWindow())
	{
		HRESULT hr_ = HRESULT_FROM_WIN32(::GetLastError());
		ATLASSERT(FALSE);
		return  hr_;
	}
	m_panel.m_hWnd = NULL;
	return S_OK;
}

const HWND CPanelBase::GetControl_Safe(const UINT nCtrlId) const
{
	if (!m_panel.IsWindow())
		return NULL;
	::ATL::CWindow ctrl_ = m_panel.GetDlgItem((INT)nCtrlId);
	return ctrl_;
}

const 
CWindow&   CPanelBase::GetWindow_Ref(void) const
{
	return m_panel;
}

CWindow&   CPanelBase::GetWindow_Ref(void)
{
	return m_panel;
}

bool       CPanelBase::HasStyle(const DWORD dwStyle) const
{
	return (0 != (dwStyle & m_dwStyle));
}

bool       CPanelBase::IsValid(void)const
{
	return !!m_panel.IsWindow();
}

HRESULT    CPanelBase::Refresh(const bool bWithAsyncOpt, const bool bWithChildrenIncludeOpt)
{
	if (!m_panel.IsWindow() || !m_panel.IsWindowVisible())
		return S_FALSE;

	if (true == bWithAsyncOpt)
		m_panel.Invalidate(TRUE);
	else if (true == bWithChildrenIncludeOpt)
		m_panel.RedrawWindow(
					0,
					0,
					RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_ALLCHILDREN
				);
	else
		m_panel.RedrawWindow(
					0,
					0,
					RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW|RDW_NOCHILDREN
				);
	return S_OK;
}

HRESULT    CPanelBase::Show(const bool _flag)
{
	if (!m_panel.IsWindow())
		return OLE_E_BLANK;

	if (_flag && m_panel.IsWindowVisible())
		return S_OK;
	if (!_flag && !m_panel.IsWindowVisible())
		return S_OK;

	if (_flag)
		m_panel.SetWindowPos(
			HWND_TOP,
			0, 0, 0, 0,
			SWP_SHOWWINDOW|SWP_NOMOVE|SWP_NOSIZE
		);
	else
		m_panel.SetWindowPos(
			HWND_BOTTOM,
			0, 0, 0, 0,
			SWP_HIDEWINDOW|SWP_NOMOVE|SWP_NOSIZE
		);
	return S_OK;
}

HRESULT    CPanelBase::SetBkgndRenderer(IRenderer* const pRenderer)
{
	m_panel.m_bkgnd_renderer = pRenderer;
	return S_OK;
}

HRESULT    CPanelBase::SetParentRenderer(IRenderer* const pRenderer)
{
	m_panel.m_parent_renderer = pRenderer;
	return S_OK;
}

HRESULT    CPanelBase::UpdateLayout(LPRECT const pRect)
{
	if (pRect && !::IsRectEmpty(pRect) && this->IsValid())
	{
		RECT rc_ = *pRect;
		m_panel.SetWindowPos(NULL, &rc_, SWP_NOZORDER|SWP_NOACTIVATE);
	}
	return S_OK;
}