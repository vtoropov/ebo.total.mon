/*
	Created by Tech_dog (VToropov) on 24-Mar-2014 at 3:56:13am, GMT+4, Saint-Petersburg, Monday;
	This is UIX Frame Library Image Header class implementation file.
*/
#include "StdAfx.h"
#include "UIX_ImgHeader.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::frames;

////////////////////////////////////////////////////////////////////////////

CImageHeader::CImageHeaderWnd::CImageHeaderWnd(const ATL::_U_STRINGorID RID):
	m_pImage(NULL),
	m_hResult(OLE_E_BLANK)
{
	m_szHeader.cx = m_szHeader.cy = 0;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();

	m_hResult = ex_ui::draw::CGdiPlusPngLoader::LoadResource(RID, hInstance, m_pImage);
	ATLASSERT(S_OK == m_hResult);
	if (S_OK == m_hResult && m_pImage)
	{
		m_szHeader.cx = m_pImage->GetWidth();
		m_szHeader.cy = m_pImage->GetHeight();
	}
}

CImageHeader::CImageHeaderWnd::~CImageHeaderWnd(void)
{
	if (m_pImage)
	{
		delete m_pImage;
		m_pImage = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////

LRESULT CImageHeader::CImageHeaderWnd::OnCreate(UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

LRESULT CImageHeader::CImageHeaderWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

LRESULT CImageHeader::CImageHeaderWnd::OnErase(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;
	return 1;
}

LRESULT CImageHeader::CImageHeaderWnd::OnPaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (S_OK == m_hResult && m_pImage)
	{
		HDC hDC = GetDC();

		{
			Gdiplus::Graphics gpls__(hDC);
			gpls__.DrawImage(m_pImage, 0, 0, m_pImage->GetWidth(), m_pImage->GetHeight());
		}
		ReleaseDC(hDC);
		hDC = NULL;
	}
	return 0;
}

////////////////////////////////////////////////////////////////////////////

CImageHeader::CImageHeader(const ATL::_U_STRINGorID RID):
	m_wnd(RID)
{
}

CImageHeader::~CImageHeader(void)
{
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CImageHeader::Create(const HWND hParent)
{
	if (S_OK != m_wnd.m_hResult)
		return  m_wnd.m_hResult;

	if (!::IsWindow(hParent))
		return E_INVALIDARG;

	RECT rc__ = {0, 0, m_wnd.m_szHeader.cx, m_wnd.m_szHeader.cy};

	HWND hWnd = m_wnd.Create(hParent, rc__, 0, WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN);
	if (!hWnd)
		return HRESULT_FROM_WIN32(::GetLastError());
	return S_OK;
}

HRESULT     CImageHeader::Destroy(void)
{
	if (!m_wnd.IsWindow())
		return S_FALSE;
	m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

const SIZE& CImageHeader::GetSize(void) const
{
	if (m_wnd.m_pImage)
	{
		m_wnd.m_szHeader.cx = (*m_wnd.m_pImage).GetWidth();
		m_wnd.m_szHeader.cy = (*m_wnd.m_pImage).GetHeight();
	}
	return m_wnd.m_szHeader;
}

::ATL::CWindow&     CImageHeader::GetWindow_Ref(void)
{
	return m_wnd;
}