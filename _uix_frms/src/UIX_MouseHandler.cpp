/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 8:22:28pm, GMT+7, Phuket, Rawai, Saturday;
	This is UIX library mouse handler class implementation file.
*/
#include "StdAfx.h"
#include "UIX_MouseHandler.h"

using namespace ex_ui::frames;

/////////////////////////////////////////////////////////////////////////////

namespace ex_ui { namespace frames { namespace details
{
	HWND&    CMouseEvtHandler_InvalidHandle(void)
	{
		static HWND hwnd_ = NULL;
		return hwnd_;
	}

	HHOOK&   CMouseEvtHandler_HookRef(void)
	{
		static HHOOK hook_ = NULL;
		return hook_;
	}

	class    CMouseEvtHandler_Accessor
	{
	private:
		const
		CMouseEvtSubscribers& m_subs;
	public:
		CMouseEvtHandler_Accessor(const CMouseEvtSubscribers& _subs) : m_subs(_subs){}
	public:
		HRESULT  Handle(const PMOUSEHOOKSTRUCT _data, const DWORD _msg)
		{
			if (!_data)
				return S_FALSE;
			for (INT i_ = m_subs.Count() - 1; i_ >= 0; i_--)
			{
				const HWND& sub_ = m_subs.Subscriber(i_);
				if (!sub_ || !::IsWindow(sub_))
					continue;
				if (!::IsChild(sub_, _data->hwnd) && sub_ != _data->hwnd)
					continue;
				::SendMessage(
					sub_,
					_msg,
					MK_LBUTTON,
					MAKELONG(
						_data->pt.x,
						_data->pt.y
					)
				);
				break;
			}
			return S_OK;
		}
	};

	static LRESULT
	CALLBACK CMouseEvtHandler_Func(INT nCode, WPARAM wParam, LPARAM lParam)
	{
		const HHOOK hHook = CMouseEvtHandler_HookRef();
		if (HC_ACTION != nCode)
			return ::CallNextHookEx(hHook, nCode, wParam, lParam);

		PMOUSEHOOKSTRUCT msg_ =  reinterpret_cast<PMOUSEHOOKSTRUCT>(lParam);
		if (msg_)
			if (WM_LBUTTONDOWN == wParam || WM_LBUTTONUP == wParam)
			{
				CMouseEvtHandler_Accessor acc_(
						CMouseEvtHandler::GetObjectRef().Subscribers()
					);
				acc_.Handle(
						msg_,
						static_cast<DWORD>(wParam)
					);
			}
		return ::CallNextHookEx(hHook, nCode, wParam, lParam);
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CMouseEvtSubscribers::CMouseEvtSubscribers(void)
{
	for (INT i_ = 0; i_ < CMouseEvtSubscribers::eStaticCacheSize; i_++)
		m_cache[i_] = NULL;
}

/////////////////////////////////////////////////////////////////////////////

INT          CMouseEvtSubscribers::Count(void)const
{
	return CMouseEvtSubscribers::eStaticCacheSize;
}

bool         CMouseEvtSubscribers::IsEmpty(void)const
{
	for (INT i_ = 0; i_ < CMouseEvtSubscribers::eStaticCacheSize; i_++)
		if (m_cache[i_] && ::IsWindow(m_cache[i_]))
			return false;

	return true;
}

bool         CMouseEvtSubscribers::IsSubscribed(const HWND _hwnd)const
{
	if (!_hwnd)
		return false;
	for (INT i_ = 0; i_ < CMouseEvtSubscribers::eStaticCacheSize; i_++)
		if (m_cache[i_] == _hwnd)
			return true;

	return false;
}

const
HWND&        CMouseEvtSubscribers::Subscriber(const INT nIndex)const
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_cache[nIndex];
	else
		return details::CMouseEvtHandler_InvalidHandle();
}

HWND&        CMouseEvtSubscribers::Subscriber(const INT nIndex)
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_cache[nIndex];
	else
		return details::CMouseEvtHandler_InvalidHandle();
}

/////////////////////////////////////////////////////////////////////////////

CMouseEvtHandler::CMouseEvtHandler(void)
{
	m_error.Source(_T("CMouseEvtHandler"));
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CMouseEvtHandler::Error(void)const
{
	return m_error;
}

HRESULT      CMouseEvtHandler::Subscribe(const HWND _hwnd)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!::IsWindow(_hwnd))
		return (m_error = E_INVALIDARG);

	if (m_subs.IsSubscribed(_hwnd))
		return (m_error = S_OK);

	bool bSuccess = false;

	for (INT i_ = 0; i_ < m_subs.Count(); i_++)
	{
		if (NULL == m_subs.Subscriber(i_)
		|| !::IsWindow(m_subs.Subscriber(i_)))
		{
			m_subs.Subscriber(i_) = _hwnd;
			bSuccess = true;
			break;
		}
	}

	if (bSuccess)
	{
		HHOOK& ref_ = details::CMouseEvtHandler_HookRef();
		if (!ref_)
		{
			ref_ = ::SetWindowsHookEx(
						WH_MOUSE,
						details::CMouseEvtHandler_Func,
						::ATL::_AtlBaseModule.GetModuleInstance(),
						::GetCurrentThreadId()
					);
			if (!ref_)
				m_error = ::GetLastError();
		}
	}
	else
		m_error.SetState(
				OLE_E_NOCACHE,
				_T("Handler cache is full")
			);

	return m_error;
}

HRESULT      CMouseEvtHandler::Unsubscribe(const HWND _hwnd)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (NULL == _hwnd)
		return (m_error = E_INVALIDARG);

	for (INT i_ = 0; i_ < m_subs.Count(); i_++)
		if (m_subs.Subscriber(i_) == _hwnd)
		{
			m_subs.Subscriber(i_) = NULL;
			break;
		}

	if (m_subs.IsEmpty())
	{
		HHOOK& ref_ = details::CMouseEvtHandler_HookRef();
		if (ref_)
		{
			::UnhookWindowsHookEx(ref_);
			ref_ = NULL;
		}
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

const
CMouseEvtSubscribers& CMouseEvtHandler::Subscribers(void)const
{
	return m_subs;
}

/////////////////////////////////////////////////////////////////////////////

CMouseEvtHandler& CMouseEvtHandler::GetObjectRef(void)
{
	static CMouseEvtHandler obj_;
	return obj_;
}