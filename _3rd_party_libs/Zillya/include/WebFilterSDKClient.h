#ifndef WEBFILTERAPICLIENT_H
#define WEBFILTERAPICLIENT_H

#pragma once

namespace Zillya
{
	struct RequestHttpData
	{
		unsigned int	 code;
		bool			 block;
		const char		*httpStatus;
		const char		*httpHost;
	};

	extern "C" __declspec(dllexport) bool __stdcall LoadHashTableA(unsigned int, const char *);
	extern "C" __declspec(dllexport) bool __stdcall LoadHashTableW(unsigned int, const wchar_t *);
	extern "C" __declspec(dllexport) bool __stdcall LoadBlockHtmlA(const char *);
	extern "C" __declspec(dllexport) bool __stdcall LoadBlockHtmlW(const wchar_t *);
	extern "C" __declspec(dllexport) bool __stdcall GetBlockState(unsigned int);
	extern "C" __declspec(dllexport) void __stdcall SetBlockState(unsigned int, bool);
	extern "C" __declspec(dllexport) bool __stdcall SetOnRequestDataCallback(bool (*)(const struct RequestHttpData &));

	#ifdef _UNICODE
		#define LoadHashTable(code, path)	LoadHashTableW(code, path)
		#define LoadBlockHtml(path)			LoadBlockHtmlW(path)
	#else
		#define LoadHashTable(code, path)	LoadHashTableA(code, path)
		#define LoadBlockHtml(path)			LoadBlockHtmlA(path)
	#endif
}

#endif //WEBFILTERAPICLIENT_H