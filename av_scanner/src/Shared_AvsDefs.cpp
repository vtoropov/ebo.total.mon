/*
	Created by Tech_dog (VToropov) on 4-Dec-2015 at 8:31:19am, GMT+7, Phuket, Rawai, Friday;
	This is Shared AV scanner common definition implementation file.
*/
#include "StdAfx.h"
#include "Shared_AvsDefs.h"

using namespace shared::avs;

#include "Shared_GenericSyncObject.h"

using namespace shared::lite::runnable;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace details
{
	CGenericSyncObject& ScannerEngine_GetCrtDataGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}

	CGenericSyncObject& ScannerEngine_GetEvtSafeGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}

	CGenericSyncObject& ScannerEngine_GetFolderGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}

	typedef ::std::map<IScannerEventSink*, IScannerEventSink*> TSinkMap;
	TSinkMap&           ScannerEngine_GetSinkMap(void)
	{
		static TSinkMap map_;
		return map_;
	}
}}}

#define SAFE_LOCK_CRT()  SAFE_LOCK(details::ScannerEngine_GetCrtDataGuard());
#define SAFE_LOCK_EVT()  SAFE_LOCK(details::ScannerEngine_GetEvtSafeGuard());
#define SAFE_LOCK_FLD()  SAFE_LOCK(details::ScannerEngine_GetFolderGuard());

DWORD CScanThreadCounter::m_cur_threads = 0;

using namespace shared::avs::details;
/////////////////////////////////////////////////////////////////////////////

CScanThreadCounter::CScanThreadCounter(void) : m_max_threads(1)
{
	m_error.Source(_T("CScanThreadCounter"));
	m_error = S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CScanError CScanThreadCounter::Error(void)const
{
	return m_error;
}

bool       CScanThreadCounter::IsAllowedMoreThreads(void)const
{
	SAFE_LOCK_CRT();
	return (m_cur_threads < m_max_threads);
}

bool       CScanThreadCounter::RegisterThread(void)
{
	if (!this->IsAllowedMoreThreads())
		return false;

	SAFE_LOCK_CRT();
	m_cur_threads += 1;

	return true;
}

DWORD      CScanThreadCounter::ThreadsAllowed(void)const
{
	SAFE_LOCK_CRT();
	return m_max_threads;
}

bool       CScanThreadCounter::ThreadsAllowed(const DWORD _allowed)
{
	if (_allowed > avs::eMaxAllowedThreads)
	{
		m_error.Module(_T(__FUNCTION__));
		CAtlString cs_error;
		cs_error.Format(
				_T("Maximum number of threads (%u) cannot be exceeded"),
				avs::eMaxAllowedThreads
			);

		m_error.SetState(
				(DWORD)ERROR_TOO_MANY_THREADS,
				cs_error
			);
		return false;
	}

	SAFE_LOCK_CRT();
	m_max_threads = _allowed;

	return true;
}

DWORD      CScanThreadCounter::ThreadsCount(void)const
{
	SAFE_LOCK_CRT();
	return m_cur_threads;
}

bool       CScanThreadCounter::UnregisterThread(void)
{
	SAFE_LOCK_CRT();
	if (m_cur_threads > 0)
	{
		m_cur_threads -= 1;
		return true;
	}
	else
		return false;
}

/////////////////////////////////////////////////////////////////////////////

CScanFile::CScanFile(void) : m_health(CScanFileHealth::eUnknown), m_time(0)
{
}

CScanFile::~CScanFile(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CScanFileHealth::_e
             CScanFile::Health(void)const
{
	return m_health;
}

VOID         CScanFile::Health(const CScanFileHealth::_e _health)
{
	m_health = _health;
}

CAtlString   CScanFile::HealthAsText(void)const
{
	CAtlString cs_health;

	const CScanFileHealth::_e eHealth = this->Health();
	switch (eHealth)
	{
	case CScanFileHealth::eClean     :  cs_health = _T("clean")     ; break;
	case CScanFileHealth::eInfected  :  cs_health = _T("infected")  ; break;
	case CScanFileHealth::eSuspected :  cs_health = _T("suspicious"); break;
	case CScanFileHealth::eError     :  cs_health = _T("#error")    ; break;
	default:
		  cs_health = _T("not scanned"); break;
	}

	return cs_health;
}

bool         CScanFile::IsHealthy(void)const
{
	const CScanFileHealth::_e eHealth = this->Health();
	return (CScanFileHealth::eClean == eHealth);
}

bool         CScanFile::IsSick(void)const
{
	const CScanFileHealth::_e eHealth = this->Health();
	return(CScanFileHealth::eInfected == eHealth
		|| CScanFileHealth::eSuspected == eHealth);
}

bool         CScanFile::IsValid(void)const
{
	return (!m_name.IsEmpty() && !m_path.IsEmpty());
}

CAtlString   CScanFile::Name(void)const
{
	return m_name;
}

VOID         CScanFile::Name(LPCTSTR pszName)
{
	m_name = pszName;
}

CAtlString   CScanFile::Path(void)const
{
	return m_path;
}

VOID         CScanFile::Path(LPCTSTR pszPath)
{
	m_path = pszPath;
}

VOID         CScanFile::SetCurrentTime(void)
{
	::time(&m_time);
}

time_t       CScanFile::Timestamp(void)const
{
	return m_time;
}

VOID         CScanFile::Timestamp(const time_t _time)
{
	m_time = _time;
}

CAtlString   CScanFile::TimestamptAsText(void)const
{
	TCHAR buf_[_MAX_PATH] = {0};

	::_tcsftime(
			buf_,
			_countof(buf_),
			_T("%Y-%m-%d %H:%M:%S"),
			::localtime(&m_time)
		);
	CAtlString cs_time = buf_;
	return cs_time;
}

CAtlString   CScanFile::Virus(void)const
{
	return m_virus;
}

VOID         CScanFile::Virus(LPCTSTR pszVirus)
{
	m_virus = pszVirus;
}

/////////////////////////////////////////////////////////////////////////////

CScanFileList::CScanFileList(void)
{
}

CScanFileList::~CScanFileList(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CScanFileList::Append(LPCTSTR pszName, LPCTSTR pszPath)
{
	CScanFile file_;
	file_.Name(pszName);
	file_.Path(pszPath);

	if (!file_.IsValid())
		return E_INVALIDARG;

	try
	{
		SAFE_LOCK_CRT();
		m_awaiting.push(file_);
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}

CScanFile    CScanFileList::GetScanObject(void)
{
	CScanFile file_;
	{
		SAFE_LOCK_CRT();
		if (m_awaiting.size())
		{
			file_ = m_awaiting.front();
			m_awaiting.pop();
		}
	}
	return file_;
}

VOID         CScanFileList::PutScanObject(const CScanFile& _file)
{
	SAFE_LOCK_CRT();
	m_processed.push_back(_file);
}

/////////////////////////////////////////////////////////////////////////////

bool         CScanFileList::IsEmptyQueue(void)const
{
	bool bEmpty = true;
	{
		SAFE_LOCK_CRT();
		bEmpty = m_awaiting.empty();
	}
	return bEmpty;
}

/////////////////////////////////////////////////////////////////////////////

CPreparedFolderList::CPreparedFolderList(void)
{
}

CPreparedFolderList::~CPreparedFolderList(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CPreparedFolderList::Append(LPCTSTR pszFolder)
{
	CAtlString cs_folder(pszFolder);

	if (cs_folder.IsEmpty())
		return E_INVALIDARG;

	try
	{
		SAFE_LOCK_FLD();
		m_list.push(cs_folder);
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}

bool         CPreparedFolderList::IsEmpty(void)const
{
	bool bEmpty = true;
	{
		SAFE_LOCK_CRT();
		bEmpty = m_list.empty();
	}
	return bEmpty;
}

CAtlString   CPreparedFolderList::GetFolder(void)
{
	CAtlString folder_;
	{
		SAFE_LOCK_CRT();
		if (m_list.size())
		{
			folder_ = m_list.front();
			m_list.pop();
		}
	}
	return folder_;
}

/////////////////////////////////////////////////////////////////////////////

CScannerEvents::CScannerEvents(void)
{
}

CScannerEvents::~CScannerEvents(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID      CScannerEvents::RaiseFileScanBeginEvent(CAtlString _path)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnFileScanBegin(_path);
	}
}

VOID      CScannerEvents::RaiseFileScanFinishEvent(CScanFile _file)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnFileScanFinish(_file);
	}
}

VOID      CScannerEvents::RaiseLockEvent(const DWORD dwLockOwnerId)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnLock(dwLockOwnerId);
	}
}

VOID      CScannerEvents::RaiseInitBeginEvent(CAtlString _msg)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnEngineInitBegin(_msg);
	}
}

VOID      CScannerEvents::RaiseInitFinishEvent(CScanError _err)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnEngineInitFinish(_err);
	}
}

VOID      CScannerEvents::RaiseScanProcessBeginEvent(VOID)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnScanProcessBegin();
	}
}

VOID      CScannerEvents::RaiseScanProcessFinishEvent(VOID)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnScanProcessFinish();
	}
}

VOID      CScannerEvents::RaiseUnlockEvent(const DWORD dwLockOwnerId)
{
	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	for (TSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IScannerEventSink* pSink = it_->first;
		if (pSink)
			pSink->IScannerEvent_OnUnlock(dwLockOwnerId);
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CScannerEvents::Subscribe(IScannerEventSink* pIface)
{
	if (!pIface)
		return E_INVALIDARG;

	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	try
	{
		map_[pIface] = pIface;
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}
	return S_OK;
}

HRESULT      CScannerEvents::Unsubscribe(IScannerEventSink* pIface)
{
	if (!pIface)
		return E_INVALIDARG;

	SAFE_LOCK_EVT();

	TSinkMap& map_ = ScannerEngine_GetSinkMap();

	TSinkMap::iterator it_ = map_.find(pIface);
	if (it_ == map_.end())
		return TYPE_E_ELEMENTNOTFOUND;
	try
	{
		map_.erase(it_);
	}
	catch(...)
	{
		return E_OUTOFMEMORY;
	}
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CScanAction::CScanAction(const bool bValid):
m_valid(bValid), m_id(CScanAction::eActionNone), m_index(-1), m_state(CScanActionState::eAwaiting)
{
}

CScanAction::CScanAction(const CScanAction::_e _id, LPCTSTR pszName, const INT _ndx):
m_id(_id), m_name(pszName), m_valid(true), m_index(_ndx), m_state(CScanActionState::eAwaiting)
{
}

/////////////////////////////////////////////////////////////////////////////

CScanAction::_e CScanAction::Identifier(void)const
{
	return m_id;
}

VOID            CScanAction::Identifier(const CScanAction::_e _id)
{
	m_id = _id;
}

INT             CScanAction::Index(void)const
{
	return m_index;
}

VOID            CScanAction::Index(const INT _ndx)
{
	m_index = _ndx;
}

bool            CScanAction::IsValid(void)const
{
	return m_valid;
}

CAtlString      CScanAction::Name(void)const
{
	return m_name;
}

VOID            CScanAction::Name(LPCTSTR pszName)
{
	m_name = pszName;
}

/////////////////////////////////////////////////////////////////////////////

CScanActionState::_e CScanAction::State(void)const
{
	return m_state;
}

HRESULT              CScanAction::State(const CScanActionState::_e _state)
{
	if (CScanAction::eActionNone == m_id)
		return HRESULT_FROM_WIN32(ERROR_INVALID_STATE);
	else
		m_state = _state;
	return S_OK;
}

CAtlString           CScanAction::StateAsText(void)const
{
	CAtlString cs_state;
	if (m_state == CScanActionState::eAwaiting)
		return cs_state;
	if (m_state == CScanActionState::eError)
		return cs_state = _T("#error");

	switch (m_id)
	{
	case CScanAction::eActionDelete: cs_state = _T("deleted"); break;
	}

	return cs_state;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace details
{
	const CScanAction& ScanAction_InvalidObjectRef(void)
	{
		static CScanAction act_(false);
		return act_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////
TScanActions CScanActionEnum::m_actions;
/////////////////////////////////////////////////////////////////////////////

CScanActionEnum::CScanActionEnum(void)
{
	if (m_actions.empty())
	{
		try
		{
			INT nIndex = 0;
			m_actions.push_back(
					CScanAction(CScanAction::eActionNone, _T("Do Nothing"), nIndex++)
				);
			m_actions.push_back(
					CScanAction(CScanAction::eActionDelete, _T("Delete Threat"), nIndex++)
				);
		}
		catch(::std::bad_alloc&){}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT             CScanActionEnum::Count(void)const
{
	return static_cast<INT>(m_actions.size());
}


const
CScanAction&    CScanActionEnum::Default(void)const
{
	return this->Item(0);
}

const
CScanAction&    CScanActionEnum::Find (const CScanAction::_e _act)
{
	const INT count_ = this->Count();
	for (INT i_ = 0; i_ < count_; i_++)
	{
		const CScanAction& act_ = this->Item(i_);
		if (!act_.IsValid())
			continue;
		if (_act == act_.Identifier())
			return act_;
	}
	return details::ScanAction_InvalidObjectRef();
}

const
CScanAction&    CScanActionEnum::Item (const INT nIndex)const
{
	if (nIndex < 0 || nIndex > this->Count() - 1)
		return details::ScanAction_InvalidObjectRef();
	else
		return m_actions[nIndex];
}

/////////////////////////////////////////////////////////////////////////////

CScanFileEx::CScanFileEx(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const
CScanAction& CScanFileEx::Action(void)const
{
	return m_action;
}

CScanAction& CScanFileEx::Action(void)
{
	return m_action;
}

CAtlString   CScanFileEx::ActionAsText(void)const
{
	CAtlString cs_action;
	CScanAction::_e act_ = m_action.Identifier();

	switch (act_)
	{
	case CScanAction::eActionDelete:
		{
			if (false){}
			else if (CScanActionState::eAwaiting  == m_action.State()) cs_action = _T("not deleted");
			else if (CScanActionState::eError     == m_action.State()) cs_action = _T("#error");
			else if (CScanActionState::ePerformed == m_action.State()) cs_action = _T("deleted");
		} break;
	default:
		cs_action = _T("do nothing");
	}

	return cs_action;
}

/////////////////////////////////////////////////////////////////////////////

CScanFileEx::CScanFileEx(const CScanFile& _file)
{
	*this = _file;
}

CScanFileEx& CScanFileEx::operator= (const CScanFile& _file)
{
	CScanActionEnum enum_;

	((CScanFile&)*this) = _file;
	m_action = enum_.Default();

	return *this;
}