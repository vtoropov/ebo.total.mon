/*
	Created by Tech_dog (VToropov) on 7-Dec-2015 at 9:37:13am, GMT+7, Phuket, Rawai, Monday;
	This is shared scanner Zillya engine scanning task class implementation file.
*/
#include "StdAfx.h"
#include "Shared_EngineZillya_Task.h"

using namespace shared::avs::impl_;

#include "Shared_SystemCore.h"
#include "Shared_GenericEvent.h"

using namespace shared::lite::sys_core;
using namespace shared::lite::runnable;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace impl_ { namespace details
{
	static UINT __stdcall ZillyaEngine_ThreadFunc(PVOID pObject)
	{
		if (NULL == pObject)
			return 1;
		CGenericRunnableObject* pRunnable = NULL;
		CZillyaTask* pTask = NULL;

		try
		{
			pRunnable  = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pTask = dynamic_cast<CZillyaTask*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CCoInitializer co_lib(false);
		if (!co_lib.IsSuccess())
			return 1;

		CZillyaTask& task_ = *pTask;
		CZillyaFunctor& func_ = task_.Functor();
		
		CScannerEvents& evt_ = task_.Events();

		global::_avs::GetScannerStateRef() = CScannerState::eInitializing;
		evt_.RaiseInitBeginEvent(
				CAtlString(_T("Starting initializing Zillya AV engine..."))
			);

		HRESULT hr_ =  func_.CoreInit(task_.CoreData());
		if (FAILED(hr_))
		{
			global::_avs::GetScannerStateRef() = CScannerState::eError;
			evt_.RaiseInitFinishEvent(
				func_.Error()
			);
			task_.MarkCompleted();
			return 0;
		}
		else
		{
			global::_avs::GetScannerStateRef() = CScannerState::eInitialized;
			evt_.RaiseInitFinishEvent(
				func_.Error()
			);
		}

		while(!task_.IsStopped())
		{
			CScanFile file_ = task_.List().GetScanObject();
			if (!file_.IsValid())
			{
				::Sleep(10);
				continue;
			}

			if (global::_avs::GetScannerStateRef()!= CScannerState::eScanning)
				global::_avs::GetScannerStateRef() = CScannerState::eScanning;

			evt_.RaiseFileScanBeginEvent(
					file_.Path()
				);

			HRESULT hr_ = func_.ScanFile(file_);
			if (!FAILED(hr_))
			{
				//task_.List().PutScanObject(file_);
			}
			file_.SetCurrentTime();

			evt_.RaiseFileScanFinishEvent(
					file_
				);

			if (task_.List().IsEmptyQueue())
			{
				global::_avs::GetScannerStateRef() = CScannerState::eSuspended;
				evt_.RaiseScanProcessFinishEvent();
			}
		}
		task_.MarkCompleted();
		return 0;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CZillyaTask::CZillyaTask(HMODULE _avs, CScanFileList& _lst, CScannerEvents& _events) :
	TRunnable(
			details::ZillyaEngine_ThreadFunc,
			*this,
			_variant_t((LONG)CZillyaTask::eId)
		),
	m_avs_module(_avs),
	m_lst(_lst),
	m_events(_events),
	m_functor(m_avs_module)
{
	::memset(&m_core_info, 0, sizeof(TCoreData));
}

CZillyaTask::~CZillyaTask(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CZillyaTask::GenericEvent_OnNotify(const _variant_t v_evt_id)
{
	v_evt_id;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

const
TCoreData&      CZillyaTask::CoreData(void)const
{
	return m_core_info;
}

TCoreData&      CZillyaTask::CoreData(void)
{
	return m_core_info;
}

CZillyaFunctor& CZillyaTask::Functor(void)
{
	return m_functor;
}

CScannerEvents& CZillyaTask::Events(void)
{
	return m_events;
}

HMODULE         CZillyaTask::Module(void)const
{
	return m_avs_module;
}

CScanFileList&  CZillyaTask::List(void)
{
	return m_lst;
}