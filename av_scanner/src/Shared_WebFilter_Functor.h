#ifndef _SHAREDWEBFILTERFUNCTOR_H_610B6EF3_8A6D_4e1c_8894_FF93D123A5C2_INCLUDED
#define _SHAREDWEBFILTERFUNCTOR_H_610B6EF3_8A6D_4e1c_8894_FF93D123A5C2_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 2:02:31pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared Antivirus Zillya Web Filter Functor class declaration file.
*/
#include "WebFilterSDKClient.h"

#include "Shared_SystemError.h"

namespace shared { namespace avs { namespace impl_
{
	using Zillya::RequestHttpData;

	typedef bool (WINAPI *PF_LoadHashTable)(unsigned int _uid, const wchar_t* _path);
	typedef void (WINAPI *PF_SetBlockState)(unsigned int _uid, bool);
	typedef bool (WINAPI *PF_SetOnRequestDataCallback)(bool(*)(const struct RequestHttpData&));

	typedef bool TRequestDataCallback(const struct RequestHttpData&);

	using shared::lite::common::CSysError;

	class CZillyaWebFilter
	{
	private:
		HMODULE                      m_module;
		PF_LoadHashTable             m_pf_load_hash;
		PF_SetBlockState             m_pf_set_block;
		PF_SetOnRequestDataCallback  m_pf_set_callback;
		CSysError                    m_error;
	public:
		CZillyaWebFilter(void);
		~CZillyaWebFilter(void);
	public:
		TErrorRef      Error(void)const;
		HRESULT        Initialize(void);
		HRESULT        LoadHashTable(unsigned int _uid, LPCTSTR pszFilePath);
		HRESULT        SetBlockState(unsigned int _uid, bool);
		HRESULT        SetCallback(TRequestDataCallback);
		VOID           Terminate(void);
	public:
		static
		HMODULE        LoadModule(void);
	};
}}}

#endif/*_SHAREDWEBFILTERFUNCTOR_H_610B6EF3_8A6D_4e1c_8894_FF93D123A5C2_INCLUDED*/