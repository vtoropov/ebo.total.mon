#ifndef _SHAREDENGINEZILLYATASK_H_B5D0FB53_7F42_4974_86C2_F7347A3E322C_INCLUDED
#define _SHAREDENGINEZILLYATASK_H_B5D0FB53_7F42_4974_86C2_F7347A3E322C_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Dec-2015 at 9:25:22am, GMT+7, Phuket, Rawai, Monday;
	This is shared scanner Zillya engine scanning task class declaration file.
*/
#include "Shared_GenericEvent.h"
#include "Shared_GenericRunnableObject.h"
#include "Shared_GenericSyncObject.h"

#include "Shared_AvsDefs.h"
#include "Shared_EngineZillya_Impl.h"

namespace shared { namespace avs { namespace impl_
{
	using shared::lite::runnable::CGenericRunnableObject;
	using shared::lite::runnable::CGenericEvent;
	using shared::lite::runnable::CGenericSyncObject;
	using shared::lite::runnable::IGenericEventNotify;

	using shared::avs::CScanFileList;
	using shared::avs::CScannerEvents;

	class CZillyaTask :
		public  IGenericEventNotify,
		public  CGenericRunnableObject
	{
		typedef CGenericRunnableObject TRunnable;
	public:
		enum {
			eId = 10,
		};
	private:
		HMODULE             m_avs_module;
		CZillyaFunctor      m_functor;
		CScanFileList&      m_lst;
		CGenericSyncObject  m_object;
		CScannerEvents&     m_events;
		TCoreData           m_core_info;
	public:
		CZillyaTask(HMODULE _avs, CScanFileList&, CScannerEvents&);
		~CZillyaTask(void);
	private: // IGenericEventNotify
		HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed;
	public:
		const
		TCoreData&          CoreData(void)const;
		TCoreData&          CoreData(void);
		CZillyaFunctor&     Functor(void);
		CScannerEvents&     Events(void);
		HMODULE             Module(void)const;
		CScanFileList&      List(void);
	};
}}}

#endif/*_SHAREDENGINEZILLYATASK_H_B5D0FB53_7F42_4974_86C2_F7347A3E322C_INCLUDED*/