/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 11:30:13am, GMT+7, Phuket, Rawai, Thursday;
	This is shared AV scanner interface implementation file.
*/
#include "StdAfx.h"
#include "Shared_AvsIface.h"

using namespace shared;
using namespace shared::avs;

#include "Shared_EngineZillya_Impl.h"
#include "Shared_EngineZillya_Task.h"

using namespace shared::avs::impl_;

#include "Shared_GenericSyncObject.h"
#include "Shared_GenericEvent.h"
#include "Shared_RawData.h"
#include "Shared_DateTime.h"

using namespace shared::lite::common;
using namespace shared::lite::runnable;
using namespace shared::lite::data;

#include "Shared_FileEnumAsync.h"
#include "Shared_FileSystem.h"

using namespace shared::ntfs;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace details
{
	CScanFileList& ScannerEngine_GetFileListRef(void)
	{
		static CScanFileList lst_;
		return lst_;
	}

	CPreparedFolderList& ScannerEngine_GetPreparedFolders(void)
	{
		static CPreparedFolderList lst_;
		return lst_;
	}

	HMODULE        ScannerEngine_GetModuleHandle(void)
	{
		static HMODULE hModule = CZillyaFunctor::LoadModule();
		return hModule;
	}

	DWORD&         ScannerEngine_GetLockOwner(void)
	{
		static DWORD dwLockOwnerId = 0;
		return dwLockOwnerId;
	}

	class CScannerEngine_TaskCache
	{
		typedef ::std::vector<CZillyaTask*> TTaskList;
	private:
		TTaskList       m_tasks;
		CFileEnumAsync  m_enum_task;
	public:
		CScannerEngine_TaskCache(void) : m_enum_task(ScannerEngine_GetPreparedFolders(), ScannerEngine_GetFileListRef())
		{
		}
		~CScannerEngine_TaskCache(void){}
	public:
		VOID            AddTask(CScannerEvents& _events)
		{
			CZillyaTask* task_ = NULL;
			try
			{
				task_ = new CZillyaTask(
						details::ScannerEngine_GetModuleHandle(),
						details::ScannerEngine_GetFileListRef(),
						_events
					);
				m_tasks.push_back(task_);
				task_->Start();
			}
			catch(::std::bad_alloc&)
			{}
		}

		VOID            Clear(void)
		{
			for (TTaskList::iterator it_ = m_tasks.begin(); it_ != m_tasks.end(); ++it_)
			{
				CZillyaTask* pTask = *it_;
				if (pTask && !pTask->IsStopped())
					pTask->Stop(true);
				delete *it_;
			}
			m_tasks.clear();
		}

		CZillyaTask*    GetTaskObject(const size_t nIndex)
		{
			if (nIndex > this->TaskCount() - 1)
				return NULL;
			else
				return m_tasks[nIndex];
		}

		size_t          TaskCount(void)const
		{
			return m_tasks.size();
		}

		VOID            StopAll(void)
		{
			m_enum_task.Stop(true);
			for (TTaskList::iterator it_ = m_tasks.begin(); it_ != m_tasks.end(); ++it_)
				(*it_)->Stop(true);
		}
	public:
		CFileEnumAsync&  GetFileEnumTask(void)
		{
			return m_enum_task;
		}
	};

	CScannerEngine_TaskCache&  ScannerEngine_GetCache(void)
	{
		static CScannerEngine_TaskCache cache_;
		return cache_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CScannerVirusDatabase::CScannerVirusDatabase(void)
{
}

CScannerVirusDatabase::~CScannerVirusDatabase(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TVirusDefinitionFiles
                CScannerVirusDatabase::Files(void)const
{
	return m_files;
}

HRESULT         CScannerVirusDatabase::Initialize(void)
{
	if (m_files.empty() == false)
		m_files.clear();
	CAtlString db_path = this->Path();
	CGenericFolder folder_(db_path);
	
	TFileListSorted lst_;

	HRESULT hr_ = folder_.EnumerateFiles(lst_, false, _T("*.dat"));
	if (!FAILED(hr_))
		for (TFileListSorted::const_iterator it_ = lst_.begin(); it_ != lst_.end(); ++it_)
			m_files.push_back(it_->second);
	return  hr_;
}

CAtlString      CScannerVirusDatabase::Path (void)const
{
	CAtlString db_path = CZillyaFunctor::VirusDatabasePath();
	return db_path;
}

DWORD           CScannerVirusDatabase::Records(void)const
{
	details::CScannerEngine_TaskCache& cache_ = details::ScannerEngine_GetCache();
	if (cache_.TaskCount() < 1)
		return 0;
	else
		return cache_.GetTaskObject(0)->CoreData().AVERecCount;
}

time_t          CScannerVirusDatabase::Timestamp(void)const
{
	CAtlString rel_info = CZillyaFunctor::VirusDatabasePath();
	rel_info += _T("release.dat");

	CRawData raw_data;
	CGenericFile file_(rel_info);

	HRESULT hr_ = file_.Read(raw_data);
	if (FAILED(hr_))
		return 0;

	INT val_[5] = {0};

	::sscanf_s(
			reinterpret_cast<LPCSTR>(raw_data.GetData()),
			"%d.%d.%d %d:%d",
			&val_[0], &val_[1], &val_[2], &val_[3], &val_[4]
		);

	SYSTEMTIME st_ = {0};
	st_.wYear   = static_cast<WORD>(val_[0]);
	st_.wMonth  = static_cast<WORD>(val_[1]);
	st_.wDay    = static_cast<WORD>(val_[2]);
	st_.wHour   = static_cast<WORD>(val_[3]);
	st_.wMinute = static_cast<WORD>(val_[4]);

	time_t tm_ = CDateTime::SystemTimeToUnix(st_);
	return tm_;
}

CAtlString      CScannerVirusDatabase::TimestampAsText(void)const
{
	time_t time_ = this->Timestamp();
	TCHAR buf_[_MAX_PATH] = {0};

	::_tcsftime(
			buf_,
			_countof(buf_),
			_T("%d/%m/%Y"),
			::localtime(&time_)
		);

	CAtlString cs_time(buf_);
	return cs_time;
}

/////////////////////////////////////////////////////////////////////////////

CScannerEngine::CScannerEngine(
					const CScannerVendor::_e _vendor
				) : m_eVendor(_vendor)
{
	m_error.Source(_T("CScannerEngine"));
	HMODULE avs_ = CZillyaFunctor::LoadModule();
	if (!avs_)
		m_error = ::GetLastError();
}

CScannerEngine::~CScannerEngine(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CScannerEngine::CancelScans(void)
{
	details::CScannerEngine_TaskCache& cache_ = details::ScannerEngine_GetCache();
	const size_t cnt_ = cache_.TaskCount();
	for ( size_t i_ = 0; i_ < cnt_; i_++ )
	{
		CZillyaTask* pTask = cache_.GetTaskObject(i_);
		if (!pTask)
			continue;
		pTask->Functor().StopScan();
	}
	return S_OK;
}

CScanError      CScannerEngine::Error(void)const
{
	CScanError err_obj = m_error;
	return err_obj;
}

CScannerEvents& CScannerEngine::Events(void)
{
	return m_events;
}

HRESULT         CScannerEngine::Initialize(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	m_database.Initialize();

	details::CScannerEngine_TaskCache& cache_ = details::ScannerEngine_GetCache();
	if (cache_.TaskCount() < 1)
		cache_.AddTask(
			m_events
		);

	return m_error;
}

bool            CScannerEngine::IsInitialized(void)const
{
	const CScannerState::_e eState = global::_avs::GetScannerStateRef();
	switch (eState)
	{
	case CScannerState::eInitialized:   return true;
	case CScannerState::eScanning   :   return true;
	case CScannerState::eSuspended  :   return true;
	}
	return false;
}

bool            CScannerEngine::IsReady(void)const
{
	return (CScannerState::eSuspended == global::_avs::GetScannerStateRef());
}

HRESULT         CScannerEngine::Lock(DWORD& dwCookie)
{
	if (details::ScannerEngine_GetLockOwner())
		return HRESULT_FROM_WIN32(ERROR_BUSY);
	else
	{
		dwCookie = ::GetTickCount();
		details::ScannerEngine_GetLockOwner() = dwCookie;
		m_events.RaiseLockEvent(dwCookie);
	}
	return S_OK;
}

HRESULT         CScannerEngine::ScanFolderOrFile(LPCTSTR pszPath)
{
	m_error.Module(_T(__FUNCTION__));

	CAtlString cs_path(pszPath);

	if (cs_path.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("File or directory path cannot be empty")
			);
		return m_error;
	}

	CGenericFolder folder_(cs_path);

	m_events.RaiseScanProcessBeginEvent();

	if (folder_.IsFolder() &&
		folder_.IsExist())
	{
		this->_ScanFolder(cs_path);
	}
	else
	{
		this->_ScanFile(cs_path);
	}

	return m_error;
}

CScannerState::_e
                CScannerEngine::State(void)const
{
	return global::_avs::GetScannerStateRef();
}

HRESULT         CScannerEngine::Terminate(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (m_error)
		m_error.Clear();

	details::CScannerEngine_TaskCache& cache_ = details::ScannerEngine_GetCache();
	cache_.StopAll();

	return m_error;
}

CScannerVendor::_e
                CScannerEngine::Vendor(void)const
{
	return m_eVendor;
}

DWORD           CScannerEngine::Version(void)const
{
	details::CScannerEngine_TaskCache& cache_ = details::ScannerEngine_GetCache();
	if (cache_.TaskCount() < 1)
		return 0;
	else
		return cache_.GetTaskObject(0)->CoreData().AVEVers;
}

CAtlString      CScannerEngine::VersionAsText(void)const
{
	const DWORD dw_ver = this->Version();
	CAtlString  cs_ver;
	if (dw_ver > 0)
	{
		cs_ver.Format(
				_T("%d.%d.%d.%d"),
				dw_ver >> 24 & 0xff,
				dw_ver >> 16 & 0xff,
				dw_ver >>  8 & 0xff,
				dw_ver >>  0 & 0xff
			);
	}
	else
		cs_ver = _T("0.0.0.0");
	return cs_ver;
}

CScannerVirusDatabase
                CScannerEngine::VirusDatabase(void)const
{
	return m_database;
}

HRESULT         CScannerEngine::Unlock(const DWORD dwCookie)
{
	if (details::ScannerEngine_GetLockOwner() != dwCookie)
		return HRESULT_FROM_WIN32(ERROR_LOCKED);
	details::ScannerEngine_GetLockOwner() = 0;
	m_events.RaiseUnlockEvent(dwCookie);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CScannerEngine::_ScanFile(LPCTSTR pszFilePath)
{
	m_error.Module(_T(__FUNCTION__));

	CGenericFile file_(pszFilePath);

	details::ScannerEngine_GetFileListRef().Append(
			file_.Name(),
			pszFilePath
		);

	details::CScannerEngine_TaskCache& cache_ = details::ScannerEngine_GetCache();
	if (cache_.TaskCount() < 1)
		cache_.AddTask(
			m_events
		);
	return m_error;
}

HRESULT      CScannerEngine::_ScanFolder(LPCTSTR pszFolderPath)
{
	m_error.Module(_T(__FUNCTION__));

	CPreparedFolderList& lst_ = details::ScannerEngine_GetPreparedFolders();
	HRESULT hr_ = lst_.Append(pszFolderPath);
	if (FAILED(hr_))
		return (m_error = hr_);

	details::CScannerEngine_TaskCache& cache_ = details::ScannerEngine_GetCache();
	CFileEnumAsync& task_ = cache_.GetFileEnumTask();
	hr_ = task_.AppendFolder(pszFodlerPath);
	if (FAILED(hr_))
		return (m_error = task_.Error());

	if (cache_.TaskCount() < 1)
		cache_.AddTask(
			m_events
		);

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CScannerLocker::CScannerLocker(CScannerEngine& _engine, DWORD& _locker) : m_engine(_engine), m_locker(_locker)
{
	m_error.Source(_T("Scanner accessor"));
}

CScannerLocker::~CScannerLocker(void)
{
	if (this->IsLocked())
	{
		m_engine.Unlock(m_locker);
	}
	m_locker = 0;
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef    CScannerLocker::Error(void)const
{
	return m_error;
}

bool         CScannerLocker::IsLocked(void)const
{
	return (!m_error && m_locker);
}

HRESULT      CScannerLocker::Lock(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->IsLocked())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Scanner engine is already locked")
			);
		return m_error;
	}

	HRESULT hr_ = m_engine.Lock(m_locker);
	if (FAILED(hr_))
	{
		m_error.SetState(
				hr_,
				_T("Cannot get exclusive access (lock) to the scanner")
			);
	}
	return m_error;
}

HRESULT      CScannerLocker::Unlock(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->IsLocked())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Scanner engine is already unlocked")
			);
		return m_error;
	}

	HRESULT hr_ = m_engine.Unlock(m_locker);
	if (FAILED(hr_))
	{
		m_error.SetState(
				hr_,
				_T("Cannot unlock the scanner; it's owned by another object")
			);
	}
	else
		m_locker = 0;
	return m_error;
}