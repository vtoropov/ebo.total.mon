/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 12:19:23pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared AV scanner Zillya engine wrapper class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_EngineZillya_Impl.h"

using namespace shared::avs;
using namespace shared::avs::impl_;

#include "Shared_GenericAppObject.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace impl_ { namespace details
{
	CAtlString ZillyaEngine_GetCoreName(void)
	{
		static CAtlString cs_name;
		if (cs_name.IsEmpty())
			cs_name = _T("CoreMain.DLL");
		return cs_name;
	}

	CAtlString ZillyaEngine_GetCorePath(void)
	{
		static CAtlString cs_path;
		if (cs_path.IsEmpty())
		{
			CApplication the_app;
			the_app.GetPathFromAppFolder(_T(".\\aveng\\"), cs_path);
		}
		return cs_path;
	}

	CAtlString ZillyaEngine_GetVirusBasePath(void)
	{
		static CAtlString cs_path;
		if (cs_path.IsEmpty())
		{
			CApplication().GetPathFromAppFolder(_T(".\\aveng\\"), cs_path);
		}
		return cs_path;
	}

	CAtlString ZillyaEngine_GetCoreInitError(const DWORD _core_handle)
	{
		CAtlString cs_error;

		switch (_core_handle)
		{
		case 0x00000000: cs_error = _T("Null value or utility handle"); break;
		case 0xffffffff: cs_error = _T("Failed to initialize AVEHAL" ); break;
		case 0xfffffffe: cs_error = _T("Not found AVEHAL");             break;
		case 0xfffffffd: cs_error = _T("Failed to initialize AVEMem" ); break;
		case 0xfffffffc: cs_error = _T("Not found AVEMem");             break;
		case 0xfffffffb: cs_error = _T("Failed to load vl001.dat");     break;
		case 0xfffffffa: cs_error = _T("Failed to load vl002.dat");     break;
		case 0xfffffff9: cs_error = _T("Failed to load vl003.dat");     break;
		case 0xfffffff8: cs_error = _T("Failed to load vl004.dat");     break;
		case 0xfffffff7: cs_error = _T("Failed to initialize the internal functions"); break;
		default:
			cs_error = _T("Unknown internal AVE error");
		}

		return cs_error;
	}

	CAtlString ZillyaEngine_GetFileScanError(const INT _code)
	{
		CAtlString cs_error;

		switch (_code)
		{
		case -1: cs_error = _T("General error of the scan engine")   ; break;
		case -2: cs_error = _T("Incorrect number of scanning thread"); break;
		case -3: cs_error = _T("Specified thread does not exist")    ; break;
		case -4: cs_error = _T("Set too long path for the file")     ; break;
		case -5: cs_error = _T("Error opening/reading the file")     ; break;
		default:
			cs_error = _T("Unknown file scan error");
		}

		return cs_error;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CZillyaFunctor::CZillyaFunctor(HMODULE _lib) :
	m_module  (NULL),
	m_pfn_init(NULL),
	m_pfn_free(NULL),
	m_pfn_scan(NULL),
	m_pfn_stop(NULL),
	m_core_handle(0)
{
	m_error.Source(_T("CZillyaFunctor"));
	m_error.Module(_T("{ctor}"));

	if (true || !_lib)
		m_module = CZillyaFunctor::LoadModule();
	else
		m_module = _lib;
	if (m_module == NULL)
		m_error  = ::GetLastError();
	else
	{
		m_error.Clear();
		m_pfn_init = reinterpret_cast<PCoreInit>(::GetProcAddress(m_module, "CoreInit"));
		if (!m_pfn_init)
			m_error.SetState(
					(DWORD)ERROR_PROC_NOT_FOUND,
					_T("CoreInit procedure is not found")
				);
		else
		{
			m_pfn_free = reinterpret_cast<PCoreFree>(::GetProcAddress(m_module, "CoreFree"));
			if (!m_pfn_free)
				m_error.SetState(
						(DWORD)ERROR_PROC_NOT_FOUND,
						_T("CoreFree procedure is not found")
					);
			else
			{
				m_pfn_scan = reinterpret_cast<PCoreFScan>(::GetProcAddress(m_module, "CoreFScan"));
				if (!m_pfn_scan)
					m_error.SetState(
							(DWORD)ERROR_PROC_NOT_FOUND,
							_T("CoreFScan procedure is not found")
						);
				else
				{
					m_pfn_stop = reinterpret_cast<PCoreStopScan>(::GetProcAddress(m_module, "CoreStopScan"));
					if (!m_pfn_stop)
						m_error.SetState(
								(DWORD)ERROR_PROC_NOT_FOUND,
								_T("CoreStopScan procedure is not found")
							);
				}
			}
		}
	}
}

CZillyaFunctor::~CZillyaFunctor(void)
{
	if (m_core_handle)
	{
		if (m_pfn_free)
			m_pfn_free(m_core_handle);
		m_core_handle = 0;
	}
	if (m_module)
	{
		::FreeLibrary(m_module); m_module = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CZillyaFunctor::CoreInit(TCoreData& _data)
{
	m_error.Module(_T(__FUNCTION__));

	if (m_core_handle)
	{
		m_error.SetState(
				(DWORD)ERROR_ALREADY_INITIALIZED,
				_T("Engine Core is already initialized")
			);
		return m_error;
	}
	if (!m_pfn_init)
		return (m_error = OLE_E_BLANK);

	CAtlString cs_path = details::ZillyaEngine_GetCorePath();

	TCoreData core_ = {0}; core_;
	TCoreInit_Interface params = {0};
	params.EPath = cs_path;
	params.VPath = cs_path;
	params.CoreData = &_data;

	CPathFinder path_(cs_path);

	m_core_handle = m_pfn_init(&params);
	if (!ZSDK_INIT_SUCCEEDED(m_core_handle))
	{
		m_error.SetState(
				E_FAIL,
				details::ZillyaEngine_GetCoreInitError(m_core_handle)
			);
		m_core_handle = 0;
	}

	return m_error;
}

CScanError  CZillyaFunctor::Error(void)const
{
	CScanError err_obj = m_error;
	return err_obj;
}

HRESULT     CZillyaFunctor::ScanFile(CScanFile& _file)
{
	m_error.Module(_T(__FUNCTION__));

	if (!m_core_handle)
		return (m_error = OLE_E_BLANK);

	if (!m_pfn_scan)
		return (m_error = OLE_E_BLANK);

	TCoreUnpackMode unpackMode  = {0};
	TCoreResult     scanResult  = {0};
	TCoreFScan_Interface params = {0};

	char vNameBuffer[64] = {0};

	params.BaseMode   = (DWORD)-1;
	params.FName      = _file.Path();
	params.VName      = vNameBuffer;
	params.CoreResult = &scanResult;
	params.UnpackMode = &unpackMode;

	const INT result_ = m_pfn_scan(m_core_handle, &params);
	if (result_ < 0)
	{
		_file.Health(CScanFileHealth::eError);
		m_error.SetState(
				E_FAIL,
				details::ZillyaEngine_GetFileScanError(result_)
			);
	}
	else if (result_ == 0)
		_file.Health(CScanFileHealth::eClean);
	else if (result_ == 1)
		_file.Health(CScanFileHealth::eSuspected);
	else if (result_ == 2)
		_file.Health(CScanFileHealth::eInfected);

	if (::strlen(vNameBuffer))
		_file.Virus(CAtlString(vNameBuffer));

	return m_error;
}

HRESULT     CZillyaFunctor::StopScan(void)
{
	if (!m_pfn_stop)
		return OLE_E_BLANK;
	if (!m_core_handle)
		return OLE_E_BLANK;

	m_pfn_stop(m_core_handle);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

HMODULE     CZillyaFunctor::LoadModule(void)
{
	CAtlString cs_path = details::ZillyaEngine_GetCorePath();

	CPathFinder path_(cs_path);

	HMODULE module_ = ::LoadLibrary(
				details::ZillyaEngine_GetCoreName()
			);
	return module_;
}

CAtlString  CZillyaFunctor::VirusDatabasePath(void)
{
	CAtlString cs_path = details::ZillyaEngine_GetVirusBasePath();
	return cs_path;
}