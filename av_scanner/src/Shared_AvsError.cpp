/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 10:14:44pm, GMT+7, Phuket, Rawai, Thursday;
	This is shared AV scanner error class implementation file.
*/
#include "StdAfx.h"
#include "Shared_AvsError.h"

using namespace shared;
using namespace shared::avs;

#include "Shared_GenericSyncObject.h"

using namespace shared::lite::runnable;
using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace details
{
	CGenericSyncObject& ScannerEngine_GetErrorGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}
}}}

#define SAFE_LOCK_ERROR() SAFE_LOCK(details::ScannerEngine_GetErrorGuard());
/////////////////////////////////////////////////////////////////////////////

CScanError::CScanError(LPCTSTR pszModule) : TBase(pszModule)
{
	this->Source(_T("CScanError"));
}

/////////////////////////////////////////////////////////////////////////////

CScanError::CScanError(const CScanError& _obj)
{
	*this = _obj;
}

CScanError& CScanError::operator= (const CScanError& _obj)
{
	SAFE_LOCK_ERROR();
	this->m_buffer   = _obj.m_buffer;
	this->m_dwError  = _obj.m_dwError;
	this->m_hrError  = _obj.m_hrError;
	this->m_lng_id   = _obj.m_lng_id;
	if (!_obj.m_module.IsEmpty())
		this->m_module = _obj.m_module;
	if (!_obj.m_source.IsEmpty())
		this->m_source = _obj.m_source;
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

VOID        CScanError::Clear(void)
{
	SAFE_LOCK_ERROR();
	TBase::Clear();
}

VOID        CScanError::Module(LPCTSTR pszModule)
{
	SAFE_LOCK_ERROR();
	TBase::Module(pszModule);
}

VOID        CScanError::SetState(const DWORD  dwError, LPCTSTR pszDescription)
{
	SAFE_LOCK_ERROR();
	TBase::SetState(dwError, pszDescription);
}

VOID        CScanError::SetState(const HRESULT hError, LPCTSTR pszDescription)
{
	SAFE_LOCK_ERROR();
	TBase::SetState(hError, pszDescription);
}

VOID        CScanError::Source(LPCTSTR pszSource)
{
	SAFE_LOCK_ERROR();
	TBase::Source(pszSource);
}

/////////////////////////////////////////////////////////////////////////////

CScanError& CScanError::operator= (const DWORD _code)
{
	SAFE_LOCK_ERROR();
	TBase::SetHresult(__HRESULT_FROM_WIN32(_code));	
	return *this;
}

CScanError& CScanError::operator= (const HRESULT hResult)
{
	SAFE_LOCK_ERROR();
	TBase::SetHresult(hResult);	
	return *this;
}