/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 3:56:46pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared Antivirus Zillya Web Filter Functor class implementation file.
*/
#include "StdAfx.h"
#include "Shared_WebFilter_Functor.h"

using namespace shared::avs::impl_;
using namespace Zillya;

#include "Shared_GenericAppObject.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace impl_ { namespace details
{
	CAtlString ZillyaWebFilter_GetCoreName(void)
	{
		static CAtlString cs_name;
		if (cs_name.IsEmpty())
			cs_name = _T("WebFilterSDKClient.dll");
		return cs_name;
	}

	CAtlString ZillyaWebFilter_GetCorePath(void)
	{
		static CAtlString cs_path;
		if (cs_path.IsEmpty())
		{
			CApplication the_app;
			the_app.GetPathFromAppFolder(_T(".\\webflt\\"), cs_path);
		}
		return cs_path;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CZillyaWebFilter::CZillyaWebFilter(void) :
	m_module(NULL),
	m_pf_load_hash(NULL),
	m_pf_set_block(NULL),
	m_pf_set_callback(NULL)
{
	m_error.Source(_T("CZillyaWebFilter"));
}

CZillyaWebFilter::~CZillyaWebFilter(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CZillyaWebFilter::Error(void)const
{
	return m_error;
}

HRESULT     CZillyaWebFilter::Initialize(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_module = CZillyaWebFilter::LoadModule();
	if (m_module == NULL)
		m_error  = ::GetLastError();
	else
	{
		m_error.Clear();
		m_pf_load_hash = reinterpret_cast<PF_LoadHashTable>(::GetProcAddress(m_module, "_LoadHashTableW@8"));
		if (!m_pf_load_hash)
			m_error.SetState(
					(DWORD)ERROR_PROC_NOT_FOUND,
					_T("LoadHashTableW procedure is not found")
				);
		else
		{
			m_pf_set_block = reinterpret_cast<PF_SetBlockState>(::GetProcAddress(m_module, "_SetBlockState@8"));
			if (!m_pf_set_block)
				m_error.SetState(
						(DWORD)ERROR_PROC_NOT_FOUND,
						_T("SetBlockState procedure is not found")
					);
			else
			{
				m_pf_set_callback = reinterpret_cast<PF_SetOnRequestDataCallback>(::GetProcAddress(m_module, "_SetOnRequestDataCallback@4"));
				if (!m_pf_set_callback)
					m_error.SetState(
							(DWORD)ERROR_PROC_NOT_FOUND,
							_T("SetOnRequestDataCallback procedure is not found")
						);
			}
		}
	}
	return m_error;
}

HRESULT     CZillyaWebFilter::LoadHashTable(unsigned int _uid, LPCTSTR pszFilePath)
{
	m_error.Module(_T(__FUNCTION__));
	if (!_uid)
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Hash table identifier cannot be zero")
			);
		return m_error;
	}

	CAtlString cs_path(pszFilePath);
	if (cs_path.IsEmpty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Hash table file path is invalid")
			);
		return m_error;
	}

	if (!m_pf_load_hash)
		return (m_error = OLE_E_BLANK);

	const bool bResult = m_pf_load_hash(_uid, cs_path.GetString());
	if (!bResult)
		m_error.SetState(
				E_FAIL,
				_T("Cannot load hash table")
			);
	else
		m_error = S_OK;

	return m_error;
}

HRESULT     CZillyaWebFilter::SetBlockState(unsigned int _uid, bool _set)
{
	m_error.Module(_T(__FUNCTION__));
	if (!_uid)
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Hash table identifier cannot be zero")
			);
		return m_error;
	}

	if (!m_pf_set_block)
		return (m_error = OLE_E_BLANK);

	m_pf_set_block(_uid, _set);
	if (m_error)
		m_error.Clear();

	return m_error;
}

HRESULT     CZillyaWebFilter::SetCallback(TRequestDataCallback _fn)
{
	m_error.Module(_T(__FUNCTION__));

	if (!m_pf_set_callback)
		return (m_error = OLE_E_BLANK);

	m_pf_set_callback(_fn);
	if (m_error)
		m_error.Clear();

	return m_error;
}

VOID        CZillyaWebFilter::Terminate(void)
{
	if (m_module)
	{
		/*::FreeLibrary(m_module);*/ m_module = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HMODULE     CZillyaWebFilter::LoadModule(void)
{
	CAtlString cs_path = details::ZillyaWebFilter_GetCorePath();

	CPathFinder path_(cs_path);

	HMODULE module_ = ::LoadLibrary(
				details::ZillyaWebFilter_GetCoreName()
			);
	return module_;
}