/*
	Created by Tech_dog (VToropov) on 12-Feb-2016 at 7:39:25pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Scanner Virus definitions database updater class implementation file.
*/
#include "StdAfx.h"
#include "Shared_AvsUpdater.h"

using namespace shared::avs;

#include "Shared_Registry.h"
#include "Shared_DateTime.h"
#include "Shared_FileSystem.h"
using namespace shared::lite::data;
using namespace shared::lite::common;

#include "Shared_WebProvider.h"
#include "Shared_NetResource.h"

using namespace shared::net;

#include "Shared_SystemCore.h"
#include "Shared_GenericEvent.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_SystemThreadPool.h"

using namespace shared::lite::sys_core;
using namespace shared::lite::runnable;

#include "XUnzip.h"

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace details
{
	class Updater_NetEventAdapter : public IInternetProviderCallback
	{
	private:
		IUpdaterCallback&     m_callback;
		DWORD                 m_received;
		DWORD                 m_total;
	public:
		Updater_NetEventAdapter(IUpdaterCallback& _callback, const DWORD _total) : 
		m_callback(_callback), m_received(0), m_total(_total) {}
	private: // IInternetProviderCallback
		virtual bool   InternetProvider_CanContinue(void) override sealed
		{
			return m_callback.IUpdater_OnCanContinue();
		}

		virtual void   InternetProvider_DataReceived(const DWORD dwDataSize) override sealed
		{
			m_received += dwDataSize;
			m_callback.IUpdater_OnDataReceived(m_received, m_total);
		}

		virtual void   InternetProvider_DownloadFinished(void) override sealed
		{
			m_callback.IUpdater_OnDownloadFinished();
		}

		virtual void   InrernetProvider_DownloadStarted(void) override sealed
		{
			m_received = 0;
			m_callback.IUpdater_OnDownloadStart();
		}
	};

	CAtlString  Updater_GetCheckUrl(void)
	{
		CRegistryStorage stg_(HKEY_LOCAL_MACHINE, true);
		CAtlString cs_url;

		HRESULT hr_ = stg_.Load(
							_T("Updater"),
							_T("ver_url"),
							cs_url
						);
		if (FAILED(hr_))
			cs_url = _T("about:blank");
#if defined(_DEBUG)
		cs_url = _T("http://localhost/CI/tmon_check_ver.php");
#endif
		return cs_url;
	}

	CAtlString  Updater_GetDownloadUrl(void)
	{
		CRegistryStorage stg_(HKEY_LOCAL_MACHINE, true);
		CAtlString cs_url;

		HRESULT hr_ = stg_.Load(
							_T("Updater"),
							_T("down_url"),
							cs_url
						);
		if (FAILED(hr_))
			cs_url = _T("about:blank");
#if defined(_DEBUG)
		cs_url = _T("http://localhost/CI/tmon_download.php");
#endif
		return cs_url;
	}

	CAtlString  Updater_GetDownloadFolder(void)
	{
		CAtlString db_path = CScannerVirusDatabase().Path();
		if (!db_path.IsEmpty())
		{
			db_path = db_path.Left(db_path.GetLength() - 1);  // removes trailing slash if any;
			const INT n_pos = db_path.ReverseFind(_T('\\'));  // finds the last part of the folder name
			if (-1 != n_pos)
				db_path = db_path.Left(n_pos + 1);            // preserves the found slash
		}
		return db_path;
	}

	CAtlString  Updater_GetDownloadFile(void)
	{
		CAtlString cs_file;
		cs_file += Updater_GetDownloadFolder();
		cs_file += _T("tmon_vdb_update.zip");
		return cs_file;
	}

	CAtlString  Updater_GetUnpackFolder(void)
	{
		CAtlString cs_path;
		cs_path += Updater_GetDownloadFolder();
		cs_path += "_update";
		return cs_path;
	}

	HRESULT     Updater_ParseToDate(const CAtlString& _raw_date, time_t& _timestamp)
	{
		INT val_[5] = {0};
		INT result_ = ::_stscanf_s(
				_raw_date,
				_T("%d.%d.%d %d:%d"),
				&val_[0], &val_[1], &val_[2], &val_[3], &val_[4]
			);
		if (result_ != 5)
			return (CSysError((DWORD)ERROR_INVALID_DATA));
		SYSTEMTIME st_ = {0};
		st_.wYear      = static_cast<WORD>(val_[0]);
		st_.wMonth     = static_cast<WORD>(val_[1]);
		st_.wDay       = static_cast<WORD>(val_[2]);
		st_.wHour      = static_cast<WORD>(val_[3]);
		st_.wMinute    = static_cast<WORD>(val_[4]);

		_timestamp     = CDateTime::SystemTimeToUnix(st_);

		HRESULT hr_ = S_OK;
		return  hr_;
	};

	CGenericSyncObject&
	            Updater_GetSafeGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}

	static UINT __stdcall Updater_DownloadThreadFunc(PVOID pObject)
	{
		if (NULL == pObject)
			return 1;

		CGenericRunnableObject* pRunnable = NULL;
		CUpdateDownloader* pDownloader = NULL;

		try
		{
			pRunnable  = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pDownloader = dynamic_cast<CUpdateDownloader*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CCoInitializer com_core(false);
		if (!com_core.IsSuccess())
			return 1;
	
		CUpdateDownloader& downloader_ = *pDownloader;
		IUpdaterCallback&  callback_ = downloader_.Callback();

		CAtlString db_folder = details::Updater_GetDownloadFolder();
		CAtlString cs_url    = details::Updater_GetDownloadUrl();

		do{
			CInternetProvider inet_;
			HRESULT hr_ = inet_.Initialize();
			if (FAILED(hr_))
			{
				callback_.IUpdater_OnUpdateError(inet_.Error());
				break;
			}
			if (downloader_.IsStopped())
				break;

			CInternetResource res_(inet_, cs_url, eInternetOption::eOneRequest);
			if (!res_.IsValid())
			{
				callback_.IUpdater_OnUpdateError(res_.Error());
				break;
			}
			if (downloader_.IsStopped())
				break;

			DWORD size_ = 0;
			hr_ = res_.GetContentLength(size_);
			if (FAILED(hr_))
			{
				callback_.IUpdater_OnUpdateError(res_.Error());
				break;
			}
			if (downloader_.IsStopped())
				break;

			details::Updater_NetEventAdapter adapter_(
					callback_,
					size_
				);
			hr_ = res_.Save(
				details::Updater_GetDownloadFile(),
				&adapter_
			);
			if (FAILED(hr_))
			{
				callback_.IUpdater_OnUpdateError(res_.Error());
				break;
			}
			if (downloader_.IsStopped())
				break;

			static const DWORD expected_ = 1024 * 1024;

			if (size_ < expected_)
			{
				CSysError err_obj;
				err_obj.Source(_T("Downloader"));
				err_obj.SetState(
						(DWORD)ERROR_INVALID_DATA,
						_T("An error occurred on web service side")
					);
				callback_.IUpdater_OnUpdateError(err_obj);
			}

		} while (false);
		downloader_.MarkCompleted();
		return 0;
	}

	static UINT __stdcall Updater_UnpackThreadFunc(PVOID pObject)
	{
		if (NULL == pObject)
			return 1;

		CGenericRunnableObject* pRunnable = NULL;
		CUpdateUnpacker* pUnpacker = NULL;

		try
		{
			pRunnable = reinterpret_cast<CGenericRunnableObject*>(pObject);
			pUnpacker = dynamic_cast<CUpdateUnpacker*>(pRunnable);
		}
		catch (::std::bad_cast&) { return 1; }

		CCoInitializer com_core(false);
		if (!com_core.IsSuccess())
			return 1;
	
		CUpdateUnpacker& unpacker_  = *pUnpacker;
		IUpdaterCallback& callback_ = unpacker_.Callback();

		CSysError err_obj(S_OK);
		err_obj.Source(_T("Unpacker"));
		err_obj.Module(_T("Updater"));

		HZIP hz = NULL;

		do{
			CAtlString cs_path = Updater_GetUnpackFolder();

			CGenericFolder folder_(cs_path);

			if (folder_.IsExist() == true) // unsuccessful previous installation
				folder_.Delete();

			if (folder_.IsExist() == false)
				folder_.Create();


			CCurrentFolder unpack_(
							cs_path
						);
			if (unpack_.Error())
			{
				err_obj = unpack_.Error();
				break;
			}

			callback_.IUpdater_OnUnpackStart();

			CAtlString cs_zip = Updater_GetDownloadFile();
			hz = OpenZip(cs_zip.GetBuffer(), 0, ZIP_FILENAME);
			if (!hz)
			{
				err_obj.SetState(
						(DWORD)ERROR_INVALID_DATA,
						_T("Cannot open installation archive")
					);
				break;
			}
			if (callback_.IUpdater_OnCanContinue() == false)
				break;

			ZIPENTRYW zentry_ = {0};
			ZRESULT zresult_ = GetZipItem(hz, -1, &zentry_);
			if (ZR_OK == zresult_)
			{
				const INT total_ = zentry_.index;
				for ( INT i_ = 0; i_ < total_; i_ ++)
				{

					if (callback_.IUpdater_OnCanContinue() == false)
						break;

					zresult_ = GetZipItem(hz, i_, &zentry_);
					if (ZR_OK != zresult_)
					{
						err_obj.SetState(
								E_FAIL,
								_T("Invalid archive entry")
							);
						break;
					}
					callback_.IUpdater_OnUnpackProgress(
							static_cast<DWORD>(i_ + 1),
							static_cast<DWORD>(total_),
							CAtlString(zentry_.name)
						);
					zresult_ = UnzipItem(hz , i_ ,zentry_.name, 0 , ZIP_FILENAME);
					if (ZR_OK != zresult_)
					{
						CAtlString cs_error;
						cs_error.Format(
								_T("Cannot unpack file %s"),
								zentry_.name
							);
						err_obj.SetState(
								E_FAIL,
								cs_error
							);
						break;
					}
				}
			}
			else
			{
				err_obj.SetState(
						(DWORD)ERROR_INVALID_DATA,
						_T("Cannot retrieve an archive content properties")
					);
			}
		} while(false);

		if (hz)
			CloseZip(hz);

		if (!err_obj)
			callback_.IUpdater_OnUnpackFinish();
		else
			callback_.IUpdater_OnUpdateError(err_obj);

		unpacker_.MarkCompleted();
		return 0;
	}
}}}

#define SAFE_LOCK_UPDATE()  SAFE_LOCK(details::Updater_GetSafeGuard());

/////////////////////////////////////////////////////////////////////////////

CUpdateDownloader::CUpdateDownloader(IUpdaterCallback& _callback) :
	TRunnable(
			details::Updater_DownloadThreadFunc,
			*this,
			_variant_t((LONG)CUpdateDownloader::eId)
		),
	m_callback(_callback)
{
}

CUpdateDownloader::~CUpdateDownloader(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CUpdateDownloader::GenericEvent_OnNotify(const _variant_t v_evt_id)
{
	v_evt_id;
	HRESULT hr_ = S_OK;
	return  hr_;
}
/////////////////////////////////////////////////////////////////////////////

IUpdaterCallback&  CUpdateDownloader::Callback(void)
{
	return m_callback;
}

/////////////////////////////////////////////////////////////////////////////

CUpdateUnpacker::CUpdateUnpacker(IUpdaterCallback& _callback) :
	TRunnable(
		details::Updater_UnpackThreadFunc,
			*this,
			_variant_t((LONG)CUpdateUnpacker::eId)
		),
	m_callback(_callback)
{
}

CUpdateUnpacker::~CUpdateUnpacker(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CUpdateUnpacker::GenericEvent_OnNotify(const _variant_t v_evt_id)
{
	v_evt_id;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

IUpdaterCallback&  CUpdateUnpacker::Callback(void)
{
	return m_callback;
}

/////////////////////////////////////////////////////////////////////////////

CUpdater::CUpdater(CScannerEngine& _engine, IUpdaterCallback& _callback) : 
	m_engine(_engine),
	m_downloader(_callback),
	m_unpacker(_callback)
{
	m_error.Source(_T("CUpdater"));
}

CUpdater::~CUpdater(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CUpdater::CheckVersion(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	const CAtlString url_ = details::Updater_GetCheckUrl();

	CWebProvider web_;
	HRESULT hr_ = web_.Initialize();
	if (FAILED(hr_))
		return (m_error = web_.Error());

	CAtlString response_;
	hr_ = web_.DoGetRequest(url_, response_);
	if (FAILED(hr_))
		return (m_error = web_.Error());

	time_t release_ = 0;
	hr_ = details::Updater_ParseToDate(response_, release_);
	if (FAILED(hr_))
	{
		m_error.SetState(
				hr_,
				_T("Invalid virus database release date format")
			);
		return m_error;
	}

	const CScannerVirusDatabase vdb_;
	const time_t current_ = vdb_.Timestamp();

	if (current_ < release_)
		m_error.SetState(
			S_OK,
			_T("New release is available")
		);
	else
		m_error.SetState(
			S_FALSE,
			_T("The version of virus database is uptodate")
		);

	return m_error;
}

HRESULT      CUpdater::DownloadData(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Cannot start a download when update process is in progress")
			);
		return m_error;
	}

	m_error = m_downloader.Start();
	
	return m_error;
}

TErrorRef    CUpdater::Error(void)  const
{
	return m_error;
}

HRESULT      CUpdater::InstallDatabase(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	return m_error;
}

HRESULT      CUpdater::Interrupt(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_downloader.IsStopped() == false)
		m_downloader.Stop(false);

	if (m_unpacker.IsStopped() == false)
		m_unpacker.Stop(false);

	return m_error;
}

bool         CUpdater::IsRunning(void)const
{
	bool bRunning = false;

	if (!bRunning) bRunning = !m_downloader.IsStopped();
	if (!bRunning) bRunning = !m_unpacker.IsStopped();

	return bRunning;
}

HRESULT      CUpdater::UnpackDatabase(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Cannot start an unpacking when update process is in progress")
			);
		return m_error;
	}

	m_error = m_unpacker.Start();
	
	return m_error;
}