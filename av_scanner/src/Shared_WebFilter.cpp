/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 2:27:39pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared Antivirus Web Filter class implementation file.
*/
#include "StdAfx.h"
#include "Shared_WebFilter.h"

using namespace shared::avs;

#include "Shared_GenericSyncObject.h"
#include "Shared_GenericHandle.h"
#include "Shared_GenericAppObject.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;
using namespace shared::lite::runnable;

#include "Shared_WebFilter_Functor.h"

using namespace shared::avs::impl_;
using namespace Zillya;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace avs { namespace details
{
	CGenericSyncObject& WebFilterEvents_GetEvtSafeGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}

	typedef ::std::map<IWebFilterEventSink*, IWebFilterEventSink*> TWebFilterSinkMap;

	TWebFilterSinkMap&  WebFilterEvents_GetEvtSinkMap(void)
	{
		static TWebFilterSinkMap map_;
		return map_;
	}

	CWebFilterEvents&   WebFilterEvents_GetObject(void)
	{
		static CWebFilterEvents obj_;
		return obj_;
	}

	CZillyaWebFilter&   WebFilter_GetFunctor(void)
	{
		static CZillyaWebFilter fn_;
		return fn_;
	}


	class CWebFilter_TempFile
	{
	private:
		CSysError   m_error;
	public:
		CWebFilter_TempFile(const TWebBlackList& _list)
		{
			CAtlStringA content_;
			for (TWebBlackList::const_iterator it_ = _list.begin(); it_ != _list.end(); ++it_)
			{
				content_ += CAtlStringA(it_->first);
				content_ += "\r\n";
			}
			content_ += "\r\n";

			CAutoHandle hTarget = ::CreateFile(
									this->GetFilePath(),
									GENERIC_WRITE,
									0,
									NULL,
									CREATE_ALWAYS,
									FILE_ATTRIBUTE_NORMAL,
									NULL
								);
			if (!hTarget.IsValid())
				m_error = ::GetLastError();
			else
			{
				m_error = S_OK;
				DWORD dwBytesWritten = 0;
				const BOOL  bResult  = ::WriteFile(
									hTarget,
									(void*)content_.GetString(),
									(DWORD)content_.GetLength(),
									&dwBytesWritten,
									NULL
								);
				if (!bResult)
					m_error = ::GetLastError();
			}
		}
		~CWebFilter_TempFile(void)
		{
			if (!m_error)
				::DeleteFile(
						this->GetFilePath()
					);
		}
	public:
		TErrorRef   Error(void)const
		{
			return m_error;
		}

		CAtlString  GetFilePath(void) const
		{
			static CAtlString cs_path;
			if (cs_path.IsEmpty())
			{
				CApplication the_app;
				the_app.GetPathFromAppFolder(_T(".\\black_list.txt"), cs_path);
			}
			return cs_path;
		}
	};

	class CWebFilter_HashFile
	{
	private:
		CSysError   m_error;
	public:
		CWebFilter_HashFile(void)
		{
			CAtlString exe_;
			{
				CApplication the_app;
				the_app.GetPathFromAppFolder(_T(".\\hash.exe"), exe_);
			}
			CAtlString dir_;
			{
				CApplication the_app;
				the_app.GetPathFromAppFolder(_T(".\\"), dir_);
			}
			SHELLEXECUTEINFO info_ = {0};

			info_.cbSize = sizeof(SHELLEXECUTEINFO);
			info_.lpVerb = _T("open");
			info_.lpFile = exe_;
			info_.lpDirectory  = dir_;
			info_.lpParameters = _T("black_list.txt hash_table.wfapi_ht");
			info_.hwnd   = HWND_DESKTOP;
			info_.nShow  = SW_HIDE;

			const BOOL bResult = ::ShellExecuteEx(&info_);
			if (!bResult)
				m_error = ::GetLastError();
			else
			{
				CGenericFile file_(this->GetFilePath());
				if (file_.Error())
					m_error = file_.Error();
				else
					m_error = S_OK;
			}
		}
	public:
		TErrorRef   Error(void)const
		{
			return m_error;
		}
		static
		CAtlString  GetFilePath(void)
		{
			static CAtlString cs_path;
			if (cs_path.IsEmpty())
			{
				CApplication the_app;
				the_app.GetPathFromAppFolder(_T(".\\hash_table.wfapi_ht"), cs_path);
			}
			return cs_path;
		}
	};

	static bool WebFilter_Callback(const struct RequestHttpData& _data)
	{
		_data;
		return true;
	}
}}}

#define SAFE_LOCK_EVT()  SAFE_LOCK(details::WebFilterEvents_GetEvtSafeGuard());

using shared::avs::details::TWebFilterSinkMap;
/////////////////////////////////////////////////////////////////////////////

CWebFilterEvents::CWebFilterEvents(void)
{
	m_error.Source(_T("CWebFilterEvents"));
}

CWebFilterEvents::~CWebFilterEvents(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID      CWebFilterEvents::RaiseBlockEvent(CAtlString _url)
{
	SAFE_LOCK_EVT();

	TWebFilterSinkMap& map_ = details::WebFilterEvents_GetEvtSinkMap();

	for (TWebFilterSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		IWebFilterEventSink* pSink = it_->first;
		if (pSink)
			pSink->IWebFilterEvent_OnBlock(_url);
	}
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef CWebFilterEvents::Error(void)const
{
	return m_error;
}

HRESULT   CWebFilterEvents::Subscribe(IWebFilterEventSink* pIface)
{
	SAFE_LOCK_EVT();

	m_error.Module(_T(__FUNCTION__));
	if (!pIface)
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Invalid web filter sink pointer")
			);
		return m_error;
	}

	TWebFilterSinkMap& map_ = details::WebFilterEvents_GetEvtSinkMap();
	try
	{
		map_[pIface] = pIface;
		m_error = S_OK;
	}
	catch(::std::bad_alloc&)
	{
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}

HRESULT   CWebFilterEvents::Unsubscribe(IWebFilterEventSink* pIface)
{
	SAFE_LOCK_EVT();

	m_error.Module(_T(__FUNCTION__));
	if (!pIface)
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Invalid web filter sink pointer")
			);
		return m_error;
	}

	TWebFilterSinkMap& map_ = details::WebFilterEvents_GetEvtSinkMap();

	TWebFilterSinkMap::iterator it_ = map_.find(pIface);
	if (it_ == map_.end())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Web filter sink is not found")
			);
		return m_error;
	}
	else
	{
		try
		{
			map_.erase(it_);
			m_error = S_OK;
		}
		catch(...)
		{
			m_error = E_OUTOFMEMORY;
		}
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CWebFilter::CWebFilter(void) : m_active(false)
{
	m_error.Source(_T("CWebFilter"));
}

CWebFilter::~CWebFilter(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CWebFilter::Activate(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (this->IsActivated())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Web filter is already active")
			);
		return m_error;
	}

	CAtlString cs_path = details::CWebFilter_HashFile::GetFilePath();

	CZillyaWebFilter& fn_ = details::WebFilter_GetFunctor();
	HRESULT hr_ = fn_.LoadHashTable(1, cs_path);
	if (FAILED(hr_))
		return (m_error = fn_.Error());

	hr_ = fn_.SetBlockState(1, true);
	if (FAILED(hr_))
		return (m_error = fn_.Error());

	m_active = true;
	if (m_error)
		m_error.Clear();

	return m_error;
}

HRESULT         CWebFilter::Deactivate(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (!this->IsActivated())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Web filter is already deactived")
			);
		return m_error;
	}

	CAtlString cs_path = details::CWebFilter_HashFile::GetFilePath();

	CZillyaWebFilter& fn_ = details::WebFilter_GetFunctor();

	HRESULT hr_ = fn_.LoadHashTable(1, cs_path);
	if (FAILED(hr_))
		return (m_error = fn_.Error());

	hr_ = fn_.SetBlockState(1, false);
	if (FAILED(hr_))
		return (m_error = fn_.Error());

	m_active = false;
	if (m_error)
		m_error.Clear();

	return m_error;
}

TErrorRef       CWebFilter::Error(void)const
{
	return m_error;
}

CWebFilterEvents&
                CWebFilter::Events(void)
{
	return details::WebFilterEvents_GetObject();
}

HRESULT         CWebFilter::GenerateHashTable(const TWebBlackList& _list)
{
	m_error.Module(_T(__FUNCTION__));

	if (_list.empty())
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Web filter cannot be initialized by empty black list")
			);
		return m_error;
	}

	details::CWebFilter_TempFile temp_(_list);
	if (temp_.Error())
		return (m_error = temp_.Error());

	details::CWebFilter_HashFile hash_;
	if (hash_.Error())
		return (m_error = hash_.Error());

	if (m_error)
		m_error.Clear();

	return m_error;
}

HRESULT         CWebFilter::Initialize(void)
{
	m_error.Module(_T(__FUNCTION__));

	CZillyaWebFilter& fn_ =  details::WebFilter_GetFunctor();
	HRESULT hr_ = fn_.Initialize();
	if (FAILED(hr_))
		m_error = fn_.Error();
	else if (m_error)
		m_error.Clear();

	fn_.SetCallback(details::WebFilter_Callback);

	return m_error;
}

bool            CWebFilter::IsActivated(void)const
{
	return m_active;
}

HRESULT         CWebFilter::Terminate(void)
{
	m_error.Module(_T(__FUNCTION__));

	CZillyaWebFilter& fn_ =  details::WebFilter_GetFunctor();

	fn_.SetCallback(NULL);
	fn_.Terminate();

	if (m_error)
		m_error.Clear();

	return m_error;
}