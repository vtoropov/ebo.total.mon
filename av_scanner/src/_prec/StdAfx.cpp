/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 9:03:55pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared AV Scanner Library precompiled headers implementation file.
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

namespace global { namespace _avs
{
	volatile
	CScannerState::_e&   GetScannerStateRef(void)
	{
		static volatile CScannerState::_e state_ = CScannerState::eNotInitialized;
		return state_;
	}
}}