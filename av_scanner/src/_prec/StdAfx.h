#ifndef _SHAREDSCANNERSTDAFX_H_C26D7B61_BE3D_4345_AD62_126BEC7B5684_INCLUDED
#define _SHAREDSCANNERSTDAFX_H_C26D7B61_BE3D_4345_AD62_126BEC7B5684_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 9:01:37pm, GMT+7, Phuket, Rawai, Thursday;
	This is Shared AV Scanner Library precompiled headers declaration file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0603  // this is for use IE 6 SP3 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>
#include <queue>
#include <time.h>

#include <shellapi.h>

#include "Shared_AvsDefs.h"

namespace global { namespace _avs
{
	using shared::avs::CScannerState;

	volatile
	CScannerState::_e&   GetScannerStateRef(void);
}}

#endif/*_SHAREDSCANNERSTDAFX_H_C26D7B61_BE3D_4345_AD62_126BEC7B5684_INCLUDED*/