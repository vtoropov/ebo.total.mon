#ifndef _SHAREDENGINEZILLYAIMPL_H_2A44CA02_7701_4a87_A17A_3F7F5BEF28B8_INCLUDED
#define _SHAREDENGINEZILLYAIMPL_H_2A44CA02_7701_4a87_A17A_3F7F5BEF28B8_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 11:52:06am, GMT+7, Phuket, Rawai, Thursday;
	This is Shared AV scanner Zillya engine wrapper class(es) declaration file.
*/
#include "engine_def.h"

#include "Shared_AvsError.h"
#include "Shared_AvsDefs.h"

namespace shared { namespace avs { namespace impl_
{
	using shared::avs::CScanError;

	class CZillyaFunctor
	{
	private:
		HMODULE       m_module;
		CScanError    m_error;
		PCoreInit     m_pfn_init;
		PCoreFree     m_pfn_free;
		PCoreFScan    m_pfn_scan;
		PCoreStopScan m_pfn_stop;
		DWORD         m_core_handle;
	public:
		CZillyaFunctor(HMODULE _lib = NULL);
		~CZillyaFunctor(void);
	public:
		HRESULT       CoreInit(TCoreData&);
		CScanError    Error(void)const;
		HRESULT       ScanFile(CScanFile&);
		HRESULT       StopScan(void);       // actually, this function is called from main thread only
	public:
		static
		HMODULE       LoadModule(void);
		static
		CAtlString    VirusDatabasePath(void);
	};
}}}

#endif/*_SHAREDENGINEZILLYAIMPL_H_2A44CA02_7701_4a87_A17A_3F7F5BEF28B8_INCLUDED*/