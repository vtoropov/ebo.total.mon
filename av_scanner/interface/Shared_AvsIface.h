#ifndef _SHAREDAVSIFACE_H_776A7F68_BC91_4c00_81BD_41C766A83590_INCLUDED
#define _SHAREDAVSIFACE_H_776A7F68_BC91_4c00_81BD_41C766A83590_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 11:20:43am, GMT+7, Phuket, Rawai, Thursday;
	This is shared AV scanner interface declaration file.
*/
#include "Shared_AvsDefs.h"
#include "Shared_AvsError.h"

namespace shared { namespace avs
{
	class CScannerVendor
	{
	public:
		enum _e{
			eUndefined = 0x0,
			eZillya    = 0x1,
		};
	};

	typedef ::std::vector<CAtlString> TVirusDefinitionFiles;

	class CScannerVirusDatabase
	{
	private:
		TVirusDefinitionFiles m_files;
	public:
		CScannerVirusDatabase(void);
		~CScannerVirusDatabase(void);
	public:
		TVirusDefinitionFiles Files(void)const;
		HRESULT               Initialize(void);
		CAtlString            Path (void)const;                    // returns the path to database folder
		DWORD                 Records(void)const;
		time_t                Timestamp(void)const;                // actually a release date of database
		CAtlString            TimestampAsText(void)const;
	};

	class CScannerEngine
	{	
	private:
		CScanError            m_error;
		CScannerVendor::_e    m_eVendor;
		CScannerEvents        m_events;
		CScannerVirusDatabase m_database;
	public:
		CScannerEngine(const CScannerVendor::_e);
		~CScannerEngine(void);
	public:
		HRESULT               CancelScans(void);            // cancels all scans that are currently running
		CScanError            Error(void)const;
		CScannerEvents&       Events(void);
		HRESULT               Initialize(void);
		bool                  IsInitialized(void)const;
		bool                  IsReady(void)const;
		HRESULT               Lock(DWORD& dwCookie);        // tries to gain an exclusive access to the engine
		HRESULT               ScanFolderOrFile(LPCTSTR pszPath);
		CScannerState::_e     State(void)const;
		HRESULT               Terminate(void);              // this function must be called on application exit only
		DWORD                 ThreadCount(void)const;
		HRESULT               ThreadCount(const DWORD);
		CScannerVendor::_e    Vendor(void)const;
		DWORD                 Version(void)const;
		CAtlString            VersionAsText(void)const;
		CScannerVirusDatabase VirusDatabase(void)const;
		HRESULT               Unlock(const DWORD dwCookie); // tries to release/unlock the scanner object
	private:
		HRESULT              _ScanFile(LPCTSTR pszFilePath);
		HRESULT              _ScanFolder(LPCTSTR pszFolderPath);
	};

	class CScannerLocker
	{
	private:
		CSysError             m_error;
		DWORD&                m_locker;
		CScannerEngine&       m_engine;
	public:
		CScannerLocker(CScannerEngine&, DWORD& _locker);
		~CScannerLocker(void);
	public:
		TErrorRef             Error(void)const;
		bool                  IsLocked(void)const;
		HRESULT               Lock(void);
		HRESULT               Unlock(void);
	private:
		CScannerLocker(const CScannerLocker&);
		CScannerLocker& operator= (const CScannerLocker&);
	};
}}

#endif/*_SHAREDAVSIFACE_H_776A7F68_BC91_4c00_81BD_41C766A83590_INCLUDED*/