#ifndef _SHAREDAVSDEFS_H_112C4CF9_0720_4e0a_B5E7_1A1814921600_INCLUDED
#define _SHAREDAVSDEFS_H_112C4CF9_0720_4e0a_B5E7_1A1814921600_INCLUDED
/*
	Created by Tech_dog (VToropov) on 4-Dec-2015 at 8:24:23am, GMT+7, Phuket, Rawai, Friday;
	This is Shared AV scanner common definition declaration file.
*/
#include "Shared_AvsError.h"

namespace shared { namespace avs
{
	enum{
		eMaxAllowedThreads = 10,
	};

	class CScanThreadCounter
	{
	private:
		static
		DWORD         m_cur_threads;   // current number of threads in the thread pool
		DWORD         m_max_threads;   // currently set maximum of threads in the pool; default is one thread
		CScanError    m_error;
	public:
		CScanThreadCounter(void);
	public:
		CScanError    Error(void)const;
		bool          IsAllowedMoreThreads(void)const;
		bool          RegisterThread(void);
		DWORD         ThreadsAllowed(void)const;
		bool          ThreadsAllowed(const DWORD);
		DWORD         ThreadsCount  (void)const;
		bool          UnregisterThread(void);
	};

	class CScanFileHealth
	{
	public:
		enum _e{
			eUnknown     = 0,
			eClean       = 1,
			eSuspected   = 2,
			eInfected    = 3,
			eError       = 4,
		};
	};

	class CScanFile
	{
	private:
		time_t              m_time;
		CAtlString          m_path;
		CAtlString          m_name;
		CScanFileHealth::_e m_health;
		CAtlString          m_virus;
	public:
		CScanFile(void);
		~CScanFile(void);
	public:
		CScanFileHealth::_e Health(void)const;
		VOID                Health(const CScanFileHealth::_e);
		CAtlString          HealthAsText(void)const;
		bool                IsHealthy(void)const;
		bool                IsSick(void)const;
		bool                IsValid(void)const;
		CAtlString          Name(void)const;
		VOID                Name(LPCTSTR);
		CAtlString          Path(void)const;
		VOID                Path(LPCTSTR);
		VOID                SetCurrentTime(void);
		time_t              Timestamp(void)const;
		VOID                Timestamp(const time_t);
		CAtlString          TimestamptAsText(void)const;
		CAtlString          Virus(void)const;
		VOID                Virus(LPCTSTR);
	};

	typedef ::std::queue<CScanFile>  TFileScanQueue;
	typedef ::std::vector<CScanFile> TFileScanList;

	class CScanFileList
	{
	private:
		TFileScanQueue      m_awaiting;
		TFileScanList       m_processed;
	public:
		CScanFileList(void);
		~CScanFileList(void);
	public:
		HRESULT             Append(LPCTSTR pszName, LPCTSTR pszPath); // appends a file to the queue for scanning
		CScanFile           GetScanObject(void);                      // gets first available object for scanning; if no file is available, invalid object is returned;
		VOID                PutScanObject(const CScanFile&);          // puts the object to the list of scanned files
	public:
		bool                IsEmptyQueue(void)const;
	};

	typedef ::std::queue<CAtlString>  TFolderQueue;

	class CPreparedFolderList
	{
	private:
		TFolderQueue        m_list;   // folder list that is prepared for file enumeration and further scanning
	public:
		CPreparedFolderList(void);
		~CPreparedFolderList(void);
	public:
		HRESULT             Append(LPCTSTR pszFolder); // appends a folder for file enumeration
		bool                IsEmpty(void)const;        // checks the list
		CAtlString          GetFolder(void);           // gets a prepared folder for file enumeration
	};

	interface IScannerEventSink
	{
	public:
		virtual VOID    IScannerEvent_OnEngineInitBegin(CAtlString)      {}
		virtual VOID    IScannerEvent_OnEngineInitFinish(CScanError)     {}
		virtual VOID    IScannerEvent_OnFileScanBegin(CAtlString _path)  {_path;}
		virtual VOID    IScannerEvent_OnFileScanFinish(CScanFile _file)  {_file;}
		virtual VOID    IScannerEvent_OnLock(const DWORD dwLockOwnerId)  {dwLockOwnerId;}
		virtual VOID    IScannerEvent_OnScanProcessBegin(VOID)           {}
		virtual VOID    IScannerEvent_OnScanProcessFinish(VOID)          {}
		virtual VOID    IScannerEvent_OnUnlock(const DWORD dwLockOwnerId){dwLockOwnerId;}
	};

	class CScannerEvents
	{
	public:
		CScannerEvents(void);
		~CScannerEvents(void);
	public:
		VOID      RaiseFileScanBeginEvent(CAtlString _path);
		VOID      RaiseFileScanFinishEvent(CScanFile _file);
		VOID      RaiseInitBeginEvent(CAtlString _msg);
		VOID      RaiseInitFinishEvent(CScanError);
		VOID      RaiseLockEvent(const DWORD dwLockOwnerId);
		VOID      RaiseScanProcessBeginEvent(VOID);
		VOID      RaiseScanProcessFinishEvent(VOID);
		VOID      RaiseUnlockEvent(const DWORD dwLockOwnerId);
	public:
		HRESULT   Subscribe(IScannerEventSink*);
		HRESULT   Unsubscribe(IScannerEventSink*);
	};

	class CScannerState
	{
	public:
		enum _e{
			eNotInitialized = 0,
			eInitializing   = 1,
			eInitialized    = 2,
			eScanning       = 3,
			eSuspended      = 4,
			eError          = 5,
		};
	};

	class CScanActionState
	{
	public:
		enum _e{
			eAwaiting       = 0,
			ePerformed      = 1,
			eError          = 2,
		};
	};

	class CScanAction
	{
	public:
		enum _e{
			eActionNone     = 0,
		//	eActionCure     = 1,
		//	eActionQuarantine = 2,
			eActionDelete   = 3,
		};
	private:
		CScanAction::_e      m_id;
		CAtlString           m_name;
		bool                 m_valid;
		INT                  m_index;   // enumeration index
		CScanActionState::_e m_state;
	public:
		CScanAction(const bool bValid = true);
		CScanAction(const CScanAction::_e, LPCTSTR pszName, const INT _ndx = -1);
	public:
		CScanAction::_e      Identifier(void)const;
		VOID                 Identifier(const CScanAction::_e);
		INT                  Index(void)const;
		VOID                 Index(const INT);
		bool                 IsValid(void)const;
		CAtlString           Name(void)const;
		VOID                 Name(LPCTSTR);
		CScanActionState::_e State(void)const;
		HRESULT              State(const CScanActionState::_e);
		CAtlString           StateAsText(void)const;
	};

	typedef ::std::vector<CScanAction> TScanActions;

	class CScanActionEnum
	{
	private:
		static
		TScanActions        m_actions;
	public:
		CScanActionEnum(void);
	public:
		INT                 Count(void)const;
		const
		CScanAction&        Default(void)const;
		const
		CScanAction&        Find (const CScanAction::_e);
		const
		CScanAction&        Item (const INT nIndex)const;
	};

	class CScanFileEx : public CScanFile
	{
	private:
		CScanAction         m_action;   // applied action
	public:
		CScanFileEx(void);
	public:
		const
		CScanAction&        Action(void)const;
		CScanAction&        Action(void);
		CAtlString          ActionAsText(void)const;
	public:
		CScanFileEx(const CScanFile&);
		CScanFileEx& operator= (const CScanFile&);
	};

	typedef ::std::vector<CScanFileEx> TFileScanListEx;
}}

#endif/*_SHAREDAVSDEFS_H_112C4CF9_0720_4e0a_B5E7_1A1814921600_INCLUDED*/