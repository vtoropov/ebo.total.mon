#ifndef _SHAREDAVSUPDATER_H_B76E49D8_5320_4103_8527_FCE33EDBD95E_INCLUDED
#define _SHAREDAVSUPDATER_H_B76E49D8_5320_4103_8527_FCE33EDBD95E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 12-Feb-2016 at 7:30:21pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Scanner Virus definitions database updater class declaration file.
*/
#include "Shared_AvsDefs.h"
#include "Shared_AvsError.h"
#include "Shared_AvsIface.h"

#include "Shared_GenericEvent.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_GenericRunnableObject.h"

namespace shared { namespace avs
{
	using shared::lite::runnable::CGenericRunnableObject;
	using shared::lite::runnable::CGenericEvent;
	using shared::lite::runnable::CGenericSyncObject;
	using shared::lite::runnable::IGenericEventNotify;

	interface IUpdaterCallback
	{
		virtual bool       IUpdater_OnCanContinue (void) PURE;
		virtual void       IUpdater_OnDownloadFinished(void) PURE;
		virtual void       IUpdater_OnDataReceived(const DWORD dwReceived, const DWORD dwTotal) PURE;
		virtual void       IUpdater_OnDownloadStart(void) PURE;
		virtual void       IUpdater_OnUnpackFinish(void) PURE;
		virtual void       IUpdater_OnUnpackProgress(const DWORD dwIndex, const DWORD dwTotal, CAtlString _file)PURE;
		virtual void       IUpdater_OnUnpackStart(void)PURE;
		virtual void       IUpdater_OnUpdateError(CSysError) PURE;
	};

	class CUpdateDownloader :
		public  CGenericRunnableObject,
		public  IGenericEventNotify
	{
		typedef CGenericRunnableObject TRunnable;
	public:
		enum {
			eId = 0x10,
		};
	private:
		IUpdaterCallback&  m_callback;
	public:
		CUpdateDownloader(IUpdaterCallback&);
		~CUpdateDownloader(void);
	private:
		CUpdateDownloader(const CUpdateDownloader&);
		CUpdateDownloader& operator= (const CUpdateDownloader&);
	private:
		HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed;
	public:
		IUpdaterCallback&  Callback(void);
	};

	class CUpdateUnpacker :
		public  CGenericRunnableObject,
		public  IGenericEventNotify
	{
		typedef CGenericRunnableObject TRunnable;
	public:
		enum {
			eId = 0x11,
		};
	private:
		IUpdaterCallback&  m_callback;
	public:
		CUpdateUnpacker(IUpdaterCallback&);
		~CUpdateUnpacker(void);
	private:
		CUpdateUnpacker(const CUpdateUnpacker&);
		CUpdateUnpacker& operator= (const CUpdateUnpacker&);
	private:
		HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed;
	public:
		IUpdaterCallback&  Callback(void);
	};

	class CUpdater
	{
	private:
		CSysError          m_error;
		CScannerEngine&    m_engine;
		CUpdateDownloader  m_downloader;
		CUpdateUnpacker    m_unpacker;
	private:
		CGenericSyncObject m_sync_obj;
	public:
		CUpdater(CScannerEngine&, IUpdaterCallback&);
		~CUpdater(void);
	public:
		HRESULT            CheckVersion(void);
		HRESULT            DownloadData(void);
		TErrorRef          Error(void)  const;
		HRESULT            InstallDatabase(void);
		HRESULT            Interrupt(void);
		bool               IsRunning(void)const;
		HRESULT            UnpackDatabase(void);
	};

}}
#endif/*_SHAREDAVSUPDATER_H_B76E49D8_5320_4103_8527_FCE33EDBD95E_INCLUDED*/