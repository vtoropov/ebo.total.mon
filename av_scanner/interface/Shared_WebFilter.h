#ifndef _SHAREDWEBFILTER_H_C71FE2B3_5644_4f83_A698_E29A3B9C5F63_INCLUDED
#define _SHAREDWEBFILTER_H_C71FE2B3_5644_4f83_A698_E29A3B9C5F63_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 2:10:12pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared Antivirus Web Filter class declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace avs
{
	using shared::lite::common::CSysError;

	interface IWebFilterEventSink
	{
	public:
		virtual VOID      IWebFilterEvent_OnBlock(CAtlString _url)     {}
	};

	class CWebFilterEvents
	{
	private:
		CSysError         m_error;
	public:
		CWebFilterEvents(void);
		~CWebFilterEvents(void);
	public:
		VOID              RaiseBlockEvent(CAtlString _url);
	public:
		TErrorRef         Error(void)const;
		HRESULT           Subscribe(IWebFilterEventSink*);
		HRESULT           Unsubscribe(IWebFilterEventSink*);
	};

	typedef ::std::map<CAtlString, CAtlString>  TWebBlackList;

	class CWebFilter
	{
	private:
		CSysError         m_error;
		bool              m_active;
	public:
		CWebFilter(void);
		~CWebFilter(void);
	public:
		HRESULT           Activate(void);
		HRESULT           Deactivate(void);
		TErrorRef         Error(void)const;
		CWebFilterEvents& Events(void);
		HRESULT           GenerateHashTable(const TWebBlackList&);
		HRESULT           Initialize(void);
		bool              IsActivated(void)const;
		HRESULT           Terminate(void);
	};
}}

#endif/*_SHAREDWEBFILTER_H_C71FE2B3_5644_4f83_A698_E29A3B9C5F63_INCLUDED*/