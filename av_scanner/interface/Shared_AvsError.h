#ifndef _SHAREDAVSERROR_H_B319DA3C_0C7C_403e_882D_94C072123556_INCLUDED
#define _SHAREDAVSERROR_H_B319DA3C_0C7C_403e_882D_94C072123556_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 10:10:52pm, GMT+7, Phuket, Rawai, Thursday;
	This is shared AV scanner error class declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace avs
{
	using shared::lite::common::CSysError;

	class CScanError : public CSysError
	{
		typedef CSysError TBase;
	public:
		CScanError(LPCTSTR pszModule = NULL);
	public:
		CScanError(const CScanError&);
		CScanError& operator= (const CScanError&);
	public:
		VOID          Clear(void);
		VOID          Module(LPCTSTR);
		VOID          SetState(const DWORD  dwError, LPCTSTR pszDescription);
		VOID          SetState(const HRESULT hError, LPCTSTR pszDescription);
		VOID          Source(LPCTSTR);
	public:
		CScanError& operator= (const DWORD);
		CScanError& operator= (const HRESULT);
	};
}}

#endif/*_SHAREDAVSERROR_H_B319DA3C_0C7C_403e_882D_94C072123556_INCLUDED*/