/*
	Created by Tech_dog (VToropov) on 26-Jan-2016 at 4:30:25am, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared Library Task Scheduler class implementation file.
*/
#include "StdAfx.h"
#include "Shared_TskScheduler.h"

using namespace shared::tasks;

#include "Shared_SystemCore.h"
#include "Shared_GenericEvent.h"
#include "Shared_GenericSyncObject.h"

using namespace shared::lite::sys_core;
using namespace shared::lite::runnable;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace tasks { namespace details
{
	CGenericSyncObject&    TaskScheduler_GetEvtSafeGuard(void)
	{
		static CGenericSyncObject guard_;
		return guard_;
	}

	typedef ::std::map<ITaskSchedulerEventSink*, ITaskSchedulerEventSink*> TTaskSchedulerSinkMap;
	TTaskSchedulerSinkMap& TaskScheduler_GetSinkMap(void)
	{
		static TTaskSchedulerSinkMap map_;
		return map_;
	}
}}}

#define SAFE_LOCK_EVT()  SAFE_LOCK(details::TaskScheduler_GetEvtSafeGuard());

using namespace shared::tasks::details;
/////////////////////////////////////////////////////////////////////////////

CTaskSchedulerEvents::CTaskSchedulerEvents(void)
{
}

CTaskSchedulerEvents::~CTaskSchedulerEvents(void)
{
}

/////////////////////////////////////////////////////////////////////////////

VOID      CTaskSchedulerEvents::RaiseCustomTaskEvent(const CCustomTask _task)
{
	SAFE_LOCK_EVT();

	TTaskSchedulerSinkMap& map_ = TaskScheduler_GetSinkMap();

	for (TTaskSchedulerSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		ITaskSchedulerEventSink* pSink = it_->first;
		if (pSink)
			pSink->ITaskSchedulerEvent_OnCustomTask(_task);
	}
}

VOID      CTaskSchedulerEvents::RaiseErrorEvent(const CSysError _err)
{
	SAFE_LOCK_EVT();

	TTaskSchedulerSinkMap& map_ = TaskScheduler_GetSinkMap();

	for (TTaskSchedulerSinkMap::iterator it_ = map_.begin(); it_ != map_.end(); ++it_)
	{
		ITaskSchedulerEventSink* pSink = it_->first;
		if (pSink)
			pSink->ITaskSchedulerEvent_OnError(_err);
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CTaskSchedulerEvents::Subscribe(ITaskSchedulerEventSink* pIface)
{
	if (!pIface)
		return E_INVALIDARG;

	SAFE_LOCK_EVT();

	TTaskSchedulerSinkMap& map_ = TaskScheduler_GetSinkMap();

	try
	{
		map_[pIface] = pIface;
	}
	catch(::std::bad_alloc&)
	{
		return E_OUTOFMEMORY;
	}
	return S_OK;
}

HRESULT   CTaskSchedulerEvents::Unsubscribe(ITaskSchedulerEventSink* pIface)
{
	if (!pIface)
		return E_INVALIDARG;

	SAFE_LOCK_EVT();

	TTaskSchedulerSinkMap& map_ = TaskScheduler_GetSinkMap();

	TTaskSchedulerSinkMap::iterator it_ = map_.find(pIface);

	if (it_ == map_.end())
		return TYPE_E_ELEMENTNOTFOUND;
	try
	{
		map_.erase(it_);
	}
	catch(...)
	{
		return E_OUTOFMEMORY;
	}
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CTaskScheduler::CTaskScheduler(void) : m_state(CThreadState::eStopped)
{
	m_error.Source(_T("CTaskScheduler"));
}

CTaskScheduler::~CTaskScheduler(void)
{
}

/////////////////////////////////////////////////////////////////////////////

CSysError     CTaskScheduler::Error(void)const
{
	CSysError err_obj;
	{
		SAFE_LOCK(m_sync_obj);
		err_obj = m_error;
	}
	return err_obj;
}

bool          CTaskScheduler::IsRunning(void)const
{
	bool bRunning = false;
	{
		SAFE_LOCK(m_sync_obj);
		bRunning = !!(CThreadState::eWorking & m_state);
	}
	return bRunning;
}

HRESULT       CTaskScheduler::Start(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Scheduler is already running")
			);
		return m_error;
	}

	m_crt.IsStopped(false);

	if (!CThreadPool::QueueUserWorkItem(&CTaskScheduler::SchedulerWorkerThread, this))
		m_error = ::GetLastError();
	else if (m_error)
		m_error.Clear();

	if (m_error)
		m_state = CThreadState::eError;
	else
		m_state = CThreadState::eWorking;

	return m_error;
}

DWORD         CTaskScheduler::State(void)const
{
	DWORD state_ = 0;
	{
		SAFE_LOCK(m_sync_obj);
		state_ = m_state;
	}
	return state_;
}

HRESULT       CTaskScheduler::Stop (void)
{
	m_error.Module(_T(__FUNCTION__));

	if (!this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Scheduler is already stopped")
			);
		return m_error;
	}
	m_crt.IsStopped(true);

	const DWORD dwResult = ::WaitForSingleObject(m_crt.EventObject(), INFINITE);

	if (dwResult != WAIT_OBJECT_0)
		m_error = ::GetLastError();
	else if (m_error)
		m_error.Clear();

	if (m_error)
	{
		SAFE_LOCK(m_sync_obj);
		m_state = CThreadState::eError;
	}
	else
	{
		SAFE_LOCK(m_sync_obj);
		m_state = CThreadState::eStopped;
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CTaskSchedulerEvents& CTaskScheduler::Events(void)
{
	return m_events;
}

/////////////////////////////////////////////////////////////////////////////

VOID          CTaskScheduler::SchedulerWorkerThread(void)
{
	CCoInitializer com_core(false);

	CGenericWaitObject wait_(10, 1000 * 60); // checks once per minute

	while (!m_crt.IsStopped())
	{
		wait_.Wait();
		if (wait_.IsElapsed())
			wait_.Reset();
		else
			continue;
		CCustomTaskMgr mgr_;
		HRESULT hr_ = mgr_.Initialize();
		if (FAILED(hr_))
		{
			CSysError err_ = mgr_.Error();
			m_events.RaiseErrorEvent(err_);
			break;
		}

		SYSTEMTIME now_ = {0};
		::GetLocalTime(&now_);

		const TCustomTasks& tasks_ = mgr_.TaskList();
		for (size_t i_ = 0; i_ < tasks_.size(); i_++)
		{
			const CCustomTask& task_ = tasks_[i_];
			if (!task_.Active())
				continue;
			const CTaskPeriod& period_ = task_.Period();
			// (1) checks a day
			switch (period_.Identifier())
			{
			case CTaskPeriod::eWeekly:
				{
					const CTaskDay& day_ = task_.Day();
					if (day_.Identifier() != now_.wDayOfWeek)
						continue;
				} break;
			case CTaskPeriod::eWorkdays:
				{
					const bool bWeekends = (now_.wDayOfWeek < 1 && now_.wDayOfWeek > 5);
					if (bWeekends)
						continue;
				} break;
			case CTaskPeriod::eDaily:
			case CTaskPeriod::eHourly:
				{
				} break;
			default:
				continue;
			}
			// (2) checks a time
			switch (period_.Identifier())
			{
			case CTaskPeriod::eWeekly:
			case CTaskPeriod::eWorkdays:
			case CTaskPeriod::eDaily:
			case CTaskPeriod::eHourly:
				{
					const SYSTEMTIME ttm_ = task_.Time();
					const LONG marker_0 = now_.wHour * 60 + now_.wMinute;
					const LONG marker_1 = ttm_.wHour * 60 + ttm_.wMinute;

					if (marker_0 < marker_1)
						continue;
					if (CTaskPeriod::eHourly == period_.Identifier())
					{
						if (now_.wMinute - ttm_.wMinute > 1)
							continue;
					}
					else
					{
						if (marker_0 - marker_1 > 1)
							continue;
					}
				} break;
			default:
				continue;
			}

			m_events.RaiseCustomTaskEvent(task_);
		}
	}
	::SetEvent(m_crt.EventObject());
}