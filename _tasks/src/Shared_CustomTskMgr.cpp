/*
	Created by Tech_dog (VToropov) on 24-Jan-2016 at 12:24:28pm, GMT+7, Phuket, Rawai, Sunday;
	This is Shared Task Manager class implementation file.
*/
#include "StdAfx.h"
#include "Shared_CustomTskMgr.h"

using namespace shared::tasks;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace tasks { namespace details
{
	class CCustomTaskMgr_Registry
	{
	private:
		CRegistryEnumerator m_enum;
	public:
		CCustomTaskMgr_Registry(void) : m_enum(HKEY_CURRENT_USER, true){}
	public:
		HRESULT     Load(TCustomTasks& _tasks)
		{
			const TRegistryKeyEnum keys_ = m_enum.Enumerate(
						this->_TaskMgrFolderPath()
					);
			HRESULT hr_ = m_enum.Error();
			if (FAILED(hr_))
				return hr_;

			const size_t count_ = keys_.size();
			for ( size_t i_ = 0; i_ < count_; i_++)
			{
				const CAtlString& cs_key = keys_[i_];
				CCustomTaskPersistent pers_;
				hr_ = pers_.Load(cs_key);
				if (FAILED(hr_))
					break;
				try
				{
					_tasks.push_back(pers_);
				}
				catch(::std::bad_alloc&)
				{
					hr_ = E_OUTOFMEMORY; break;
				}
			}

			return  hr_;
		}

		HRESULT     Save(const TCustomTasks& _tasks)
		{
			HRESULT hr_ = S_OK;
			const size_t count_ = _tasks.size();
			for ( size_t i_ = 0; i_ < count_; i_++)
			{
				CCustomTaskPersistent pers_ = _tasks[i_];
				hr_ = pers_.Save();
				if (FAILED(hr_))
					break;
			}
			return  hr_;
		}
	private:
		CAtlString _TaskMgrFolderPath(void) const{return  CAtlString(_T("Settings\\Schedule")); }
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CCustomTaskMgr::CCustomTaskMgr(void)
{
	m_error.Source(_T("CCustomTaskMgr"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CCustomTaskMgr::Add(const CCustomTask& _task)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (this->Has(_task.Identifier()))
	{
		m_error.SetState(
				(DWORD)ERROR_OBJECT_ALREADY_EXISTS,
				_T("Task with such identifier already exists")
			);
		return m_error;
	}

	try
	{
		CCustomTaskPersistent pers_ = _task;
		m_error = pers_.Save();
		if (!m_error)
			m_tasks.push_back(_task);
	}
	catch(::std::bad_alloc&)
	{
		m_error = E_OUTOFMEMORY;
	}

	return m_error;
}

HRESULT       CCustomTaskMgr::Delete(const CAtlString& _uid)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!this->Has(_uid))
	{
		m_error.SetState(
				TYPE_E_ELEMENTNOTFOUND,
				_T("Task with such identifier cannot be found")
			);
		return m_error;
	}
	CCustomTaskPersistent pers_;
	HRESULT hr_ = pers_.Delete(_uid);
	if (FAILED(hr_))
		m_error = pers_.Error();
	else
	{
		for (TCustomTasks::iterator it_ = m_tasks.begin(); it_ != m_tasks.end(); ++it_)
		{
			if (0 == (*it_).Identifier().CompareNoCase(_uid))
			{
				m_tasks.erase(it_);
				break;
			}
		}
	}

	return m_error;
}

TErrorRef     CCustomTaskMgr::Error(void)const
{
	return m_error;
}

bool          CCustomTaskMgr::Has(const CAtlString& _uid)const
{
	if (_uid.IsEmpty())
		return false;
	for (size_t i_ = 0; i_ < m_tasks.size(); i_++)
		if (0 == _uid.CompareNoCase(m_tasks[i_].Identifier()))
			return true;

	return false;
}

HRESULT       CCustomTaskMgr::Initialize(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (!m_tasks.empty())m_tasks.clear();

	details::CCustomTaskMgr_Registry reg_;
	m_error = reg_.Load(m_tasks);

	return m_error;
}

const
TCustomTasks& CCustomTaskMgr::TaskList(void)const
{
	return m_tasks;
}

HRESULT       CCustomTaskMgr::Update(const CCustomTask& _src, const INT _target)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (-1 < _target && _target < static_cast<INT>(m_tasks.size()))
	{
		CCustomTask& target_ = m_tasks[_target];
		const CAtlString cs_uid = target_.Identifier();

		CCustomTaskPersistent pers_ = _src;
		pers_.Identifier(cs_uid);

		HRESULT hr_ = pers_.Save();
		if (FAILED(hr_))
			m_error = pers_.Error();
		else
			target_ = pers_;
	}
	else
		m_error = DISP_E_BADINDEX;

	return m_error;
}