/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 5:23:22pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Task Scheduler Data Model class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_CustomTskDefs.h"

using namespace shared::tasks;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"
#include "Shared_DateTime.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

CTaskPeriod::CTaskPeriod(const bool bValid) : m_valid(bValid), m_id(CTaskPeriod::eHourly)
{
}

CTaskPeriod::CTaskPeriod(const CTaskPeriod::_e _id, LPCTSTR pszTitle) : m_valid(true), m_id(_id), m_title(pszTitle)
{
}

/////////////////////////////////////////////////////////////////////////////

CTaskPeriod::_e CTaskPeriod::Identifier(void)const
{
	return m_id;
}

VOID            CTaskPeriod::Identifier(const CTaskPeriod::_e _id)
{
	m_id = _id;
}

bool            CTaskPeriod::IsValid(void)const
{
	return m_valid;
}

CAtlString      CTaskPeriod::Title(void)const
{
	return m_title;
}

VOID            CTaskPeriod::Title(LPCTSTR pszTitle)
{
	m_title = pszTitle;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace tasks { namespace details
{
	TTaskPeriodRef  CTaskPeriod_InvalidRef(void)
	{
		static CTaskPeriod period_(false);
		return period_;
	}

	TTaskDayRef     CTaskDay_InvalidRef(void)
	{
		static CTaskDay day_(false);
		return day_;
	}
}}}


CTaskPeriodEnum::TTaskPeriods CTaskPeriodEnum::m_periods;
/////////////////////////////////////////////////////////////////////////////

CTaskPeriodEnum::CTaskPeriodEnum(void)
{
	if (m_periods.empty())
	{
		try
		{
			m_periods.push_back(CTaskPeriod(CTaskPeriod::eHourly  , _T("Hourly"  )));
			m_periods.push_back(CTaskPeriod(CTaskPeriod::eDaily   , _T("Daily"   )));
			m_periods.push_back(CTaskPeriod(CTaskPeriod::eWorkdays, _T("Workdays")));
			m_periods.push_back(CTaskPeriod(CTaskPeriod::eWeekly  , _T("Weekly"  )));
		}
		catch(::std::bad_alloc&){}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT             CTaskPeriodEnum::Count(void)const
{
	return static_cast<INT>(m_periods.size());
}

TTaskPeriodRef  CTaskPeriodEnum::Default(void)const
{
	if (this->Count() > 0)
		return m_periods[this->Count() - 1];
	else
		return details::CTaskPeriod_InvalidRef();
}

TTaskPeriodRef  CTaskPeriodEnum::Item (const INT nIndex)const
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_periods[nIndex];
	else
		return details::CTaskPeriod_InvalidRef();
}

/////////////////////////////////////////////////////////////////////////////

CTaskDay::CTaskDay(const bool bValid) : m_valid(bValid), m_id(CTaskDay::eNone)
{
}

CTaskDay::CTaskDay(const CTaskDay::_e _id, LPCTSTR pszName) : m_valid(true), m_id(_id), m_name(pszName)
{
}

/////////////////////////////////////////////////////////////////////////////

CTaskDay::_e    CTaskDay::Identifier(void)const
{
	return m_id;
}

VOID            CTaskDay::Identifier(const CTaskDay::_e _id)
{
	m_id = _id;
}

bool            CTaskDay::IsValid(void)const
{
	return m_valid;
}

bool            CTaskDay::IsWorking(void)const
{
	return (CTaskDay::eSunday < m_id && m_id < CTaskDay::eSaturday);
}

CAtlString      CTaskDay::Name(void)const
{
	return m_name;
}

VOID            CTaskDay::Name(LPCTSTR _name)
{
	m_name = _name;
}

/////////////////////////////////////////////////////////////////////////////
CTaskDayEnum::TTaskDays CTaskDayEnum::m_days;
/////////////////////////////////////////////////////////////////////////////

CTaskDayEnum::CTaskDayEnum(void)
{
	if (m_days.empty())
	{
		try
		{
			m_days.push_back(CTaskDay(CTaskDay::eSunday   , _T("Sunday"   )));
			m_days.push_back(CTaskDay(CTaskDay::eMonday   , _T("Monday"   )));
			m_days.push_back(CTaskDay(CTaskDay::eTuesday  , _T("Tuesday"  )));
			m_days.push_back(CTaskDay(CTaskDay::eWednesday, _T("Wednesday")));
			m_days.push_back(CTaskDay(CTaskDay::eThursday , _T("Thursday" )));
			m_days.push_back(CTaskDay(CTaskDay::eFriday   , _T("Friday"   )));
			m_days.push_back(CTaskDay(CTaskDay::eSaturday , _T("Saturday" )));
		}
		catch(::std::bad_alloc&){}
	}
}

/////////////////////////////////////////////////////////////////////////////

INT         CTaskDayEnum::Count(void)const
{
	return static_cast<INT>(m_days.size());
}

TTaskDayRef CTaskDayEnum::Item (const INT nIndex)const
{
	if (-1 < nIndex && nIndex < this->Count())
		return m_days[nIndex];
	else
		return details::CTaskDay_InvalidRef();
}

/////////////////////////////////////////////////////////////////////////////

CCustomTask::CCustomTask(void) : m_active(false)
{
	m_error.Source(_T("CCustomTask"));

	GUID guid_ = {0};
	m_error = ::CoCreateGuid(&guid_);
	if (!m_error)
	{
		TCHAR buffer_[_MAX_PATH] = {0};
		::StringFromGUID2(guid_, buffer_, _countof(buffer_));
		m_uid = buffer_;
	}
}

CCustomTask::CCustomTask(LPCTSTR _uid) : m_active(false), m_uid(_uid)
{
	m_error.Source(_T("CCustomTask"));
}

/////////////////////////////////////////////////////////////////////////////

bool         CCustomTask::Active(void)const
{
	return m_active;
}

VOID         CCustomTask::Active(const bool _active)
{
	m_active = _active;
}

CAtlString   CCustomTask::ActiveAsText(void)const
{
	CAtlString cs_active;
	if (m_active)
		cs_active = _T("Active");
	return cs_active;
}

const
CTaskDay&    CCustomTask::Day(void)const
{
	return m_day;
}

CTaskDay&    CCustomTask::Day(void)
{
	return m_day;
}

CAtlString   CCustomTask::Description(void)const
{
	return m_desc;
}

VOID         CCustomTask::Description(LPCTSTR pszDesc)
{
	m_desc = pszDesc;
}

CSysError&   CCustomTask::Error(void)
{
	return m_error;
}

TErrorRef    CCustomTask::Error(void)const
{
	return m_error;
}

CAtlString   CCustomTask::Identifier(void)const
{
	return m_uid;
}

VOID         CCustomTask::Identifier(LPCTSTR _uid)
{
	m_uid = _uid;
}

CAtlString   CCustomTask::Path(void)const
{
	return m_path;
}

VOID         CCustomTask::Path(LPCTSTR pszPath)
{
	m_path = pszPath;
}

const
CTaskPeriod& CCustomTask::Period(void)const
{
	return m_period;
}

CTaskPeriod& CCustomTask::Period(void)
{
	return m_period;
}

SYSTEMTIME   CCustomTask::Time(void)const
{
	return m_time;
}

VOID         CCustomTask::Time(const SYSTEMTIME& _time)
{
	m_time = _time;
}

time_t       CCustomTask::Timestamp(void)const
{
	time_t tm_ = CDateTime::SystemTimeToUnix(m_time);
	return tm_;
}

CAtlString   CCustomTask::TimestampAsText(void)const
{
	CAtlString cs_time;
	cs_time.Format(
			_T("%ld"),
			this->Timestamp()
		);
	return cs_time;
}

bool         CCustomTask::Validate(void)const
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	if (m_uid.IsEmpty())
		m_error.SetState(
				E_UNEXPECTED,
				_T("Internal error")
			);
	else if (m_desc.IsEmpty())
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA,
				_T("Task description cannot be empty")
			);
	else if(CTaskPeriod::eWeekly == m_period.Identifier() && CTaskDay::eNone == m_day.Identifier())
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA,
				_T("Task day must be selected for the specified period")
			);
	else if (m_path.IsEmpty())
		m_error.SetState(
				(DWORD)ERROR_INVALID_DATA,
				_T("Task path cannot be empty")
			);

	return !m_error;
}

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace tasks { namespace details
{
	class CCustomTask_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
		CAtlString       m_uid;
	public:
		CCustomTask_Registry(LPCTSTR _uuid) : m_uid(_uuid), m_stg(HKEY_CURRENT_USER, true){}
	public:
		HRESULT     Delete(void)
		{
			HRESULT hr_ = m_stg.Remove(this->_TaskFolderPath());
			return  hr_;
		}

		HRESULT     Load(CCustomTask& _task)
		{
			HRESULT hr_ = S_OK;

			CAtlString cs_key = this->_TaskFolderPath();
			CAtlString cs_value;

			hr_ = m_stg.Load(
					cs_key,
					this->_NamedValueTaskDesc(),
					cs_value
				);
			if (S_OK == hr_)
				_task.Description(cs_value);

			LONG l_value = 0;

			hr_ = m_stg.Load(
					cs_key,
					this->_NamedValueTaskPeriod(),
					l_value
				);
			if (S_OK == hr_)
				_task.Period().Identifier(static_cast<CTaskPeriod::_e>(l_value));

			hr_ = m_stg.Load(
					cs_key,
					this->_NamedValueTaskDay(),
					l_value
				);
			if (S_OK == hr_)
				_task.Day().Identifier(static_cast<CTaskDay::_e>(l_value));

			SYSTEMTIME st_ = {0};
			hr_ = m_stg.Load(
					cs_key,
					this->_NamedValueTaskTime(),
					st_
				);
			if (S_OK == hr_)
				_task.Time(st_);

			hr_ = m_stg.Load(
					cs_key,
					this->_NamedValueTaskActive(),
					l_value
				);
			if (S_OK == hr_)
				_task.Active(!!l_value);

			hr_ = m_stg.Load(
					cs_key,
					this->_NamedValueTaskPath(),
					cs_value
				);
			if (S_OK == hr_)
				_task.Path(cs_value);

			return  hr_;
		}

		HRESULT     Save(const CCustomTask& _task)
		{
			HRESULT hr_ = S_OK;

			CAtlString cs_key = this->_TaskFolderPath();

			hr_ = m_stg.Save(
					cs_key,
					this->_NamedValueTaskDesc(),
					_task.Description()
				);

			hr_ = m_stg.Save(
					cs_key,
					this->_NamedValueTaskPeriod(),
					static_cast<LONG>(_task.Period().Identifier())
				);

			hr_ = m_stg.Save(
					cs_key,
					this->_NamedValueTaskDay(),
					static_cast<LONG>(_task.Day().Identifier())
				);

			hr_ = m_stg.Save(
					cs_key,
					this->_NamedValueTaskTime(),
					_task.Time()
				);

			hr_ = m_stg.Save(
					cs_key,
					this->_NamedValueTaskActive(),
					static_cast<LONG>(_task.Active())
				);

			hr_ = m_stg.Save(
					cs_key,
					this->_NamedValueTaskPath(),
					_task.Path()
				);

			return  hr_;
		}

	private:
		CAtlString _TaskFolderPath(void) const
		{
			CAtlString cs_path;
			cs_path.Format(
					_T("Settings\\Schedule\\%s"),
					m_uid
				);
			return cs_path;
		}
		CAtlString _NamedValueTaskActive(void)  const{ return CAtlString(_T("TaskActive"));         }
		CAtlString _NamedValueTaskDay(void)     const{ return CAtlString(_T("TaskDay"));            }
		CAtlString _NamedValueTaskDesc(void)    const{ return CAtlString(_T("TaskDescription"));    }
		CAtlString _NamedValueTaskPath(void)    const{ return CAtlString(_T("TaskPath"));           }
		CAtlString _NamedValueTaskPeriod(void)  const{ return CAtlString(_T("TaskPeriod"));         }
		CAtlString _NamedValueTaskTime(void)    const{ return CAtlString(_T("TaskTime"));           }
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CCustomTaskPersistent::CCustomTaskPersistent(void) : TBase(NULL)
{
	m_error.Source(_T("CCustomTaskPersistent"));
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CCustomTaskPersistent::Delete(LPCTSTR _uid)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	m_uid = _uid;

	details::CCustomTask_Registry reg_(m_uid);
	m_error = reg_.Delete();

	return m_error;
}

HRESULT         CCustomTaskPersistent::Load(LPCTSTR _uid)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	m_uid = _uid;

	details::CCustomTask_Registry reg_(m_uid);
	HRESULT hr_ = reg_.Load(*this);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

HRESULT         CCustomTaskPersistent::Save(void)
{
	m_error.Module(_T(__FUNCTION__));
	m_error = S_OK;

	details::CCustomTask_Registry reg_(m_uid);
	HRESULT hr_ = reg_.Save(*this);
	if (FAILED(hr_))
		m_error = hr_;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CCustomTaskPersistent::CCustomTaskPersistent(const CCustomTask& _task)
{
	*this = _task;
}

CCustomTaskPersistent& CCustomTaskPersistent::operator= (const CCustomTask& _task)
{
	((CCustomTask&)*this) = _task;
	return *this;
}