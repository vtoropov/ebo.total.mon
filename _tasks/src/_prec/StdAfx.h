#ifndef _SHAREDSCHEDULERSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
#define _SHAREDSCHEDULERSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 3:45:20pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Task Scheduler Library precompiled headers declaration file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0501  // this is for use Windows XP (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0603  // this is for use IE 6 SP3 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <atlsafe.h>
#include <comdef.h>
#include <comutil.h>
#include <vector>
#include <map>

#include <math.h>
#include <time.h>

#endif/*_SHAREDSCHEDULERSTDAFX_H_09A60A75_94F1_47e3_AA71_97B971F6BF44_INCLUDED*/