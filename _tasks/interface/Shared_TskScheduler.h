#ifndef _SHAREDTSKSCHEDULER_H_807FC61E_5736_4487_9C3A_733B643B5A23_INCLUDED
#define _SHAREDTSKSCHEDULER_H_807FC61E_5736_4487_9C3A_733B643B5A23_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Jan-2016 at 4:15:54am, GMT+7, Phuket, Rawai, Tuesday;
	This is Shared Library Task Scheduler class declaration file.
*/
#include "Shared_CustomTskMgr.h"

#include "Shared_SystemError.h"
#include "Shared_GenericSyncObject.h"
#include "Shared_SystemThreadPool.h"

namespace shared { namespace tasks
{
	using shared::lite::common::CSysError;
	using shared::lite::runnable::CGenericSyncObject;

	using shared::lite::sys_core::CThreadCrtData;
	using shared::lite::sys_core::CThreadState;

	interface ITaskSchedulerEventSink
	{
	public:
		virtual VOID ITaskSchedulerEvent_OnCustomTask(const CCustomTask) PURE;
		virtual VOID ITaskSchedulerEvent_OnError(const CSysError) PURE;
	};

	class CTaskSchedulerEvents
	{
	public:
		CTaskSchedulerEvents(void);
		~CTaskSchedulerEvents(void);
	public:
		VOID      RaiseCustomTaskEvent(const CCustomTask);
		VOID      RaiseErrorEvent(const CSysError);
	public:
		HRESULT   Subscribe(ITaskSchedulerEventSink*);
		HRESULT   Unsubscribe(ITaskSchedulerEventSink*);
	};

	class CTaskScheduler
	{
	private:
		CThreadCrtData        m_crt;
		CGenericSyncObject    m_sync_obj;
	private:
		CTaskSchedulerEvents  m_events;
	private:
		volatile
		DWORD                 m_state;
		CSysError             m_error;
	public:
		CTaskScheduler(void);
		~CTaskScheduler(void);
	public:
		CSysError             Error(void)const;
		bool                  IsRunning(void)const;
		HRESULT               Start(void);
		DWORD                 State(void)const;
		HRESULT               Stop (void);
	public:
		CTaskSchedulerEvents& Events(void);
	private:
		VOID                  SchedulerWorkerThread(void);
	};
}}

#endif/*_SHAREDTSKSCHEDULER_H_807FC61E_5736_4487_9C3A_733B643B5A23_INCLUDED*/