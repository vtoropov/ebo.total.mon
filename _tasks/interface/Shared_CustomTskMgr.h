#ifndef _SHAREDCUSTOMTSKMGR_H_EA691022_03A5_41a6_975D_F2D8EE2C733D_INCLUDED
#define _SHAREDCUSTOMTSKMGR_H_EA691022_03A5_41a6_975D_F2D8EE2C733D_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Jan-2016 at 9:13:33am, GMT+7, Phuket, Rawai, Sunday;
	This is Shared Task Manager class declaration file.
*/
#include "Shared_CustomTskDefs.h"

namespace shared { namespace tasks
{
	using shared::lite::common::CSysError;

	class CCustomTaskMgr
	{
	private:
		TCustomTasks       m_tasks;
		CSysError          m_error;
	public:
		CCustomTaskMgr(void);
	public:
		HRESULT            Add(const CCustomTask&);
		HRESULT            Delete(const CAtlString& _uid);
		TErrorRef          Error(void)const;
		bool               Has(const CAtlString& _uid)const;
		HRESULT            Initialize(void);
		const
		TCustomTasks&      TaskList(void)const;
		HRESULT            Update(const CCustomTask& _src, const INT _target);
	};
}}

#endif/*_SHAREDCUSTOMTSKMGR_H_EA691022_03A5_41a6_975D_F2D8EE2C733D_INCLUDED*/