#ifndef _SHAREDCUSTOMTSKDEFS_H_9293C033_D304_4c42_9644_FBDF753EC64B_INCLUDED
#define _SHAREDCUSTOMTSKDEFS_H_9293C033_D304_4c42_9644_FBDF753EC64B_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 4:34:48pm, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Task Scheduler Data Model class(es) declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace tasks
{
	class CTaskPeriod
	{
	public:
		enum _e{
			eHourly   = 0,
			eDaily    = 1,
			eWorkdays = 2,
			eWeekly   = 3,
		};
	private:
		_e              m_id;
		CAtlString      m_title;
		bool            m_valid;
	public:
		CTaskPeriod(const bool bValid = true);
		CTaskPeriod(const CTaskPeriod::_e, LPCTSTR pszTitle);
	public:
		CTaskPeriod::_e Identifier(void)const;
		VOID            Identifier(const CTaskPeriod::_e);
		bool            IsValid(void)const;
		CAtlString      Title(void)const;
		VOID            Title(LPCTSTR);
	};

	typedef const CTaskPeriod& TTaskPeriodRef;

	class CTaskPeriodEnum
	{
		typedef ::std::vector<CTaskPeriod> TTaskPeriods;
	private:
		static
		TTaskPeriods    m_periods;
	public:
		CTaskPeriodEnum(void);
	public:
		INT             Count(void)const;
		TTaskPeriodRef  Default(void)const;
		TTaskPeriodRef  Item (const INT nIndex)const;
	};

	class CTaskDay
	{
	public:
		enum _e{
			eNone       = -1,
			eSunday     =  0, eMonday, eTuesday, eWednesday, eThursday, eFriday, eSaturday
		};
	private:
		_e              m_id;
		CAtlString      m_name;
		bool            m_valid;
	public:
		CTaskDay(const bool bValid = true);
		CTaskDay(const CTaskDay::_e _id, LPCTSTR pszName);
	public:
		CTaskDay::_e    Identifier(void)const;
		VOID            Identifier(const CTaskDay::_e);
		bool            IsValid(void)const;
		bool            IsWorking(void)const;
		CAtlString      Name(void)const;
		VOID            Name(LPCTSTR);
	};

	typedef const CTaskDay& TTaskDayRef;

	class CTaskDayEnum
	{
		typedef ::std::vector<CTaskDay> TTaskDays;
	private:
		static
		TTaskDays        m_days;
	public:
		CTaskDayEnum(void);
	public:
		INT             Count(void)const;
		TTaskDayRef     Item (const INT nIndex)const;
	};

	using shared::lite::common::CSysError;

	class CCustomTask
	{
	protected:
		mutable
		CSysError       m_error;
		CAtlString      m_uid;    // auto-generated
		CAtlString      m_desc;
		CTaskPeriod     m_period;
		CTaskDay        m_day;
		SYSTEMTIME      m_time;
		bool            m_active;
		CAtlString      m_path;
	public:
		CCustomTask(void);
		CCustomTask(LPCTSTR _uid);
	public:
		bool            Active(void)const;
		VOID            Active(const bool);
		CAtlString      ActiveAsText(void)const;
		const
		CTaskDay&       Day(void)const;
		CTaskDay&       Day(void);
		CAtlString      Description(void)const;
		VOID            Description(LPCTSTR);
		CSysError&      Error(void);
		TErrorRef       Error(void)const;
		CAtlString      Identifier(void)const;
		VOID            Identifier(LPCTSTR);
		CAtlString      Path(void)const;
		VOID            Path(LPCTSTR);
		const
		CTaskPeriod&    Period(void)const;
		CTaskPeriod&    Period(void);
		SYSTEMTIME      Time(void)const;
		VOID            Time(const SYSTEMTIME&);
		time_t          Timestamp(void)const;
		CAtlString      TimestampAsText(void)const;
		bool            Validate(void)const;
	};

	typedef ::std::vector<CCustomTask>   TCustomTasks;

	class CCustomTaskPersistent : public CCustomTask
	{
		typedef CCustomTask TBase;
	public:
		CCustomTaskPersistent(void);
	public:
		HRESULT         Delete(LPCTSTR _uid);
		HRESULT         Load(LPCTSTR _uid);
		HRESULT         Save(void);
	public:
		CCustomTaskPersistent(const CCustomTask&);
		CCustomTaskPersistent& operator= (const CCustomTask&);
	};
}}

typedef const shared::tasks::CCustomTask&  TCustomTaskRef;

#endif/*_SHAREDCUSTOMTSKDEFS_H_9293C033_D304_4c42_9644_FBDF753EC64B_INCLUDED*/