/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 10:52:48am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Operating System Core class(es) implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemCore.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::sys_core;

////////////////////////////////////////////////////////////////////////////

CCoInitializer::CCoInitializer(const bool bMultiThreaded)
{
	m_error.Reset();
	if (bMultiThreaded)
	{
		m_error = ::CoInitializeEx(0, COINIT_MULTITHREADED);
	}
	else
	{
		m_error = ::CoInitialize(NULL);
	}
}

CCoInitializer::~CCoInitializer(void)
{
	if (IsSuccess())
	{
		m_error.Reset();
		::CoUninitialize();
	}
}

////////////////////////////////////////////////////////////////////////////

TErrorRef  CCoInitializer::Error(void) const
{
	return m_error;
}

bool       CCoInitializer::IsSuccess(void) const
{
	return (!m_error);
}