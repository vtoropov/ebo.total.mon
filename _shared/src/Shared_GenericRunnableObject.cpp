/*
	Created by Tech_dog (VToropov) on 7-Dec-2015 at 9:12:56am, GMT+7, Phuket, Rawai, Monday;
	This is Shared Lite Library Generic Asynchronous Event Runnable Object class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericRunnableObject.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::runnable;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace runnable { namespace details
{
	HRESULT GenericRunnableObject_ForceToTerminate(HANDLE event_, HANDLE thread_)
	{
		HRESULT hr_ = S_OK;
		const DWORD dwRet = ::WaitForSingleObject(event_, 10);
		if (WAIT_OBJECT_0!= dwRet)
		{
			if (thread_)
			{
				::TerminateThread(thread_, DWORD(-1));
			}
		}
		return  hr_;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CGenericRunnableObject::CGenericRunnableObject(TRunnableFunc func, IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
	m_async_evt(sink_ref, v_evt_id),
	m_hThread(NULL),
	m_hEvent(NULL),
	m_bStopped(true),
	m_function(func)
{
	m_hEvent = ::CreateEvent(0, TRUE, TRUE, 0);
	if (INVALID_HANDLE_VALUE == m_hEvent || NULL == m_hEvent)
	{
		ATLASSERT(FALSE); m_hEvent = NULL;
	}
	m_async_evt.Create();
}

CGenericRunnableObject::~CGenericRunnableObject(void)
{
	m_async_evt.Destroy();
	if (m_hEvent)
	{
		::CloseHandle(m_hEvent); 
		m_hEvent = NULL;
	}
}

////////////////////////////////////////////////////////////////////////////

CGenericEvent&   CGenericRunnableObject::Event(void)
{
	return m_async_evt;
}

bool             CGenericRunnableObject::IsStopped(void) const
{
	return m_bStopped;
}

void             CGenericRunnableObject::MarkCompleted(void)
{
	if (m_hEvent)
	{
		::SetEvent(m_hEvent);
	}
}

HRESULT          CGenericRunnableObject::Start(void)
{
	if (NULL == m_hEvent) return OLE_E_BLANK;
	if (!IsStopped()) return S_OK;
	if (m_hThread)
	{
		::CloseHandle(m_hThread);
		m_hThread = NULL;
	}
	m_hThread  = (HANDLE)::_beginthreadex(NULL, NULL, m_function, this, CREATE_SUSPENDED, 0);
	if (INVALID_HANDLE_VALUE == m_hThread || NULL == m_hThread)
	{
		return E_OUTOFMEMORY;
	}
	m_bStopped = false;
	::ResetEvent(m_hEvent);
	::SetThreadPriority(m_hThread, BELOW_NORMAL_PRIORITY_CLASS);
	::ResumeThread(m_hThread);
	HRESULT hr__ = S_OK;
	return  hr__;
}

HRESULT          CGenericRunnableObject::Stop(const bool bForced)
{
	if (IsStopped()) return S_OK;
	m_bStopped = true;
	if (NULL == m_hThread) return S_OK;
	if (bForced)
	{
		details::GenericRunnableObject_ForceToTerminate(m_hEvent, m_hThread);
	}
	else
	{
		INT cnt_ = 0;
		while (WAIT_OBJECT_0 != ::WaitForSingleObject(m_hEvent, 200))
		{
			::Sleep(100);
			cnt_ ++;
			if (cnt_ > 10) // anti-blocker, actually should never happen
			{
				return details::GenericRunnableObject_ForceToTerminate(m_hEvent, m_hThread);
			}
		}
	}
	if (m_hThread)
	{
		::CloseHandle(m_hThread);
		m_hThread = NULL;
	}
	HRESULT hr__ = S_OK;
	return  hr__;
}

HRESULT          CGenericRunnableObject::Wait(const DWORD dPeriod)
{
	const DWORD dResult = ::WaitForSingleObject(m_hEvent, (!dPeriod ? INFINITE : dPeriod));
	return (WAIT_OBJECT_0 == dResult ? S_OK : S_FALSE);
}

////////////////////////////////////////////////////////////////////////////

HANDLE           CGenericRunnableObject::EventHandle(void)const
{
	return m_hEvent;
}