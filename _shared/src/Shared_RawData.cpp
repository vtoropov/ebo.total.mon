/*
	Created by Tech_dog (VToropov) on 29-Nov-2014 at 0:42:03am, GMT+3, Taganrog, Saturday;
	This is Shared Lite Library raw data class implementation file.
*/
#include "StdAfx.h"
#include "Shared_RawData.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::common;
using namespace shared::lite::data;

////////////////////////////////////////////////////////////////////////////

CRawData::CRawData(void):
	m_pData(NULL),
	m_dwSize(0)
{
	m_error.Reset();
}

CRawData::CRawData(const DWORD dwSize):
	m_pData(NULL),
	m_dwSize(0)
{
	this->Create(dwSize);
}

CRawData::CRawData(const PBYTE pData, const DWORD dwSize):
	m_pData(NULL),
	m_dwSize(0)
{
	this->Create(pData, dwSize);
}

CRawData::~CRawData(void)
{
	this->Reset();
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CRawData::Append(const PBYTE pData, const DWORD dwSize)
{
	if (!pData || !dwSize)
		return E_INVALIDARG;
	if (m_error)m_error.Clear();

	PBYTE pCurrentLocator = m_pData;

	if (m_pData)
	{
		const DWORD dwNewSize = m_dwSize + dwSize;
		m_pData = reinterpret_cast<PBYTE>(
					::HeapReAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, pCurrentLocator, dwNewSize)
				);
		pCurrentLocator += m_dwSize;
		m_dwSize = dwNewSize;
	}
	else
	{
		m_dwSize = dwSize;
		m_pData = reinterpret_cast<PBYTE>(
					::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize)
				);
		pCurrentLocator = m_pData;
	}
	if (!m_pData)
	{
		m_error  = ::GetLastError();
		m_dwSize = 0;
	}
	else
	{
		const errno_t err_ = ::memcpy_s(pCurrentLocator, dwSize, pData, dwSize);
		if (err_)
			m_error = E_OUTOFMEMORY;
		else if (m_error)
			m_error.Clear();
	}
	return m_error;
}

HRESULT     CRawData::CopyTo(_variant_t& v_data, const VARTYPE varType)const
{
	if (!this->IsValid())
		return (m_error = ERROR_INVALID_DATA);
	if (VT_BSTR != varType && (VT_ARRAY|VT_UI1) != varType)
		return (m_error = DISP_E_TYPEMISMATCH);

	if (VT_EMPTY != v_data.vt)v_data.Clear();

	if (VT_BSTR == varType)
	{
		v_data.Clear();
		v_data.vt = VT_BSTR;
		v_data.bstrVal = ::SysAllocStringByteLen((LPCSTR)m_pData, m_dwSize);
	}
	else
	{
		v_data.vt = VT_ARRAY|VT_UI1;
		v_data.parray = NULL;
		this->CopyTo(v_data.parray);
	}
	return m_error;
}

HRESULT     CRawData::CopyTo(SAFEARRAY*& ptr_ref) const
{
	if (!this->IsValid())
		return (m_error = ERROR_INVALID_DATA);
	if (ptr_ref)
		return (m_error = ERROR_OBJECT_ALREADY_EXISTS);

	SAFEARRAYBOUND bound[1] ={0};
	bound[0].lLbound        = 0;
	bound[0].cElements      = (ULONG)m_dwSize;

	ptr_ref = ::SafeArrayCreate(VT_UI1, 1, bound);
	if (!ptr_ref)
		return (m_error = E_OUTOFMEMORY);

	UCHAR HUGEP* pData = NULL;
	m_error = ::SafeArrayAccessData(ptr_ref, (void HUGEP**)&pData);
	if (m_error)
	{
		::SafeArrayDestroy(ptr_ref); ptr_ref = NULL;
		return m_error;
	}
	const errno_t err_no = ::memcpy_s((void*)pData, m_dwSize, (const void*)m_pData, m_dwSize);
	if (err_no)
		m_error = E_OUTOFMEMORY;
	::SafeArrayUnaccessData(ptr_ref);
	pData = NULL;

	if (m_error)
	{
		::SafeArrayDestroy(ptr_ref); ptr_ref = NULL;
	}

	return m_error;
}

HRESULT     CRawData::Create(const DWORD dwSize)
{
	if (this->IsValid())
		this->Reset();
	if (dwSize)
	{
		m_pData = reinterpret_cast<PBYTE>(
					::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize)
				);
		if (!m_pData)
			m_error = ::GetLastError();
		else
		{
			m_error.Clear();
			m_dwSize = dwSize;
		}
	}
	return m_error;
}

HRESULT     CRawData::Create(const PBYTE pData, const DWORD dwSize)
{
	this->Reset();
	return this->Append(pData, dwSize);
}

TErrorRef   CRawData::Error(void)const
{
	return m_error;
}

const PBYTE CRawData::GetData(void) const
{
	return m_pData;
}

PBYTE       CRawData::GetData(void)
{
	return m_pData;
}

DWORD       CRawData::GetSize(void) const
{
	return m_dwSize;
}

bool        CRawData::IsValid(void) const
{
	return (NULL != m_pData && 0 != m_dwSize);
}

HRESULT     CRawData::Reset(void)
{
	m_dwSize = 0;
	if (m_pData)
	{
		if (!::HeapFree(::GetProcessHeap(), 0, m_pData))
			m_error = ::GetLastError();
		else
			m_error = S_OK;
		m_pData = NULL;
		if (m_error)
			return m_error;
	}
	m_error.Reset();
	return S_OK;
}

CAtlString  CRawData::ToString(void)const
{
	if (!this->IsValid())
		return ::ATL::CAtlString(_T(""));

	::ATL::CAtlString cs_data(
			reinterpret_cast<LPCSTR>(this->GetData()),
			static_cast<INT>(this->GetSize())
		);
	return cs_data;
}

////////////////////////////////////////////////////////////////////////////

CRawData::operator const    PBYTE(void)const
{
	return m_pData;
}

CRawData::operator          PBYTE(void)
{
	return m_pData;
}

////////////////////////////////////////////////////////////////////////////

CRawData::CRawData(const CRawData& raw_ref)
{
	*this = raw_ref;
}

CRawData& CRawData::operator= (const CRawData& raw_ref)
{
	this->Create(raw_ref.GetData(), raw_ref.GetSize());
	return *this;
}