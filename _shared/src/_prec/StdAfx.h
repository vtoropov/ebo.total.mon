#ifndef _SHAREDLITELIBRARYPRECOMPILEDHEADERS_H_094BF254_1C76_44eb_96F9_11A826EFC206_INCLUDED
#define _SHAREDLITELIBRARYPRECOMPILEDHEADERS_H_094BF254_1C76_44eb_96F9_11A826EFC206_INCLUDED
/*
	This is Shared Lite Library Precompiled Header definition file.
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600 // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700 // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481) // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996) // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h>
#include <atlstr.h>
#include <comdef.h>
#include <new>
#include <map>
#include <set>
#include <vector>
#include <typeinfo>
#include <deque>

#include <atlsafe.h>
#include <atlconv.h>

#include <atlfile.h>

#include <atlcomtime.h>
#include <comutil.h>
#include <shellapi.h>

#include "atlapp.h"

#endif/*_SHAREDLITELIBRARYPRECOMPILEDHEADERS_H_094BF254_1C76_44eb_96F9_11A826EFC206_INCLUDED*/