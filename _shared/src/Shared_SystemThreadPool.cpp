/*
	Created by Tech_dog (VToropov) on 11-Jan-2016 at 3:37:45pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Lite library system thread pool related class(s) implementation file.
*/
#include "StdAfx.h"
#include "Shared_SystemThreadPool.h"

using namespace shared::lite::sys_core;

/////////////////////////////////////////////////////////////////////////////

CThreadCrtDataBase::CThreadCrtDataBase(void) : m_bStopped(false), m_hStopEvent(NULL)
{
}

CThreadCrtDataBase::~CThreadCrtDataBase(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HANDLE&    CThreadCrtDataBase::EventObject(void)
{
	return m_hStopEvent;
}

HRESULT    CThreadCrtDataBase::Initialize(void)
{
	if (m_hStopEvent)
		return (HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED));

	m_hStopEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	if (!m_hStopEvent)
		return (HRESULT_FROM_WIN32(::GetLastError()));
	else
		return S_OK;
}

bool       CThreadCrtDataBase::IsStopped(void)const
{
	return m_bStopped;
}

VOID       CThreadCrtDataBase::IsStopped(const bool _stop)
{
	m_bStopped = _stop;
}

HRESULT    CThreadCrtDataBase::Terminate(void)
{
	if (m_hStopEvent)
	{
		::CloseHandle(m_hStopEvent);
		m_hStopEvent = NULL;
	}
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CThreadCrtData::CThreadCrtData(void)
{
	TBase::Initialize();
}

CThreadCrtData::~CThreadCrtData(void)
{
	TBase::Terminate();
}

/////////////////////////////////////////////////////////////////////////////

CThreadBase::CThreadBase(void) : m_state(CThreadState::eStopped), m_error(m_sync_obj)
{
	m_error.Source(_T("CThreadBase"));
}

CThreadBase::~CThreadBase(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CThreadBase::Start(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Thread is already running")
			);
		return m_error;
	}

	m_crt.IsStopped(false);

	if (!CThreadPool::QueueUserWorkItem(&CThreadBase::ThreadFunction, this))
		m_error = ::GetLastError();
	else if (m_error)
		m_error = S_OK;

	if (m_error)
		m_state = CThreadState::eError;
	else
		m_state = CThreadState::eWorking;

	return m_error;
}

HRESULT    CThreadBase::Stop (void)
{
	m_error.Module(_T(__FUNCTION__));

	if (!this->IsRunning())
	{
		m_error.SetState(
				(DWORD)ERROR_INVALID_STATE,
				_T("Thread is already stopped")
			);
		return m_error;
	}
	m_crt.IsStopped(true);

	const DWORD dwResult = ::WaitForSingleObject(m_crt.EventObject(), INFINITE);

	if (dwResult != WAIT_OBJECT_0)
		m_error = ::GetLastError();
	else if (m_error)
		m_error = S_OK;

	if (m_error)
		m_state = CThreadState::eError;
	else
		m_state = CThreadState::eStopped;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CSysError  CThreadBase::Error(void)const
{
	CSysError err_obj;
	{
		SAFE_LOCK(m_sync_obj);
		err_obj = m_error;
	}
	return err_obj;
}

bool       CThreadBase::IsRunning(void)const
{
	bool bRunning = false;
	{
		SAFE_LOCK(m_sync_obj);
		bRunning = !!(CThreadState::eWorking & m_state);
	}
	return bRunning;
}

DWORD      CThreadBase::State(void)const
{
	DWORD state_ = 0;
	{
		SAFE_LOCK(m_sync_obj);
		state_ = m_state;
	}
	return state_;
}