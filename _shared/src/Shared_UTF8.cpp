/*
	Created by Tech_dog (VToropov) on 19-Feb-2015 at 5:58:02pm, GMT+3, Taganrog, Thursday;
	This is shared lite library UTF-8/Unicode conversion class implementation file. 
*/
#include "StdAfx.h"
#include "Shared_UTF8.h"
#include "Shared_RawData.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::data;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	VOID _dm() {::SetDllDirectory(_T(""));}
}}}}

////////////////////////////////////////////////////////////////////////////

CAtlString  CUTF8::ConvertMbsToWcs(const _variant_t& utf8_raw)
{
	utf8_raw;
	m_error = E_NOTIMPL;
	return CAtlString();
}

_variant_t  CUTF8::ConvertWcsToMbs(LPCTSTR pszData)
{
	CRawData utf8_raw;
	m_error = CUTF8::ConvertWcsToMbsBuffer(pszData, utf8_raw);
	if (m_error)
		return _variant_t((LPCTSTR)NULL);

	_variant_t v_data; v_data.Clear();
	m_error = utf8_raw.CopyTo(v_data, VT_BSTR);
	return v_data;
}

TErrorRef   CUTF8::Error(void) const
{
	return m_error;
}

VOID        CUTF8::Reset(void)
{
	if (m_error)m_error.Clear();
}

////////////////////////////////////////////////////////////////////////////

HRESULT     CUTF8::ConvertWcsToMbsBuffer(LPCTSTR pszData, CRawData& mbs_buffer)
{
	if (!pszData || !::_tcslen(pszData))
		return E_INVALIDARG;

	if (mbs_buffer.IsValid())
		return HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS);

	const INT n_req = ::WideCharToMultiByte(
			CP_UTF8,
			0,
			pszData,
			static_cast<INT>(::_tcslen(pszData)),
			NULL,
			0,
			NULL,
			NULL
		);
	if (!n_req)
		return HRESULT_FROM_WIN32(::GetLastError());

	HRESULT hr_ = mbs_buffer.Create(static_cast<DWORD>(n_req));
	if (S_OK != hr_)
		return  hr_;

	const INT n_len = ::WideCharToMultiByte(
			CP_UTF8,
			0,
			pszData,
			static_cast<INT>(::_tcslen(pszData)),
			reinterpret_cast<LPSTR>(mbs_buffer.GetData()),
			static_cast<INT>(mbs_buffer.GetSize()),
			NULL,
			NULL
		);
	if (!n_len)
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT     CUTF8::ConvertMbsToWcsBuffer(const BYTE* pbData, const DWORD dwCount, CAtlString& wcs_buffer)
{
	if (!pbData || dwCount < 1)
		return E_INVALIDARG;

	const INT n_req = ::MultiByteToWideChar(
			CP_UTF8,
			0,
			reinterpret_cast<LPCSTR>(pbData),
			static_cast<INT>(dwCount),
			NULL,
			0
		);
	if (!n_req)
		return HRESULT_FROM_WIN32(::GetLastError());

	HRESULT hr_ = S_OK;
	try
	{
		const INT nSize  = n_req + 1;
		TCHAR* pszBuffer = new TCHAR[nSize];
		::memset(pszBuffer, 0, nSize * sizeof(TCHAR));

		const INT n_len = ::MultiByteToWideChar(
			CP_UTF8,
			0,
			reinterpret_cast<LPCSTR>(pbData),
			static_cast<INT>(dwCount),
			pszBuffer,
			nSize
		);

		if (!n_len)
			hr_ = HRESULT_FROM_WIN32(::GetLastError());
		else
			wcs_buffer = pszBuffer;

		if (pszBuffer)
		{
			delete[] pszBuffer; pszBuffer = NULL;
		}
	}
	catch (::std::bad_alloc&)
	{
		hr_ = E_OUTOFMEMORY;
	}

	return  hr_;
}