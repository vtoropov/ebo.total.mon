/*
	Created by Tech_dog (VToropov) on 29-Apr-2015 at 2:24:00pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Registry Data Provider class implementation file.
*/
#include "StdAfx.h"
#include "Shared_Registry.h"
#include "Shared_GenericAppObject.h"
#include "Shared_DateTime.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace data { namespace details
{
	using shared::lite::common::CApplication;
	CApplication&
	        RegistryStorage_AppObject(void)
	{
		static CApplication the_app;
		return the_app;
	}

	VOID    RegistryStorage_FormatQryPath(LPCTSTR pszFolder, ::ATL::CAtlString& __in_out, const bool bAutoComplete)
	{
		if (bAutoComplete)
			__in_out.Format(_T("Software\\%s\\%s"), RegistryStorage_AppObject().GetName(), pszFolder);
		else
			__in_out.Format(_T("Software\\%s"), pszFolder);
	}

	HRESULT RegistryStorage_CheckFolderName(LPCTSTR pFolder)
	{
		if (!pFolder)
			return E_INVALIDARG;  // cannot be nothing
		if (1 > ::_tcslen(pFolder))
			return E_INVALIDARG;  // cannot be zero-length
		else
			return S_OK;
	}

	HRESULT RegistryStorage_CheckPropertyName(LPCTSTR pProperty)
	{
		if (!pProperty)
			return E_INVALIDARG;  // cannot be nothing, but can be zero-length (default)
		else
			return S_OK;
	}

	HRESULT RegistryStorage_CheckArgs(LPCTSTR pszFolder, LPCTSTR pszValueName, const bool bAutoComplete)
	{
		HRESULT hr__ = details::RegistryStorage_CheckFolderName(pszFolder);
		if (S_OK != hr__)
			return  hr__;
		hr__ = details::RegistryStorage_CheckPropertyName(pszValueName);
		if (S_OK != hr__)
			return  hr__;
		if (bAutoComplete)
		{
			hr__ = details::RegistryStorage_AppObject().GetLastResult();
			if (S_OK != hr__)
				return  hr__;
		}
		return hr__;
	}
}}}}

/////////////////////////////////////////////////////////////////////////////

CRegistryStorage::CRegistryStorage(const HKEY hRoot, const bool bAutoComplete):
	m_hRoot(hRoot), m_auto_complete(bAutoComplete)
{
}

CRegistryStorage::~CRegistryStorage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CRegistryStorage::Load(LPCTSTR pszFolder, LPCTSTR pszValueName, ::ATL::CAtlString& _out_value)
{
	if (!_out_value.IsEmpty())_out_value.Empty();

	HRESULT hr__ = details::RegistryStorage_CheckArgs(pszFolder, pszValueName, m_auto_complete);
	if (S_OK != hr__)
		return  hr__;

	::ATL::CAtlString qry__;
	details::RegistryStorage_FormatQryPath(pszFolder, qry__, m_auto_complete);

	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(m_hRoot, qry__);
	switch (nLastResult)
	{
	case ERROR_FILE_NOT_FOUND:
	case ERROR_PATH_NOT_FOUND:
		{
			return hr__;
		} break;
	}
	if (ERROR_SUCCESS != nLastResult)
		return __HRESULT_FROM_WIN32(nLastResult);

	static const ULONG n_buff_len = 4096;
	TCHAR local_buffer[n_buff_len] = {0};
	ULONG local_length = n_buff_len;
	nLastResult = reg_key.QueryStringValue(pszValueName, local_buffer, &local_length);

	switch (nLastResult)
	{
	case ERROR_FILE_NOT_FOUND:
	case ERROR_PATH_NOT_FOUND:
		{
			return hr__;
		} break;
	}

	if (ERROR_SUCCESS != nLastResult)
		return __HRESULT_FROM_WIN32(nLastResult);

	_out_value = local_buffer;
	return  hr__;
}

HRESULT      CRegistryStorage::Load(LPCTSTR pszFolder, LPCTSTR pszValueName, LONG& _out_value, const LONG _default)
{
	CAtlString cs_value;
	HRESULT hr_ = this->Load(pszFolder, pszValueName, cs_value);
	if (S_OK != hr_)
	{
		_out_value = _default;
		return  hr_;
	}
	else if (cs_value.IsEmpty())
		_out_value = _default;
	else
		_out_value = ::_tstol(cs_value);

	return  hr_;
}

HRESULT      CRegistryStorage::Load(LPCTSTR pszFolder, LPCTSTR pszValueName, SYSTEMTIME& _out_value)
{
	CAtlString cs_value;
	HRESULT hr_ = this->Load(pszFolder, pszValueName, cs_value);
	if (S_OK == hr_)
	{
		time_t tm_ = static_cast<time_t>(::_tstoi64(cs_value));
		CDateTime dt_(tm_);
		_out_value = dt_.GetCurrent();
	}
	return hr_;
}

HRESULT      CRegistryStorage::Remove(LPCTSTR pszFolder)
{
	HRESULT hr_ = details::RegistryStorage_CheckFolderName(pszFolder);
	if (FAILED(hr_))
		return hr_;

	::ATL::CAtlString qry__;
	details::RegistryStorage_FormatQryPath(pszFolder, qry__, m_auto_complete);

	LONG lResult = ::RegDeleteKey(m_hRoot, qry__);

	if (ERROR_FILE_NOT_FOUND == lResult) // registry can be clean
		lResult = ERROR_SUCCESS;

	hr_ = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
	return hr_;
}

HRESULT      CRegistryStorage::Save(LPCTSTR pszFolder, LPCTSTR pszValueName, const ::ATL::CAtlString& _in_value)
{
	HRESULT hr_ = details::RegistryStorage_CheckArgs(pszFolder, pszValueName, m_auto_complete);
	if (S_OK != hr_)
		return  hr_;
	::ATL::CAtlString qry__;
	details::RegistryStorage_FormatQryPath(pszFolder, qry__, m_auto_complete);
	::ATL::CRegKey  reg_key;
	LONG nLastResult = reg_key.Create(m_hRoot, qry__);
	if (ERROR_SUCCESS != nLastResult)
		return HRESULT_FROM_WIN32(nLastResult);
	nLastResult = reg_key.SetStringValue(pszValueName, _in_value.GetString());
	if (ERROR_SUCCESS != nLastResult)
		hr_ = HRESULT_FROM_WIN32(nLastResult);
	return  hr_;
}

HRESULT      CRegistryStorage::Save(LPCTSTR pszFolder, LPCTSTR pszValueName, const LONG _in_value)
{
	HRESULT hr_ = details::RegistryStorage_CheckArgs(pszFolder, pszValueName, m_auto_complete);
	if (S_OK != hr_)
		return  hr_;
	::ATL::CAtlString cs_value;
	cs_value.Format(
			_T("%d"),
			_in_value
		);
	hr_ = this->Save(pszFolder, pszValueName, cs_value);
	return  hr_;
}

HRESULT      CRegistryStorage::Save(LPCTSTR pszFolder, LPCTSTR pszValueName, const SYSTEMTIME& _in_value)
{
	const time_t tm_ = CDateTime::SystemTimeToUnix(_in_value);
	CAtlString cs_time;
	cs_time.Format(
			_T("%ld"),
			tm_
		);
	HRESULT hr_ = this->Save(pszFolder, pszValueName, cs_time);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CRegistryEnumerator::CRegistryEnumerator(const HKEY hRoot, const bool bAutoComplete): m_hRoot(hRoot), m_auto_complete(bAutoComplete)
{
	m_error.Source(_T("CRegistryEnumerator"));
}

CRegistryEnumerator::~CRegistryEnumerator(void)
{
}

/////////////////////////////////////////////////////////////////////////////

TRegistryKeyEnum CRegistryEnumerator::Enumerate(LPCTSTR pszFolder)
{
	m_error.Module(_T(__FUNCTION__));

	TRegistryKeyEnum enum_;

	if (!::_tcslen(pszFolder))
	{
		m_error.SetState(
				E_INVALIDARG,
				_T("Folder name cannot be empty")
			);
		return enum_;
	}

	if (m_error)
		m_error.Clear();

	::ATL::CAtlString qry__;
	details::RegistryStorage_FormatQryPath(pszFolder, qry__, m_auto_complete);

	HKEY reg_ = NULL;
	LSTATUS lResult = ::RegOpenKey(m_hRoot, qry__, &reg_);

	m_error = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
	if (m_error)
		return enum_;
	DWORD dwFolders = 0;
	lResult = ::RegQueryInfoKey(
					reg_,
					NULL,
					NULL,
					NULL,
					&dwFolders,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL,
					NULL
				);
	m_error = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
	if (!m_error)
	{
		for (DWORD i_ = 0; i_ < dwFolders; i_++)
		{
			TCHAR    pszKey[_MAX_PATH]   = {0};
			DWORD    cbName              =  _countof(pszKey);
			FILETIME ftLastWriteTime     = {0};

			lResult = ::RegEnumKeyEx(
						reg_,
						i_,
						pszKey,
						&cbName,
						NULL,
						NULL,
						NULL,
						&ftLastWriteTime
					);
			m_error = __HRESULT_FROM_WIN32(static_cast<DWORD>(lResult));
			if (m_error)
				break;
			try
			{
				CAtlString cs_key(pszKey);
				enum_.push_back(cs_key);
			}
			catch(::std::bad_alloc&){ m_error = E_OUTOFMEMORY; break; }
		}
	}
	::RegCloseKey(reg_); reg_ = NULL;

	return enum_;
}

TErrorRef        CRegistryEnumerator::Error(void)const
{
	return m_error;
}