/*
	This is Shared Lite Library Generic Handle class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericHandle.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::common;

////////////////////////////////////////////////////////////////////////////

namespace shared { namespace lite { namespace common { namespace details
{
	static HRESULT  Shared_CloseHandleSafe(HANDLE& handle_)
	{
		if (!handle_ || INVALID_HANDLE_VALUE == handle_)
			return E_INVALIDARG;
		if (!::CloseHandle(handle_))
		{
			return HRESULT_FROM_WIN32(::GetLastError());
		}
		handle_ = NULL;
		return S_OK;
	}

	static HANDLE&  Shared_GetInvalidHandleRef(void)
	{
		static HANDLE handle = INVALID_HANDLE_VALUE;
		return handle;
	}
}}}}

////////////////////////////////////////////////////////////////////////////

CAutoHandle::CAutoHandle(void):
	m_handle(NULL)
{
}

CAutoHandle::CAutoHandle(const HANDLE handle_):
	m_handle(handle_)
{
}

CAutoHandle::~CAutoHandle(void)
{
	details::Shared_CloseHandleSafe(m_handle);
}

////////////////////////////////////////////////////////////////////////////

CAutoHandle& CAutoHandle::operator=(const HANDLE h_)
{
	details::Shared_CloseHandleSafe(m_handle);
	m_handle = h_;
	return *this;
}

CAutoHandle::operator HANDLE(void)
{
	return m_handle;
}

CAutoHandle::operator HANDLE(void) const
{
	return m_handle;
}

////////////////////////////////////////////////////////////////////////////

PHANDLE    CAutoHandle::operator&(void)
{
	return &m_handle;
}

////////////////////////////////////////////////////////////////////////////
HANDLE     CAutoHandle::Handle(void) const
{
	return m_handle;
}

bool       CAutoHandle::IsValid(void) const
{
	return (NULL != m_handle && INVALID_HANDLE_VALUE != m_handle);
}

void       CAutoHandle::Reset(void)
{
	details::Shared_CloseHandleSafe(m_handle);
}