/*
	Created by Tech_dog (VToropov) on 20-Aug-2014 at 8:04:23pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Generic Event class implementation file.
*/
#include "StdAfx.h"
#include "Shared_GenericEvent.h"
#include "Shared_SystemError.h"

using namespace shared;
using namespace shared::lite;
using namespace shared::lite::runnable;
using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

CGenericEvent::CMessageHandler::CMessageHandler(IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
	m_sink_ref(sink_ref),
	m_event_id(v_evt_id),
	m_threadId(::GetCurrentThreadId())
{
}

CGenericEvent::CMessageHandler::~CMessageHandler(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT  CGenericEvent::CMessageHandler::OnGenericEventNotify(UINT, WPARAM wParam, LPARAM, BOOL&)
{
	if (!wParam)
		m_sink_ref.GenericEvent_OnNotify(m_event_id);
	else
		m_sink_ref.GenericEvent_OnNotify(_variant_t((LONG)wParam));
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

DWORD    CGenericEvent::CMessageHandler::_OwnerThreadId(void)const
{
	return m_threadId;
}

/////////////////////////////////////////////////////////////////////////////

CGenericEvent::CGenericEvent(IGenericEventNotify& sink_ref, const _variant_t& v_evt_id):
	m_handler(sink_ref, v_evt_id)
{
}

CGenericEvent::~CGenericEvent(void)
{
	Destroy();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericEvent::Create(void)
{
	if (m_handler.IsWindow())
		return S_OK;
	m_handler.Create(HWND_MESSAGE);
	return (m_handler.IsWindow() ? S_OK : HRESULT_FROM_WIN32(::GetLastError()));
}

HRESULT  CGenericEvent::Destroy(void)
{
	if (!m_handler.IsWindow())
		return S_OK;
	if (m_handler.DestroyWindow())
		m_handler.m_hWnd = NULL;
	else
	{
		CSysError err_obj(::GetLastError());
		ATLASSERT(0);
	}
	return S_OK;
}

HWND     CGenericEvent::GetHandle_Safe(void) const
{
	return (m_handler.IsWindow() ? m_handler.m_hWnd : NULL);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericEvent::Fire(const bool bAsynch, const LONG evtId)
{
	const HWND hHandler = this->GetHandle_Safe();
	return CGenericEvent::Fire(hHandler, bAsynch, evtId);
}

HRESULT  CGenericEvent::Fire(const HWND hHandler, const bool bAsynch, const LONG evtId)
{
	if (!::IsWindow(hHandler))
		return OLE_E_INVALIDHWND;
	if (bAsynch)
		::PostMessage(hHandler, CGenericEvent::eInternalMsgId, (WPARAM)evtId, (LPARAM)0);
	else
		::SendMessage(hHandler, CGenericEvent::eInternalMsgId, (WPARAM)evtId, (LPARAM)0);
	return S_OK;
}


/////////////////////////////////////////////////////////////////////////////

HRESULT  CGenericEvent::Fire2(void)
{
	const HWND  hHandler = this->GetHandle_Safe();
	if (NULL == hHandler)
		return OLE_E_INVALIDHWND;
	if (!::PostThreadMessage(
			m_handler._OwnerThreadId(),
			CGenericEvent::eInternalMsgId,
			NULL,
			NULL
		))
		return __HRESULT_FROM_WIN32(::GetLastError());
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CGenericWaitObject::CGenericWaitObject(const DWORD nTimeSlice, const DWORD nTimeFrame):
m_nTimeSlice(nTimeSlice),
m_nTimeFrame(nTimeFrame),
m_nCurrent(0)
{
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
}

CGenericWaitObject::~CGenericWaitObject(void)
{
}

/////////////////////////////////////////////////////////////////////////////

bool CGenericWaitObject::IsElapsed(void) const
{
	return (m_nCurrent >= m_nTimeFrame);
}

bool CGenericWaitObject::IsReset(void) const
{
	return (m_nCurrent == 0);
}

VOID CGenericWaitObject::Reset(const DWORD nTimeSlice, const DWORD nTimeFrame)
{
	if (nTimeSlice != CGenericWaitObject::eInfinite) m_nTimeSlice = nTimeSlice;
	if (nTimeFrame != CGenericWaitObject::eInfinite) m_nTimeFrame = nTimeFrame;
	ATLASSERT(m_nTimeSlice);
	ATLASSERT(m_nTimeFrame);
	m_nCurrent = 0;
}

void CGenericWaitObject::Wait(void)
{
	::Sleep(m_nTimeSlice);
	m_nCurrent += m_nTimeSlice;
}