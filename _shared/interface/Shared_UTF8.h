#ifndef _SHAREDLITEUTF8_H_49C8D5ED_9552_4341_A3A2_7E5060AE912A_INCLUDED
#define _SHAREDLITEUTF8_H_49C8D5ED_9552_4341_A3A2_7E5060AE912A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Feb-2015 at 5:29:46pm, GMT+3, Taganrog, Thursday;
	This is shared lite library UTF-8/Unicode conversion class declaration file. 
*/
#include "Shared_SystemError.h"
#include "Shared_RawData.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;

	class CUTF8
	{
	private:
		CSysError   m_error;
	public:
		CAtlString  ConvertMbsToWcs(const _variant_t& utf8_raw);
		_variant_t  ConvertWcsToMbs(LPCTSTR pszData);
		TErrorRef   Error(void) const;
		VOID        Reset(void);
	public:
		static
		HRESULT     ConvertWcsToMbsBuffer(LPCTSTR pszData, CRawData& mbs_buffer);
		static
		HRESULT     ConvertMbsToWcsBuffer(const BYTE* pbData, const DWORD dwCount, CAtlString& wcs_buffer);
	};
}}}

#endif/*_SHAREDLITEUTF8_H_49C8D5ED_9552_4341_A3A2_7E5060AE912A_INCLUDED*/