#ifndef _SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED
#define _SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED
/*
	Created by Tech_dog (VToropov) on 29-Apr-2015 at 1:01:25pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Registry Data Provider class declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;

	class CRegistryStorage
	{
	private:
		HKEY         m_hRoot;
		bool         m_auto_complete;  // if true, the regestry path is automatically composed by using application name from the executable
	public:
		CRegistryStorage(const HKEY hRoot, const bool bAutoComplete = true);
		~CRegistryStorage(void);
	public:
		HRESULT      Load  (LPCTSTR pszFolder, LPCTSTR pszValueName, ::ATL::CAtlString& _out_value);
		HRESULT      Load  (LPCTSTR pszFolder, LPCTSTR pszValueName, LONG& _out_value, const LONG _default = 0);
		HRESULT      Load  (LPCTSTR pszFolder, LPCTSTR pszValueName, SYSTEMTIME& _out_value);
		HRESULT      Remove(LPCTSTR pszFolder);
		HRESULT      Save  (LPCTSTR pszFolder, LPCTSTR pszValueName, const ::ATL::CAtlString& _in_value);
		HRESULT      Save  (LPCTSTR pszFolder, LPCTSTR pszValueName, const LONG _in_value);
		HRESULT      Save  (LPCTSTR pszFolder, LPCTSTR pszValueName, const SYSTEMTIME& _in_value);
	private:
		CRegistryStorage(const CRegistryStorage&);
		CRegistryStorage& operator= (const CRegistryStorage&);
	};

	typedef ::std::vector<CAtlString> TRegistryKeyEnum;

	class CRegistryEnumerator
	{
	private:
		HKEY         m_hRoot;
		bool         m_auto_complete;  // if true, the regestry path is automatically composed by using application name from the executable
	private:
		CSysError        m_error;
	public:
		CRegistryEnumerator(const HKEY hRoot, const bool bAutoComplete = true);
		~CRegistryEnumerator(void);
	public:
		TRegistryKeyEnum Enumerate(LPCTSTR pszFolder);
		TErrorRef        Error(void)const;
	};
}}}

#endif/*_SHAREDLITELIBRARYREGISTRYDATAPROVIDER_H_2F09C567_4655_42e2_9729_6E40697EF2B8_INCLUDED*/