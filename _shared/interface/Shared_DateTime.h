#ifndef _SHAREDDATETIME_H_24C438F9_D1CA_4541_A02C_9D4C52874657_INCLUDED
#define _SHAREDDATETIME_H_24C438F9_D1CA_4541_A02C_9D4C52874657_INCLUDED
/*
	Created by Tech_dog (VToropov) on 10-Jan-2016 at 2:41:14am, GMT+7, Phuket, Rawai, Sunday;
	This is shared lite library date & time related class(es) declaration file.
*/

namespace shared { namespace lite { namespace data
{
	class CSystemTime : public SYSTEMTIME
	{
		typedef SYSTEMTIME TBase;
	public:
		CSystemTime(void);                 // initializes to current local time
		CSystemTime(const FILETIME&);      // initializes from file time provided
		CSystemTime(const SYSTEMTIME&);    // initializes to system time provided
	public:
		bool          IsValid(void)const;
	public:
		CSystemTime& operator= (const SYSTEMTIME&);
	};

	class CDateTime
	{
	private:
		SYSTEMTIME    m_time;
	public:
		CDateTime(void);
		CDateTime(const time_t);           // initializes from unix time
	public:
		SYSTEMTIME    GetCurrent(void)const;
		CAtlString    GetTimeAsUnix(void) const; // YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)
	public:
		static INT    Compare(
		                const FILETIME& _t0,
		                const FILETIME& _t1,
		                const WORD _msec_threshold = 0
		            ); // 0 - equal; -1 - _t0 later; 1 - _t1 later
		static INT    Compare(
		                const SYSTEMTIME& _t0,
		                const SYSTEMTIME& _t1,
		                const WORD _msec_threshold = 0
		            ); // 0 - equal; -1 - _t0 later; 1 - _t1 later
		static time_t FileTimeToUnix(const FILETIME&);
		static time_t SystemTimeToUnix(const SYSTEMTIME&);
	};
}}}

#endif/*_SHAREDDATETIME_H_24C438F9_D1CA_4541_A02C_9D4C52874657_INCLUDED*/