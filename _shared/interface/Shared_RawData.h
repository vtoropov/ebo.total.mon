#ifndef _SHAREDLITELIBRARYRAWDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
#define _SHAREDLITELIBRARYRAWDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
/*
	Created by Tech_dog (VToropov) on 29-Nov-2014 at 0:42:03am, GMT+3, Taganrog, Saturday;
	This is Shared Lite Library raw data class declaration file.
*/
#include "Shared_SystemError.h"

namespace shared { namespace lite { namespace data
{
	using shared::lite::common::CSysError;

	class CRawData
	{
	private:
		mutable
		CSysError         m_error;
		PBYTE             m_pData;
		DWORD             m_dwSize;
	public:
		CRawData(void);
		CRawData(const DWORD dwSize);
		CRawData(const PBYTE pData, const DWORD dwSize);
		~CRawData(void);
	public:
		HRESULT           Append (const PBYTE pData, const DWORD dwSize);
		HRESULT           CopyTo (_variant_t&, const VARTYPE = VT_BSTR)const;
		HRESULT           CopyTo (SAFEARRAY*&) const;        // creates new safe array and copies the data into it
		HRESULT           Create (const DWORD dwSize);
		HRESULT           Create (const PBYTE pData, const DWORD dwSize);
		TErrorRef         Error  (void) const;
		const PBYTE       GetData(void) const;
		PBYTE             GetData(void)      ;
		DWORD             GetSize(void) const;
		bool              IsValid(void) const;
		HRESULT           Reset  (void)      ;
		::ATL::CAtlString ToString(void)const;  // assumes the buffer contains non-unicode string
	public:
		operator const    PBYTE(void)const;
		operator          PBYTE(void);
	public:
		CRawData(const CRawData&);
		CRawData& operator= (const CRawData&);
	};
}}}

#endif/*_SHAREDLITELIBRARYRAWDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED*/