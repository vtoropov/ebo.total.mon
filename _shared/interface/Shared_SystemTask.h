#ifndef _SHAREDSYSTEMTASK_H_67FDB04D_D8FE_4b7c_BF4C_4556944A9660_INCLUDED
#define _SHAREDSYSTEMTASK_H_67FDB04D_D8FE_4b7c_BF4C_4556944A9660_INCLUDED
/*
	Created by Tech_dog (VToropov) on 17-Feb-2013 at 12:21:21pm, GMT+3, Rostov-on-Don, Sunday;
	This is Shared Lite Library Task Scheduler Wrapper class declaration file.
*/

#include <mstask.h>
#include <Taskschd.h>

#pragma comment(lib, "taskschd.lib")

namespace shared { namespace lite { namespace sys_core
{
	class CTaskState
	{
	public:
		enum _e{
			TS_Unknown     = TASK_STATE_UNKNOWN,
			TS_Disabled    = TASK_STATE_DISABLED,
			TS_Queued      = TASK_STATE_QUEUED,
			TS_Ready       = TASK_STATE_READY,
			TS_Running     = TASK_STATE_RUNNING,
		};
	private:
		CTaskState::_e  m_current;
	public:
		CTaskState(void);
		CTaskState(const CTaskState::_e);
		~CTaskState(void);
	public:
		const
		CTaskState::_e& Current(void) const;
		CTaskState::_e& Current(void);
		LPCTSTR         Description(void) const;
	public:
		operator DWORD(void) const;
	};

	class CTask
	{
	private:
		CAtlString      m_name;
		CAtlString      m_path;
		CAtlString      m_xml;
		CAtlString      m_author;
		CTaskState      m_state;
		DWORD           m_delay;  // in seconds
	public:
		CTask(void);
		CTask(LPCTSTR pName, const CTaskState::_e = CTaskState::TS_Unknown);
		~CTask(void);
	public:
		const
		CAtlString&     Author(void) const;
		CAtlString&     Author(void);
		const
		DWORD&          Delay(void) const;
		DWORD&          Delay(void);
		const
		CAtlString&     Name(void) const;
		CAtlString&     Name(void);
		const
		CAtlString&     Path(void) const;
		CAtlString&     Path(void);
		const
		CTaskState&     State(void) const;
		CTaskState&     State(void);
		const
		CAtlString&     XML(void) const;
		CAtlString&     XML(void);
	};

	class CTaskCollectionBase;

	class CTaskCollection
	{
	protected:
		CTaskCollectionBase*  m_pTasks;
	protected:
		CTaskCollection(void);
		virtual ~CTaskCollection(void);
	public:
		virtual INT           Count(void)const;
		virtual const CTask&  Item(const INT nIndex)const;
		virtual HRESULT       Update(void) = NULL;
	};

	class CTaskEnumerator : public CTaskCollection
	{
	public:
		CTaskEnumerator(const bool bUpdateOnCreateOption = true);
		~CTaskEnumerator(void);
	public:
		HRESULT         Update(void) override sealed;
	};

	class CTaskService : public CTaskCollection
	{
	public:
		CTaskService(const bool bUpdateOnCreateOption = true);
		~CTaskService(void);
	public:
		HRESULT         CreateLogonTask(const CTask& new_ref) const;
		HRESULT         DeleteTaskOf(LPCTSTR pPath);
		HRESULT         GetTaskOf(LPCTSTR pPath, CTask& result_ref) const;
		HRESULT         IsTaskExist(LPCTSTR pPath)const;
		HRESULT         Update(void) override sealed;
	};

	class CStartupTask
	{
	public:
		CStartupTask(void);
		~CStartupTask(void);
	public:
		bool            IsOn(void) const;
		HRESULT         Off(void);
		HRESULT         On(void);
	};
}}}

#endif/*_SHAREDSYSTEMTASK_H_67FDB04D_D8FE_4b7c_BF4C_4556944A9660_INCLUDED*/