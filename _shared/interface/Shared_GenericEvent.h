#ifndef _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
#define _SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED
/*
	Created by Tech_dog (VToropov) on 20-Aug-2014 at 8:03:27pm, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Library Generic Event class declaration file.
*/

namespace shared { namespace lite { namespace runnable
{
	interface IGenericEventNotify
	{
		virtual HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) PURE;
		virtual HRESULT  GenericEvent_OnNotify(const LONG n_evt_id, const _variant_t v_data) {n_evt_id; v_data; return E_NOTIMPL; }
	};
	class CGenericEvent
	{
		enum {
			eInternalMsgId = WM_USER + 1,
		};
	protected:
		class CMessageHandler:
			public ::ATL::CWindowImpl<CMessageHandler>
		{
		private:
			IGenericEventNotify&    m_sink_ref;
			const _variant_t        m_event_id;
			DWORD                   m_threadId;
		public:
			BEGIN_MSG_MAP(CMessageHandler)
				MESSAGE_HANDLER(CGenericEvent::eInternalMsgId, OnGenericEventNotify)
			END_MSG_MAP()
		private:
			virtual LRESULT  OnGenericEventNotify(UINT, WPARAM, LPARAM, BOOL&);
		public:
			CMessageHandler(IGenericEventNotify&, const _variant_t& v_evt_id);
			virtual ~CMessageHandler(void);
		public:
			DWORD       _OwnerThreadId(void)const;
		};
	protected:
		CMessageHandler  m_handler;
	public:
		CGenericEvent(IGenericEventNotify&, const _variant_t& v_evt_id);
		virtual ~CGenericEvent(void);
	public:
		virtual HRESULT  Create(void);
		virtual HRESULT  Destroy(void);
		virtual HWND     GetHandle_Safe(void) const;
	private:
		CGenericEvent(const CGenericEvent&);
		CGenericEvent& operator= (const CGenericEvent&);
	public:
		virtual HRESULT  Fire(const bool bAsynch = true, const LONG evtId = 0);
		static  HRESULT  Fire(const HWND hHandler, const bool bAsynch = true, const LONG evtId = 0);
	public:
		virtual HRESULT  Fire2(void);
	};

	class CGenericWaitObject
	{
	public:
		enum{
			eInfinite = (DWORD)-1
		};
	protected:
		DWORD     m_nTimeSlice;    // in milliseconds
		DWORD     m_nCurrent;      // current time
		DWORD     m_nTimeFrame;    // total time to wait for
	public:
		CGenericWaitObject(const DWORD nTimeSlice, const DWORD nTimeFrame);
		virtual ~CGenericWaitObject(void);
	public:
		virtual bool IsElapsed(void) const;
		virtual bool IsReset  (void) const;
		virtual VOID Reset(const DWORD nTimeSlice = eInfinite, const DWORD nTimeFrame = eInfinite);
		virtual VOID Wait (void);
	};
}}}

#endif/*_SHAREDLITELIBGENERICEVENT_H_DF255B0D_D53A_4917_B1EC_8F7D231AA515_INCLUDED*/