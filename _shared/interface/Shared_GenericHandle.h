#ifndef _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
#define _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
/*
	This is Shared Lite Library Generic Handle class declaration file.
*/

namespace shared { namespace lite { namespace common
{
	class CAutoHandle
	{
	private:
		HANDLE     m_handle;
	public:
		CAutoHandle(void);
		CAutoHandle(const HANDLE);
		~CAutoHandle(void);
	public:
		CAutoHandle& operator=(const HANDLE);
	public:
		operator   HANDLE(void);
		operator   HANDLE(void) const;
	public:
		PHANDLE    operator&(void);
	private:
		CAutoHandle(const CAutoHandle&);
		CAutoHandle& operator= (const CAutoHandle&);
	public:
		HANDLE     Handle(void) const;
		bool       IsValid(void) const;
		void       Reset(void);
	};

}}}

#endif/*_SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED*/