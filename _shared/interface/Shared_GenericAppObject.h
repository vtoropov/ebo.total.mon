#ifndef _SHAREDLITEGENERICAPPLICATIONOBJECT_H_920767BA_8B5E_475b_98A4_CE06582BB19E_INCLUDED
#define _SHAREDLITEGENERICAPPLICATIONOBJECT_H_920767BA_8B5E_475b_98A4_CE06582BB19E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 8:17:25am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite Generic Application Object class declaration file.
*/
#include <map>
#pragma comment(lib, "version.lib")

namespace shared { namespace lite { namespace common
{
	class CApplication
	{
	public:
		class CProcess
		{
		private:
			HANDLE              m_mutex;
			::ATL::CAtlString   m_mutex_name;
			DWORD               m_proc_state;
			CONST CApplication& m_app_obj_ref;
		public:
			explicit CProcess(CONST CApplication&);
			~CProcess(VOID);
		public:
			bool                IsSingleton(void) const;
			HRESULT             RegisterSingleton(LPCTSTR pMutexName);
			HRESULT             UnregisterSingleton(VOID);
		};
		class CVersion
		{
		private:
			mutable HRESULT     m_hResult;     // last result
			LPVOID              m_pVerInfo;    // file version info pointer
		public:
			CVersion(void);
			~CVersion(void);
		public:
			::ATL::CAtlString   CompanyName(void)        const;
			::ATL::CAtlString   CopyRight(void)          const;
			::ATL::CAtlString   FileDescription(void)    const;
			::ATL::CAtlString   FileType(void)           const;
			::ATL::CAtlString   FileVersion(void)        const;
			::ATL::CAtlString   FileVersionFixed(void)   const;
			::ATL::CAtlString   InternalName(void)       const;
			::ATL::CAtlString   OriginalFileName(void)   const;
			::ATL::CAtlString   Platform(void)           const;
			::ATL::CAtlString   ProductName(void)        const;
			::ATL::CAtlString   ProductVersion(void)     const;
			::ATL::CAtlString   ProductVersionFixed(void)const;
		};
		class CCommandLine
		{
			typedef ::std::map<::ATL::CAtlString, ::ATL::CAtlString> TArguments;
		private:
			::ATL::CAtlString   m_module_full_path;
			TArguments          m_args;
		public:
			CCommandLine(void);
			~CCommandLine(void);
		public:
			::ATL::CAtlString   Argument(LPCTSTR pName)const;
			INT                 Count(void)const;
			bool                Has(LPCTSTR pArgName)const;
			::ATL::CAtlString   ModuleFullPath(void)const;
		};
	private:
		HRESULT                 m_hResult;     // last result
		::ATL::CAtlString       m_app_name;
		mutable CApplication&   m_smart_ass;
		CVersion                m_version;
		CProcess                m_process;
		CCommandLine            m_cmd_line;
	public:
		CApplication(void);
		~CApplication(void);
	public:
		const CCommandLine&     CommandLine(void)   const;
		HRESULT                 GetLastResult(void) const;
		LPCTSTR                 GetName(void)       const;
		CAtlString              GetFullPath(void)   const;
		HRESULT                 GetPath(::ATL::CAtlString& __in_out_ref) const;
		HRESULT                 GetPathFromAppFolder(LPCTSTR pPattern, ::ATL::CAtlString& __in_out_ref) const;
		bool                    Is64bit(VOID)const;
		const CProcess&         Process(VOID)const;
		CProcess&               Process(VOID)     ;
		const CVersion&         Version(VOID)const;
	public:
		static bool             IsRelatedToAppFolder(LPCTSTR pPath);
	private:
		CApplication(const CApplication&);
		CApplication& operator= (const CApplication&);
	};

	class CApplicationIconLoader
	{
	private:
		HICON   m__small;
		HICON   m__big;
	public:
		CApplicationIconLoader(const UINT nIconId); // it is assumed that resource identifier points to multiframe icon resource
		CApplicationIconLoader(const UINT nSmallIconId, const UINT nBigIconId);
		~CApplicationIconLoader(void);
	public:
		HICON   DetachBigIcon(void);
		HICON   DetachSmallIcon(void);
		HICON   GetBigIcon(void) const;
		HICON   GetSmallIcon(void) const;
	private:
		CApplicationIconLoader(const CApplicationIconLoader&);
		CApplicationIconLoader& operator= (const CApplicationIconLoader&);
	};

	class CApplicationCursor
	{
	private:
		bool    m_resource_owner;
	public:
		CApplicationCursor(LPCTSTR lpstrCursor = IDC_WAIT);
		~CApplicationCursor(void);
	};
}}}

#endif/*_SHAREDLITEGENERICAPPLICATIONOBJECT_H_920767BA_8B5E_475b_98A4_CE06582BB19E_INCLUDED*/