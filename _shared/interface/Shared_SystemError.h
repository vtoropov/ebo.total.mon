#ifndef _SHAREDLITESYSTEMERROR_H_E4336D27_03C6_4005_B45C_D443D6D971ED_INCLUDED
#define _SHAREDLITESYSTEMERROR_H_E4336D27_03C6_4005_B45C_D443D6D971ED_INCLUDED
/*
	Created by Tech_dog (VToropov) on 19-Mar-2014 at 7:02:24am, GMT+4, Taganrog, Wednesday;
	This is Shared Lite System Error class declaration file.
*/
#include "Shared_GenericSyncObject.h"

namespace shared { namespace lite { namespace common
{
	struct CLangId
	{
		DWORD  dwPrimary;  // primary language Id
		DWORD  dwSecond;   // sub-language Id

		CLangId(void) : dwPrimary(LANG_NEUTRAL), dwSecond(SUBLANG_NEUTRAL) {}
		CLangId(const DWORD _primary, const DWORD _second) : dwPrimary(_primary), dwSecond(_second) {}
	};

	class CSysError
	{
	protected:
		::ATL::CAtlString m_buffer;
		::ATL::CAtlString m_source;
		::ATL::CAtlString m_module;
		DWORD             m_dwError;
		HRESULT           m_hrError;
		CLangId           m_lng_id;
	public:
		static const DWORD dwEmpty = (DWORD)-1;
	public:
		CSysError(LPCTSTR pszModule = NULL);
		CSysError(const DWORD dwError, const CLangId& = CLangId());
		CSysError(const HRESULT hError, const CLangId& = CLangId());
		CSysError(const CSysError&);
		virtual ~CSysError(void);
	public:
		virtual void      SetHresult(const HRESULT);                            // sets the result and updates the error description
	public:
		void              Clear(void);                                          // sets the error object to success state
		DWORD             GetCode(void)const;                                   // gets win32 error code
		LPCTSTR           GetDescription(void) const;                           // gets the current description
		CAtlString        GetFormattedDetails(void)const;                       // gets formatted string like this: module, code, description, source
		HRESULT           GetHresult(void) const;                               // gets the current result code
		bool              HasDetails(void) const;                               // checks the error description buffer, if empty, returns false, otherwise, true
		LPCTSTR           Module(void) const;                                   // gets module name that produces the error, if any
		VOID              Module(LPCTSTR);                                      // sets module name that produces the error
		void              Reset(void);                                          // re-sets the error object to empty/initial state (OLE_E_BLANK)
		void              SetState(const DWORD  dwError, LPCTSTR pDescription); // sets the object state manually
		void              SetState(const DWORD  dwError, const UINT resId);     // sets the object state manually, description is loaded from string resource specified by identifier
		void              SetState(const HRESULT hError, LPCTSTR pDescription); // sets the object state manually
		void              SetState(const HRESULT hError, const UINT resId);     // sets the object state manually, description is loaded from string resource specified by identifier
		LPCTSTR           Source(void) const;                                   // gets the error source
		void              Source(LPCTSTR);                                      // sets the error source
	public:
		CSysError& operator= (const _com_error&);
		CSysError& operator= (const DWORD);
		CSysError& operator= (const HRESULT);
		CSysError& operator= (const CSysError&);
	public:
		operator bool(void)const;
		operator HRESULT(void) const;
	};

	using shared::lite::runnable::CGenericSyncObject;

	class CSysErrorSafe : public CSysError
	{
		typedef CSysError TBase;
	private:
		CGenericSyncObject&  m_sync_obj;
	public:
		CSysErrorSafe(CGenericSyncObject&);
	public:
		CSysErrorSafe& operator= (const CSysError&);
	public:
		VOID          Clear(void);
		CAtlString    GetFormattedDetails(void)const;
		VOID          Module(LPCTSTR);
		VOID          SetState(const DWORD  dwError, LPCTSTR pszDescription);
		VOID          SetState(const HRESULT hError, LPCTSTR pszDescription);
		VOID          Source(LPCTSTR);
	public:
		CSysErrorSafe& operator= (const DWORD);
		CSysErrorSafe& operator= (const HRESULT);
	public:
		operator bool(void)const;
		operator HRESULT(void) const;
	private:
		CSysErrorSafe(const CSysErrorSafe&);   // cannot copy a sync object reference from a source
	};
}}}
typedef const shared::lite::common::CSysError&  TErrorRef;

#endif/*_SHAREDLITESYSTEMERROR_H_E4336D27_03C6_4005_B45C_D443D6D971ED_INCLUDED*/