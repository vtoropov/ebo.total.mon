/*
	Created by Tech_dog (VToropov) on 12-Feb-2016 at 6:57:40pm, GMT+7, Phuket, Rawai, Friday;
	This is Shared Net library internet provider class implementation file.
*/
#include "StdAfx.h"
#include "Shared_NetProvider.h"

using namespace shared::net;

/////////////////////////////////////////////////////////////////////////////

CInternetProvider::CInternetProvider(void) :
	m_session(NULL),
	m_bManaged(true)
{
	m_error.Reset();
}

CInternetProvider::~CInternetProvider(void)
{
	this->Terminate();
}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  CInternetProvider::Error(void)const
{
	return m_error;
}

DWORD      CInternetProvider::GetConnectionLimit(void)const
{
	m_error.Clear();

	if (!this->IsInitialized())
		m_error = OLE_E_BLANK;
	if (m_error)
		return 0;
	DWORD dwValue = 0;
	DWORD dwLength = sizeof(DWORD);
	const BOOL bResult = ::InternetQueryOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_SERVER, (LPVOID)&dwValue, &dwLength);
	if (!bResult)
	{
		m_error = ::GetLastError();
		return 0;
	}
	else
		return dwValue;
}

HRESULT    CInternetProvider::Initialize(void)
{
	if (this->IsInitialized())
		return (m_error = ERROR_ALREADY_INITIALIZED);
	m_session = ::InternetOpen(_T("inet_provider"), INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	if (m_session == NULL)
		m_error = ::GetLastError();
	else
	{
		m_bManaged = true;
		m_error.Clear();
	}
	return m_error;
}

bool       CInternetProvider::IsInitialized(void)const
{
	return (NULL != m_session);
}

HINTERNET  CInternetProvider::Session(void)const
{
	return m_session;
}

HRESULT    CInternetProvider::SetConnectionLimit(const DWORD dw_limit)
{
	if (!dw_limit)
		return (m_error = E_INVALIDARG);
	m_error.Clear();

	DWORD dwValue = dw_limit;
	const BOOL bResult = ::InternetSetOption(NULL, INTERNET_OPTION_MAX_CONNS_PER_SERVER, (LPVOID)&dwValue, sizeof(DWORD));
	if (!bResult)
		m_error = ::GetLastError();

	return m_error;
}

HRESULT    CInternetProvider::Terminate(void)
{
	if (!this->IsInitialized())
		return S_OK;
	if (m_bManaged && m_session) ::InternetCloseHandle(m_session); m_session = NULL;
	m_error.Reset();
	m_bManaged = false;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CInternetProvider::IsAccessible(void)
{
	const DWORD dwResult = ::InternetAttemptConnect(0);
	return (ERROR_SUCCESS == dwResult ? S_OK : HRESULT_FROM_WIN32(dwResult));
}

/////////////////////////////////////////////////////////////////////////////

CInternetProvider::CInternetProvider(const CInternetProvider& _prov_ref) :
	m_session(_prov_ref.Session())
{
	m_bManaged = false;
	m_error = _prov_ref.Error();
}