/*
	Created by Tech_dog (VToropov) on 11-Jan-2016 at 7:14:14pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared net common library web provider class implementation file.
*/
#include "StdAfx.h"
#include "Shared_WebProvider.h"

using namespace shared::net;

/////////////////////////////////////////////////////////////////////////////

CWebProvider::CWebProvider(void): m_bInitialized(false)
{
	m_error.Source(_T("CWebProvider"));
}

CWebProvider::~CWebProvider(void)
{
	if (m_bInitialized)
		this->Terminate();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CWebProvider::DoGetRequest(LPCTSTR _url, CAtlString& _response)
{
	m_error.Module(_T(__FUNCTION__));

	if (!_url || ::_tcslen(_url) < 7)
		return (m_error = E_INVALIDARG);
	
	m_error = S_OK;
	try
	{
		if (!m_pWebService)
			return (m_error = OLE_E_BLANK);
		m_error = m_pWebService->Open(_bstr_t(_T("GET")), _bstr_t(_url), _variant_t((bool)false));
		if (!m_error)
		{
			m_error = m_pWebService->SetRequestHeader(_bstr_t(_T("Content-Type")), _bstr_t(_T("application/xml")));
			if (!m_error)
			{
				m_error = m_pWebService->Send(_variant_t(_T("")));
				if (!m_error)
				{
					::ATL::CComBSTR bsResponse;
					m_error = m_pWebService->get_ResponseText(&bsResponse);
					if (!m_error)
						_response = bsResponse;
				}
			}
		}
	}
	catch(_com_error& err_ref)
	{
		m_error = err_ref.Error();
	}
	return m_error;
}

TErrorRef  CWebProvider::Error(void)const
{
	return m_error;
}

HRESULT    CWebProvider::Initialize(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (m_bInitialized)
		return (m_error = S_OK);

	m_error = S_OK;
	try
	{
		m_error = m_pWebService.CoCreateInstance(_T("WinHttp.WinHttpRequest.5.1"));
	}
	catch(_com_error& err_)
	{
		m_error = err_.Error();
		return m_error;
	}
	m_bInitialized = !m_error;
	return  m_error;
}

HRESULT    CWebProvider::Terminate(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (!m_bInitialized)
		return (m_error = S_OK);

	try
	{
		m_pWebService = NULL;
	}
	catch(_com_error& err_ref)
	{
		return err_ref.Error();
	}

	m_bInitialized = false;
	m_error = OLE_E_BLANK;
	return S_OK;
}