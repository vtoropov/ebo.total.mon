#ifndef _SHAREDWEBPROVIDER_H_0A3D8248_44A0_4f2f_9E05_9AF15D09021B_INCLUDED
#define _SHAREDWEBPROVIDER_H_0A3D8248_44A0_4f2f_9E05_9AF15D09021B_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Jan-2016 at 7:07:39pm, GMT+7, Phuket, Rawai, Monday;
	This is Shared Net library web provider class declaration file.
*/
#include "Shared_SystemError.h"

#include <httprequest.h>

/*
	VERY IMPORTANT NOTES:
	-------------------------------------------------------------------------------
	in order to get the httprequest.h header file you have to perform the following:
	1) Run as administrator "Visual Studio 2008 Command Prompt";
	2) cd C:\Program Files\Microsoft SDKs\Windows\v6.0A\Include\;
	3) midl httprequest.idl;
*/

namespace shared { namespace net
{
	using shared::lite::common::CSysError;

	class CWebProvider
	{
		typedef ::ATL::CComPtr<IWinHttpRequest> TService;
	private:
		CSysError        m_error;
		TService         m_pWebService;
		bool             m_bInitialized;
	public:
		CWebProvider(void);
		~CWebProvider(void);
	public:
		HRESULT          DoGetRequest(LPCTSTR _url, CAtlString& _response);
		TErrorRef        Error(void)const;
		HRESULT          Initialize(void);
		HRESULT          Terminate(void);
	};
}}

#endif/*_SHAREDWEBPROVIDER_H_0A3D8248_44A0_4f2f_9E05_9AF15D09021B_INCLUDED*/