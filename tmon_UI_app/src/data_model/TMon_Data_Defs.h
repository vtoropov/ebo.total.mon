#ifndef _TMONDATADEFS_H_079E98D8_9CEA_4d4f_BAEF_EAAE0CD91BA8_INCLUDED
#define _TMONDATADEFS_H_079E98D8_9CEA_4d4f_BAEF_EAAE0CD91BA8_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Dec-2015 at 2:11:30pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring application data model class(es) declaration file.
*/
#include "Shared_AvsDefs.h"
#include "Shared_CustomTskDefs.h"

namespace tmon { namespace data
{
	using shared::tasks::CCustomTask;

	class CCustomTaskEx : public CCustomTask
	{
		typedef CCustomTask TBase;
	private:
		bool        m_bNew;
	public:
		CCustomTaskEx(const bool bNew = true);
	public:
		bool        IsNew(void)const;
	};
}}

#endif/*_TMONDATADEFS_H_079E98D8_9CEA_4d4f_BAEF_EAAE0CD91BA8_INCLUDED*/