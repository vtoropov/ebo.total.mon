/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 7:37:16am, GMT+7, Phuket, Rawai, Saturday;
	This is Total Monitoring application data model class(es) implementation file.
*/
#include "StdAfx.h"
#include "TMon_Data_Defs.h"

using namespace tmon::data;

/////////////////////////////////////////////////////////////////////////////

CCustomTaskEx::CCustomTaskEx(const bool bNew) : m_bNew(bNew)
{
}

/////////////////////////////////////////////////////////////////////////////

bool    CCustomTaskEx::IsNew(void)const
{
	return m_bNew;
}