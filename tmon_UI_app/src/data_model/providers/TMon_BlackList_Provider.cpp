/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 7:13:59am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Web Filter Black List data provider class implementation file.
*/
#include "StdAfx.h"
#include "TMon_BlackList_Provider.h"

using namespace tmon::data;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"
#include "Shared_GenericHandle.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace data { namespace details
{
	class CWebBlackListProvider_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CWebBlackListProvider_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		HRESULT       Load(TWebBlackList& _lst)
		{
			LONG lCount = 0;
			m_stg.Load(
					this->_BlackListFolder(),
					this->_BlackListCountRegName(),
					lCount
				);
			for (LONG i_ = 0; i_ < lCount; i_++)
			{
				CAtlString cs_item;
				cs_item.Format(
						this->_BlackListItemRegName(),
						i_
					);
				CAtlString cs_value;
				HRESULT hr_ = m_stg.Load(
					this->_BlackListFolder(),
					cs_item,
					cs_value
				);
				if (FAILED(hr_))
					break;
				if (cs_value.IsEmpty())
					continue;
				try
				{
					_lst.insert(
							::std::make_pair(cs_value, cs_value)
						);
				}
				catch(::std::bad_alloc&){ return E_OUTOFMEMORY; }
			}
			return S_OK;
		}

		HRESULT          Save(const TWebBlackList& _lst)
		{
			const LONG lCount = static_cast<LONG>(_lst.size());

			HRESULT hr_ = m_stg.Save(
					this->_BlackListFolder(),
					this->_BlackListCountRegName(),
					lCount
				);
			if (S_OK != hr_)
				return  hr_;

			INT n_iter_ = 0;
			for (TWebBlackList::const_iterator it_ = _lst.begin(); it_ != _lst.end(); ++it_)
			{
				CAtlString cs_item;
				cs_item.Format(
						this->_BlackListItemRegName(),
						n_iter_
					);
				hr_ = m_stg.Save(
					this->_BlackListFolder(),
					cs_item,
					it_->second.GetString()
				);
				if (FAILED(hr_))
					break;
				n_iter_+= 1;
			}
			return hr_;
		}

	private:
		CAtlString      _BlackListFolder(void)            const { return CAtlString(_T("BlackList"));         }
		CAtlString      _BlackListCountRegName(void)      const { return CAtlString(_T("BlackListItems"));    }
		CAtlString      _BlackListItemRegName(void)       const { return CAtlString(_T("BlackListItem_%.4d"));} 
	};

}}}

/////////////////////////////////////////////////////////////////////////////

CWebBlackListProvider::CWebBlackListProvider(void)
{
}

CWebBlackListProvider::~CWebBlackListProvider(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT        CWebBlackListProvider::Add (LPCTSTR _url)
{
	CAtlString url_(_url);
	if (url_.IsEmpty())
		return E_INVALIDARG;
	
	url_.MakeLower();

	TWebBlackList::const_iterator it_ = m_black_lst.find(url_);
	if (it_ != m_black_lst.end())
		return (m_error = ERROR_ALREADY_EXISTS);
	try
	{
		m_black_lst[url_] = url_;
	}
	catch(::std::bad_alloc&)
	{
		return (m_error = E_OUTOFMEMORY);
	}
	return (m_error = S_OK);
}

TErrorRef      CWebBlackListProvider::Error(void)const
{
	return m_error;
}

TWebBlackList  CWebBlackListProvider::List(void)const
{
	return m_black_lst;
}

HRESULT        CWebBlackListProvider::Load(void)
{
	if (!m_black_lst.empty())
		m_black_lst.clear();

	details::CWebBlackListProvider_Registry reg_;

	m_error = reg_.Load(m_black_lst);
	return m_error;
}

HRESULT        CWebBlackListProvider::Remove(LPCTSTR _url)
{
	CAtlString url_(_url);
	if (url_.IsEmpty())
		return (m_error = E_INVALIDARG);

	TWebBlackList::iterator it_ = m_black_lst.find(url_);
	if (it_ == m_black_lst.end())
		return (m_error = TYPE_E_ELEMENTNOTFOUND);

	m_black_lst.erase(it_);
	return (m_error = S_OK);

}

HRESULT        CWebBlackListProvider::Save(void)
{
	details::CWebBlackListProvider_Registry reg_;

	m_error = reg_.Save(m_black_lst);
	return m_error;
}