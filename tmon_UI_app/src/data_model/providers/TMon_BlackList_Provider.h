#ifndef _TMONBLACKLISTPROVIDER_H_B100D4DE_F020_4c62_87D0_C4ACFCEC27AC_INCLUDED
#define _TMONBLACKLISTPROVIDER_H_B100D4DE_F020_4c62_87D0_C4ACFCEC27AC_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 7:02:01am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Web Filter Black List data provider class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_WebFilter.h"

namespace tmon { namespace data
{
	using shared::lite::common::CSysError;

	using shared::avs::TWebBlackList;

	class CWebBlackListProvider
	{
	private:
		mutable
		CSysError          m_error;
		TWebBlackList      m_black_lst;
	public:
		CWebBlackListProvider(void);
		~CWebBlackListProvider(void);
	public:
		HRESULT            Add (LPCTSTR _url);
		TErrorRef          Error(void)const;
		TWebBlackList      List(void)const;
		HRESULT            Load(void);
		HRESULT            Remove(LPCTSTR _url);
		HRESULT            Save(void);
	};
}}

#endif/*_TMONBLACKLISTPROVIDER_H_B100D4DE_F020_4c62_87D0_C4ACFCEC27AC_INCLUDED*/