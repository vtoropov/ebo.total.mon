#ifndef _TMONLOGPROVIDER_H_E207CFE5_6813_4bd8_B602_7963E09D8155_INCLUDED
#define _TMONLOGPROVIDER_H_E207CFE5_6813_4bd8_B602_7963E09D8155_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 9:37:28pm, GMT+7, Phuket, Rawai, Saturday;
	This is Totral Monitoring Log Data Provider class declaration file.
*/
#include "Shared_SystemError.h"
#include "Shared_AvsDefs.h"

namespace tmon { namespace data
{
	using shared::lite::common::CSysError;
	using shared::avs::CScanFile;
	using shared::avs::CScanFileEx;

	class CLogProviderBase
	{
	protected:
		INT             m_level;     // cached value of log level from global settings
		FILE*           m_log;
		CSysError       m_error;
		CAtlString      m_path;
	public:
		CLogProviderBase(LPCTSTR pszLogPath = NULL);
		~CLogProviderBase(void);
	public:
		virtual HRESULT Close(void);
		virtual HRESULT Open (void);
		virtual HRESULT Write(const CScanFile&);
	public:
		TErrorRef       Error(void)const;
	private:
		CLogProviderBase(const CLogProviderBase&);
		CLogProviderBase& operator= (const CLogProviderBase&);
	};

	using shared::avs::TFileScanListEx;

	class CThreatReport : public CLogProviderBase
	{
		typedef CLogProviderBase  TBase;
	private:
		TFileScanListEx m_threats;
		bool            m_bDirty;
	public:
		CThreatReport(void);
		~CThreatReport(void);
	public:
		virtual HRESULT Close(void) override sealed;
		virtual HRESULT Open (void) override sealed;
	public:
		HRESULT         Append(const CScanFileEx&);
		HRESULT         Clear(void);
		const
		CScanFileEx&    Item (const INT nIndex)const;
		CScanFileEx&    Item (const INT nIndex);
		INT             ItemCount(void)const;
		HRESULT         Reset(void);                     // actually, creates new report file with name base on current timestamp
		HRESULT         Save (void);
	};
}}

#endif/*_TMONLOGPROVIDER_H_E207CFE5_6813_4bd8_B602_7963E09D8155_INCLUDED*/