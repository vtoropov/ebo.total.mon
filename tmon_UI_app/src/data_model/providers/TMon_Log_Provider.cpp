/*
	Created by Tech_dog (VToropov) on 16-Jan-2016 at 9:50:18pm, GMT+7, Phuket, Rawai, Saturday;
	This is Total Monitoring Log Data Provider class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Log_Provider.h"
#include "TMon_Settings.h"

using namespace tmon::common;
using namespace tmon::data;

#include "Shared_GenericAppObject.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;

#include "Shared_AvsDefs.h"

using namespace shared::avs;

#define _LONG_PATH (_MAX_PATH + _MAX_DRIVE + _MAX_DIR)
/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace data { namespace details
{
	CAtlString  LogProviderBase_GetFileName(LPCTSTR _pfx, LPCTSTR _ext)
	{
		CAtlString cs_file;
		SYSTEMTIME st_ = {0};

		::GetLocalTime(&st_);

		cs_file.Format(
			_T("%s_%04d_%02d_%02d_%02d_%02d_%02d_%04d%s"), 
			_pfx, 
			st_.wYear,
			st_.wMonth,
			st_.wDay,
			st_.wHour,
			st_.wMinute,
			st_.wSecond,
			st_.wMilliseconds,
			_ext
		);

		return cs_file;
	}

	CAtlString  LogProviderBase_GetFilePath(void)
	{
		CAtlString cs_path = global::GetSettings().Log().Folder();

		if (CApplication::IsRelatedToAppFolder(cs_path))
		{
			CApplication& the_app = global::GetAppObjectRef();
			the_app.GetPathFromAppFolder(cs_path, cs_path);
		}

		if (cs_path.Right(1) != _T('\\'))
			cs_path += _T("\\");

		cs_path += LogProviderBase_GetFileName(_T("tmon"), _T(".log"));
		return cs_path;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CLogProviderBase::CLogProviderBase(LPCTSTR pszLogPath) : m_log(NULL), m_level(global::GetSettings().Log().Level()), m_path(pszLogPath)
{
	m_error.Source(_T("CLogProviderBase"));
}

CLogProviderBase::~CLogProviderBase(void)
{
	this->Close();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT         CLogProviderBase::Close(void)
{
	m_error.Module(_T(__FUNCTION__));

	if (m_log)
	{
		::fclose(m_log); m_log = NULL;
	}
	if (m_error)
		m_error = S_OK;

	return m_error;
}

TErrorRef       CLogProviderBase::Error(void)const
{
	return m_error;
}

HRESULT         CLogProviderBase::Open (void)
{
	m_error.Module(_T(__FUNCTION__));

	if (m_log)
	{
		m_error.SetState(
				(DWORD)ERROR_ALREADY_INITIALIZED,
				_T("Log file is already open")
			);
		return m_error;
	}

	if (m_path.IsEmpty())
		m_path = details::LogProviderBase_GetFilePath();

	errno_t err_ = ::_tfopen_s(
			&m_log,
			m_path,
			_T("a")
		);
	if (err_)
	{
		CAtlString cs_error = ::_tcserror(err_);
		if (cs_error.IsEmpty())
			cs_error = _T("Cannot create/open a log file");

		m_error.SetState(
				(DWORD)ERROR_ACCESS_DENIED,
				cs_error
			);
	}
	else
		m_error = S_OK;

	return m_error;
}

HRESULT         CLogProviderBase::Write(const CScanFile& _file)
{
	static bool bFirst = false;
	if (bFirst == false)
	{
		bFirst = true;
		m_error.Module(_T(__FUNCTION__));
	}

	if (!m_log)
	{
		m_error.SetState(
				OLE_E_BLANK,
				_T("Log file is not open")
			);
		return m_error;
	}

	bool bWritable = false;

	switch (_file.Health())
	{
	case CScanFileHealth::eClean    : bWritable = (TLogCfg::eAll == m_level); break;
	case CScanFileHealth::eSuspected: bWritable = (TLogCfg::eErrorsAndThreats >= m_level); break;
	case CScanFileHealth::eInfected : bWritable = (TLogCfg::eErrorsAndThreats >= m_level); break;
	case CScanFileHealth::eError    : bWritable = (TLogCfg::eErrorsAndThreats == m_level); break;
	}
	if (bWritable)
	{
		::_ftprintf(
				m_log,
				_T("%s; %s; %s; %s\r\n"),
				_file.TimestamptAsText(),
				_file.Path(),
				_file.HealthAsText(),
				_file.Virus()
			);
		::fflush(m_log);
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace data { namespace details
{
	CSysError    ThreatReport_GetFilePath(CAtlString& _path)
	{
		CSysError err_obj(_T(__FUNCTION__)); err_obj = S_OK;

		CSpecialFolder spec_;
		_path = spec_.ProgramFiles86(); // can be 32-bit platform
		if (_path.IsEmpty())
			_path = spec_.ProgramFiles();
		if (_path.IsEmpty())
			return (err_obj = spec_.Error());

		_path += _T("\\Tmagent\\configs\\");

		return err_obj;
	}

	CAtlString   ThreatReport_GetFileName(void)
	{
		CAtlString cs_name = LogProviderBase_GetFileName(_T("rpt"), _T(""));
		return cs_name;
	}

	CScanFileEx& ThreatReport_GetInvalidItemRef(void)
	{
		static CScanFileEx file_;
		return file_;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CThreatReport::CThreatReport(void) : m_bDirty(false)
{
	m_error.Source(_T("CThreatReport"));
	m_error = details::ThreatReport_GetFilePath(TBase::m_path);
	if (!m_error)
	{
		CGenericFolder folder_(TBase::m_path);
		if (!folder_.IsExist())
			m_error = folder_.Create();

		TBase::m_path += details::ThreatReport_GetFileName();
	}
}

CThreatReport::~CThreatReport(void)
{
	this->Close();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CThreatReport::Close(void)
{
	HRESULT hr_ = TBase::Close();
	return  hr_;
}

HRESULT      CThreatReport::Open (void)
{
	HRESULT hr_ = TBase::Open();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CThreatReport::Append(const CScanFileEx& _file)
{
	try
	{
		m_threats.push_back(_file);
		m_error = S_OK;
		if (m_bDirty == false)
			m_bDirty  = true;
	}
	catch(::std::bad_alloc&)
	{
		m_error = E_OUTOFMEMORY;
	}
	return m_error;
}

HRESULT      CThreatReport::Clear(void)
{
	m_bDirty = false;
	if (!m_threats.empty())m_threats.clear();
	HRESULT hr_ = S_OK;
	return  hr_;
}

const
CScanFileEx& CThreatReport::Item (const INT nIndex)const
{
	if (-1 < nIndex && nIndex < this->ItemCount())
		return m_threats[nIndex];
	else
		return details::ThreatReport_GetInvalidItemRef();
}

CScanFileEx& CThreatReport::Item (const INT nIndex)
{
	if (-1 < nIndex && nIndex < this->ItemCount())
		return m_threats[nIndex];
	else
		return details::ThreatReport_GetInvalidItemRef();
}

INT          CThreatReport::ItemCount(void)const
{
	return static_cast<INT>(m_threats.size());
}

HRESULT      CThreatReport::Reset(void)
{
	m_error.Module();
	m_error = S_OK;

	this->Clear();
	this->Close();

	m_error = details::ThreatReport_GetFilePath(TBase::m_path);
	if (m_error)
		return m_error;
	{
		CGenericFolder folder_(TBase::m_path);
		if (!folder_.IsExist())
			m_error = folder_.Create();

		TBase::m_path += details::ThreatReport_GetFileName();
	}

	this->Open();

	return m_error;
}

HRESULT      CThreatReport::Save (void)
{
	if (!m_bDirty)
		return (m_error = (DWORD)ERROR_INVALID_STATE);

	static bool bFirst = false;
	if (bFirst == false)
	{
		bFirst = true;
		m_error.Module(_T(__FUNCTION__));
	}

	if (!m_log)
	{
		m_error.SetState(
				OLE_E_BLANK,
				_T("Log file is not open")
			);
		return m_error;
	}
	const INT count_ = this->ItemCount();
	for ( INT i_ = 0; i_ < count_; i_++)
	{
		const CScanFileEx& file_ = m_threats[i_];
		::_ftprintf(
				m_log,
				_T("%s; %s; %s; %s; %s\r\n"),
				file_.TimestamptAsText(),
				file_.Path(),
				file_.HealthAsText(),
				file_.Virus(),
				file_.ActionAsText()
			);
	}
	::fflush(m_log);
	m_bDirty = false;

	return m_error;
}