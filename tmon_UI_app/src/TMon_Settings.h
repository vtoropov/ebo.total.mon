#ifndef _TMONSETTINGS_H_DD2F7C58_3B71_48c1_8937_1DB6C5500012_INCLUDED
#define _TMONSETTINGS_H_DD2F7C58_3B71_48c1_8937_1DB6C5500012_INCLUDED
/*
	Created by Tech_dog (VToropov) on 28-Nov-2015 at 9:21:12pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring application settings class declaration file.
*/
#include "TMon_Data_Defs.h"

namespace tmon { namespace common
{
	using shared::avs::CScanAction;

	class CSettings
	{
	public:
		class CGeneral
		{
		private:
			bool        m_splash;
			bool        m_onlogon;
			bool        m_minimized;
		public:
			CGeneral(void);
		public:
			bool        IsSilentStart (void)const; // aggrigates two options <Show Splash> and <Start Minimized> together
			bool        ShowSplash    (void)const;
			VOID        ShowSplash    (const bool);
			bool        StartMinimized(void)const;
			VOID        StartMinimized(const bool);
			bool        StartOnLogon  (void)const;
			VOID        StartOnLogon  (const bool);
		};

		class CLog
		{
		public:
			enum {
				eAll  = 0,
				eErrorsAndThreats = 1,
				eThreatsOnly      = 2,
			};
		private:
			bool        m_enabled;
			INT         m_level;      // 0 - all messages; 1 - errors & threats; 2 - threats only
			CAtlString  m_folder;
		public:
			CLog(void);
		public:
			CAtlString  Folder(void)const;
			VOID        Folder(LPCTSTR);
			bool        Enabled(void)const;
			VOID        Enabled(const bool);
			INT         Level(void)const;
			HRESULT     Level(const INT);
		};

		class CProtection
		{
		private:
			CScanAction m_action;
			bool        m_bReport;   // reporting to software vendor;
			bool        m_bRemote;   // enable remote control over threat(s)
		public:
			CProtection(void);
		public:
			CScanAction DefaultAction(void)const;
			VOID        DefaultAction(const INT); // sets default action from list by index provided
			bool        DefaultAction(const CScanAction::_e _id);
			bool        RemoteControl(void)const;
			VOID        RemoteControl(const bool);
			bool        Reporting(void)const;
			VOID        Reporting(const bool);
		};

	private:
		CGeneral        m_general;
		CLog            m_log;
		CProtection     m_protect;
	public:
		CSettings(void);
		~CSettings(void);
	public:
		const
		CGeneral&       General(void)const;
		CGeneral&       General(void);
		HRESULT         Load(void);
		const
		CLog&           Log(void)const;
		CLog&           Log(void);
		const
		CProtection&    Protection(void)const;
		CProtection&    Protection(void);
		HRESULT         Save(void);
	};

	typedef CSettings::CGeneral    TGeneralCfg;
	typedef CSettings::CLog        TLogCfg;
	typedef CSettings::CProtection TProtectCfg;
}}

#endif/*_TMONSETTINGS_H_DD2F7C58_3B71_48c1_8937_1DB6C5500012_INCLUDED*/