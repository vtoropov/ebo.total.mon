#ifndef _TMONSYSTRAY_H_3CD25026_BECB_4991_AD54_9DEB77C97012_INCLUDED
#define _TMONSYSTRAY_H_3CD25026_BECB_4991_AD54_9DEB77C97012_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Jan-2016 at 0:13:49am, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring System Tray Wrapper class declaration file.
*/
#include "UIX_NotifyTrayArea.h"

namespace tmon { namespace common
{
	using ex_ui::CNotifyTrayArea;
	using ex_ui::INotifyTrayAreaCallback;

	class CSystemTray : public INotifyTrayAreaCallback
	{
	private:
		CNotifyTrayArea   m_tray;
		HWND              m_frame;
	public:
		CSystemTray(void);
		~CSystemTray(void);
	public:
		HRESULT  HideIcon(void);
		VOID     SetMainFrameHandle(const HWND);
		HRESULT  SetTooltip(LPCTSTR);
		HRESULT  ShowIcon(LPCTSTR pszTooltip);
	private: // INotifyTrayAreaCallback
		HRESULT  NotifyTray_OnClickEvent(const UINT eventId)       override sealed;
		HRESULT  NotifyTray_OnContextMenuEvent(const UINT eventId) override sealed;
	private:
		CSystemTray(const CSystemTray&);
		CSystemTray& operator= (const CSystemTray&);
	};
}}

#endif/*_TMONSYSTRAY_H_3CD25026_BECB_4991_AD54_9DEB77C97012_INCLUDED*/