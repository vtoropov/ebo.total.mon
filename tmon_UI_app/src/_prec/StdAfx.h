#ifndef _TMONPRECOMPILEDHEADER_H_A17C1C15_A3FE_40bf_B1BA_5DD3C8B7A9C9_INCLUDED
#define _TMONPRECOMPILEDHEADER_H_A17C1C15_A3FE_40bf_B1BA_5DD3C8B7A9C9_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Nov-2015 at 6:15:09pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring application precompiled headers definition file.
*/
#include "TMon_TargetVersion.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlframe.h>
#include <atlctrls.h>
#include <atldlgs.h>
#include <atlcrack.h>
#include <atlsafe.h>
#include <atltheme.h>
#include <atlfile.h>

#include <comdef.h>
#include <comutil.h>

#include <map>
#include <vector>
#include <queue>

#include <math.h>

namespace std {
#include <time.h>
}

#include <shellapi.h>

#include "Shared_GenericAppObject.h"
#include "Shared_SystemError.h"
#include "Shared_AvsIface.h"

#include "TMon_StatusBarEx.h"
#include "TMon_Settings.h"
#include "TMon_SysTray.h"

namespace global
{
	using shared::lite::common::CApplication;
	using shared::lite::common::CSysError;

	CApplication& GetAppObjectRef(void);

	using tmon::UI::components::CStatusBarEx;

	CStatusBarEx& GetStatusCtrlRef(void);

	VOID  SetErrorMessage(TErrorRef);
	VOID  SetErrorMessage(LPCTSTR);
	VOID  SetInfoMessage (LPCTSTR);
	VOID  SetWaitMessage (LPCTSTR);
	VOID  SetWarnMessage (LPCTSTR);

	using shared::avs::CScannerEngine;

	CScannerEngine& GetScannerRef(void);

	using tmon::common::CSettings;
	using tmon::common::CSystemTray;

	CSettings&   GetSettings(void);
	CSystemTray& GetSystemTray(void);
}

#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

#ifdef _DEBUG
	#pragma comment(lib, "SharedLite_V9D.lib"   )
	#pragma comment(lib, "UIX_Draw_V9D.lib"     )
	#pragma comment(lib, "UIX_Frame_V9D.lib"    )
	#pragma comment(lib, "SharedScanner_V9D.lib")
	#pragma comment(lib, "TaskScheduler_V9D.lib")
	#pragma comment(lib, "SharedNet_V9D.lib"    )
	#pragma comment(lib, "xZip_V9D.lib"         )
#else
	#pragma comment(lib, "SharedLite_V9.lib"    )
	#pragma comment(lib, "UIX_Draw_V9.lib"      )
	#pragma comment(lib, "UIX_Frame_V9.lib"     )
	#pragma comment(lib, "SharedScanner_V9.lib" )
	#pragma comment(lib, "TaskScheduler_V9.lib" )
	#pragma comment(lib, "SharedNet_V9.lib"     )
	#pragma comment(lib, "xZip_V9.lib"          )
#endif

#include <Shlobj.h>
#pragma comment(lib, "Shell32.lib")

#endif/*_TMONPRECOMPILEDHEADER_H_A17C1C15_A3FE_40bf_B1BA_5DD3C8B7A9C9_INCLUDED*/