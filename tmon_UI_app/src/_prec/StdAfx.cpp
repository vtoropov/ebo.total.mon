/*
	Created by Tech_dog (VToropov) on 23-Nov-2015 at 6:21:13pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring application precompiled headers implementation file.
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

namespace global
{
	CApplication& GetAppObjectRef(void)
	{
		static CApplication the_app;
		return the_app;
	}

	CStatusBarEx& GetStatusCtrlRef(void)
	{
		static CStatusBarEx ctrl_;
		return ctrl_;
	}

	CSettings&    GetSettings(void)
	{
		static CSettings cfg_;
		return cfg_;
	}

	using tmon::UI::components::CStatusType;

	VOID  SetErrorMessage(TErrorRef _err)
	{
		CAtlString src_(_err.Source());
		if (src_.IsEmpty())
			src_ = _T("#n/a");

		CAtlString cs_error;
		cs_error.Format(
				_T("Error: %s; code=0x%x; source=%s"),
				_err.GetDescription(),
				_err.GetHresult(),
				src_
			);
		SetErrorMessage(cs_error);
	}

	VOID  SetErrorMessage(LPCTSTR pszText)
	{
		GetStatusCtrlRef().SetText(
					pszText,
					CStatusType::eError
				);
	}

	VOID  SetInfoMessage (LPCTSTR pszText)
	{
		if (NULL == pszText)
			GetStatusCtrlRef().SetText(
					_T("Ready"),
					CStatusType::eInfo
				);
		else
			GetStatusCtrlRef().SetText(
						pszText,
						CStatusType::eInfo
					);
	}

	VOID  SetWaitMessage (LPCTSTR pszText)
	{
		GetStatusCtrlRef().SetText(
					pszText,
					CStatusType::eWaiting
				);
	}

	VOID  SetWarnMessage (LPCTSTR pszText)
	{
		GetStatusCtrlRef().SetText(
					pszText,
					CStatusType::eWarning
				);
	}

	CScannerEngine& GetScannerRef(void)
	{
		static CScannerEngine avs_(shared::avs::CScannerVendor::eZillya);
		return avs_;
	}

	CSystemTray&    GetSystemTray(void)
	{
		static CSystemTray sys_tray;
		return sys_tray;
	}
}