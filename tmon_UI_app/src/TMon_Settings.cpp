/*
	Created by Tech_dog (VToropov) on 28-Nov-2015 at 9:35:14pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring application settings class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Settings.h"

using namespace tmon::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

#include "Shared_AvsDefs.h"

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace common { namespace details
{
	class CSettings_Base
	{
	protected:
		mutable
		CRegistryStorage m_stg;
	protected:
		CSettings_Base(void) : m_stg(HKEY_CURRENT_USER){}
	};

	class CSettings_General : public CSettings_Base
	{
	public:
		CSettings_General(void){}
	public:
		HRESULT   Load(TGeneralCfg& _cfg)
		{
			LONG lValue = 0;
			m_stg.Load(
					this->_GeneralCfgFolder(),
					this->_NamedValueGenShowSplash(),
					lValue,
					1
				);
			_cfg.ShowSplash(!!lValue);

			m_stg.Load(
					this->_GeneralCfgFolder(),
					this->_NamedValueGenStartMinimized(),
					lValue
				);
			_cfg.StartMinimized(!!lValue);

			m_stg.Load(
					this->_GeneralCfgFolder(),
					this->_NamedValueGenStartOnLogon(),
					lValue
				);
			_cfg.StartOnLogon(!!lValue);

			HRESULT hr_ = S_OK;
			return  hr_;
		}

		HRESULT   Save(const TGeneralCfg& _cfg)
		{
			m_stg.Save(
					this->_GeneralCfgFolder(),
					this->_NamedValueGenShowSplash(),
					static_cast<LONG>(_cfg.ShowSplash())
				);

			m_stg.Save(
					this->_GeneralCfgFolder(),
					this->_NamedValueGenStartMinimized(),
					static_cast<LONG>(_cfg.StartMinimized())
				);

			m_stg.Save(
					this->_GeneralCfgFolder(),
					this->_NamedValueGenStartOnLogon(),
					static_cast<LONG>(_cfg.StartOnLogon())
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		CAtlString    _GeneralCfgFolder(void)             const { return CAtlString(_T("Settings\\General")); }
		CAtlString    _NamedValueGenShowSplash(void)      const { return CAtlString(_T("ShowSplash"));        }
		CAtlString    _NamedValueGenStartOnLogon(void)    const { return CAtlString(_T("StartOnLogon"));      }
		CAtlString    _NamedValueGenStartMinimized(void)  const { return CAtlString(_T("StartMinimized"));    }
	};

	class CSettings_Log : public CSettings_Base
	{
	public:
		CSettings_Log(void){}
	public:
		HRESULT   Load(TLogCfg& _cfg)
		{
			LONG lValue = 0;
			m_stg.Load(
					this->_LogCfgFolder(),
					this->_NamedValueLogEnabled(),
					lValue,
					1
				);
			_cfg.Enabled(!!lValue);

			m_stg.Load(
					this->_LogCfgFolder(),
					this->_NamedValueLogLevel(),
					lValue
				);
			_cfg.Level((INT)lValue);

			CAtlString cs_value;

			m_stg.Load(
					this->_LogCfgFolder(),
					this->_NamedValueLogFolder(),
					cs_value
				);
			_cfg.Folder(cs_value);

			HRESULT hr_ = S_OK;
			return  hr_;
		}

		HRESULT   Save(const TLogCfg& _cfg)
		{
			m_stg.Save(
					this->_LogCfgFolder(),
					this->_NamedValueLogEnabled(),
					static_cast<LONG>(_cfg.Enabled())
				);

			m_stg.Save(
					this->_LogCfgFolder(),
					this->_NamedValueLogLevel(),
					static_cast<LONG>(_cfg.Level())
				);

			m_stg.Save(
					this->_LogCfgFolder(),
					this->_NamedValueLogFolder(),
					_cfg.Folder()
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		CAtlString    _LogCfgFolder(void)                 const { return CAtlString(_T("Settings\\Log"));     }
		CAtlString    _NamedValueLogEnabled(void)         const { return CAtlString(_T("Enabled"));           }
		CAtlString    _NamedValueLogLevel(void)           const { return CAtlString(_T("Level"));             }
		CAtlString    _NamedValueLogFolder(void)          const { return CAtlString(_T("Folder"));            }
	};

	class CSettings_Pro : public CSettings_Base
	{
	public:
		CSettings_Pro(void){}
	public:
		HRESULT   Load(TProtectCfg& _cfg)
		{
			LONG lValue = 0;
			m_stg.Load(
					this->_ProtectCfgFolder(),
					this->_NamedValueDefAction(),
					lValue,
					1
				);
			const CScanAction::_e id_ = static_cast<CScanAction::_e>(lValue);
			const bool bValid = _cfg.DefaultAction(id_);
			if (!bValid)
				_cfg.DefaultAction(CScanAction::eActionNone);

			m_stg.Load(
					this->_ProtectCfgFolder(),
					this->_NamedValueReporting(),
					lValue,
					0
				);
			_cfg.Reporting(!!lValue);

			m_stg.Load(
					this->_ProtectCfgFolder(),
					this->_NamedValueRemoteCtrl(),
					lValue,
					0
				);
			_cfg.RemoteControl(!!lValue);

			HRESULT hr_ = S_OK;
			return  hr_;
		}

		HRESULT   Save(const TProtectCfg& _cfg)
		{
			const CScanAction act_ = _cfg.DefaultAction();
			m_stg.Save(
					this->_ProtectCfgFolder(),
					this->_NamedValueDefAction(),
					static_cast<LONG>(act_.Identifier())
				);

			m_stg.Save(
					this->_ProtectCfgFolder(),
					this->_NamedValueReporting(),
					static_cast<LONG>(_cfg.Reporting())
				);

			m_stg.Save(
					this->_ProtectCfgFolder(),
					this->_NamedValueRemoteCtrl(),
					static_cast<LONG>(_cfg.RemoteControl())
				);

			HRESULT hr_ = S_OK;
			return  hr_;
		}
	private:
		CAtlString    _ProtectCfgFolder(void)             const { return CAtlString(_T("Settings\\Protect")); }
		CAtlString    _NamedValueDefAction(void)          const { return CAtlString(_T("DefaultAction"));     }
		CAtlString    _NamedValueReporting(void)          const { return CAtlString(_T("Reporting"));         }
		CAtlString    _NamedValueRemoteCtrl(void)         const { return CAtlString(_T("RemoteCtrl"));        }
	};

	static LPCTSTR pszDefaultLogFolder = _T(".\\logs\\");
}}}

/////////////////////////////////////////////////////////////////////////////

CSettings::CGeneral::CGeneral(void) : m_splash(true), m_onlogon(false), m_minimized(false)
{
}

/////////////////////////////////////////////////////////////////////////////

bool      CSettings::CGeneral::IsSilentStart(void)const
{
	const bool bSilent = (!this->ShowSplash() || this->StartMinimized());
	return bSilent;
}

bool      CSettings::CGeneral::ShowSplash(void)const
{
	return m_splash;
}

VOID      CSettings::CGeneral::ShowSplash(const bool _set)
{
	m_splash = _set;
}

bool      CSettings::CGeneral::StartMinimized(void)const
{
	return m_minimized;
}

VOID      CSettings::CGeneral::StartMinimized(const bool _set)
{
	m_minimized = _set;
}

bool      CSettings::CGeneral::StartOnLogon(void)const
{
	return m_onlogon;
}

VOID      CSettings::CGeneral::StartOnLogon(const bool _set)
{
	m_onlogon = _set;
}

/////////////////////////////////////////////////////////////////////////////

CSettings::CLog::CLog(void) : m_enabled(true), m_level((INT)CLog::eThreatsOnly), m_folder(details::pszDefaultLogFolder)
{
}

/////////////////////////////////////////////////////////////////////////////

CAtlString  CSettings::CLog::Folder(void)const
{
	return m_folder;
}

VOID        CSettings::CLog::Folder(LPCTSTR _folder)
{
	m_folder = _folder;
	if (m_folder.IsEmpty())
		m_folder = details::pszDefaultLogFolder;
}

bool        CSettings::CLog::Enabled(void)const
{
	return m_enabled;
}

VOID        CSettings::CLog::Enabled(const bool _enabled)
{
	m_enabled = _enabled;
}

INT         CSettings::CLog::Level(void)const
{
	return m_level;
}

HRESULT     CSettings::CLog::Level(const INT _level)
{
	if (_level < CLog::eAll)
	{
		m_level = CLog::eAll;
		return E_INVALIDARG;
	}
	if (_level > CLog::eThreatsOnly)
	{
		m_level = CLog::eThreatsOnly;
		return E_INVALIDARG;
	}

	m_level = _level;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

CSettings::CProtection::CProtection(void) : m_action(false), m_bReport(false), m_bRemote(false)
{
}

/////////////////////////////////////////////////////////////////////////////

CScanAction CSettings::CProtection::DefaultAction(void)const
{
	return m_action;
}

VOID        CSettings::CProtection::DefaultAction(const INT nIndex)
{
	CScanActionEnum enum_;
	
	const CScanAction& act_ = enum_.Item(nIndex);
	m_action = act_;
}

bool        CSettings::CProtection::DefaultAction(const CScanAction::_e _id)
{
	CScanActionEnum enum_;
	
	const CScanAction& act_ = enum_.Find(_id);
	m_action = act_;
	return act_.IsValid();
}

bool        CSettings::CProtection::RemoteControl(void)const
{
	return m_bRemote;
}

VOID        CSettings::CProtection::RemoteControl(const bool _remote)
{
	m_bRemote = _remote;
}

bool        CSettings::CProtection::Reporting(void)const
{
	return m_bReport;
}

VOID        CSettings::CProtection::Reporting(const bool _report)
{
	m_bReport = _report;
}

/////////////////////////////////////////////////////////////////////////////

CSettings::CSettings(void)
{
}

CSettings::~CSettings(void)
{
}

/////////////////////////////////////////////////////////////////////////////

const
TGeneralCfg& CSettings::General(void)const
{
	return m_general;
}

TGeneralCfg& CSettings::General(void)
{
	return m_general;
}

HRESULT      CSettings::Load(void)
{
	details::CSettings_General gen_;
	details::CSettings_Log     log_;
	details::CSettings_Pro     pro_;

	HRESULT hr_ = S_OK;

	hr_ = gen_.Load(m_general);
	hr_ = log_.Load(m_log);
	hr_ = pro_.Load(m_protect);

	return  hr_;
}

const
TLogCfg&     CSettings::Log(void)const
{
	return m_log;
}

TLogCfg&     CSettings::Log(void)
{
	return m_log;
}

const
TProtectCfg& CSettings::Protection(void)const
{
	return m_protect;
}

TProtectCfg& CSettings::Protection(void)
{
	return m_protect;
}

HRESULT      CSettings::Save(void)
{
	details::CSettings_General gen_;
	details::CSettings_Log     log_;
	details::CSettings_Pro     pro_;

	HRESULT hr_ = S_OK;
	
	hr_ = gen_.Save(m_general);
	hr_ = log_.Save(m_log);
	hr_ = pro_.Save(m_protect);

	return  hr_;
}