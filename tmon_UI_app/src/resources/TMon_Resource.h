//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TMon_Resource.rc
//
// Created by Tech_dog (VToropov) on 23-Nov-2015 at 6:26:01pm, GMT+7, Phuket, Rawai, Monday
// This is Total Monitoring Application Resource Declaration file.
//

#define IDD_TMON_MAIN_DLG                        101
#define IDR_TMON_MAIN_DLG_ICON                   103
#define IDR_TMON_MAIN_DLG_BKGND                  105
#define IDR_TMON_MAIN_DLG_HEADER                 107
#define IDC_TMON_MAIN_DLG_CLOSE                  109
#define IDR_TMON_MAIN_DLG_CLOSE_NORM             111
#define IDR_TMON_MAIN_DLG_CLOSE_HOT              113
#define IDR_TMON_MAIN_DLG_CLOSE_PRES             115
#define IDR_TMON_MAIN_DLG_MIN_NORM               117
#define IDR_TMON_MAIN_DLG_MIN_HOT                119
#define IDR_TMON_MAIN_DLG_MIN_PRES               121
#define IDC_TMON_MAIN_DLG_MIN                    123
#define IDR_TMON_MAIN_DLG_STAT_BKGND             125
#define IDR_TMON_MAIN_DLG_STAT_STRIP             127
#define IDR_TMON_MAIN_DLG_WARN_IMAGE             129
#define IDR_TMON_MAIN_DLG_WARN_I16PX             131
#define IDR_TMON_MAIN_DLG_WARN_I24PX             133

#define IDR_TMON_CHILD_PANEL_BKGND               135
#define IDR_TMON_CHILD_PANEL_TRANS               137

/////////////////////////////////////////////////////////////////////////////
//
// Main Dialog Tab Strip Control
//
/////////////////////////////////////////////////////////////////////////////

#define IDC_TMON_MAIN_DLG_TABS                   213
#define IDR_TMON_MAIN_DLG_TABS_BKGND             215
#define IDC_TMON_MAIN_DLG_TAB_HOME               217
#define IDR_TMON_MAIN_DLG_TAB_HOME_NORM          219
#define IDR_TMON_MAIN_DLG_TAB_HOME_HOT           221
#define IDC_TMON_MAIN_DLG_TAB_SCAN               223
#define IDR_TMON_MAIN_DLG_TAB_SCAN_NORM          225
#define IDR_TMON_MAIN_DLG_TAB_SCAN_HOT           227
#define IDC_TMON_MAIN_DLG_TAB_THRT               229
#define IDR_TMON_MAIN_DLG_TAB_THRT_NORM          231
#define IDR_TMON_MAIN_DLG_TAB_THRT_HOT           233
#define IDC_TMON_MAIN_DLG_TAB_UPDT               235
#define IDR_TMON_MAIN_DLG_TAB_UPDT_NORM          237
#define IDR_TMON_MAIN_DLG_TAB_UPDT_HOT           239
#define IDC_TMON_MAIN_DLG_TAB_SETS               241
#define IDR_TMON_MAIN_DLG_TAB_SETS_NORM          243
#define IDR_TMON_MAIN_DLG_TAB_SETS_HOT           245
#define IDC_TMON_MAIN_DLG_TAB_HELP               247
#define IDR_TMON_MAIN_DLG_TAB_HELP_NORM          249
#define IDR_TMON_MAIN_DLG_TAB_HELP_HOT           251

/////////////////////////////////////////////////////////////////////////////
//
// Splash Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_SPLASH_DLG                      301
#define IDR_TMON_SPLASH_DLG_BKGND                303
#define IDD_TMON_SPLASH_PANEL                    305

/////////////////////////////////////////////////////////////////////////////
//
// Scheduler Task Property Dialog
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_TASK_DLG                        331
#define IDR_TMON_TASK_DLG_BKGND                  333

/////////////////////////////////////////////////////////////////////////////
//
// Home Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_HOME_TAB_PAGE                  1001
#define IDC_TMON_HOME_BTN_PROTECT               1003
#define IDR_TMON_HOME_BTN_PROTECT_NORM          1005
#define IDR_TMON_HOME_BTN_PROTECT_BROKEN        1007
#define IDC_TMON_HOME_BTN_UPDATE                1009
#define IDR_TMON_HOME_BTN_UPDATE_NORM           1011
#define IDC_TMON_HOME_BTN_THREAT                1013
#define IDR_TMON_HOME_BTN_THREAT_NORM           1015
#define IDR_TMON_HOME_BTN_THREAT_FOUND          1017
#define IDC_TMON_HOME_BTN_SCANNER               1019
#define IDR_TMON_HOME_BTN_SCANNER_NORM          1021

/////////////////////////////////////////////////////////////////////////////
//
// Scan Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_SCAN_TAB_PAGE                  2001
#define IDR_TMON_SCAN_TAB_BKGND                 2003
#define IDC_TMON_SCAN_EDT_FILES                 2005
#define IDC_TMON_SCAN_BTN_BROWSE                2007
#define IDC_TMON_SCAN_BTN_SCAN                  2009
#define IDC_TMON_SCAN_TAG_QUICK                 2011
#define IDR_TMON_SCAN_TAG_QUICK_SEL             2013
#define IDR_TMON_SCAN_TAG_QUICK_NOT             2015
#define IDR_TMON_SCAN_PAN_QUICK_BKG             2017
#define IDC_TMON_SCAN_TAG_CUSTM                 2019
#define IDR_TMON_SCAN_TAG_CUSTM_SEL             2021
#define IDR_TMON_SCAN_TAG_CUSTM_NOT             2023
#define IDR_TMON_SCAN_PAN_CUSTM_BKG             2025
#define IDD_TMON_SCAN_QUICK_PANEL               2027
#define IDR_TMON_SCAN_QUICK_PANEL_BKGND         IDR_TMON_SCAN_PAN_QUICK_BKG
#define IDD_TMON_SCAN_CUSTM_PANEL               2029
#define IDR_TMON_SCAN_CUSTM_PANEL_BKGND         IDR_TMON_SCAN_PAN_CUSTM_BKG
#define IDC_TMON_SCAN_QUICK_START               2031
#define IDC_TMON_SCAN_EVT_BEGIN                 2033
#define IDC_TMON_SCAN_EVT_END                   2035
#define IDC_TMON_SCAN_TAG_FULL                  2037
#define IDR_TMON_SCAN_TAG_FULL_SEL              2039
#define IDR_TMON_SCAN_TAG_FULL_NOT              2041
#define IDR_TMON_SCAN_PAN_FULL_BKG              2043
#define IDD_TMON_SCAN_FULL_PANEL                2045
#define IDR_TMON_SCAN_FULL_PANEL_BKGND          IDR_TMON_SCAN_PAN_FULL_BKG
#define IDC_TMON_SCAN_FULL_START                2047

/////////////////////////////////////////////////////////////////////////////
//
// Threat Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_THREAT_TAB_PAGE                3001
#define IDR_TMON_THREAT_TAB_BKGND               3003
#define IDC_TMON_THREAT_BTN_APPLY               3005
#define IDC_TMON_THREAT_LST_ACT                 3007
#define IDD_TMON_THREAT_PANEL                   3009
#define IDR_TMON_THREAT_PANEL_BKGND             IDR_TMON_CHILD_PANEL_BKGND

/////////////////////////////////////////////////////////////////////////////
//
// Update Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_UPDATE_TAB_PAGE                4001
#define IDR_TMON_UPDATE_TAB_BKGND               4003
#define IDR_TMON_UPDATE_DIG_STRIP               4005
#define IDC_TMON_UPDATE_BTN_NOW                 4007
#define IDD_TMON_UPDATE_PRG_PANEL               4009
#define IDR_TMON_UPDATE_PRG_BKGND               IDR_TMON_CHILD_PANEL_BKGND

/////////////////////////////////////////////////////////////////////////////
//
// Settings Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_SETUP_TAB_PAGE                 5001
#define IDR_TMON_SETUP_TAB_BKGND                5003
#define IDC_TMON_SETUP_TABSET                   5005
#define IDR_TMON_SETUP_TAB_BANGEN               4999
#define IDR_TMON_SETUP_TAB_BANPRO               4997
#define IDR_TMON_SETUP_TAB_BANSCH               4995
#define IDR_TMON_SETUP_TAB_BANRAD               4993            

#define IDD_TMON_SETUP_WEB_PAGE                 5007
#define IDC_TMON_SETUP_WEB_ENABLE               5009
#define IDC_TMON_SETUP_WEB_URL                  5011
#define IDC_TMON_SETUP_WEB_ADD                  5013
#define IDC_TMON_SETUP_WEB_REMOVE               5015
#define IDC_TMON_SETUP_WEB_LIST                 5017
#define IDC_TMON_SETUP_WEB_APPLY                5019

#define IDD_TMON_SETUP_GEN_PAGE                 5101
#define IDC_TMON_SETUP_GEN_SPLASH               5103
#define IDC_TMON_SETUP_GEN_LOGON                5105
#define IDC_TMON_SETUP_GEN_MINMZD               5107

#define IDD_TMON_SETUP_LOG_PAGE                 5201
#define IDC_TMON_SETUP_LOG_LEVEL                5203
#define IDC_TMON_SETUP_LOG_ENABLE               5205
#define IDC_TMON_SETUP_LOG_FOLDER               5207
#define IDC_TMON_SETUP_LOG_BROWSE               5209
#define IDC_TMON_SETUP_LOG_IMAGE                5211

#define IDD_TMON_SETUP_PRO_PAGE                 5301
#define IDC_TMON_SETUP_PRO_ENGINE               5303
#define IDC_TMON_SETUP_PRO_ACTION               5305
#define IDC_TMON_SETUP_PRO_EXCLAM               5307
#define IDC_TMON_SETUP_PRO_EXTEXT               5309
#define IDC_TMON_SETUP_PRO_APPLY                5311
#define IDC_TMON_SETUP_PRO_REPORT               5313
#define IDC_TMON_SETUP_PRO_RMCTRL               5315

#define IDD_TMON_SETUP_JOB_PAGE                 5401
#define IDC_TMON_SETUP_JOB_FOLDER               5403
#define IDC_TMON_SETUP_JOB_BROWSE               5405
#define IDC_TMON_SETUP_JOB_TITLE                5407
#define IDC_TMON_SETUP_JOB_FREQ                 5409
#define IDC_TMON_SETUP_JOB_TIME                 5411
#define IDC_TMON_SETUP_JOB_DAYS                 5413
#define IDC_TMON_SETUP_JOB_ACTIVE               5415
#define IDC_TMON_SETUP_JOB_ADD                  5417
#define IDC_TMON_SETUP_JOB_LIST                 5419
#define IDC_TMON_SETUP_JOB_EDIT                 5421
#define IDC_TMON_SETUP_JOB_DELETE               5423
#define IDC_TMON_SETUP_JOB_REFRESH              5425

/////////////////////////////////////////////////////////////////////////////
//
// Support Tab Page
//
/////////////////////////////////////////////////////////////////////////////

#define IDD_TMON_HELP_TAB_PAGE                  6001
#define IDR_TMON_HELP_TAB_BKGND                 6003
#define IDS_TMON_HELP_WEB_URL                   6005
#define IDS_TMON_HELP_WEB_TITLE                 6007


