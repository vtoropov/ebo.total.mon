#ifndef _TMONNOUIINITIALIZER_H_F6385B8D_4F72_4067_8C31_9BF4CB0F8E62_INCLUDED
#define _TMONNOUIINITIALIZER_H_F6385B8D_4F72_4067_8C31_9BF4CB0F8E62_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Jan-2016 at 10:57:54am, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring AV UI-less Initializer class declaration file.
*/
#include "Shared_AvsDefs.h"
#include "Shared_AvsError.h"
#include "Shared_AvsIface.h"

namespace tmon { namespace common
{
	using shared::avs::TVirusDefinitionFiles;
	using shared::avs::IScannerEventSink;
	using shared::avs::CScanError;

	class CAvsInitializer : public IScannerEventSink
	{
	private:
		DWORD       m_threadId;
		bool        m_started;
	public:
		CAvsInitializer(void);
		~CAvsInitializer(void);
	public:
		HRESULT     Start(void);
	private: // IScannerEventSink
		VOID        IScannerEvent_OnEngineInitFinish(CScanError) override sealed;
	};
}}

#endif/*_TMONNOUIINITIALIZER_H_F6385B8D_4F72_4067_8C31_9BF4CB0F8E62_INCLUDED*/