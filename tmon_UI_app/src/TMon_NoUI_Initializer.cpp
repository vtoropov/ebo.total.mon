/*
	Created by Tech_dog (VToropov) on 18-Jan-2016 at 11:05:05am, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring AV UI-less Initializer class implementation file.
*/
#include "StdAfx.h"
#include "TMon_NoUI_Initializer.h"
#include "TMon_Resource.h"
#include "TMon_SysTray.h"
#include "TMon_Settings.h"

using namespace tmon::common;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace common { namespace details
{
	TVirusDefinitionFiles& CAvsInitializer_VirusDefFiles(void)
	{
		static TVirusDefinitionFiles vs_def_files_;
		return vs_def_files_;
	}

	static VOID CALLBACK   CAvsInitializer_TimerProc( HWND, UINT, UINT_PTR, DWORD)
	{
		static size_t ptr_ = 0;

		CAtlString cs_msg;

		TVirusDefinitionFiles& vs_def_files = details::CAvsInitializer_VirusDefFiles();
		if (ptr_ < vs_def_files.size())
		{
			const DWORD nPercent = DWORD((FLOAT(ptr_ + 1) / FLOAT(vs_def_files.size())) * 100);
			cs_msg.Format(
						_T("Loading virus definition: %s (%d%%)"),
						vs_def_files[ptr_].GetString(),
						nPercent
					);
		}
		else
			cs_msg = _T("Optimizing virus database...");

		global::GetSystemTray().SetTooltip(cs_msg);
		ptr_ += 1;
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CAvsInitializer::CAvsInitializer(void) : m_started(false), m_threadId(::GetCurrentThreadId())
{
	global::GetScannerRef().Events().Subscribe(this);
}

CAvsInitializer::~CAvsInitializer(void)
{
	global::GetScannerRef().Events().Unsubscribe(this);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CAvsInitializer::Start(void)
{
	if (m_started)
		return HRESULT_FROM_WIN32(ERROR_INVALID_STATE);

	m_started = true;

	MSG  msg_ = {0};
	BOOL ret_ = FALSE;

	global::GetSystemTray().ShowIcon(
				_T("Starting initialization...")
			);

	global::GetScannerRef().Initialize();

	TVirusDefinitionFiles& vs_def_files = details::CAvsInitializer_VirusDefFiles();
	vs_def_files = global::GetScannerRef().VirusDatabase().Files();

	DWORD dwPeriod = 500;
	if (vs_def_files.size() > 50)
		dwPeriod = 50 * 1000 / vs_def_files.size();

	const UINT_PTR evtId  = ::GetTickCount();
	const UINT_PTR timer_ = ::SetTimer(
					NULL,
					evtId,
					dwPeriod,
					details::CAvsInitializer_TimerProc
				);

	while( (ret_ = ::GetMessage( &msg_, NULL, 0, 0 )) > 0)
	{
		::TranslateMessage(&msg_);
		::DispatchMessage(&msg_);
	}

	::KillTimer(NULL, timer_);

	global::GetSystemTray().SetTooltip(
				_T("Total Monitoring protects your system")
			);

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID     CAvsInitializer::IScannerEvent_OnEngineInitFinish(CScanError)
{
	::PostThreadMessage(
			m_threadId,
			WM_QUIT,
			0,
			0
		);
}