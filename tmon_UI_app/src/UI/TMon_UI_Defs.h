#ifndef _TMONUIDEFS_H_CE811783_BDB6_471e_A7A9_4652262C6953_INCLUDED
#define _TMONUIDEFS_H_CE811783_BDB6_471e_A7A9_4652262C6953_INCLUDED
/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 12:51:50am, GMT+7, Phuket, Rawai, Thursday;
	This is Total Monitoring application UI common definition declaration file.
*/
#define WM_SHOWMAINFORM (WM_USER + 1)

namespace tmon { namespace UI
{
	interface ITabPageCallback
	{
		virtual LRESULT TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) PURE;
	};

	class CTabPageBase:
		public  ::ATL::CDialogImpl<CTabPageBase>
	{
		typedef ::ATL::CDialogImpl<CTabPageBase> TBaseDialog;
	public:
		const UINT IDD;
	protected:
		bool                m_bInitilized;
		INT                 m_nIndex;
		::WTL::CTabCtrl&    m_ctrl_ref;      // parent tabbed control reference
		ITabPageCallback&   m_evt_sink;
	protected:
		CTabPageBase(const UINT RID, ::WTL::CTabCtrl&, ITabPageCallback&);
		virtual ~CTabPageBase(void);
	public:
		BEGIN_MSG_MAP(CTabPageBase)
			MESSAGE_HANDLER (WM_INITDIALOG,  OnPageInit )
			MESSAGE_HANDLER (WM_PAINT,       OnPagePaint)
			m_evt_sink.TabPage_OnEvent(uMsg, wParam, lParam, bHandled);
			if (bHandled)
				return TRUE;
		END_MSG_MAP()
	public:
		virtual CAtlString  GetPageTitle(void) const PURE;
		virtual VOID        UpdateData  (void)       {};
		virtual VOID        UpdateLayout(void)       {};
		virtual HRESULT     Validate(void)     const { return E_NOTIMPL; };
	protected:
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
		virtual LRESULT     OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	public:
		INT                 Index(void)const;
		VOID                Index(const INT);
	};

	VOID       AlignComboHeightTo(const CWindow& _host, const UINT _combo_id, const UINT _height);
	VOID       AlignCtrlsByHeight(const CWindow& _host, const UINT _ctrl_0, const UINT _ctrl_1, const SIZE& _shifts);

	class CCtrlAutoState
	{
	private:
		const
		UINT      m_ctrlId;
		CWindow   m_host;
		bool      m_bEnabled; // the old enable status that will be restored on this class object destruction
	public:
		CCtrlAutoState(const CWindow& _host, const UINT ctrlId, const bool bEnabled);
		~CCtrlAutoState(void);
	};
}}

#endif/*_TMONUIDEFS_H_CE811783_BDB6_471e_A7A9_4652262C6953_INCLUDED*/