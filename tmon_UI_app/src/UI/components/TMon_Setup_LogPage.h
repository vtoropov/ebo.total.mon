#ifndef _TMONSETUPLOGPAGE_H_3621B3EF_0622_4207_846C_DB2C50D9A118_INCLUDED
#define _TMONSETUPLOGPAGE_H_3621B3EF_0622_4207_846C_DB2C50D9A118_INCLUDED
/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 6:56:09pm, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring Log Setup Tab Page class declaration file.
*/
#include "TMon_UI_Defs.h"

namespace tmon { namespace UI { namespace components
{
	class CLogSetupPage:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase TBasePage;
	public:
		CLogSetupPage(::WTL::CTabCtrl&);
		~CLogSetupPage(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}

#endif/*_TMONSETUPLOGPAGE_H_3621B3EF_0622_4207_846C_DB2C50D9A118_INCLUDED*/