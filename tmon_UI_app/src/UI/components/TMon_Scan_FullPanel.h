#ifndef _TMONSCANFULLPANEL_H_00CD6997_426A_4ed4_8F5B_330369C6FB2E_INCLUDED
#define _TMONSCANFULLPANEL_H_00CD6997_426A_4ed4_8F5B_330369C6FB2E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 28-Feb-2016 at 6:28:02pm, GMT+7, Phuket, Sunday;
	This is Total Monitoring Full Scan Panel class declaration file.
*/
#include "TMon_Scan_BasePanel.h"

#include "Shared_SystemError.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::CLabel;

	using shared::lite::common::CSysError;

	class CFullScanPanel :
		public CBaseScanPanel
	{
		typedef CBaseScanPanel TScanBase;
	public:
		CFullScanPanel(IRenderer& _parent);
		~CFullScanPanel(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IScannerEventSink
		VOID        IScannerEvent_OnEngineInitFinish(CScanError) override sealed;
		VOID        IScannerEvent_OnLock(const DWORD dwLockOwnerId) override sealed;
		VOID        IScannerEvent_OnScanProcessFinish(VOID) override sealed;
		VOID        IScannerEvent_OnUnlock(const DWORD dwLockOwnerId) override sealed;
	public:
		VOID        UpdateCtrlState(const bool bEnabled);
	};
}}}

#endif/*_TMONSCANFULLPANEL_H_00CD6997_426A_4ed4_8F5B_330369C6FB2E_INCLUDED*/