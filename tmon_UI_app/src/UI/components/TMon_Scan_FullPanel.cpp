/*
	Created by Tech_dog (VToropov) on 28-Feb-2016 at 7:21:01pm, GMT+7, Phuket, Sunday;
	This is Total Monitoring Full Scan Panel class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Scan_FullPanel.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon::UI::components;
using namespace ex_ui::controls;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;

#include "Shared_AvsIface.h"

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CFullScan_Init
	{
	public:
	};

	class CFullScan_Handler
	{
	private:
		const
		CWindow&      m_page_ref;
		DWORD&        m_locker_ref;
	public:
		CFullScan_Handler(CWindow& page_ref, DWORD& _locker_ref) : m_page_ref(page_ref), m_locker_ref(_locker_ref){}
	public:
		BOOL          OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;

			if (false){}
			else if (0 == wCtrlId)
			{
				
			}
			else
				bHandled = FALSE;

			return bHandled;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CFullScanPanel::CFullScanPanel(IRenderer& _parent):
	TScanBase  (IDD_TMON_SCAN_FULL_PANEL, IDR_TMON_SCAN_FULL_PANEL_BKGND)
{
	_parent;
}

CFullScanPanel::~CFullScanPanel(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CFullScanPanel::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TScanBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	TScanBase::m_cap_label.Text(
			_T("Scan the all files on this computer. This operation can take a long period of time.")
		);

	CWindow host_ = TScanBase::GetWindow_Ref();

	this->UpdateCtrlState(true);

	return hr_;
}

HRESULT     CFullScanPanel::Destroy(void)
{
	HRESULT hr_ = TScanBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT     CFullScanPanel::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			details::CFullScan_Handler handler_(
						TScanBase::GetWindow_Ref(),
						TScanBase::m_locker
					);
			if (IDC_TMON_SCAN_FULL_START == ctrlId)
			{
				bHandled = TRUE;
				CScannerLocker locker_(
							global::GetScannerRef(),
							TScanBase::m_locker
						);

				HRESULT hr_ = locker_.Lock();
				if (FAILED(hr_))
				{
					global::SetErrorMessage(locker_.Error());
					return 0;
				}

				CGenericDrive drive_;
				const TDriveList lst_ = drive_.Enumerate(CGenericDriveType::eFixed);
				if (drive_.Error())
				{
					global::SetErrorMessage(drive_.Error());
					return 0;
				}

				for ( TDriveList::const_iterator it_ = lst_.begin(); it_!= lst_.end(); ++it_)
				{
					CAtlString cs_target = *it_;
					CAtlString cs_msg;
					cs_msg.Format(
							_T("Preparing file list for logical drive: %s"),
							cs_target
						);
					TScanBase::m_sta_label.Text(
							cs_msg
						);
					hr_ = global::GetScannerRef().ScanFolderOrFile(cs_target);

					if (FAILED(hr_))
					{
						global::SetErrorMessage(
							global::GetScannerRef().Error()
						);
						break;
					}
				}
			}
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

VOID        CFullScanPanel::IScannerEvent_OnEngineInitFinish(CScanError _error)
{
	this->UpdateCtrlState(true);
	TScanBase::IScannerEvent_OnEngineInitFinish(_error);
}

VOID        CFullScanPanel::IScannerEvent_OnLock(const DWORD dwLockOwnerId)
{
	dwLockOwnerId; // doesn't care who is locker; just disables all scan related controls;
	this->UpdateCtrlState(false);
}

VOID        CFullScanPanel::IScannerEvent_OnScanProcessFinish(VOID)
{
	TScanBase::IScannerEvent_OnScanProcessFinish();
	if (TScanBase::m_locker) // this module is the owner of a scan process;
		global::GetScannerRef().Unlock(TScanBase::m_locker);
	else
		this->UpdateCtrlState(true);
}

VOID        CFullScanPanel::IScannerEvent_OnUnlock(const DWORD dwLockOwnerId)
{
	if (TScanBase::m_locker == dwLockOwnerId)
		TScanBase::m_locker = 0;
	this->UpdateCtrlState(true); // the scanner has been unlocked; now a scan is enabled
}

/////////////////////////////////////////////////////////////////////////////

VOID        CFullScanPanel::UpdateCtrlState(const bool bEnabled)
{
	CWindow host_ = TScanBase::GetWindow_Ref();
	CWindow ctrl_ = host_.GetDlgItem(IDC_TMON_SCAN_FULL_START);
	if (ctrl_)
		ctrl_.EnableWindow(bEnabled && global::GetScannerRef().IsInitialized());
}