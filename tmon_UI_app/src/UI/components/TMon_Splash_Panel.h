#ifndef _TMONSPLASHPANEL_H_83406BEB_AD72_4b3c_B628_B228051808D2_INCLUDED
#define _TMONSPLASHPANEL_H_83406BEB_AD72_4b3c_B628_B228051808D2_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Dec-2015 at 1:09:54pm, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring Splash panel class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_Renderer.h"
#include "UIX_Label.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CLabel;

	using shared::avs::TVirusDefinitionFiles;

	class CSplashPanel :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify
	{
		typedef CPanelBase TPageBase;
	private:
		CBackgroundTileRenderer  m_bkg_rnd;
		CLabel                   m_label;
		UINT_PTR                 m_timer;
		TVirusDefinitionFiles    m_files;
		WTL::CProgressBarCtrl    m_progress;
	public:
		CSplashPanel(void);
		~CSplashPanel(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	};
}}}

#endif/*_TMONSPLASHPANEL_H_83406BEB_AD72_4b3c_B628_B228051808D2_INCLUDED*/