/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 12:45:57am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Setup Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_TabPage_Setup.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CTabPageSetup_Layout
	{
	private:
		CWindow&     m_page_ref;
		RECT         m_client_area;
	public:
		CTabPageSetup_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT         GetBannerRect(void)const
		{
			RECT rc_ = {
					0,
					0,
					282, // from PSD
					455  // from PSD
				};
			return rc_;
		}

		RECT         GetSubTabsRect(void)const
		{
			const INT nLeft = 278;
			const INT nTop  =  12;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 545,
					nTop  + 427
				};
			return rc_;
		}

		HRESULT     UpdateBanner(CImage& _banner, const INT _tab_ndx)
		{
			HRESULT hr_ = S_OK;
			switch (_tab_ndx)
			{
			case 0:    hr_ = _banner.SetImage(IDR_TMON_SETUP_TAB_BANGEN); break;
			case 1:    hr_ = _banner.SetImage(IDR_TMON_SETUP_TAB_BANPRO); break;
			case 2:    hr_ = _banner.SetImage(IDR_TMON_SETUP_TAB_BANSCH); break;
			case 3:    hr_ = _banner.SetImage(IDR_TMON_SETUP_TAB_BANRAD); break;
			default:   hr_ = _banner.SetImage(IDR_TMON_SETUP_TAB_BANGEN); break;
			}
			hr_ = _banner.UpdateLayout();
			return hr_;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageSetup::CTabPageSetup(IRenderer& _parent):
	TPageBase  (IDD_TMON_SETUP_TAB_PAGE , *this),
	m_bkg_rnd  (IDR_TMON_SETUP_TAB_BKGND, TPageBase::GetWindow_Ref()),
	m_banner   (NULL),
	m_tabs     (*this)
{
	_parent;
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);
	m_banner.SetParentRendererPtr(&m_bkg_rnd);
}

CTabPageSetup::~CTabPageSetup(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageSetup::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CTabPageSetup_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetBannerRect();
		hr_ = m_banner.Create(
					host_,
					rc_
				);
	}
	{
		const RECT rc_ = layout_.GetSubTabsRect();
		hr_ = m_tabs.Create(
					host_,
					rc_
				);
	}

	return hr_;
}

HRESULT    CTabPageSetup::Destroy(void)
{
	m_tabs.Destroy();
	m_banner.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageSetup::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam;
	switch (uMsg)
	{
	case WM_NOTIFY:
		{
			LPNMHDR pNotifyHeader = reinterpret_cast<LPNMHDR>(lParam);
			if (pNotifyHeader == NULL){}
			else if (pNotifyHeader->code == TCN_SELCHANGE)
			{
				m_tabs.UpdateLayout();

				CWindow host_ = TPageBase::GetWindow_Ref();

				details::CTabPageSetup_Layout layout_(host_);
				layout_.UpdateBanner(
							m_banner,
							m_tabs.SelectedIndex()
						);
			}
		}break;
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageSetup::IControlNotify_OnClick(const UINT ctrlId)
{
	if (m_tabs.Identifier() == ctrlId)
	{
		m_tabs.UpdateLayout();

		CWindow host_ = TPageBase::GetWindow_Ref();

		details::CTabPageSetup_Layout layout_(host_);
		layout_.UpdateBanner(
					m_banner,
					m_tabs.SelectedIndex()
				);
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}