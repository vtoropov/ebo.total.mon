#ifndef _TMONTABPAGETHREATS_H_7E862F58_73B0_4b1a_8630_562BA195A648_INCLUDED
#define _TMONTABPAGETHREATS_H_7E862F58_73B0_4b1a_8630_562BA195A648_INCLUDED
/*
	Created by Tech_dog (VToropov) on 8-Dec-2015 at 2:42:05pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring application scan log/threat tab page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_Button.h"
#include "UIX_Renderer.h"
#include "UIX_Label.h"

#include "TMon_Threat_Panel.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CButton;
	using ex_ui::controls::CLabel;

	using shared::avs::IScannerEventSink;
	using shared::avs::CScanError;
	using shared::avs::CScanFile;

	class CTabPageThreats :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify,
		public  IScannerEventSink
	{
		typedef CPanelBase TPageBase;
	private:
		struct CTotals
		{
			LONG _infected;
			LONG _suspected;
		public:
			CTotals(void) { this->Clear(); }
		public:
			VOID Clear(VOID)
			{
				_infected = _suspected = 0;
			}
			LONG Total(VOID)
			{
				return _infected + _suspected;
			}
		};
	private:
		CBackgroundTileRenderer   m_bkg_rnd;
		CLabel                    m_lst_label;
		CTotals                   m_stat;
		CThreatPanel              m_panel;
	public:
		CTabPageThreats(IRenderer& _parent);
		~CTabPageThreats(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	private: // IScannerEventSink
		VOID        IScannerEvent_OnFileScanFinish(CScanFile _file) override sealed;
		VOID        IScannerEvent_OnScanProcessBegin(VOID) override sealed;
	};
}}}
#endif/*_TMONTABPAGETHREATS_H_7E862F58_73B0_4b1a_8630_562BA195A648_INCLUDED*/