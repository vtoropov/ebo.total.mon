#ifndef _TMONTABPAGEHOME_H_587A1C77_FBF2_479d_B145_7F4FC5489DC5_INCLUDED
#define _TMONTABPAGEHOME_H_587A1C77_FBF2_479d_B145_7F4FC5489DC5_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Nov-2015 at 9:20:43am, GMT+7, Phuket, Rawai, Thursday;
	This is Total Monitoring application home tab page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_Button.h"
#include "UIX_Image.h"
#include "UIX_Label.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CButton;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CLabel;

	using shared::avs::IScannerEventSink;
	using shared::avs::CScanError;
	using shared::avs::CScanFile;

	class CTabPageHome :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify,
		public  IScannerEventSink
	{
		typedef CPanelBase TPageBase;
	private:
		struct CTotals
		{
			LONG _infected;
			LONG _suspected;
		public:
			CTotals(void) { this->Clear(); }
		public:
			VOID Clear(VOID)
			{
				_infected = _suspected = 0;
			}
			LONG Total(VOID)
			{
				return _infected + _suspected;
			}
		};
	private:
		IControlNotify& m_evt_sink;
		CImage      m_btn_protect;
		CLabel      m_lbl_protect;
		CImage      m_btn_update;
		CLabel      m_lbl_update;
		CImage      m_btn_threat;
		CLabel      m_lbl_threat;
		CImage      m_btn_scanner;
		CLabel      m_lbl_scanner;
		CTotals     m_stat;
	public:
		CTabPageHome(IRenderer& _parent, IControlNotify& _notify);
		~CTabPageHome(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	private: // IScannerEventSink
		VOID        IScannerEvent_OnFileScanFinish(CScanFile _file) override sealed;
		VOID        IScannerEvent_OnScanProcessBegin(VOID) override sealed;
		VOID        IScannerEvent_OnScanProcessFinish(VOID) override sealed;
	};
}}}

#endif/*_TMONTABPAGEHOME_H_587A1C77_FBF2_479d_B145_7F4FC5489DC5_INCLUDED*/