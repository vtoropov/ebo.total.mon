/*
	Created by Tech_dog (VToropov) on 15-Feb-2016 at 7:34:58pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring Update Progress panel class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Update_Progress.h"
#include "TMon_Resource.h"

using namespace tmon::UI::components;

using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CUpdateProgress_Layout
	{
	private:
		CWindow&    m_panel;
		RECT        m_client_rect;
	public:
		CUpdateProgress_Layout(CWindow& _panel) : m_panel(_panel)
		{
			if (!m_panel)
				::SetRectEmpty(&m_client_rect);
			else
				m_panel.GetClientRect(&m_client_rect);
		}
	public:
		RECT        GetCaptionRect(void)const
		{
			const INT nLeft = 15;
			const RECT rc_  = {
				nLeft, 0, m_client_rect.right, 30
			};
			return rc_;
		}

		RECT        GetProgressRect(void)const
		{
			RECT rc_ = this->GetCaptionRect();
			rc_.top  = rc_.bottom;
			rc_.bottom += 25;

			return rc_;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CUpdateProgress::CUpdateProgress(void) :
	TPanelBase(IDD_TMON_UPDATE_PRG_PANEL, *this),
	m_bkg_rnd (IDR_TMON_UPDATE_PRG_BKGND, TPanelBase::GetWindow_Ref()),
	m_caption (CControlCrt(1, m_bkg_rnd , *this))
{
	TPanelBase::SetBkgndRenderer(&m_bkg_rnd);
}

CUpdateProgress::~CUpdateProgress(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CUpdateProgress::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPanelBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPanelBase::GetWindow_Ref();

	details::CUpdateProgress_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetCaptionRect();
		hr_ = m_caption.Create(
				host_,
				rc_,
				_T("")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_caption.ForeColor(RGB(0, 0, 0));
		m_caption.FontSize(10);
	}
	{
		RECT rc_ = layout_.GetProgressRect();
		m_progress.Create(
				host_,
				rc_,
				NULL,
				WS_VISIBLE|WS_CHILD
			);
		m_progress.SetRange(1, 100);
	}

	return  hr_;
}

HRESULT     CUpdateProgress::Destroy(void)
{
	if (m_progress)
		m_progress.SendMessage(WM_CLOSE);
	m_caption.Destroy();
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CUpdateProgress::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CUpdateProgress::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;
	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID        CUpdateProgress::SetCaption(LPCTSTR pszText)
{
	m_caption.Text(pszText);
}

VOID        CUpdateProgress::SetProgress(const INT _position)
{
	if (m_progress)
		m_progress.SetPos(_position);
}