/*
	Created by Tech_dog (VToropov) on 20-Jan-2016 at 10:06:24pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring Protection Settings Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Setup_ProPage.h"
#include "TMon_Resource.h"

using namespace tmon::common;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

#include "Shared_AvsDefs.h"

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CProSetupPage_Init
	{
	private:
		CWindow&      m_page_ref;
	public:
		CProSetupPage_Init(CWindow& _page) : m_page_ref(_page){}
	public:
		VOID          ApplySettings(void)
		{
			const TProtectCfg& cfg_ = global::GetSettings().Protection();

			::WTL::CComboBox cbo_= m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_ACTION);
			if (cbo_)
				cbo_.SetCurSel(cfg_.DefaultAction().Index());

			::WTL::CButton check_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_REPORT);
			if (check_)
				check_.SetCheck(static_cast<INT>(cfg_.Reporting()));

			check_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_RMCTRL);
			if (check_)
				check_.SetCheck(static_cast<INT>(cfg_.RemoteControl()));
		}

		VOID          InitCtrls(void)
		{
			::WTL::CComboBox cbo_;
			
			cbo_= m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_ENGINE);
			if (cbo_)
			{
				cbo_.AddString(_T("Zillya! Antivirus"));
				cbo_.SetCurSel(0);
			}

			cbo_= m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_ACTION);
			if (cbo_)
			{
				CScanActionEnum enum_;
				const INT count_ = enum_.Count();

				for (INT i_ = 0; i_ < count_; i_++)
					cbo_.AddString(enum_.Item(i_).Name());
				cbo_.SetCurSel(0);
			}
		}

		VOID          UpdateSettings(void)
		{
			TProtectCfg& cfg_ = global::GetSettings().Protection();

			::WTL::CComboBox cbo_= m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_ACTION);
			if (cbo_)
				cfg_.DefaultAction(cbo_.GetCurSel());

			::WTL::CButton check_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_REPORT);
			if (check_)
				cfg_.Reporting(!!check_.GetCheck());

			check_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_RMCTRL);
			if (check_)
				cfg_.RemoteControl(!!check_.GetCheck());
		}
	};

	class CProSetupPage_Layout
	{
	private:
		RECT          m_client_area;
		CWindow&      m_page_ref;
	public:
		CProSetupPage_Layout(CWindow& _page) : m_page_ref(_page)
		{
			m_page_ref.GetClientRect(&m_client_area);
		}
	public:
		VOID          UpdateCtrls(void)
		{
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_TMON_MAIN_DLG_WARN_I24PX,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_EXCLAM);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}

		VOID          UpdateState(void)
		{
			INT nVidible = SW_HIDE;
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_PRO_ACTION);
			if (cbo_ && 1 == cbo_.GetCurSel())
				nVidible = SW_SHOW;

			const UINT ctrlId[] = {
				IDC_TMON_SETUP_PRO_EXCLAM,
				IDC_TMON_SETUP_PRO_EXTEXT,
			};
			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (!ctrl_)
					continue;
				ctrl_.ShowWindow(nVidible);
			}
		}
	};

	class CProSetupPage_Handler
	{
	private:
		CWindow&      m_page_ref;
	public:
		CProSetupPage_Handler(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL          OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;
			if (false){}
			else if (IDC_TMON_SETUP_PRO_ACTION == wCtrlId)
			{
				if (CBN_SELCHANGE == wNotifyCode)
				{
					CProSetupPage_Layout layout_(m_page_ref);
					layout_.UpdateState();
				}
			}
			else if (IDC_TMON_SETUP_PRO_APPLY  == wCtrlId)
			{
				CProSetupPage_Init init_(m_page_ref);
				init_.UpdateSettings();
			}

			return bHandled;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CProSetupPage::CProSetupPage(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_TMON_SETUP_PRO_PAGE, tab_ref, *this)
{
}

CProSetupPage::~CProSetupPage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CProSetupPage::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;

			details::CProSetupPage_Layout layout_(*this);
			layout_.UpdateCtrls();

			details::CProSetupPage_Init init_(*this);
			init_.InitCtrls();
			init_.ApplySettings();
			layout_.UpdateState();

			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			details::CProSetupPage_Init init_(*this);
			init_.UpdateSettings();
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			details::CProSetupPage_Handler handler_(*this);
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CProSetupPage::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Protection"));
	return cs_title;
}