/*
	Created by Tech_dog (VToropov) on 8-Dec-2015 at 11:10:16pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring application scan log/threat tab page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_TabPage_Threats.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

using namespace ex_ui::controls;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CTabPageThreats_Layout
	{
	private:
		enum _e{
			e_label_w = 100,
			e_label_h =  30,
		};
	private:
		CWindow&     m_page_ref;
		RECT         m_client_area;
	public:
		CTabPageThreats_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT         GetObjLabelRect(void)
		{
			const INT nLeft =  75;
			const INT nTop  = 150;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CTabPageThreats_Layout::e_label_w,
					nTop  + CTabPageThreats_Layout::e_label_h
				};
			return rc_;
		}

		RECT         GetThreatPanelRect(void)
		{
			const INT nLeft = 290;
			const INT nTop  =  15;

			const RECT rc_ = {
						nLeft,
						nTop ,
						nLeft + 536, // from PSD file
						nTop  + 420  // from PSD file
					};
			return rc_;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageThreats::CTabPageThreats(IRenderer& _parent):
	TPageBase  (IDD_TMON_THREAT_TAB_PAGE , *this),
	m_bkg_rnd  (IDR_TMON_THREAT_TAB_BKGND, TPageBase::GetWindow_Ref()),
	m_lst_label(CControlCrt(1, m_bkg_rnd , *this)),
	m_panel    (m_bkg_rnd)
{
	_parent;
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);
}

CTabPageThreats::~CTabPageThreats(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageThreats::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CTabPageThreats_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetObjLabelRect();
		hr_ = m_lst_label.Create(
				host_,
				rc_,
				_T("Objects: 0")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_lst_label.ForeColor(RGB(0xff, 0xcc, 0x99));
		m_lst_label.FontSize(13);
	}
	{
		const RECT rc_ = layout_.GetThreatPanelRect();
		hr_ = m_panel.Create(
					host_,
					rc_,
#if defined(_DEBUG)
					true
#else
					false
#endif
				);
	}

	global::GetScannerRef().Events().Subscribe(this);
	return hr_;
}

HRESULT    CTabPageThreats::Destroy(void)
{
	global::GetScannerRef().Events().Unsubscribe(this);

	m_panel.Destroy();
	m_lst_label.Destroy();

	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageThreats::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageThreats::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID       CTabPageThreats::IScannerEvent_OnFileScanFinish(CScanFile _file)
{
	bool bChanged = false;
	switch (_file.Health())
	{
	case CScanFileHealth::eInfected:
		{
			bChanged = true;
			m_stat._infected += 1;
		} break;
	case CScanFileHealth::eSuspected:
		{
			bChanged = true;
			m_stat._suspected += 1;
		} break;
	}
	if (bChanged)
	{
		CAtlString cs_msg;
		cs_msg.Format(
				_T("Objects: %d"),
				m_stat.Total()
			);
		m_lst_label.Text(cs_msg);
		m_panel.Show(true);
	}
}

VOID       CTabPageThreats::IScannerEvent_OnScanProcessBegin(VOID)
{
	m_stat.Clear();
	m_lst_label.Text(
		_T("Objects: 0")
		);
	m_panel.Show(false);
}