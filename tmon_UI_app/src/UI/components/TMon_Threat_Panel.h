#ifndef _TMONTHREATPANEL_H_1DC4851C_3425_4ae2_9237_D519D3FDE61F_INCLUDED
#define _TMONTHREATPANEL_H_1DC4851C_3425_4ae2_9237_D519D3FDE61F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 21-Dec-2015 at 8:32:20pm, GMT+7, Phuket, Rawai;
	This is Total Monitoring Active threat panel class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_Label.h"
#include "UIX_Renderer.h"

#include "TMon_Log_Provider.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CLabel;

	using shared::avs::IScannerEventSink;
	using shared::avs::CScanError;
	using shared::avs::CScanFile;

	using tmon::data::CThreatReport;

	class CThreatPanel :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify,
		public  IScannerEventSink
	{
		typedef CPanelBase TPageBase;
	private:
		CBackgroundTileRenderer  m_bkg_rnd;
		CLabel                   m_act_label;
		::ATL::CWindow           m_list;
		HIMAGELIST               m_images;
		CThreatReport            m_report;
	public:
		CThreatPanel(IRenderer& _parent);
		~CThreatPanel(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	private: // IScannerEventSink
		VOID        IScannerEvent_OnFileScanFinish(CScanFile _file) override sealed;
		VOID        IScannerEvent_OnScanProcessBegin(VOID) override sealed;
	};
}}}

#endif/*_TMONTHREATPANEL_H_1DC4851C_3425_4ae2_9237_D519D3FDE61F_INCLUDED*/