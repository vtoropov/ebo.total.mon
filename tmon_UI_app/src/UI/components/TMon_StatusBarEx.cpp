/*
	Created by Tech_dog (VToropov) on 4-Dec-2015 at 11:39:17am, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring application status bar UI component class implementation file.
*/
#include "StdAfx.h"
#include "TMon_StatusBarEx.h"
#include "TMon_Resource.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	static INT   StatusBar_MsgTypeToImageIndex(const CStatusType::_e eType)
	{
		switch (eType)
		{
		case CStatusType::eError:     return ( 2);
		case CStatusType::eInfo:      return ( 0);
		case CStatusType::eWarning:   return ( 1);
		case CStatusType::eWaiting:   return ( 4);
		}
		return -1;
	}

	class CStatusBar_Layout
	{
	private:
		enum { // from PSD files
			e_image_w  =  16,
			e_image_h  =  16,
			e_status_h =  30,
			e_status_w = 850,
			e_margin_h =   7,
		};
	public:
		static RECT GetCtrlRect(void)
		{
			RECT rc_ = {
					0,
					0,
					CStatusBar_Layout::e_status_w,
					CStatusBar_Layout::e_status_h
				};
			return rc_;
		}
		static RECT GetImageRect(void)
		{
			const INT nLeft = CStatusBar_Layout::e_margin_h;
			const INT nTop  =(CStatusBar_Layout::e_status_h - CStatusBar_Layout::e_image_h)/2;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CStatusBar_Layout::e_image_w,
					nTop  + CStatusBar_Layout::e_image_h
				};
			return rc_;
		}
		static RECT GetTextRect(void)
		{
			const INT nLeft = CStatusBar_Layout::GetImageRect().right + CStatusBar_Layout::e_margin_h;
			RECT rc_ = {
					nLeft,
					0,
					CStatusBar_Layout::e_status_w - CStatusBar_Layout::e_margin_h,
					CStatusBar_Layout::e_status_h
				};
			return rc_;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CStatusBarEx::CStatusBarExWnd::CStatusBarExWnd(void):
	m_bkgnd(IDR_TMON_MAIN_DLG_STAT_BKGND, *this),
	m_images(NULL),
	m_type(CStatusType::eInfo)
{
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	CGdiPlusPngLoader::CreateImages(
			IDR_TMON_MAIN_DLG_STAT_STRIP,
			hInstance,
			5,
			m_images
		);
	m_text = _T("Ready");
}

CStatusBarEx::CStatusBarExWnd::~CStatusBarExWnd(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CStatusBarEx::CStatusBarExWnd::OnCreate (UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

LRESULT CStatusBarEx::CStatusBarExWnd::OnDestroy(UINT, WPARAM, LPARAM, BOOL&)
{
	if (m_images)
	{
		::ImageList_Destroy(m_images); m_images = NULL;
	}
	m_bkgnd.ClearCache();
	return 0;
}

LRESULT CStatusBarEx::CStatusBarExWnd::OnErase  (UINT, WPARAM wParam, LPARAM, BOOL&)
{
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);

	CZBuffer dc_((HDC)wParam, rc_);

	::OffsetRect(&rc_, -rc_.left, -rc_.top);
	m_bkgnd.Draw(dc_, rc_);

	if (m_images)
	{
		static const RECT rc_ = details::CStatusBar_Layout::GetImageRect();
		ImageList_DrawEx(
				m_images,
				details::StatusBar_MsgTypeToImageIndex(m_type),
				dc_,
				rc_.left,
				rc_.top ,
				16,
				16,
				CLR_DEFAULT,
				CLR_DEFAULT,
				ILD_NORMAL
			);
	}
	static ex_ui::draw::common::CFont fnt_(
			_T("verdana"),
			eCreateFontOption::eRelativeSize,
			-1
		);
	if (m_text.GetLength())
	{
		static const RECT rc_ = details::CStatusBar_Layout::GetTextRect();
		dc_.DrawTextExt(
				m_text,
				fnt_.GetHandle(),
				rc_,
				::GetSysColor(COLOR_WINDOWTEXT),
				DT_LEFT|DT_VCENTER|DT_SINGLELINE|DT_NOCLIP|DT_END_ELLIPSIS
			);
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CStatusBarEx::CStatusBarEx(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CStatusBarEx::Create(const HWND _parent, const RECT& _area)
{
	if (::IsRectEmpty(&_area))
		return E_INVALIDARG;

	if (!::IsWindow(_parent))
		return OLE_E_BLANK;

	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);

	RECT rc_ = _area;

	HWND hwnd_ = m_wnd.Create(
						_parent,
						rc_,
						NULL,
						WS_CHILD|WS_VISIBLE
					);
	if (!hwnd_)
		return HRESULT_FROM_WIN32(::GetLastError());

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT      CStatusBarEx::Destroy(void)
{
	if (!m_wnd.IsWindow())
		return S_OK;

	m_wnd.SendMessage(WM_CLOSE);

	HRESULT hr_ = S_OK;
	return  hr_;
}

SIZE         CStatusBarEx::GetSize(void)const
{
	SIZE sz_ = {
			details::CStatusBar_Layout::GetCtrlRect().right,
			details::CStatusBar_Layout::GetCtrlRect().bottom
		};
	return sz_;
}

HRESULT      CStatusBarEx::SetText(LPCTSTR lpszText, const CStatusType::_e _type)
{
	m_wnd.m_text = lpszText;
	m_wnd.m_type = _type;
	if (m_wnd)
		m_wnd.RedrawWindow(NULL, NULL, RDW_ERASE|RDW_INVALIDATE|RDW_ERASENOW);
	HRESULT hr_ = S_OK;
	return  hr_;
}