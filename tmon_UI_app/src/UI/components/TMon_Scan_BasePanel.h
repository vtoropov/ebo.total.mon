#ifndef _TMONSCANBASEPANEL_H_D531F3D4_1244_49f5_A8A1_1E90A3CBF4B1_INCLUDED
#define _TMONSCANBASEPANEL_H_D531F3D4_1244_49f5_A8A1_1E90A3CBF4B1_INCLUDED
/*
	Created by Tech_dog (VToropov) on 27-Dec-2015 at 2:45:49pm, GMT+8, Kuala Lumpur, Sunday;
	This is Total Monitoring Scan panel base class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_Label.h"
#include "UIX_Renderer.h"

#include "TMon_Log_Provider.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CLabel;

	using shared::avs::IScannerEventSink;
	using shared::avs::CScanError;
	using shared::avs::CScanFile;

	using tmon::data::CLogProviderBase;

	class CBaseScanPanel :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify,
		public  IScannerEventSink
	{
		typedef CPanelBase TPageBase;
	protected:
		struct CTotals
		{
			LONG _clean;
			LONG _infected;
			LONG _errors;
			LONG _suspected;
			bool _finished;
		public:
			CTotals(void) { this->Clear(); }
		public:
			VOID Clear(VOID)
			{
				_clean = _infected = _errors = _suspected = 0; _finished = false;
			}
			LONG Total(VOID)
			{
				return _clean + _infected + _errors + _suspected;
			}
		};

	protected:
		CBackgroundTileRenderer  m_bkg_rnd;
		CLabel                   m_cap_label;
		CLabel                   m_sta_label;
		CLabel                   m_esc_label;
		CTotals                  m_stat;
		CLogProviderBase         m_log;
		DWORD                    m_locker;    // exclusive access to the scanner object
	protected:
		CBaseScanPanel(const UINT nDlgResId, const UINT nBkgResId);
		virtual ~CBaseScanPanel(void);
	public:    // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override;
		HRESULT     Destroy(void) override;
	protected: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override;
	protected: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override;
	protected: // IScannerEventSink
		VOID        IScannerEvent_OnEngineInitBegin(CAtlString) override;
		VOID        IScannerEvent_OnEngineInitFinish(CScanError) override;
		VOID        IScannerEvent_OnFileScanBegin(CAtlString _path) override;
		VOID        IScannerEvent_OnFileScanFinish(CScanFile _file) override;
		VOID        IScannerEvent_OnScanProcessBegin(VOID) override;
		VOID        IScannerEvent_OnScanProcessFinish(VOID) override;
	};
}}}

#endif/*_TMONSCANBASEPANEL_H_D531F3D4_1244_49f5_A8A1_1E90A3CBF4B1_INCLUDED*/