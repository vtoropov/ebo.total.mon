/*
	Created by Tech_dog (VToropov) on 24-Nov-2015 at 10:58:20am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Main Form Tab Strip control class implementation file.
*/
#include "StdAfx.h"
#include "TMon_TabStrip.h"
#include "TMon_Resource.h"

using namespace tmon::UI;
using namespace tmon::UI::components;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CTabStrip_Layout
	{
	public:
		enum{
			e_def_w  = 850,
			e_def_h  =  35,
			e_btn_w  = 100,
			e_btn_h  =  35,
			e_margin =   3,
		};
	private:
		CWindow&      m_frame_ref;
		RECT          m_client_area;
		LONG          m_btn_w[6];
	public:
		CTabStrip_Layout(CWindow& frame_ref) : m_frame_ref(frame_ref)
		{
			m_btn_w[0] =  85; // from PSD
			m_btn_w[1] =  85; // from PSD
			m_btn_w[2] = 120; // from PSD
			m_btn_w[3] = 120; // from PSD
			m_btn_w[4] = 130; // from PSD
			m_btn_w[5] = 130; // from PSD
			if (m_frame_ref)
				m_frame_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT          GetButtonRect(const INT _ndx)const
		{
			INT nLeft = CTabStrip_Layout::e_margin;

			for (INT i_ = 0; i_ < _countof(m_btn_w) && i_ < _ndx;  i_++)
				nLeft  += m_btn_w[i_] + CTabStrip_Layout::e_margin;

			RECT rc_ = {
					nLeft,
					0,
					m_btn_w[_ndx] + nLeft,
					CTabStrip_Layout::e_btn_h
				};
			return rc_;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CTabStrip::CTabStripWnd::CTabStripWnd(IRenderer& _rnd, IControlNotify& _notify) : 
	m_prn_renderer(_rnd),
	m_evt_sink(_notify),
	m_bkgnd (IDR_TMON_MAIN_DLG_TABS_BKGND, *this),
	m_btn_home  (IDC_TMON_MAIN_DLG_TAB_HOME, m_bkgnd, _notify),
	m_btn_scan  (IDC_TMON_MAIN_DLG_TAB_SCAN, m_bkgnd, _notify),
	m_btn_threat(IDC_TMON_MAIN_DLG_TAB_THRT, m_bkgnd, _notify),
	m_btn_update(IDC_TMON_MAIN_DLG_TAB_UPDT, m_bkgnd, _notify),
	m_btn_setup (IDC_TMON_MAIN_DLG_TAB_SETS, m_bkgnd, _notify),
	m_btn_help  (IDC_TMON_MAIN_DLG_TAB_HELP, m_bkgnd, _notify)
{
	HRESULT hr_ = S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	{
		hr_ = m_btn_home.SetImage  (eControlState::eNormal   , IDR_TMON_MAIN_DLG_TAB_HOME_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_home.SetImage  (eControlState::eDisabled , IDR_TMON_MAIN_DLG_TAB_HOME_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_home.SetImage  (eControlState::ePressed  , IDR_TMON_MAIN_DLG_TAB_HOME_HOT , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_home.SetImage  (eControlState::eHovered  , IDR_TMON_MAIN_DLG_TAB_HOME_HOT , hInstance); ATLASSERT(S_OK == hr_);
		m_btn_home.Style(CButtonStyle::eChecked);
	}
	{
		hr_ = m_btn_scan.SetImage  (eControlState::eNormal   , IDR_TMON_MAIN_DLG_TAB_SCAN_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_scan.SetImage  (eControlState::eDisabled , IDR_TMON_MAIN_DLG_TAB_SCAN_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_scan.SetImage  (eControlState::ePressed  , IDR_TMON_MAIN_DLG_TAB_SCAN_HOT , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_scan.SetImage  (eControlState::eHovered  , IDR_TMON_MAIN_DLG_TAB_SCAN_HOT , hInstance); ATLASSERT(S_OK == hr_);
		m_btn_scan.Style(CButtonStyle::eChecked);
	}
	{
		hr_ = m_btn_threat.SetImage(eControlState::eNormal   , IDR_TMON_MAIN_DLG_TAB_THRT_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_threat.SetImage(eControlState::eDisabled , IDR_TMON_MAIN_DLG_TAB_THRT_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_threat.SetImage(eControlState::ePressed  , IDR_TMON_MAIN_DLG_TAB_THRT_HOT , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_threat.SetImage(eControlState::eHovered  , IDR_TMON_MAIN_DLG_TAB_THRT_HOT , hInstance); ATLASSERT(S_OK == hr_);
		m_btn_threat.Style(CButtonStyle::eChecked);
	}
	{
		hr_ = m_btn_update.SetImage(eControlState::eNormal   , IDR_TMON_MAIN_DLG_TAB_UPDT_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_update.SetImage(eControlState::eDisabled , IDR_TMON_MAIN_DLG_TAB_UPDT_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_update.SetImage(eControlState::ePressed  , IDR_TMON_MAIN_DLG_TAB_UPDT_HOT , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_update.SetImage(eControlState::eHovered  , IDR_TMON_MAIN_DLG_TAB_UPDT_HOT , hInstance); ATLASSERT(S_OK == hr_);
		m_btn_update.Style(CButtonStyle::eChecked);
	}
	{
		hr_ = m_btn_setup.SetImage (eControlState::eNormal   , IDR_TMON_MAIN_DLG_TAB_SETS_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_setup.SetImage (eControlState::eDisabled , IDR_TMON_MAIN_DLG_TAB_SETS_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_setup.SetImage (eControlState::ePressed  , IDR_TMON_MAIN_DLG_TAB_SETS_HOT , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_setup.SetImage (eControlState::eHovered  , IDR_TMON_MAIN_DLG_TAB_SETS_HOT , hInstance); ATLASSERT(S_OK == hr_);
		m_btn_setup.Style(CButtonStyle::eChecked);
	}
	{
		hr_ = m_btn_help.SetImage  (eControlState::eNormal   , IDR_TMON_MAIN_DLG_TAB_HELP_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_help.SetImage  (eControlState::eDisabled , IDR_TMON_MAIN_DLG_TAB_HELP_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_help.SetImage  (eControlState::ePressed  , IDR_TMON_MAIN_DLG_TAB_HELP_HOT , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_btn_help.SetImage  (eControlState::eHovered  , IDR_TMON_MAIN_DLG_TAB_HELP_HOT , hInstance); ATLASSERT(S_OK == hr_);
		m_btn_help.Style(CButtonStyle::eChecked);
	}
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CTabStrip::CTabStripWnd::OnCreate    (UINT, WPARAM, LPARAM, BOOL&)
{
	details::CTabStrip_Layout layout(*this);

	HRESULT hr_ = S_OK;

	INT nIndex = 0;
	{
		RECT rc_ = layout.GetButtonRect(nIndex++);
		hr_ = m_btn_home.Create(
					TWindow::m_hWnd,
					&rc_,
					false
				);
	}
	{
		RECT rc_ = layout.GetButtonRect(nIndex++);
		hr_ = m_btn_scan.Create(
					TWindow::m_hWnd,
					&rc_,
					false
				);
	}
	{
		RECT rc_ = layout.GetButtonRect(nIndex++);
		hr_ = m_btn_threat.Create(
					TWindow::m_hWnd,
					&rc_,
					false
				);
	}
	{
		RECT rc_ = layout.GetButtonRect(nIndex++);
		hr_ = m_btn_update.Create(
					TWindow::m_hWnd,
					&rc_,
					false
				);
	}
	{
		RECT rc_ = layout.GetButtonRect(nIndex++);
		hr_ = m_btn_setup.Create(
					TWindow::m_hWnd,
					&rc_,
					false
				);
	}
	{
		RECT rc_ = layout.GetButtonRect(nIndex++);
		hr_ = m_btn_help.Create(
					TWindow::m_hWnd,
					&rc_,
					false
				);
	}
	m_evt_sink.IControlNotify_OnClick(IDC_TMON_MAIN_DLG_TAB_HOME);
	return 0;
}

LRESULT CTabStrip::CTabStripWnd::OnDestroy   (UINT, WPARAM, LPARAM, BOOL&)
{
	m_btn_help.Destroy();
	m_btn_setup.Destroy();
	m_btn_update.Destroy();
	m_btn_threat.Destroy();
	m_btn_scan.Destroy();
	m_btn_home.Destroy();
	return 0;
}

LRESULT CTabStrip::CTabStripWnd::OnErase     (UINT, WPARAM wParam, LPARAM, BOOL&)
{
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);

	CZBuffer dc_((HDC)wParam, rc_);

	::OffsetRect(&rc_, -rc_.left, -rc_.top);
	m_bkgnd.Draw(dc_, rc_);

	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CTabStrip::CTabStrip(IRenderer& _rnd, IControlNotify& _notify) :
	m_wnd(_rnd, *this),
	m_cur_index(-1),
	m_evt_parent(_notify)
{
}

CTabStrip::~CTabStrip(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabStrip::IControlNotify_OnClick(const UINT ctrlId)
{
	INT nIndex = CTabIndex::eHome;
	switch (ctrlId)
	{
	case IDC_TMON_MAIN_DLG_TAB_HOME:   nIndex = CTabIndex::eHome  ; break;
	case IDC_TMON_MAIN_DLG_TAB_SCAN:   nIndex = CTabIndex::eScan  ; break;
	case IDC_TMON_MAIN_DLG_TAB_THRT:   nIndex = CTabIndex::eThreat; break;
	case IDC_TMON_MAIN_DLG_TAB_UPDT:   nIndex = CTabIndex::eUpdate; break;
	case IDC_TMON_MAIN_DLG_TAB_SETS:   nIndex = CTabIndex::eSetup ; break;
	case IDC_TMON_MAIN_DLG_TAB_HELP:   nIndex = CTabIndex::eHelp  ; break;
	}
	const bool bChanged = (nIndex != m_cur_index);
	if (bChanged)
	{
		this->SelectedTab(nIndex);
		m_evt_parent.IControlNotify_OnClick(IDC_TMON_MAIN_DLG_TABS);
	}
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabStrip::Create(HWND _parent, const RECT& _area)
{
	if (::IsRectEmpty(&_area))
		return E_INVALIDARG;

	if (!::IsWindow(_parent))
		return OLE_E_BLANK;

	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);

	RECT rc_ = _area;

	HWND hwnd_ = m_wnd.Create(
						_parent,
						rc_,
						NULL,
						WS_CHILD|WS_VISIBLE
					);
	if (!hwnd_)
		return HRESULT_FROM_WIN32(::GetLastError());

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT    CTabStrip::Destroy(void)
{
	if (!m_wnd.IsWindow())
		return S_OK;

	m_wnd.SendMessage(WM_CLOSE);

	HRESULT hr_ = S_OK;
	return  hr_;
}

SIZE       CTabStrip::GetDefaultSize(void)const
{
	const SIZE sz_ = {
			details::CTabStrip_Layout::e_def_w,
			details::CTabStrip_Layout::e_def_h
		};
	return sz_;
}

INT        CTabStrip::SelectedTab(void)const
{
	return m_cur_index;
}

VOID       CTabStrip::SelectedTab(const INT nIndex)
{
	if (nIndex < 0 || nIndex > CTabIndex::eHelp)
		return;
	m_wnd.m_btn_home  .Checked(nIndex == CTabIndex::eHome  );
	m_wnd.m_btn_scan  .Checked(nIndex == CTabIndex::eScan  );
	m_wnd.m_btn_threat.Checked(nIndex == CTabIndex::eThreat);
	m_wnd.m_btn_update.Checked(nIndex == CTabIndex::eUpdate);
	m_wnd.m_btn_setup .Checked(nIndex == CTabIndex::eSetup );
	m_wnd.m_btn_help  .Checked(nIndex == CTabIndex::eHelp  );

	m_cur_index = nIndex;
}