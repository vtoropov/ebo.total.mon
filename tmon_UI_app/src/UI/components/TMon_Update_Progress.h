#ifndef _TMONUPDATEPROGRESS_H_2EDDF7FE_81D1_456c_8027_7FF1B5EFE719_INCLUDED
#define _TMONUPDATEPROGRESS_H_2EDDF7FE_81D1_456c_8027_7FF1B5EFE719_INCLUDED
/*
	Created by Tech_dog (VToropov) on 15-Feb-2016 at 7:21:47pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring Update Progress panel class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_Label.h"
#include "UIX_Renderer.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CLabel;

	class CUpdateProgress :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify
	{
		typedef CPanelBase TPanelBase;
	private:
		CBackgroundTileRenderer  m_bkg_rnd;
		::WTL::CProgressBarCtrl  m_progress;
		CLabel                   m_caption;
	public:
		CUpdateProgress(void);
		~CUpdateProgress(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	public:
		VOID        SetCaption(LPCTSTR);
		VOID        SetProgress(const INT _position);
	};
}}}

#endif/*_TMONUPDATEPROGRESS_H_2EDDF7FE_81D1_456c_8027_7FF1B5EFE719_INCLUDED*/