#ifndef _TMONTABPAGESETUP_H_B5E6C593_C15F_48d4_BB0D_1A4F9420532D_INCLUDED
#define _TMONTABPAGESETUP_H_B5E6C593_C15F_48d4_BB0D_1A4F9420532D_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 12:38:05am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Setup Tab Page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_Button.h"
#include "UIX_Renderer.h"
#include "UIX_Label.h"
#include "UIX_Image.h"

#include "TMon_Setup_TabsSet.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::CImage;
	using ex_ui::controls::defs::IControlNotify;

	class CTabPageSetup :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify
	{
		typedef CPanelBase TPageBase;
	private:
		CBackgroundTileRenderer   m_bkg_rnd;
		CImage                    m_banner;
		CSetupTabSet              m_tabs;
	public:
		CTabPageSetup(IRenderer& _parent);
		~CTabPageSetup(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	};
}}}

#endif/*_TMONTABPAGESETUP_H_B5E6C593_C15F_48d4_BB0D_1A4F9420532D_INCLUDED*/