/*
	Created by Tech_dog (VToropov) on 27-Dec-2015 at 3:30:53pm, GMT+8, Kuala Lumpur, Central Bus Station, Sunday;
	This is Total Monitoring Scan panel base class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Scan_BasePanel.h"

using namespace tmon::UI;
using namespace tmon::UI::components;

using namespace ex_ui::controls;

using namespace shared::avs;
/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CBaseScan_Layout
	{
		enum _e{
			e_cap_label_w = 500,
			e_cap_label_h =  30,
			e_sta_label_w = 500,
			e_sta_label_h = 100,
			e_esc_label_w = 400,
			e_esc_label_h =  30,
		};
	private:
		CWindow&      m_panel_ref;
		RECT          m_client_area;
	public:
		CBaseScan_Layout(CWindow& panel_ref) : m_panel_ref(panel_ref)
		{
			if (m_panel_ref)
				m_panel_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT     GetCaptionLabelRect(void)const
		{
			const INT nLeft = 15;
			const INT nTop  = 10;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CBaseScan_Layout::e_cap_label_w,
					nTop  + CBaseScan_Layout::e_cap_label_h
				};
			return rc_;
		}

		RECT     GetEscapeLabelRect(void)const
		{
			const INT nLeft = __W(m_client_area) - CBaseScan_Layout::e_esc_label_w * 1;
			const INT nTop  = __H(m_client_area) - CBaseScan_Layout::e_esc_label_h * 2;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CBaseScan_Layout::e_esc_label_w,
					nTop  + CBaseScan_Layout::e_cap_label_h
				};
			return rc_;
		}

		RECT     GetStatusLabelRect(void)const
		{
			const INT nLeft = 15;
			const INT nTop  = __H(m_client_area) / 2 + CBaseScan_Layout::e_cap_label_h * 1;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CBaseScan_Layout::e_sta_label_w,
					nTop  + CBaseScan_Layout::e_sta_label_h
				};
			return rc_;
		}

	};
}}}

/////////////////////////////////////////////////////////////////////////////

CBaseScanPanel::CBaseScanPanel(const UINT nResId, const UINT nBkgResId) :
	TPageBase(nResId, *this),
	m_bkg_rnd(nBkgResId, TPageBase::GetWindow_Ref()),
	m_cap_label(CControlCrt(1, m_bkg_rnd, *this)),
	m_sta_label(CControlCrt(2, m_bkg_rnd, *this)),
	m_esc_label(CControlCrt(3, m_bkg_rnd, *this)),
	m_locker(0)
{
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);
}

CBaseScanPanel::~CBaseScanPanel(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CBaseScanPanel::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CBaseScan_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetCaptionLabelRect();
		hr_ = m_cap_label.Create(
				host_,
				rc_,
				_T("")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		hr_ = m_cap_label.FontSize(10);
		hr_ = m_cap_label.ForeColor(RGB(0x00, 0x66, 0x99));
	}
	{
		const RECT rc_ = layout_.GetStatusLabelRect();
		hr_ = m_sta_label.Create(
				host_,
				rc_,
				_T("")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
	}
	{
		const RECT rc_ = layout_.GetEscapeLabelRect();
		hr_ = m_esc_label.Create(
				host_,
				rc_,
				_T("")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
	}
	global::GetScannerRef().Events().Subscribe(this);
	return hr_;
}

HRESULT     CBaseScanPanel::Destroy(void)
{
	static bool bFirstTime = true;
	if (bFirstTime)
	{
		bFirstTime = false;
		global::GetScannerRef().CancelScans();
		global::GetScannerRef().Terminate();
	}
	global::GetScannerRef().Events().Unsubscribe(this);

	m_esc_label.Destroy();
	m_sta_label.Destroy();
	m_cap_label.Destroy();

	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT     CBaseScanPanel::MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&)
{
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CBaseScanPanel::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID        CBaseScanPanel::IScannerEvent_OnEngineInitBegin(CAtlString _msg)
{
	m_sta_label.Text(_msg);
	m_stat.Clear();
	global::SetWaitMessage(_msg);
}

VOID        CBaseScanPanel::IScannerEvent_OnEngineInitFinish(CScanError)
{
	m_sta_label.Text(_T(""));
}

VOID        CBaseScanPanel::IScannerEvent_OnFileScanBegin(CAtlString _path)
{
	if (!m_locker)
		return;
	CAtlString cs_sta;
	cs_sta.Format(
			_T("Scanning the file:\n%s"),
			_path
		);
	m_sta_label.Text(cs_sta);
}

VOID        CBaseScanPanel::IScannerEvent_OnFileScanFinish(CScanFile _file)
{
	if (!m_locker)
		return;
	switch(_file.Health())
	{
	case CScanFileHealth::eClean        : m_stat._clean     += 1; break;
	case CScanFileHealth::eError        : m_stat._errors    += 1; break;
	case CScanFileHealth::eInfected     : m_stat._infected  += 1; break;
	case CScanFileHealth::eSuspected    : m_stat._suspected += 1; break;
	}
	CAtlString cs_stat;
	cs_stat.Format(
			_T("Scanned: %d| Clean: %d| Infected: %d| Suspicious: %d| Errors: %d"),
			m_stat.Total()   ,
			m_stat._clean    ,
			m_stat._infected ,
			m_stat._suspected,
			m_stat._errors
		);
	if (m_stat._errors
			|| m_stat._infected
				|| m_stat._suspected)
			global::SetErrorMessage(
				cs_stat
			);
	else
		global::SetInfoMessage(
				cs_stat
			);
	m_log.Write(_file);
}

VOID        CBaseScanPanel::IScannerEvent_OnScanProcessBegin(VOID)
{
	if (!m_locker)
		return;
	m_stat.Clear();
	m_sta_label.Text(_T("Creating file list..."));
	m_esc_label.Text(_T("Press [Esc] to cancel the scan"));
}

VOID        CBaseScanPanel::IScannerEvent_OnScanProcessFinish(VOID)
{
	if (!m_locker)
		return;
	m_stat._finished = true;
	m_sta_label.Text(_T("Scanning process has finished"));
	m_esc_label.Text(NULL);
}