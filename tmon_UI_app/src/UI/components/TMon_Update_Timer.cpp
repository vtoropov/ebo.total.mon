/*
	Created by Tech_dog (VToropov) on 21-Dec-2015 at 3:04:40pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring Update Timer class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Update_Timer.h"
#include "TMon_Resource.h"

using namespace tmon::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CUpdateTimer_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CUpdateTimer_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		time_t         LastUpdateTime(void)
		{
			CAtlString cs_date;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastUpdateDate(),
					cs_date
				);

			time_t tm_ = 0;

			if (cs_date.IsEmpty())
			{
				::std::time(&tm_);
				this->LastUpdateTime(tm_);
			}

			INT nResult  = 0;
			try
			{
				nResult  = ::_stscanf_s(
							cs_date,
							_T("%ld"),
							&tm_
						);
			}
			catch(...)
			{
			}

			return tm_;
		}

		HRESULT        LastUpdateTime(const time_t& _date)
		{
			CAtlString cs_date;
			cs_date.Format(
					_T("%ld"),
					_date
				);

			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastUpdateDate(),
					cs_date
				);
			return  hr_;
		}
	private:
		CAtlString    _CommonRegFolder(void)            const { return CAtlString(_T("Common"));             }
		CAtlString    _LastUpdateDate(void)             const { return CAtlString(_T("LastUpdateDate"));     }
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CUpdateTimer::CUpdateTimerWnd::CUpdateTimerWnd(void) : m_digits(NULL), m_updatetime(0), m_timer(NULL)
{
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	CGdiPlusPngLoader::CreateImages(
			IDR_TMON_UPDATE_DIG_STRIP,
			hInstance,
			12,
			m_digits
		);

	details::CUpdateTimer_Registry reg_;
	m_updatetime = reg_.LastUpdateTime();
}

CUpdateTimer::CUpdateTimerWnd::~CUpdateTimerWnd(void)
{
	if (m_digits)
	{
		::ImageList_Destroy(m_digits); m_digits = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CUpdateTimer::CUpdateTimerWnd::OnCreate     (UINT, WPARAM, LPARAM, BOOL&)
{
	m_timer = TWindow::SetTimer(1, 1000);
	return 0;
}

LRESULT CUpdateTimer::CUpdateTimerWnd::OnDestroy    (UINT, WPARAM, LPARAM, BOOL&)
{
	if (m_timer)
	{
		TWindow::KillTimer(m_timer); m_timer = NULL;
	}
	return 0;
}

LRESULT CUpdateTimer::CUpdateTimerWnd::OnEraseBkgnd (UINT, WPARAM wParam, LPARAM, BOOL&)
{
	RECT rc_ = {0};
	TWindow::GetClientRect(&rc_);

	CZBuffer dc_((HDC)wParam, rc_);

	::OffsetRect(&rc_, -rc_.left, -rc_.top);
	dc_.FillSolidRect(
			&rc_,
			RGB(0xff, 0xff, 0xff)
		);

	if (m_digits)
	{
		time_t curr_ = 0;
		::std::time(&curr_);

		double elapsed_time = ::std::difftime(curr_, m_updatetime);
		
		INT elapsed_days = static_cast<INT>(::floor(elapsed_time/(3600.*24))); elapsed_time -= (elapsed_days * 3600.*24);
		INT elapsed_hrs  = static_cast<INT>(::floor(elapsed_time/(3600.)));    elapsed_time -= (elapsed_hrs  * 3600.);
		INT elapsed_mins = static_cast<INT>(::floor(elapsed_time/(60.)));      elapsed_time -= (elapsed_mins * 60.);
		INT elapsed_secs = static_cast<INT>(::floor(elapsed_time));

		static SIZE szImage = {34, 60};
		static INT nSlots = 11; // dd-hh:mm:ss

		LONG nLeft = rc_.left;

		for (INT i_ = 0; i_ < nSlots; i_++)
		{
			INT nIndex = -1; // 0123456789-:
			switch (i_)
			{
			case  0:
				{
					nIndex = elapsed_days / 10; elapsed_days -= nIndex * 10;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			case  1:
				{
					nIndex = elapsed_days;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			case  2:
				{
					nIndex = 10; // dash sign
				} break;
			case  3:
				{
					nIndex = elapsed_hrs / 10; elapsed_hrs -= nIndex * 10;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			case  4:
				{
					nIndex = elapsed_hrs;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			case  5:
				{
					nIndex = 11; // semicolon sign
				} break;
			case  6:
				{
					nIndex = elapsed_mins / 10; elapsed_mins -= nIndex * 10;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			case  7:
				{
					nIndex = elapsed_mins;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			case  8:
				{
					nIndex = 11; // semicolon sign
				} break;
			case  9:
				{
					nIndex = elapsed_secs / 10; elapsed_secs -= nIndex * 10;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			case 10:
				{
					nIndex = elapsed_secs;
					if (nIndex > 9)
						nIndex = 9;
				} break;
			}

			if (-1 < nIndex || nIndex < 12)
			{
				ImageList_DrawEx(
					m_digits,
					nIndex,
					dc_,
					nLeft,
					rc_.top ,
					34,
					60,
					CLR_DEFAULT,
					CLR_DEFAULT,
					ILD_NORMAL
				);
			}

			nLeft += szImage.cx;
		}
	}

	return 0;
}

LRESULT CUpdateTimer::CUpdateTimerWnd::OnPaint      (UINT, WPARAM, LPARAM, BOOL&)
{
	WTL::CPaintDC dc_(m_hWnd);
	return 0;
}

LRESULT CUpdateTimer::CUpdateTimerWnd::OnTimer      (UINT, WPARAM, LPARAM, BOOL&)
{
	TWindow::Invalidate(TRUE);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CUpdateTimer::CUpdateTimer(void)
{
}

CUpdateTimer::~CUpdateTimer(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CUpdateTimer::Create(HWND _parent, const RECT& _area)
{
	if (!::IsWindow(_parent))
		return OLE_E_INVALIDHWND;
	if (::IsRectEmpty(&_area))
		return E_INVALIDARG;

	if (m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(ERROR_ALREADY_INITIALIZED);

	RECT rc_ = _area;
	m_wnd.Create(
			_parent,
			rc_,
			NULL,
			WS_VISIBLE|WS_CHILD
		);
	if (!m_wnd.IsWindow())
		return HRESULT_FROM_WIN32(::GetLastError());
	else
		return S_OK;
}

HRESULT       CUpdateTimer::Destroy(void)
{
	if (m_wnd.IsWindow())
		m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

SIZE          CUpdateTimer::GetDefaultSize(void)
{
	static SIZE sz_ = {408, 60}; // from PSD file
	return sz_;
}