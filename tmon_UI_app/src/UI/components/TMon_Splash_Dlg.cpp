/*
	Created by Tech_dog (VToropov) on 18-Dec-2015 at 5:36:26am, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring Application Splash Screen class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Splash_Dlg.h"
#include "TMon_Resource.h"
#include "TMon_Settings.h"

using namespace tmon;
using namespace tmon::common;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class  CSplashDlg_Layout
	{
	public:
		enum { // from PSD images
			e_frame_h  = 300,
			e_frame_w  = 550,
		};
	private:
		CWindow&      m_dlg_ref;
		RECT          m_client_area;
	public:
		CSplashDlg_Layout(CWindow& dlg_ref) : m_dlg_ref(dlg_ref)
		{
			if (m_dlg_ref)
				m_dlg_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT          GetClientArea(void)const
		{
			const RECT rc_ = m_client_area;
			return rc_;
		}

		VOID          RecalcPosition(void)
		{
			const SIZE sz_ = {
				details::CSplashDlg_Layout::e_frame_w,
				details::CSplashDlg_Layout::e_frame_h
			};

			RECT rc_ = {0};
			m_dlg_ref.GetWindowRect(&rc_);
			if (sz_.cx > 0)
				rc_.right = rc_.left + sz_.cx;
			if (sz_.cy > 0)
				rc_.bottom = rc_.top + sz_.cy;
			::AdjustWindowRect(
					&rc_,
					(DWORD)m_dlg_ref.GetWindowLong(GWL_STYLE),
					FALSE
				);
			{
				m_dlg_ref.SetWindowPos(
						NULL, 
						&rc_,
						SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOMOVE
					);
				m_dlg_ref.GetClientRect(&m_client_area);
			}
			m_dlg_ref.CenterWindow();
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CSplashDlg::CSplashDlgImpl::CSplashDlgImpl(void): 
	IDD(IDD_TMON_SPLASH_DLG)
{
}

CSplashDlg::CSplashDlgImpl::~CSplashDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CSplashDlg::CSplashDlgImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;

	m_panel.Destroy();

	return 0;
}

LRESULT CSplashDlg::CSplashDlgImpl::OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	{
		LONG_PTR l_style = TDialog::GetWindowLongPtr(GWL_STYLE);
		l_style &= ~WS_BORDER;
		l_style &= ~WS_DLGFRAME;
		TDialog::SetWindowLongPtr(GWL_STYLE, l_style);
	}
	details::CSplashDlg_Layout layout_(*this);
	layout_.RecalcPosition();

	HRESULT hr_ = S_OK;
	{
		const RECT rc_ = layout_.GetClientArea();
		hr_ = m_panel.Create(
				*this,
				rc_,
				true
			);
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CSplashDlg::CSplashDlg(void)
{
	global::GetScannerRef().Events().Subscribe(this);
}

CSplashDlg::~CSplashDlg(void)
{
	global::GetScannerRef().Events().Unsubscribe(this);
}

/////////////////////////////////////////////////////////////////////////////
VOID        CSplashDlg::IScannerEvent_OnEngineInitFinish(CScanError)
{
	if (m_dlg.IsWindow())
		m_dlg.EndDialog(IDOK);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CSplashDlg::Show(void)
{
#if 1
		m_dlg.DoModal();
#endif
	HRESULT hr_ = S_OK;
	return  hr_;
}