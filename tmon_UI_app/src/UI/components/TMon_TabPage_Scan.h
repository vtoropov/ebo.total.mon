#ifndef _TMONTABPAGESCAN_H_9C9F7882_9FCA_4e31_8887_67181AB54984_INCLUDED
#define _TMONTABPAGESCAN_H_9C9F7882_9FCA_4e31_8887_67181AB54984_INCLUDED
/*
	Created by Tech_dog (VToropov) on 2-Dec-2015 at 5:47:56pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring application scan tab page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_Renderer.h"
#include "UIX_Label.h"
#include "UIX_Image.h"

#include "TMon_Scan_QuickPanel.h"
#include "TMon_Scan_CustmPanel.h"
#include "TMon_Scan_FullPanel.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CImage;
	using ex_ui::controls::CLabel;

	using shared::avs::IScannerEventSink;
	using shared::avs::CScanError;
	using shared::avs::CScanFile;
	using shared::avs::TVirusDefinitionFiles;

	class CTabPageScan :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify,
		public  IScannerEventSink
	{
		typedef CPanelBase TPageBase;	
	private:
		CBackgroundTileRenderer  m_bkg_rnd;
		CImage                   m_tag_quick;
		CImage                   m_tag_custm;
		CImage                   m_tag_compl;
		CQuickScanPanel          m_pan_quick;
		CCustomScanPanel         m_pan_custm;
		CFullScanPanel           m_pan_compl;
		volatile bool            m_blocked;
		UINT_PTR                 m_timer_id;
		TVirusDefinitionFiles    m_files;
	public:
		CTabPageScan(IRenderer& _parent);
		~CTabPageScan(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId)        override sealed;
	private: // IScannerEventSink
		VOID        IScannerEvent_OnEngineInitFinish(CScanError)     override sealed;
		VOID        IScannerEvent_OnScanProcessBegin(VOID)           override sealed;
		VOID        IScannerEvent_OnScanProcessFinish(VOID)          override sealed;
	public:
		VOID        CheckAvsEngineState(void);
	};
}}}

#endif/*_TMONTABPAGESCAN_H_9C9F7882_9FCA_4e31_8887_67181AB54984_INCLUDED*/