/*
	Created by Tech_dog (VToropov) on 26-Dec-2015 at 8:15:41pm, GMT+8, Kuala Lumpur, Saturday;
	This is Total Monitoring Custom Scan Panel class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Scan_CustmPanel.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon::UI::components;
using namespace ex_ui::controls;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	typedef ::std::map<CAtlString, CAtlString>  TRecentUrlList;

	class CCustomScan_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
		static
		TRecentUrlList   m_url_lst;
	public:
		CCustomScan_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		VOID             AddScanRecentItem(LPCTSTR pszUrl)
		{
			CAtlString cs_key(pszUrl);
			cs_key.MakeLower();
			try
			{
				m_url_lst[cs_key] = CAtlString(pszUrl);
			}
			catch(::std::bad_alloc&){}
		}

		CAtlString       LastScanFolder(void)const
		{
			CAtlString cs_folder;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastScanFolderRegName(),
					cs_folder
				);
			if (cs_folder.IsEmpty())
				global::GetAppObjectRef().GetPath(cs_folder);
			return cs_folder;
		}

		HRESULT          LastScanFolder(LPCTSTR pszLastFolder)
		{
			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastScanFolderRegName(),
					pszLastFolder
				);
			return  hr_;
		}

		TRecentUrlList   RecentScanList(void)
		{
			TRecentUrlList lst_;

			LONG lCount = 0;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastScanRecentCountRegName(),
					lCount
				);
			for (LONG i_ = 0; i_ < lCount; i_++)
			{
				CAtlString cs_item;
				cs_item.Format(
						this->_LastScanRecentItemRegName(),
						i_
					);
				CAtlString cs_value;
				HRESULT hr_ = m_stg.Load(
					this->_CommonRegFolder(),
					cs_item,
					cs_value
				);
				if (S_OK != hr_)
					break;
				if (cs_value.IsEmpty())
					continue;
				try
				{
					lst_.insert(
							::std::make_pair(cs_value, cs_value)
						);
				}
				catch(::std::bad_alloc&){ break; }
			}
			m_url_lst = lst_;
			return lst_;
		}

		VOID             RecentScanList(const bool bSave)
		{
			bSave;
			const LONG lCount = static_cast<LONG>(m_url_lst.size());

			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastScanRecentCountRegName(),
					lCount
				);
			if (S_OK != hr_)
				return;

			INT n_iter_ = 0;
			for (TRecentUrlList::const_iterator it_ = m_url_lst.begin(); it_ != m_url_lst.end(); ++it_)
			{
				CAtlString cs_item;
				cs_item.Format(
						this->_LastScanRecentItemRegName(),
						n_iter_
					);
				hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					cs_item,
					it_->second.GetString()
				);
				n_iter_+= 1;
			}
		}

		const
		TRecentUrlList&  RecentScanListRef(void)const
		{
			return m_url_lst;
		}

		bool             RemoveFolderFromList(const CAtlString& _folder)
		{
			TRecentUrlList::iterator it_ = m_url_lst.find(_folder);
			if (it_ != m_url_lst.end())
			{
				m_url_lst.erase(it_);
				return true;
			}
			else
				return false;
		}

	private:
		CAtlString      _CommonRegFolder(void)            const { return CAtlString(_T("Common"));             }
		CAtlString      _LastScanFolderRegName(void)      const { return CAtlString(_T("LastScanFolder"));     }
		CAtlString      _LastScanRecentCountRegName(void) const { return CAtlString(_T("RecentScanItems"));    }
		CAtlString      _LastScanRecentItemRegName(void)  const { return CAtlString(_T("RecentDownItem_%.4d"));} 
	};

	TRecentUrlList CCustomScan_Registry::m_url_lst;

	class CCustomScan_Init
	{
	private:
		const
		CWindow& m_page_ref;
	public:
		CCustomScan_Init(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		VOID     InitRecentURLs(void)const
		{
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SCAN_EDT_FILES);
			if (!cbo_)
				return;

			CCustomScan_Registry reg_;
			TRecentUrlList lst_ = reg_.RecentScanList();
			for (TRecentUrlList::const_iterator it_ = lst_.begin(); it_ != lst_.end(); ++it_)
				cbo_.AddString(it_->first);
		}

		VOID     RemoveFolderFromRecentList(const CAtlString& _folder)
		{
			CCustomScan_Registry reg_;
			const bool bRemoved = reg_.RemoveFolderFromList(_folder);
			if (!bRemoved)
				return;

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SCAN_EDT_FILES);
			if (!cbo_)
				return;
			cbo_.ResetContent();
			const TRecentUrlList& lst_ = reg_.RecentScanListRef();
			for ( TRecentUrlList::const_iterator it_ = lst_.begin(); it_ != lst_.end(); ++it_ )
				cbo_.AddString(it_->first);
		}
	};

	class CCustomScan_Handler
	{
	private:
		CWindow&      m_page_ref;
		DWORD&        m_locker_ref;
	public:
		CCustomScan_Handler(CWindow& page_ref, DWORD& _locker_ref) : m_page_ref(page_ref), m_locker_ref(_locker_ref){}
	public:
		BOOL          OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;

			if (false){}
			else if (IDC_TMON_SCAN_BTN_BROWSE == wCtrlId)
			{
				CAtlString cs_target = this->_SelectTarget();
				if (!cs_target.IsEmpty())
				{
					CAtlString cs_msg;
					cs_msg.Format(
							_T("Folder/file %s is selected"),
							cs_target
						);
					global::SetInfoMessage(
							cs_msg
						);
				}
			}
			else if (IDC_TMON_SCAN_BTN_SCAN == wCtrlId)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SCAN_EDT_FILES);
				if (!ctrl_)
					return bHandled;

				CAtlString cs_target;
				ctrl_.GetWindowText(cs_target);

				if (cs_target.IsEmpty())
				{
					global::SetErrorMessage(
							_T("No folder/file is selected")
						);
					return bHandled;
				}

				CGenericFolder folder_(cs_target);
				if (!folder_.IsExist())
				{
					CCustomScan_Init init_(m_page_ref);
					init_.RemoveFolderFromRecentList(cs_target);

					global::SetWarnMessage(
							_T("The selected folder doesn't exist")
						);
					return bHandled;
				}
				
				ctrl_ = m_page_ref.GetDlgItem(wCtrlId);
				if (ctrl_)
					ctrl_.EnableWindow(false);

				HRESULT hr_ = global::GetScannerRef().Lock(m_locker_ref);
				if (FAILED(hr_))
				{
					global::SetErrorMessage(_T("Scanner engine is not ready, please try later."));
					return bHandled;
				}

				hr_ = global::GetScannerRef().ScanFolderOrFile(cs_target);
				if (FAILED(hr_))
				{
					global::SetErrorMessage(
						global::GetScannerRef().Error()
					);
					global::GetScannerRef().Unlock(m_locker_ref);
				}
				else
				{
					CCustomScan_Registry reg_;
					reg_.AddScanRecentItem(cs_target);
				}
			}
			else
				bHandled = FALSE;

			return bHandled;
		}

	private:
		CAtlString   _SelectTarget(void)
		{
			::WTL::CFolderDialog dlg_(
					NULL,
					_T("Choose Folder or File for Scanning"),
					BIF_RETURNONLYFSDIRS|BIF_USENEWUI|BIF_BROWSEINCLUDEFILES|BIF_NONEWFOLDERBUTTON
				);
			CCustomScan_Registry reg_;
			CAtlString cs_folder = reg_.LastScanFolder();

			dlg_.SetInitialFolder(cs_folder);

			const INT_PTR nResult = dlg_.DoModal();
			if (IDOK == nResult)
			{
				cs_folder = dlg_.GetFolderPath();
				reg_.LastScanFolder(cs_folder);

				CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SCAN_EDT_FILES);
				if (ctrl_)
					ctrl_.SetWindowText(cs_folder);
			}
			return cs_folder;
		}
	};

	class CCustomScan_Layout
	{
		enum _e{
				e_cap_label_w = 400,
				e_cap_label_h =  30,
				e_sta_label_w = 500,
				e_sta_label_h = 100,
				e_tag_w       = 270,
				e_tag_h       = 205,
				e_margins_w   =  10,
				e_margins_h   =  15,
			};
	private:
		CWindow&      m_page_ref;
		RECT          m_client_area;
	public:
		CCustomScan_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT     GetFolderLabelRect(void)const
		{
			const INT nLeft = 15;
			const INT nTop  = __H(m_client_area) / 2 - CCustomScan_Layout::e_cap_label_h * 2;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CCustomScan_Layout::e_cap_label_w,
					nTop  + CCustomScan_Layout::e_cap_label_h
				};
			return rc_;
		}

		RECT     GetTagCustomRect(void)const
		{
			RECT rc_ = this->GetTagQuickRect();
			::OffsetRect(
					&rc_,
					0,
					CCustomScan_Layout::e_tag_h + CCustomScan_Layout::e_margins_w
				);
			return rc_;
		}

		RECT     GetTagQuickRect(void)const
		{
			const INT nLeft = CCustomScan_Layout::e_margins_w;
			const INT nTop  = CCustomScan_Layout::e_margins_h;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CCustomScan_Layout::e_tag_w,
					nTop  + CCustomScan_Layout::e_tag_h
				};
			return rc_;
		}

		VOID     InitCtrls(void)
		{
			WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SCAN_EDT_FILES);
			if (cbo_)
			{
				ex_ui::draw::common::CFont fnt_(
							_T("verdana"),
							eCreateFontOption::eRelativeSize,
							+1
						);
				cbo_.SetCueBannerText(_T("Type file/folder name"));
				cbo_.SetFont(fnt_.Detach());
			}
			const SIZE sz_ = {-1, +1};

			AlignComboHeightTo(
					m_page_ref,
					IDC_TMON_SCAN_EDT_FILES,
					25
				);

			AlignCtrlsByHeight(
					m_page_ref,
					IDC_TMON_SCAN_EDT_FILES ,
					IDC_TMON_SCAN_BTN_BROWSE,
					sz_
				);
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CCustomScanPanel::CCustomScanPanel(IRenderer& _parent):
	TScanBase  (IDD_TMON_SCAN_CUSTM_PANEL, IDR_TMON_SCAN_CUSTM_PANEL_BKGND),
	m_fld_label(CControlCrt(3, TScanBase::m_bkg_rnd, *this))
{
	_parent;
}

CCustomScanPanel::~CCustomScanPanel(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CCustomScanPanel::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TScanBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TScanBase::GetWindow_Ref();

	TScanBase::m_cap_label.Text(
			_T("Performs a scan of a selected folder or file.")
		);
	details::CCustomScan_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetFolderLabelRect();
		hr_ = m_fld_label.Create(
				host_,
				rc_,
				NULL //_T("Type folder/file path below or press [Browse] button")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
	}
	layout_.InitCtrls();

	details::CCustomScan_Init init_(host_);
	init_.InitRecentURLs();

	this->UpdateCtrlState(true);

	m_scheduler.Events().Subscribe(this);
	m_scheduler.Start();

	return hr_;
}

HRESULT     CCustomScanPanel::Destroy(void)
{
	m_scheduler.Stop();
	m_scheduler.Events().Unsubscribe(this);

	details::CCustomScan_Registry reg_;
	reg_.RecentScanList(true);

	m_fld_label.Destroy();
	
	HRESULT hr_ = TScanBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT     CCustomScanPanel::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			details::CCustomScan_Handler handler_(
						TScanBase::GetWindow_Ref(),
						TScanBase::m_locker
					);
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);

		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

VOID        CCustomScanPanel::IScannerEvent_OnEngineInitFinish(CScanError _error)
{
	this->UpdateCtrlState(true);
	TScanBase::IScannerEvent_OnEngineInitFinish(_error);
}

VOID        CCustomScanPanel::IScannerEvent_OnLock(const DWORD dwLockOwnerId)
{
	dwLockOwnerId; // doesn't care who is locker; just disables all scan related controls;
	this->UpdateCtrlState(false);
}

VOID        CCustomScanPanel::IScannerEvent_OnScanProcessFinish(VOID)
{
	TScanBase::IScannerEvent_OnScanProcessFinish();
	if (TScanBase::m_locker) // this module is the owner of a scan process;
		global::GetScannerRef().Unlock(TScanBase::m_locker);
	else
		this->UpdateCtrlState(true);
}

VOID        CCustomScanPanel::IScannerEvent_OnUnlock(const DWORD dwLockOwnerId)
{
	if (TScanBase::m_locker == dwLockOwnerId)
		TScanBase::m_locker = 0;
	this->UpdateCtrlState(true); // the scanner has been unlocked; now a scan is enabled
}

/////////////////////////////////////////////////////////////////////////////

VOID        CCustomScanPanel::ITaskSchedulerEvent_OnCustomTask(const CCustomTask _task)
{
	if (!global::GetScannerRef().IsReady())
		return;

	CGenericFolder folder_(_task.Path());
	if (!folder_.IsExist())
	{
		global::SetWarnMessage(
			_T("Task scheduler: the specified folder doesn't exist")
			);
		return;
	}

	HRESULT hr_ = global::GetScannerRef().Lock(TScanBase::m_locker);
	if (FAILED(hr_))
	{
		global::SetWarnMessage(_T("Task scheduler cannot gain exclusive access to scanner engine."));
		return;
	}

	hr_ = global::GetScannerRef().ScanFolderOrFile(folder_.Path());
	if (FAILED(hr_))
	{
		global::SetErrorMessage(
			global::GetScannerRef().Error()
		);
		global::GetScannerRef().Unlock(TScanBase::m_locker);
	}
}

VOID        CCustomScanPanel::ITaskSchedulerEvent_OnError(const CSysError _err)
{
	global::SetErrorMessage(_err);
	if (TScanBase::m_locker)
		global::GetScannerRef().Unlock(TScanBase::m_locker);
}

/////////////////////////////////////////////////////////////////////////////

VOID        CCustomScanPanel::UpdateCtrlState(const bool bEnabled)
{
	CWindow host_ = TScanBase::GetWindow_Ref();
	const UINT ctrlId[] = {
			IDC_TMON_SCAN_BTN_SCAN,
			IDC_TMON_SCAN_BTN_BROWSE
		};
	for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
	{
		CWindow ctrl_ = host_.GetDlgItem(ctrlId[i_]);
		if (ctrl_)
			ctrl_.EnableWindow(bEnabled && global::GetScannerRef().IsInitialized());
	}
}