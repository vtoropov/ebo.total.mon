/*
	Created by Tech_dog (VToropov) on 16-Dec-2015 at 8:34:47pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring Virus Database Update Tab page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_TabPage_Update.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon::UI::components;

using namespace ex_ui::controls;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;
using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CTabPageUpdate_Layout
	{
	private:
		CWindow&     m_page_ref;
		RECT         m_client_area;
	public:
		CTabPageUpdate_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT         GetDateLabelRect(void)const
		{
			const INT nLeft = 340;
			const INT nTop  = 170;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 250,
					nTop  +  30
				};
			return rc_;
		}

		RECT         GetDateTimerRect(void)const
		{
			const SIZE sz_ = CUpdateTimer::GetDefaultSize();
			RECT rc_   = this->GetDateLabelRect();
			rc_.top    = rc_.bottom;
			rc_.bottom = rc_.top  + sz_.cy;
			rc_.right  = rc_.left + sz_.cx;

			return rc_;
		}

		RECT         GetProgPanelRect(void)const
		{
			RECT rc_ = this->GetDateTimerRect();
			::OffsetRect(&rc_, -15, __H(rc_) * 2);
			return rc_;
		}

		RECT         GetPromptLabelRect(void)const
		{
			const INT nLeft = 292;
			const INT nTop  =  96;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 350,
					nTop  +  30
				};
			return rc_;
		}

		RECT         GetVerLabelRect(void)const
		{
			const INT nLeft =  20;
			const INT nTop  = 370;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 250,
					nTop  +  60
				};
			return rc_;
		}

		RECT         GetTitleLabelRect(void)const
		{
			const INT nLeft = 290;
			const INT nTop  =  53;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 350,
					nTop  +  30
				};
			return rc_;
		}
	};

	class CTabPageUpdate_Init
	{
	private:
		CWindow&     m_page_ref;
	public:
		CTabPageUpdate_Init(CWindow& page_ref) : m_page_ref(page_ref)
		{}
	public:
		VOID        UpdateCtrlState(const bool bEnabled)
		{
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_UPDATE_BTN_NOW);
			if (ctrl_)
				ctrl_.EnableWindow(bEnabled);
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageUpdate::CTabPageUpdate(IRenderer& _parent):
	TPageBase  (IDD_TMON_UPDATE_TAB_PAGE , *this),
	m_bkg_rnd  (IDR_TMON_UPDATE_TAB_BKGND, TPageBase::GetWindow_Ref()),
	m_top_label(CControlCrt(1, m_bkg_rnd , *this)),
	m_msg_label(CControlCrt(2, m_bkg_rnd , *this)),
	m_lst_label(CControlCrt(3, m_bkg_rnd , *this)),
	m_ver_label(CControlCrt(4, m_bkg_rnd , *this)),
	m_updater  (
			global::GetScannerRef(),
			*this
		),
	m_evt_object(*this, _variant_t((LONG)-1))
{
	_parent;
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);
	m_evt_object.Create();
}

CTabPageUpdate::~CTabPageUpdate(void)
{
	m_evt_object.Destroy();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageUpdate::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CTabPageUpdate_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetTitleLabelRect();
		hr_ = m_top_label.Create(
				host_,
				rc_,
				_T("The program is updated automatically")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_top_label.ForeColor(RGB(0x33, 0x66, 0x66));
		m_top_label.FontSize(11);
	}
	{
		const RECT rc_ = layout_.GetPromptLabelRect();
		hr_ = m_msg_label.Create(
				host_,
				rc_,
				_T("You can run antivirus database update manually")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_msg_label.ForeColor(RGB(0x33, 0x33, 0x33));
		m_msg_label.FontSize(10);
	}
	{
		const RECT rc_ = layout_.GetDateLabelRect();
		hr_ = m_lst_label.Create(
				host_,
				rc_,
				_T("Since the last update")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_lst_label.ForeColor(RGB(0x33, 0x66, 0x66));
		m_lst_label.FontSize(12);
	}
	{
		const RECT rc_ = layout_.GetVerLabelRect();
		hr_ = m_ver_label.Create(
				host_,
				rc_,
				_T("Virus Definitions: 0\nDatabase Release: unknown")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_ver_label.ForeColor(RGB(0x99, 0xcc, 0xcc));
		m_ver_label.FontSize(10);
	}
	{
		const RECT rc_ = layout_.GetDateTimerRect();
		hr_ = m_upd_timer.Create(
				host_,
				rc_
			);
	}
	{
		const RECT rc_ = layout_.GetProgPanelRect();
		hr_ = m_prg_panel.Create(
				host_,
				rc_,
				false
			);
	}
	return hr_;
}

HRESULT    CTabPageUpdate::Destroy(void)
{
	if (m_updater.IsRunning())
		m_updater.Interrupt();

	m_prg_panel.Destroy();
	m_ver_label.Destroy();
	m_upd_timer.Destroy();
	m_lst_label.Destroy();
	m_msg_label.Destroy();
	m_top_label.Destroy();

	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

HRESULT    CTabPageUpdate::Show(const bool _flag)
{
	static bool bFirstTime = true;
	if (_flag)
	if (bFirstTime)
	{
		bFirstTime = false;
		CAtlString cs_ver;
		cs_ver.Format(
				_T("Virus Definitions: %d\nDatabase Release: %s"),
				global::GetScannerRef().VirusDatabase().Records(),
				global::GetScannerRef().VirusDatabase().TimestampAsText()
			);
		m_ver_label.Text(cs_ver);
	}
	HRESULT hr_ = TPageBase::Show(_flag);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageUpdate::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			if (false){}
			else if (IDC_TMON_UPDATE_BTN_NOW == ctrlId)
			{
				CApplicationCursor wc_(IDC_APPSTARTING);

				HRESULT hr_ = S_OK;
				{
					if (m_updater.IsRunning())
						m_updater.Interrupt();

					CCtrlAutoState auto_(TPageBase::GetWindow_Ref(), ctrlId, false);
					hr_ = m_updater.CheckVersion();
					if (FAILED(hr_))
					{
						global::SetErrorMessage( m_updater.Error());
						return 0;
					}
					CSysError err_obj = m_updater.Error();
					if (S_FALSE == err_obj.GetHresult())
					{
						global::SetInfoMessage( err_obj.GetDescription());
						return 0;
					}
				}
				//hr_ = m_updater.DownloadData();
				hr_ = m_updater.UnpackDatabase();
				if (FAILED(hr_))
				{
					global::SetErrorMessage( m_updater.Error());
					return 0;
				}
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageUpdate::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

bool       CTabPageUpdate::IUpdater_OnCanContinue (void)
{
	return true;
}

void       CTabPageUpdate::IUpdater_OnDownloadFinished(void)
{
	m_prg_panel.SetCaption(_T(""));
	m_prg_panel.Show(false);

	details::CTabPageUpdate_Init init_(TPageBase::GetWindow_Ref());
	init_.UpdateCtrlState(true);
	global::SetInfoMessage(
			_T("Virus definition database update is downloaded")
		);
	const bool bAsync = true;
	m_evt_object.Fire(bAsync, CTabPageUpdate::eUnpackEvent);
}

void       CTabPageUpdate::IUpdater_OnDataReceived(const DWORD dwReceived, const DWORD dwTotal)
{
	const  INT nPosition = INT((FLOAT(dwReceived) / FLOAT(dwTotal)) * 100);
	static INT nPrevious = 0;
	if (nPrevious == nPosition)
		return;
	
	nPrevious  = nPosition;
	m_prg_panel.SetProgress(nPosition);

	CAtlString cs_text;
		cs_text.Format(
				_T("Downloading virus database update %d%%"),
				nPosition
			);
	m_prg_panel.SetCaption(cs_text);
}

void       CTabPageUpdate::IUpdater_OnDownloadStart(void)
{
	details::CTabPageUpdate_Init init_(TPageBase::GetWindow_Ref());
	init_.UpdateCtrlState(false);

	m_prg_panel.Show(true);
	m_prg_panel.SetCaption(_T("Download is starting..."));
}

void       CTabPageUpdate::IUpdater_OnUnpackFinish(void)
{
	m_prg_panel.SetCaption(_T(""));
	m_prg_panel.Show(false);

	details::CTabPageUpdate_Init init_(TPageBase::GetWindow_Ref());
	init_.UpdateCtrlState(true);
	global::SetInfoMessage(
			_T("Virus definition database update is unpacked")
		);
	const bool bAsync = true;
	m_evt_object.Fire(bAsync, CTabPageUpdate::eInstallEvent);
}

void       CTabPageUpdate::IUpdater_OnUnpackProgress(const DWORD dwIndex, const DWORD dwTotal, CAtlString _file)
{
	const  INT nPosition = INT((FLOAT(dwIndex) / FLOAT(dwTotal)) * 100);
	static INT nPrevious = 0;
	if (nPrevious == nPosition)
		return;
	
	nPrevious  = nPosition;
	m_prg_panel.SetProgress(nPosition);

	CAtlString cs_text;
		cs_text.Format(
				_T("Unpacking the file: %s"),
				_file
			);
	m_prg_panel.SetCaption(cs_text);
}

void       CTabPageUpdate::IUpdater_OnUnpackStart(void)
{
	details::CTabPageUpdate_Init init_(TPageBase::GetWindow_Ref());
	init_.UpdateCtrlState(false);

	m_prg_panel.Show(true);
	m_prg_panel.SetCaption(_T("Unpack is starting..."));
}

void       CTabPageUpdate::IUpdater_OnUpdateError(CSysError _error)
{
	global::SetErrorMessage(_error);
	details::CTabPageUpdate_Init init_(TPageBase::GetWindow_Ref());
	init_.UpdateCtrlState(true);

	m_prg_panel.Show(false);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageUpdate::GenericEvent_OnNotify(const _variant_t v_evt_id)
{
	if (VT_I4 != v_evt_id.vt)
		return DISP_E_TYPEMISMATCH;

	HRESULT hr_ = S_OK;

	switch (v_evt_id.lVal)
	{
	case CTabPageUpdate::eUnpackEvent:
		{
			if (m_updater.IsRunning())
				m_updater.Interrupt();

			hr_ = m_updater.UnpackDatabase();
			if (FAILED(hr_))
				global::SetErrorMessage( m_updater.Error());
		} break;
	case CTabPageUpdate::eInstallEvent:
		{
		} break;
	}
	return  hr_;
}