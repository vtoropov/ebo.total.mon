#ifndef _TMONSETUPTABSET_H_5F361DB2_7DAD_4f4e_B44F_4D5C3B865E32_INCLUDED
#define _TMONSETUPTABSET_H_5F361DB2_7DAD_4f4e_B44F_4D5C3B865E32_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 2:11:00am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Setup Tabset class declaration file.
*/
#include "TMon_Setup_WebFilter.h"
#include "TMon_Setup_GenPage.h"
#include "TMon_Setup_LogPage.h"
#include "TMon_Setup_ProPage.h"
#include "TMon_Setup_JobPage.h"

#include "UIX_CommonCtrlDefs.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::controls::defs::IControlNotify;

	class CSetupTabSet
	{
	private:
		IControlNotify&     m_evt_sink;
		::WTL::CTabCtrl     m_cTabCtrl;
		CGeneralSetupPage   m_gen_page;
		CProSetupPage       m_pro_page;
		CJobSetupPage       m_job_page;
		CLogSetupPage       m_log_page;
		CWebFilterSetupPage m_web_page;
	public:
		CSetupTabSet(IControlNotify&);
		~CSetupTabSet(void);
	public:
		HRESULT           Create(const HWND hParent, const RECT& rcArea);
		HRESULT           Destroy(void);
		UINT              Identifier(void)const;
		INT               SelectedIndex(void)const;
		VOID              UpdateLayout(void);
	};
}}}

#endif/*_TMONSETUPTABSET_H_5F361DB2_7DAD_4f4e_B44F_4D5C3B865E32_INCLUDED*/