/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 2:15:04am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Setup Tabset class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Setup_TabsSet.h"
#include "TMon_Resource.h"

using namespace tmon::UI::components;

#include "Shared_Registry.h"

using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CSetupTabSet_Layout
	{
	private:
		RECT        m_area;
	public:
		CSetupTabSet_Layout(const RECT& rcArea) : m_area(rcArea)
		{
		}
	public:
		RECT        GetTabsArea(void)const
		{
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};

	class CSetupTabSet_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CSetupTabSet_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		INT     LastIndex(void)const
		{
			LONG lIndex = 0;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					lIndex
				);
			if (lIndex < 0)
				lIndex = 0;
			return static_cast<INT>(lIndex);
		}

		HRESULT LastIndex(const INT nIndex)
		{
			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastIndexRegName(),
					static_cast<LONG>(nIndex)
				);
			return  hr_;
		}
	private:
		LPCTSTR     _CommonRegFolder(void) const
		{
			static CAtlString cs_folder(_T("Settings"));
			return cs_folder;
		}

		LPCTSTR     _LastIndexRegName(void) const
		{
			static CAtlString cs_name(_T("LastTabIndex"));
			return cs_name;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CSetupTabSet::CSetupTabSet(IControlNotify& _evt_snk):
	m_evt_sink(_evt_snk)  ,
	m_web_page(m_cTabCtrl),
	m_gen_page(m_cTabCtrl),
	m_log_page(m_cTabCtrl),
	m_pro_page(m_cTabCtrl),
	m_job_page(m_cTabCtrl)
{
}

CSetupTabSet::~CSetupTabSet(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CSetupTabSet::Create(const HWND hParent, const RECT& rcArea)
{
	if(!::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return E_INVALIDARG;

	details::CSetupTabSet_Layout layout_(rcArea);
	RECT rcTabs = layout_.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			this->Identifier()
		);
	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());

	const DWORD dwFlags = SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW;

	INT nIndex = 0;

	m_cTabCtrl.AddItem(m_gen_page.GetPageTitle());
	{
		m_gen_page.Create(m_cTabCtrl.m_hWnd);
		m_gen_page.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_gen_page.IsWindow())m_gen_page.Index(nIndex++);
	}

	const bool bShowLogPage = false;

	if (bShowLogPage)
	{
		m_cTabCtrl.AddItem(m_log_page.GetPageTitle());
		{
			m_log_page.Create(m_cTabCtrl.m_hWnd);
			m_log_page.SetWindowPos(
					HWND_TOP,
					0, 0, 0, 0,
					dwFlags
				);
			if (m_log_page.IsWindow())m_log_page.Index(nIndex++);
		}
	}

	m_cTabCtrl.AddItem(m_pro_page.GetPageTitle());
	{
		m_pro_page.Create(m_cTabCtrl.m_hWnd);
		m_pro_page.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_pro_page.IsWindow())m_pro_page.Index(nIndex++);
	}

	m_cTabCtrl.AddItem(m_job_page.GetPageTitle());
	{
		m_job_page.Create(m_cTabCtrl.m_hWnd);
		m_job_page.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_job_page.IsWindow())m_job_page.Index(nIndex++);
	}

	m_cTabCtrl.AddItem(m_web_page.GetPageTitle());
	{
		m_web_page.Create(m_cTabCtrl.m_hWnd);
		m_web_page.SetWindowPos(
				HWND_TOP,
				0, 0, 0, 0,
				dwFlags
			);
		if (m_web_page.IsWindow())m_web_page.Index(nIndex++);
	}

	details::CSetupTabSet_Registry reg_;
	nIndex = reg_.LastIndex();

	m_cTabCtrl.SetCurSel(nIndex);
	this->UpdateLayout();
	this->m_evt_sink.IControlNotify_OnClick(this->Identifier());

	return S_OK;
}

HRESULT       CSetupTabSet::Destroy(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	details::CSetupTabSet_Registry reg_;
	reg_.LastIndex(nTabIndex);

	CTabPageBase* pPages[] = {
		&m_web_page,
		&m_log_page,
		&m_job_page,
		&m_pro_page,
		&m_gen_page
	};

	for (INT i_ = 0; i_ < _countof(pPages); i_++)
	{
		CTabPageBase*  pPage = pPages[i_];
		if (!pPage || !pPage->IsWindow())
			continue;
		pPage->DestroyWindow();
		pPage->m_hWnd = NULL;
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}

UINT          CSetupTabSet::Identifier(void)const
{
	return IDC_TMON_SETUP_TABSET;
}

INT           CSetupTabSet::SelectedIndex(void)const
{
	return m_cTabCtrl.GetCurSel();
}

VOID          CSetupTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel();

	CTabPageBase* pPages[] = {
		&m_web_page,
		&m_log_page,
		&m_job_page,
		&m_pro_page,
		&m_gen_page
	};

	for (INT i_ = 0; i_ < _countof(pPages); i_++)
	{
		CTabPageBase*  pPage = pPages[i_];
		if (!pPage || !pPage->IsWindow())
			continue;
		pPage->ShowWindow(nTabIndex == pPage->Index() ? SW_SHOW : SW_HIDE);
	}
}