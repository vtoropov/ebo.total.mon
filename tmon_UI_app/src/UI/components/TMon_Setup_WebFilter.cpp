/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 3:03:08am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Web Filter Setup Tab page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Setup_WebFilter.h"
#include "TMon_Resource.h"

using namespace tmon::UI;
using namespace tmon::UI::components;

using namespace tmon::data;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CWebFilterSetupPage_Layout
	{
	private:
		CWindow&     m_page_ref;
		RECT         m_client_area;
	public:
		CWebFilterSetupPage_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT         GetListRect(void)const
		{
			const INT nLeft =  88;
			const INT nTop  = 125;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 333,
					nTop  + 120
				};
			return rc_;
		}

		VOID         InitCtrls(void)
		{
			const SIZE sz_ = {-1, +1};

			AlignCtrlsByHeight(
						m_page_ref,
						IDC_TMON_SETUP_WEB_URL,
						IDC_TMON_SETUP_WEB_ADD,
						sz_
					);
		}
	};

	class CWebFilterSetupPage_ListWrap
	{
	private:
		CWindow&     m_page_ref;
		CWindow&     m_list;
	public:
		CWebFilterSetupPage_ListWrap(CWindow& _page, CWindow& _list) : m_list(_list), m_page_ref(_page)
		{
		}
	public:
		HRESULT  Create(const RECT& _rc)const
		{
			if (m_list.IsWindow())
				return (HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS));

			RECT rc_ = _rc;
			m_list.Create(
					_T("SysListView32"),
					m_page_ref,
					rc_,
					NULL,
					WS_CHILD|WS_VISIBLE|LVS_REPORT|WS_BORDER|LVS_NOCOLUMNHEADER,
					0,
					IDC_TMON_SETUP_WEB_LIST
				);
			if (!m_list.IsWindow())
				return (HRESULT_FROM_WIN32(::GetLastError()));

			ListView_SetExtendedListViewStyle((HWND)m_list, /*LVS_EX_CHECKBOXES|*/LVS_EX_FULLROWSELECT|LVS_EX_LABELTIP);

			const HFONT hFont = m_page_ref.GetFont();
			m_list.SetFont(hFont);

			LVCOLUMN lvc ={0};
			lvc.mask     = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
			lvc.fmt      = LVCFMT_LEFT;
			lvc.pszText  = _T("URL");
			lvc.cx       = 310;

			ListView_InsertColumn(m_list, 0, &lvc);

			return S_OK;
		}

		INT      GetSelectedIndex(void)
		{
			INT nSelected = ListView_GetNextItem(m_list, -1, LVNI_SELECTED);
			return nSelected;
		}

		CAtlString GetSelectedUrl(void)
		{
			CAtlString cs_url;
			const INT nIndex = this->GetSelectedIndex();
			if (nIndex < 0)
				return cs_url;

			::WTL::CListViewCtrl ctrl_ = m_list;
			ctrl_.GetItemText(nIndex, 0, cs_url);

			return cs_url;
		}

		VOID     Initialize(const TWebBlackList& _lst)
		{
			for (TWebBlackList::const_iterator it_ = _lst.begin(); it_ != _lst.end(); ++it_)
				this->InsertRow(it_->second);
		}

		VOID     InsertRow(CAtlString _url)
		{
			const INT count_ = ListView_GetItemCount(m_list);

			LVITEM item_  = {0};
			item_.mask    = LVIF_TEXT;
			item_.iItem   = count_;
			item_.pszText = _url.GetBuffer();
			item_.iImage  = -1;

			ListView_InsertItem(m_list, &item_);
			ListView_EnsureVisible(
					m_list,
					count_,
					FALSE
				);
		}

		VOID     RemoveRow(void)
		{
			const INT nSelected = ListView_GetNextItem(m_list, -1, LVNI_SELECTED);
			if (-1 == nSelected)
				global::SetErrorMessage(_T("No url is selected"));
			else
				ListView_DeleteItem(m_list, nSelected);
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CWebFilterSetupPage::CWebFilterSetupPage(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_TMON_SETUP_WEB_PAGE, tab_ref, *this)
{
}

CWebFilterSetupPage::~CWebFilterSetupPage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CWebFilterSetupPage::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;

			details::CWebFilterSetupPage_Layout layout_(*this);
			layout_.InitCtrls();

			details::CWebFilterSetupPage_ListWrap wrap_(*this, m_list);
			wrap_.Create(layout_.GetListRect());

			HRESULT hr_ = m_provider.Load();
			if (!FAILED(hr_))
				wrap_.Initialize(m_provider.List());

			hr_ = m_filter.Initialize();
			if (FAILED(hr_))
				global::SetErrorMessage(
						m_filter.Error()
					);

			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			m_provider.Save();
			m_filter.Terminate();
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			if (false){}
			else if (IDC_TMON_SETUP_WEB_ADD == ctrlId)
			{
				CWindow ctrl_ = TBasePage::GetDlgItem(IDC_TMON_SETUP_WEB_URL);
				if (!ctrl_)
					return bHandled;
				CAtlString cs_url;
				ctrl_.GetWindowText(cs_url);
				if (cs_url.IsEmpty())
					global::SetErrorMessage(_T("Empty URL cannot be added"));
				else
				{
					HRESULT hr_ = m_provider.Add(cs_url);
					if (!FAILED(hr_))
					{
						details::CWebFilterSetupPage_ListWrap wrap_(*this, m_list);
						wrap_.InsertRow(cs_url);
						ctrl_.SetWindowText(NULL);
						global::SetInfoMessage(
									_T("New URL is added to black list")
								);
					}
				}
			}
			else if (IDC_TMON_SETUP_WEB_REMOVE == ctrlId)
			{
				details::CWebFilterSetupPage_ListWrap wrap_(*this, m_list);

				CAtlString cs_url = wrap_.GetSelectedUrl();
				if (cs_url.IsEmpty())
					global::SetErrorMessage(_T("No URL is selected"));
				else
				{
					HRESULT hr_ = m_provider.Remove(cs_url);
					if (!FAILED(hr_))
					{
						wrap_.RemoveRow();
						global::SetInfoMessage(
									_T("Selected URL has been removed from black list")
								);
					}
				}
			}
			else if (IDC_TMON_SETUP_WEB_APPLY == ctrlId)
			{
				TWebBlackList lst_ = m_provider.List();

				HRESULT hr_ = m_filter.GenerateHashTable(lst_);
				if (FAILED(hr_))
					global::SetErrorMessage(m_provider.Error());
			}
			else if (IDC_TMON_SETUP_WEB_ENABLE == ctrlId)
			{
				WTL::CButton chk_ = TBasePage::GetDlgItem(ctrlId);
				const bool bChecked = !!chk_.GetCheck();
				if (bChecked)
				{
					HRESULT hr_ = m_filter.Activate();
					if (FAILED(hr_))
						global::SetErrorMessage(
								m_filter.Error()
							);
					else
						global::SetInfoMessage(
								_T("Zillya web filter has successfully been installed")
							);
				}
				else
				{
					HRESULT hr_ = m_filter.Deactivate();
					if (FAILED(hr_))
						global::SetErrorMessage(
								m_filter.Error()
							);
					else
						global::SetInfoMessage(
								_T("Zillya web filter has successfully been uninstalled")
							);
				}
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

VOID       CWebFilterSetupPage::IWebFilterEvent_OnBlock(CAtlString _url)
{
	CAtlString cs_msg;
	cs_msg.Format(
			_T("Zillya Web Filter has blocked: %s"),
			_url.GetString()
		);
	global::SetWarnMessage(
			cs_msg
		);
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CWebFilterSetupPage::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Zillya Web Filter"));
	return cs_title;
}