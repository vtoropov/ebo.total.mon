#ifndef _TMONSTATUSBAR_H_6AED36A2_5E1F_493f_9151_7C2D74310354_INCLUDED
#define _TMONSTATUSBAR_H_6AED36A2_5E1F_493f_9151_7C2D74310354_INCLUDED
/*
	Created by Tech_dog (VToropov) on 4-Dec-2015 at 11:03:14am, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring application status bar UI component class declaration file.
*/
#include "UIX_Renderer.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	class CStatusType
	{
	public:
		enum _e{
			eInfo    = 0,
			eWarning = 1,
			eWaiting = 2,
			eError   = 3,
		};
	};

	class CStatusBarEx
	{
	private:
		class CStatusBarExWnd :
			public  ::ATL::CWindowImpl<CStatusBarExWnd>
		{
			typedef ::ATL::CWindowImpl<CStatusBarExWnd> TWindow;
			friend class CStatusBarEx;
		private:
			CBackgroundTileRenderer
			                  m_bkgnd;
			HIMAGELIST        m_images;
			CStatusType::_e   m_type;
			CAtlString        m_text;
		public:
			BEGIN_MSG_MAP(CStatusBarExWnd)
				MESSAGE_HANDLER(WM_CREATE     , OnCreate )
				MESSAGE_HANDLER(WM_DESTROY    , OnDestroy)
				MESSAGE_HANDLER(WM_ERASEBKGND , OnErase  )
			END_MSG_MAP()
		public:
			CStatusBarExWnd(void);
			~CStatusBarExWnd(void);
		private:
			LRESULT OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnErase  (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CStatusBarExWnd       m_wnd;
	public:
		CStatusBarEx(void);
	public:
		HRESULT               Create(const HWND _parent, const RECT& _area);
		HRESULT               Destroy(void);
		SIZE                  GetSize(void) const;
		HRESULT               SetText(LPCTSTR lpszText, const CStatusType::_e);
	};
}}}

#endif/*_TMONSTATUSBAR_H_6AED36A2_5E1F_493f_9151_7C2D74310354_INCLUDED*/