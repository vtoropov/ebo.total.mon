/*
	Created by Tech_dog on 28-Dec-2015 at 8:19:36pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring General Setting Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Setup_GenPage.h"
#include "TMon_Resource.h"

using namespace tmon::common;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "Shared_SystemTask.h"
#include "Shared_SystemError.h"
#include "Shared_GenericAppObject.h"

using namespace shared::lite::sys_core;
using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CGeneralSetupPage_Handler
	{
	private:
		const
		CWindow&      m_page_ref;
	public:
		CGeneralSetupPage_Handler(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL          OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;
			if (false){}
			else if (IDC_TMON_SETUP_GEN_SPLASH == wCtrlId)
			{
				CButton ctrl_ = m_page_ref.GetDlgItem(wCtrlId);
				if (ctrl_)
				{
					TGeneralCfg& cfg_ = global::GetSettings().General();
					cfg_.ShowSplash(!!ctrl_.GetCheck());
				}
			}
			else if (IDC_TMON_SETUP_GEN_LOGON  == wCtrlId)
			{
				CButton ctrl_ = m_page_ref.GetDlgItem(wCtrlId);
				if (ctrl_)
				{
					const bool bStart = !!ctrl_.GetCheck();
					TGeneralCfg& cfg_ = global::GetSettings().General();

					HRESULT hr_ = S_OK;

					CApplicationCursor wc_;

					CStartupTask task_;
					if (bStart && !task_.IsOn())
						hr_ = task_.On();
					if (!bStart && task_.IsOn())
						hr_ = task_.Off();

					if (FAILED(hr_))
						global::SetErrorMessage(
								CSysError(hr_)
							);
					else
						cfg_.StartOnLogon(bStart);
				}
			}
			else if (IDC_TMON_SETUP_GEN_MINMZD == wCtrlId)
			{
				CButton ctrl_ = m_page_ref.GetDlgItem(wCtrlId);
				if (ctrl_)
				{
					TGeneralCfg& cfg_ = global::GetSettings().General();
					cfg_.StartMinimized(!!ctrl_.GetCheck());
				}
			}
			else
				bHandled = FALSE;
			return bHandled;
		}
	};

	class CGeneralSetupPage_Init
	{
	private:
		CWindow&      m_page_ref;
	public:
		CGeneralSetupPage_Init(CWindow& _page) : m_page_ref(_page){}
	public:
		VOID          ApplySettings(void)
		{
			const TGeneralCfg& cfg_ = global::GetSettings().General();

			const UINT ctrlIds[] = {
					IDC_TMON_SETUP_GEN_SPLASH,
					IDC_TMON_SETUP_GEN_LOGON ,
					IDC_TMON_SETUP_GEN_MINMZD,
				};
			for (INT i_ = 0; i_ < _countof(ctrlIds); i_++)
			{
				::WTL::CButton btn_ = m_page_ref.GetDlgItem(ctrlIds[i_]);
				if (!btn_)
					continue;
				switch (i_)
				{
				case 0: btn_.SetCheck(static_cast<INT>(cfg_.ShowSplash())); break;
				case 1: btn_.SetCheck(static_cast<INT>(cfg_.StartOnLogon())); break;
				case 2: btn_.SetCheck(static_cast<INT>(cfg_.StartMinimized())); break;
				}
			}
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CGeneralSetupPage::CGeneralSetupPage(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_TMON_SETUP_GEN_PAGE, tab_ref, *this)
{
}

CGeneralSetupPage::~CGeneralSetupPage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CGeneralSetupPage::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;

			details::CGeneralSetupPage_Init init_(*this);
			init_.ApplySettings();

			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			details::CGeneralSetupPage_Handler handler_(*this);
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CGeneralSetupPage::GetPageTitle(void) const
{
	CAtlString cs_title(_T("General"));
	return cs_title;
}