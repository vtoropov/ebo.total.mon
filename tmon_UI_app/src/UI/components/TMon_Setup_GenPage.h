#ifndef _TMONSETUPGENPAGE_H_95CFBEE2_A1C7_4af4_B92E_B906BD4D988A_INCLUDED
#define _TMONSETUPGENPAGE_H_95CFBEE2_A1C7_4af4_B92E_B906BD4D988A_INCLUDED
/*
	Created by Tech_dog on 28-Dec-2015 at 6:22:56pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring General Setting Tab Page class declaration file.
*/
#include "TMon_UI_Defs.h"

namespace tmon { namespace UI { namespace components
{
	class CGeneralSetupPage:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase TBasePage;
	public:
		CGeneralSetupPage(::WTL::CTabCtrl&);
		~CGeneralSetupPage(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}

#endif/*_TMONSETUPGENPAGE_H_95CFBEE2_A1C7_4af4_B92E_B906BD4D988A_INCLUDED*/