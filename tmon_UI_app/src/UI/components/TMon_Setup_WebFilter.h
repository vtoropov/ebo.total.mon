#ifndef _TMONSETUPWEBFILTER_H_88E311AC_1E0F_43f6_90D5_FCC208A299B1_INCLUDED
#define _TMONSETUPWEBFILTER_H_88E311AC_1E0F_43f6_90D5_FCC208A299B1_INCLUDED
/*
	Created by Tech_dog (VToropov) on 22-Dec-2015 at 2:44:53am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Web Filter Setup Tab page class declaration file.
*/
#include "TMon_UI_Defs.h"
#include "TMon_BlackList_Provider.h"

#include "Shared_WebFilter.h"

namespace tmon { namespace UI { namespace components
{
	using tmon::data::CWebBlackListProvider;

	using shared::avs::CWebFilter;
	using shared::avs::IWebFilterEventSink;

	class CWebFilterSetupPage:
		public  CTabPageBase, 
		public  ITabPageCallback,
		public  IWebFilterEventSink
	{
		typedef CTabPageBase TBasePage;
	private:
		CWindow                m_list;
		CWebBlackListProvider  m_provider;
		CWebFilter             m_filter;
	public:
		CWebFilterSetupPage(::WTL::CTabCtrl&);
		~CWebFilterSetupPage(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	private: // IWebFilterEventSink
		virtual VOID           IWebFilterEvent_OnBlock(CAtlString _url);

	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}

#endif/*_TMONSETUPWEBFILTER_H_88E311AC_1E0F_43f6_90D5_FCC208A299B1_INCLUDED*/