#ifndef _TMONTABPAGEUPDATE_H_6813BE41_29E1_424e_9517_C12C1AB51EA1_INCLUDED
#define _TMONTABPAGEUPDATE_H_6813BE41_29E1_424e_9517_C12C1AB51EA1_INCLUDED
/*
	Created by Tech_dog (VToropov) on 16-Dec-2015 at 8:28:52pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring Virus Database Update Tab page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_Button.h"
#include "UIX_Renderer.h"
#include "UIX_Label.h"

#include "TMon_Update_Timer.h"
#include "TMon_Update_Progress.h"

#include "Shared_AvsUpdater.h"
#include "Shared_SystemError.h"
#include "Shared_GenericEvent.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CButton;
	using ex_ui::controls::CLabel;

	using shared::avs::IUpdaterCallback;
	using shared::avs::CUpdater;

	using shared::lite::common::CSysError;
	using shared::lite::runnable::CGenericEvent;
	using shared::lite::runnable::IGenericEventNotify;

	class CTabPageUpdate :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify,
		public  IUpdaterCallback,
		public  IGenericEventNotify
	{
		typedef CPanelBase TPageBase;
	private:
		enum _e{
			eUnpackEvent  = 0x20,
			eInstallEvent = 0x21
		};
	private:
		CBackgroundTileRenderer   m_bkg_rnd;
		CLabel                    m_top_label;
		CLabel                    m_msg_label;
		CLabel                    m_lst_label;
		CUpdateTimer              m_upd_timer;
		CLabel                    m_ver_label;   // antivirus database version
		CUpdateProgress           m_prg_panel;
		CUpdater                  m_updater;
		CGenericEvent             m_evt_object;
	public:
		CTabPageUpdate(IRenderer& _parent);
		~CTabPageUpdate(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
		HRESULT     Show(const bool _flag) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	private: // IUpdaterCallback
		bool        IUpdater_OnCanContinue (void) override sealed;
		void        IUpdater_OnDownloadFinished(void) override sealed;
		void        IUpdater_OnDataReceived(const DWORD dwReceived, const DWORD dwTotal) override sealed;
		void        IUpdater_OnDownloadStart(void) override sealed;
		void        IUpdater_OnUnpackFinish(void) override sealed;
		void        IUpdater_OnUnpackProgress(const DWORD dwIndex, const DWORD dwTotal, CAtlString _file) override sealed;
		void        IUpdater_OnUnpackStart(void) override sealed;
		void        IUpdater_OnUpdateError(CSysError) override sealed;
	private:
		HRESULT     GenericEvent_OnNotify(const _variant_t v_evt_id)override sealed;
	};
}}}

#endif/*_TMONTABPAGEUPDATE_H_6813BE41_29E1_424e_9517_C12C1AB51EA1_INCLUDED*/