/*
	Created by Tech_dog (VToropov) on 15-Jan-2016 at 7:01:08pm, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring Log Setup Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Setup_LogPage.h"
#include "TMon_Resource.h"

using namespace tmon::common;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	class CLogSetupPage_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CLogSetupPage_Registry(void): m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		CAtlString     LastFolder(void)const
		{
			CAtlString cs_folder;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_NamedValueLastLogFolder(),
					cs_folder
				);
			if (cs_folder.IsEmpty())
				global::GetAppObjectRef().GetPath(cs_folder);
			return cs_folder;
		}

		HRESULT        LastFolder(LPCTSTR pszLastFolder)
		{
			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_NamedValueLastLogFolder(),
					pszLastFolder
				);
			return  hr_;
		}
	private:
		CAtlString    _CommonRegFolder(void)            const { return CAtlString(_T("Common"));             }
		CAtlString    _NamedValueLastLogFolder(void)    const { return CAtlString(_T("LastLogFolder"));      }
	};

	class CLogSetupPage_Init
	{
	private:
		CWindow&      m_page_ref;
	public:
		CLogSetupPage_Init(CWindow& _page) : m_page_ref(_page){}
	public:
		VOID          ApplySettings(void)
		{
			::WTL::CButton check_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_ENABLE);
			if (check_)
				check_.SetCheck((INT)global::GetSettings().Log().Enabled());

			::WTL::CEdit edt_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_FOLDER);
			if (edt_)
			{
				edt_.SetWindowText(global::GetSettings().Log().Folder());
				edt_.SetCueBannerText(_T("Type the folder or press [Browse] button"), TRUE);
			}
			this->_InitLevels();
		}

		VOID          UpdateCtrlState(void)
		{
			bool bEnabled = false;
			::WTL::CButton check_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_ENABLE);
			if (check_)
				bEnabled = !!check_.GetCheck();
			const UINT ctrlId[] = {
					IDC_TMON_SETUP_LOG_LEVEL ,
					IDC_TMON_SETUP_LOG_FOLDER,
					IDC_TMON_SETUP_LOG_BROWSE
				};

			for (INT i_ = 0; i_ < _countof(ctrlId); i_++)
			{
				CWindow ctrl_ = m_page_ref.GetDlgItem(ctrlId[i_]);
				if (ctrl_)
					ctrl_.EnableWindow(bEnabled);
			}
		}

		VOID          UpdateSettings(void)
		{
			::WTL::CButton check_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_ENABLE);
			if (check_)
				global::GetSettings().Log().Enabled(!!check_.GetCheck());

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_LEVEL);
			if (cbo_)
				global::GetSettings().Log().Level(cbo_.GetCurSel());

			::WTL::CEdit edt_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_FOLDER);
			if (edt_)
			{
				CAtlString cs_folder;
				edt_.GetWindowText(cs_folder);
				global::GetSettings().Log().Folder(cs_folder);
			}
		}

	private:
		VOID         _InitLevels(VOID)
		{
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_LEVEL);
			if (cbo_)
			{
				cbo_.AddString(_T("All messages"));
				cbo_.AddString(_T("Errors and Threats"));
				cbo_.AddString(_T("Threats only"));

				const INT level_ = global::GetSettings().Log().Level();

				cbo_.SetCurSel(
						level_
					);
			}
		}
	};

	class CLogSetupPage_Layout
	{
	private:
		RECT          m_client_area;
		CWindow&      m_page_ref;
	public:
		CLogSetupPage_Layout(CWindow& _page) : m_page_ref(_page)
		{
			m_page_ref.GetClientRect(&m_client_area);
		}
	public:
		VOID          Update(void)
		{
			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_LEVEL);
			if (cbo_)
			{
				RECT rc_ = {0};
				cbo_.GetWindowRect(&rc_);
				::MapWindowPoints(HWND_DESKTOP, m_page_ref, (LPPOINT)&rc_, 0x2);
				::OffsetRect(&rc_, 0x0, -0x1);
				cbo_.MoveWindow(&rc_, FALSE); // aligns with a label text vertically
			}
			const SIZE sz_ = {-1, +1};
			AlignCtrlsByHeight(
					m_page_ref,
					IDC_TMON_SETUP_LOG_FOLDER,
					IDC_TMON_SETUP_LOG_BROWSE,
					sz_
				);
			HBITMAP hBitmap = NULL;
			HRESULT hr_ = CGdiPlusPngLoader::LoadResource(
								IDR_TMON_MAIN_DLG_WARN_I24PX,
								NULL,
								hBitmap
							);
			if (!FAILED(hr_))
			{
				::WTL::CStatic ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_IMAGE);
				if (ctrl_)
					ctrl_.SetBitmap(hBitmap);
				::DeleteObject(hBitmap); hBitmap = NULL;
			}
		}
	};

	class CLogSetupPage_Handler
	{
	private:
		CWindow&      m_page_ref;
	public:
		CLogSetupPage_Handler(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL          OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;
			if (false){}
			else if (IDC_TMON_SETUP_LOG_ENABLE == wCtrlId)
			{
				CLogSetupPage_Init init_(m_page_ref);
				init_.UpdateCtrlState();
			}
			else if (IDC_TMON_SETUP_LOG_BROWSE == wCtrlId)
			{
				CAtlString cs_target = this->_SelectTarget();
				if (!cs_target.IsEmpty())
				{
					CAtlString cs_msg;
					cs_msg.Format(
							_T("Log folder '%s' is selected"),
							cs_target
						);
					global::SetInfoMessage(
							cs_msg
						);
				}
			}
			else
				bHandled = FALSE;
			return bHandled;
		}
	private:
		CAtlString   _SelectTarget(void)
		{
			::WTL::CFolderDialog dlg_(
					NULL,
					_T("Choose Folder or File for Log Files"),
					BIF_RETURNONLYFSDIRS|BIF_USENEWUI
				);
			CLogSetupPage_Registry reg_;
			CAtlString cs_folder = reg_.LastFolder();

			dlg_.SetInitialFolder(cs_folder);

			const INT_PTR nResult = dlg_.DoModal();
			if (IDOK == nResult)
			{
				cs_folder = dlg_.GetFolderPath();
				reg_.LastFolder(cs_folder);

				CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_LOG_FOLDER);
				if (ctrl_)
					ctrl_.SetWindowText(cs_folder);
			}
			return cs_folder;
		}
	};

}}}}

/////////////////////////////////////////////////////////////////////////////

CLogSetupPage::CLogSetupPage(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_TMON_SETUP_LOG_PAGE, tab_ref, *this)
{
}

CLogSetupPage::~CLogSetupPage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CLogSetupPage::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;

			details::CLogSetupPage_Layout layout_(*this);
			layout_.Update();

			details::CLogSetupPage_Init init_(*this);
			init_.ApplySettings();
			init_.UpdateCtrlState();

			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			details::CLogSetupPage_Init init_(*this);
			init_.UpdateSettings();
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			details::CLogSetupPage_Handler handler_(*this);
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CLogSetupPage::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Log"));
	return cs_title;
}