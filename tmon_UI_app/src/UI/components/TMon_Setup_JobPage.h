#ifndef _TMONSETUPJOBPAGE_H_FC670FA2_2E8B_48d0_9FB0_8692AAEC3F5A_INCLUDED
#define _TMONSETUPJOBPAGE_H_FC670FA2_2E8B_48d0_9FB0_8692AAEC3F5A_INCLUDED
/*
	Created by Tech_dog (VToropov) on 20-Jan-2016 at 11:54:42pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring Scan Schedule Settings Tab Page class declaration file.
*/
#include "TMon_UI_Defs.h"

namespace tmon { namespace UI { namespace components
{
	class CJobSetupPage:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase TBasePage;
	private:
		::WTL::CListViewCtrl   m_list;
	public:
		CJobSetupPage(::WTL::CTabCtrl&);
		~CJobSetupPage(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}

#endif/*_TMONSETUPJOBPAGE_H_FC670FA2_2E8B_48d0_9FB0_8692AAEC3F5A_INCLUDED*/