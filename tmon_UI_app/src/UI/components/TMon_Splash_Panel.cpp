/*
	Created by Tech_dog (VToropov) on 18-Dec-2015 at 1:19:43pm, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring Splash panel class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Splash_Panel.h"
#include "TMon_Resource.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class  CSplashPanel_Layout
	{
	public:
		enum { // from PSD images
			e_frame_h  = 300,
			e_frame_w  = 550,
		};
	private:
		CWindow&      m_panel_ref;
		RECT          m_client_area;
	public:
		CSplashPanel_Layout(CWindow& panel_ref) : m_panel_ref(panel_ref)
		{
			if (m_panel_ref)
				m_panel_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT          GetLabelRect(void)const
		{
			RECT rc_ = {
					15,
					CSplashPanel_Layout::e_frame_h - 60,
					CSplashPanel_Layout::e_frame_w,
					CSplashPanel_Layout::e_frame_h - 30
				};
			return rc_;
		}

		RECT          GetProgressRect(void)const
		{
			RECT rc_ = {
					0,
					CSplashPanel_Layout::e_frame_h - 30,
					CSplashPanel_Layout::e_frame_w,
					CSplashPanel_Layout::e_frame_h -  5
				};
			return rc_;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CSplashPanel::CSplashPanel(void):
	TPageBase(IDD_TMON_SPLASH_PANEL , *this),
	m_bkg_rnd(IDR_TMON_SPLASH_DLG_BKGND, TPageBase::GetWindow_Ref()),
	m_label(CControlCrt(1, m_bkg_rnd , *this)),
	m_timer(NULL)
{
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);
}

CSplashPanel::~CSplashPanel(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CSplashPanel::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CSplashPanel_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetLabelRect();
		HRESULT hr_ = m_label.Create(
							host_,
							rc_,
							NULL
						);
		if (!FAILED(hr_))
			m_label.FontSize(10);
	}
	{
		RECT rc_ = layout_.GetProgressRect();
		m_progress.Create(
				host_,
				rc_,
				NULL,
				WS_VISIBLE|WS_CHILD
			);
		m_progress.SetRange(1, 100);
	}
	global::GetScannerRef().Initialize();
	m_files = global::GetScannerRef().VirusDatabase().Files();

	DWORD dwPeriod = 500;
	if (m_files.size() > 50)
		dwPeriod = 50 * 1000 / m_files.size();

	m_timer = host_.SetTimer(1, dwPeriod);

	return 0;
}

HRESULT    CSplashPanel::Destroy(void)
{
	CWindow host_ = TPageBase::GetWindow_Ref();
	if (m_timer)
	{
		host_.KillTimer(m_timer);
		m_timer = NULL;
	}

	if (m_progress)
		m_progress.SendMessage(WM_CLOSE);

	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

//////////////////////////////////////////////////////////////////////////////

LRESULT    CSplashPanel::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam; wParam;
	switch (uMsg)
	{
	case WM_TIMER:
		{
			static size_t ptr_ = 0;

			CAtlString cs_msg;

			if (ptr_ < m_files.size())
			{
				const DWORD nPercent = DWORD((FLOAT(ptr_ + 1) / FLOAT(m_files.size())) * 100);
				cs_msg.Format(
						_T("Loading virus definition: %s (%d%%)"),
						m_files[ptr_].GetString(),
						nPercent
					);
				m_progress.SetPos((INT)nPercent);
			}
			else
			{
				cs_msg = _T("Optimizing virus database...");
				m_progress.ShowWindow(SW_HIDE);
			}

			m_label.Text(cs_msg);
			ptr_ += 1;

		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CSplashPanel::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;

	HRESULT hr_ = S_OK;
	return  hr_;
}