/*
	Created by Tech_dog (VToropov) on 26-Nov-2015 at 8:59:29pm, GMT+7, Phuket, Rawai, Thursday;
	This is Total Monitoring application home tab page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Resource.h"
#include "TMon_TabPage_Home.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

using namespace ex_ui::controls;

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CTabPageHome_Layout
	{
	public:
		enum _e { // from PSD images
			e_btn_large_w   = 260,
			e_btn_large_h   = 420,
			e_btn_small_w   = 270,
			e_btn_small_h   = 205,
			e_ctl_gap_w     =  10,
			e_ctl_gap_h     =  10,
			e_ctl_margins_w =  10,
			e_ctl_margins_h =  15,
		};
	private:
		CWindow&      m_frame_ref;
		RECT          m_client_area;
	public:
		CTabPageHome_Layout(CWindow& frame_ref) : m_frame_ref(frame_ref)
		{
			if (m_frame_ref)
				m_frame_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT          GetBtnProtectRect(void)const
		{
			const INT nLeft = CTabPageHome_Layout::e_ctl_margins_w;
			const INT nTop  = CTabPageHome_Layout::e_ctl_margins_h;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CTabPageHome_Layout::e_btn_large_w,
					nTop  + CTabPageHome_Layout::e_btn_large_h
				};
			return rc_;
		}

		RECT          GetBtnThreatRect(void)const
		{
			const INT nTop  = CTabPageHome_Layout::e_ctl_margins_h + CTabPageHome_Layout::e_btn_small_h + CTabPageHome_Layout::e_ctl_gap_h;
			const INT nLeft = CTabPageHome_Layout::e_ctl_margins_w + CTabPageHome_Layout::e_btn_large_w + CTabPageHome_Layout::e_ctl_gap_w;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CTabPageHome_Layout::e_btn_small_w,
					nTop  + CTabPageHome_Layout::e_btn_small_h
				};
			return rc_;
		}

		RECT          GetBtnScannerRect(void)const
		{
			const INT nTop  = CTabPageHome_Layout::e_ctl_margins_h;
			const INT nLeft = CTabPageHome_Layout::e_ctl_gap_w + this->GetBtnUpdateRect().right;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CTabPageHome_Layout::e_btn_large_w,
					nTop  + CTabPageHome_Layout::e_btn_large_h
				};
			return rc_;
		}

		RECT          GetBtnUpdateRect(void)const
		{
			const INT nTop  = CTabPageHome_Layout::e_ctl_margins_h;
			const INT nLeft = CTabPageHome_Layout::e_ctl_margins_w + CTabPageHome_Layout::e_btn_large_w + CTabPageHome_Layout::e_ctl_gap_w;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CTabPageHome_Layout::e_btn_small_w,
					nTop  + CTabPageHome_Layout::e_btn_small_h
				};
			return rc_;
		}

		RECT          GetLblProtectRect(void)
		{
			RECT rc_ = this->GetBtnProtectRect();
			rc_.top  = rc_.bottom - 50;
			rc_.left+= 65;
			return rc_;
		}

		RECT          GetLblScannerRect(void)const
		{
			RECT rc_ = this->GetBtnScannerRect();
			rc_.top  = rc_.bottom - 50;
			rc_.left+= e_ctl_margins_w;
			return rc_;
		}

		RECT          GetLblThreatRect(void)const
		{
			RECT rc_ = this->GetBtnThreatRect();
			rc_.top  = rc_.bottom - 50;
			rc_.left+= e_ctl_margins_w;
			return rc_;
		}

		RECT          GetLblUpdateRect(void)const
		{
			RECT rc_ = this->GetBtnUpdateRect();
			rc_.top  = rc_.bottom - 30;
			rc_.left+= e_ctl_margins_w;
			return rc_;
		}
	};

	class CTabPageHome_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CTabPageHome_Registry(void) : m_stg(HKEY_CURRENT_USER)
		{
		}
	public:
		CAtlString     LastScanDate(void)const
		{
			CAtlString cs_date;
			m_stg.Load(
					this->_CommonRegFolder(),
					this->_LastScanDate(),
					cs_date
				);
			CAtlString cs_msg;
			cs_msg.Format(
					_T("Last scanning on %s"),
					cs_date.GetString()
				);
			return cs_msg;
		}

		HRESULT        LastScanDate(const SYSTEMTIME& _date)
		{
			CAtlString cs_date;
			cs_date.Format(
					_T("%.2d/%.2d/%.4d"),
					_date.wMonth,
					_date.wDay,
					_date.wYear
				);

			HRESULT hr_ = m_stg.Save(
					this->_CommonRegFolder(),
					this->_LastScanDate(),
					cs_date
				);
			return  hr_;
		}

	private:
		CAtlString    _CommonRegFolder(void)            const { return CAtlString(_T("Common"));             }
		CAtlString    _LastScanDate(void)               const { return CAtlString(_T("LastScanDate"));       }
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageHome::CTabPageHome(IRenderer& _parent, IControlNotify& _notify):
	TPageBase(IDD_TMON_HOME_TAB_PAGE, *this),
	m_evt_sink(_notify),
	m_btn_protect(&_parent),
	m_lbl_protect(CControlCrt(1, *m_btn_protect.GetRendererPtr(), *this)),
	m_btn_update (&_parent),
	m_lbl_update (CControlCrt(2, *m_btn_update.GetRendererPtr() , *this)),
	m_btn_threat (&_parent),
	m_lbl_threat (CControlCrt(3, *m_btn_threat.GetRendererPtr() , *this)),
	m_btn_scanner(&_parent),
	m_lbl_scanner(CControlCrt(4, *m_btn_scanner.GetRendererPtr(), *this))
{
}

CTabPageHome::~CTabPageHome(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageHome::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow& host_ = TPageBase::GetWindow_Ref();
	const DWORD sz_ = 11;

	details::CTabPageHome_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetBtnProtectRect();
		hr_ = m_btn_protect.Create(
				host_,
				rc_,
				NULL,
				IDC_TMON_HOME_BTN_PROTECT
			);
		hr_ = m_btn_protect.SetImage(IDR_TMON_HOME_BTN_PROTECT_NORM);
		hr_ = m_btn_protect.UpdateLayout();
	}
	{
		const RECT rc_ = layout_.GetLblProtectRect();
		hr_ = m_lbl_protect.Create(
				host_,
				rc_,
				_T("Computer Status")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_lbl_protect.ForeColor(RGB(0xee, 0xee, 0xee));
		m_lbl_protect.FontSize(sz_);
	}
	{
		const RECT rc_ = layout_.GetBtnUpdateRect();
		hr_ = m_btn_update.Create(
				host_,
				rc_,
				NULL,
				IDC_TMON_HOME_BTN_UPDATE
			);
		hr_ = m_btn_update.SetImage(IDR_TMON_HOME_BTN_UPDATE_NORM);
		hr_ = m_btn_update.UpdateLayout();
		hr_ = m_btn_update.SetParentNotifyPtr(this);
	}
	{
		const RECT rc_ = layout_.GetLblUpdateRect();
		hr_ = m_lbl_update.Create(
				host_,
				rc_,
				_T("Not updated yet")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_lbl_update.ForeColor(RGB(0xee, 0xee, 0xee));
		m_lbl_update.FontSize(sz_);
	}
	{
		const RECT rc_ = layout_.GetBtnThreatRect();
		hr_ = m_btn_threat.Create(
				host_,
				rc_,
				NULL,
				IDC_TMON_HOME_BTN_THREAT
			);
		hr_ = m_btn_threat.SetImage(IDR_TMON_HOME_BTN_THREAT_NORM);
		hr_ = m_btn_threat.UpdateLayout();
		hr_ = m_btn_threat.SetParentNotifyPtr(this);
	}
	{
		const RECT rc_ = layout_.GetLblThreatRect();
		hr_ = m_lbl_threat.Create(
				host_,
				rc_,
				_T("")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_lbl_threat.ForeColor(RGB(0xee, 0xee, 0xee));
		m_lbl_threat.FontSize(sz_);
	}
	{
		const RECT rc_ = layout_.GetBtnScannerRect();
		hr_ = m_btn_scanner.Create(
				host_,
				rc_,
				NULL,
				IDC_TMON_HOME_BTN_SCANNER
			);
		hr_ = m_btn_scanner.SetImage(IDR_TMON_HOME_BTN_SCANNER_NORM);
		hr_ = m_btn_scanner.UpdateLayout();
		hr_ = m_btn_scanner.SetParentNotifyPtr(this);
	}
	details::CTabPageHome_Registry reg_;
	{
		CAtlString cs_date = reg_.LastScanDate();
		if (cs_date.IsEmpty())
			cs_date = _T("Not scanned yet");

		const RECT rc_ = layout_.GetLblScannerRect();
		hr_ = m_lbl_scanner.Create(
				host_,
				rc_,
				cs_date
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_lbl_scanner.ForeColor(RGB(0xee, 0xee, 0xee));
		m_lbl_scanner.FontSize(sz_);
	}

	global::GetScannerRef().Events().Subscribe(this);

	return hr_;
}

HRESULT    CTabPageHome::Destroy(void)
{
	global::GetScannerRef().Events().Unsubscribe(this);

	m_lbl_scanner.Destroy();
	m_btn_scanner.Destroy();
	m_lbl_threat.Destroy();
	m_btn_threat.Destroy();
	m_lbl_update.Destroy();
	m_btn_update.Destroy();
	m_lbl_protect.Destroy();
	m_btn_protect.Destroy();
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageHome::MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE; // very important, by default it's true and WM_PAINT message eats CPU time;
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageHome::IControlNotify_OnClick(const UINT ctrlId)
{
	HRESULT hr_ = m_evt_sink.IControlNotify_OnClick(ctrlId);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID       CTabPageHome::IScannerEvent_OnFileScanFinish(CScanFile _file)
{
	bool bChanged = false;
	switch (_file.Health())
	{
	case CScanFileHealth::eInfected:
		{
			bChanged = true;
			m_stat._infected += 1;
		} break;
	case CScanFileHealth::eSuspected:
		{
			bChanged = true;
			m_stat._suspected += 1;
		} break;
	}
	if (bChanged)
	{
		CAtlString cs_msg;
		cs_msg.Format(
			_T("Objects: %d"),
				m_stat.Total()
			);
		m_btn_threat.SetImage(IDR_TMON_HOME_BTN_THREAT_FOUND);
		m_lbl_threat.Text(cs_msg);

		m_btn_protect.SetImage(IDR_TMON_HOME_BTN_PROTECT_BROKEN);
	}
}

VOID       CTabPageHome::IScannerEvent_OnScanProcessBegin(VOID)
{
	m_stat.Clear();
	m_lbl_threat.Text(
			_T("")
		);
	m_btn_threat.SetImage(IDR_TMON_HOME_BTN_THREAT_NORM);
	m_btn_protect.SetImage(IDR_TMON_HOME_BTN_PROTECT_NORM);
}

VOID       CTabPageHome::IScannerEvent_OnScanProcessFinish(VOID)
{
	SYSTEMTIME st_ = {0};
	::GetLocalTime(&st_);

	details::CTabPageHome_Registry reg_;
	reg_.LastScanDate(st_);

	m_lbl_scanner.Text(
			reg_.LastScanDate()
		);
}