#ifndef _TMONSCANQUICKPANEL_H_3F1A5123_075B_453b_A950_6F67D58DABC3_INCLUDED
#define _TMONSCANQUICKPANEL_H_3F1A5123_075B_453b_A950_6F67D58DABC3_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Dec-2015 at 6:09:25pm, GMT+8, Kuala Lumpur, Saturday;
	This is Total Monitoring Quick Scan Panel class declaration file.
*/
#include "TMon_Scan_BasePanel.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;

	class CQuickScanPanel :
		public CBaseScanPanel
	{
		typedef CBaseScanPanel TScanBase;
	public:
		CQuickScanPanel(IRenderer& _parent);
		~CQuickScanPanel(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	private: // IScannerEventSink
		VOID        IScannerEvent_OnEngineInitFinish(CScanError) override sealed;
		VOID        IScannerEvent_OnLock(const DWORD dwLockOwnerId) override sealed;
		VOID        IScannerEvent_OnScanProcessFinish(VOID) override sealed;
		VOID        IScannerEvent_OnUnlock(const DWORD dwLockOwnerId) override sealed;
	public:
		VOID        UpdateCtrlState(const bool bEnabled);
	};
}}}

#endif/*_TMONSCANQUICKPANEL_H_3F1A5123_075B_453b_A950_6F67D58DABC3_INCLUDED*/