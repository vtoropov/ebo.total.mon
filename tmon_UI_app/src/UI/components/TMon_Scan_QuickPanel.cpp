/*
	Created by Tech_dog (VToropov) on 26-Dec-2015 at 6:38:37pm, GMT+8, Kuala Lumpur, Saturday;
	This is Total Monitoring Quick Scan Panel class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Scan_QuickPanel.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon::UI::components;
using namespace ex_ui::controls;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;
using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	typedef ::std::vector<CAtlString>  TSpecialFolders;

	class CQuickScan_Init
	{
	public:
		TSpecialFolders  SpecialFolders(void)const
		{
			const INT csidlFolders[] = {
					CSIDL_APPDATA       ,
					CSIDL_INTERNET_CACHE,
					CSIDL_LOCAL_APPDATA ,
					CSIDL_MYPICTURES    ,
					CSIDL_PERSONAL      ,
					CSIDL_WINDOWS       ,
				};
			TSpecialFolders spec_;

			for (INT i_ = 0; i_ < _countof(csidlFolders); i_++)
			{
				TCHAR pszPath[_MAX_PATH] = {0};

				HRESULT hr_ = ::SHGetFolderPath(
									NULL,
									csidlFolders[i_],
									NULL,
									0,
									pszPath
								);
				if (FAILED(hr_))
					continue;
				try
				{
					spec_.push_back(CAtlString(pszPath));
				}
				catch(::std::bad_alloc&)
				{
					break;
				}
			}

			return spec_;
		}
	};

	class CQuickScan_Handler
	{
	private:
		const
		CWindow&      m_page_ref;
		DWORD&        m_locker_ref;
	public:
		CQuickScan_Handler(CWindow& page_ref, DWORD& _locker_ref) : m_page_ref(page_ref), m_locker_ref(_locker_ref){}
	public:
		BOOL          OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;

			if (false){}
			else if (0 == wCtrlId)
			{
				
			}
			else
				bHandled = FALSE;

			return bHandled;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CQuickScanPanel::CQuickScanPanel(IRenderer& _parent):
	TScanBase  (IDD_TMON_SCAN_QUICK_PANEL, IDR_TMON_SCAN_QUICK_PANEL_BKGND)
{
	_parent;
}

CQuickScanPanel::~CQuickScanPanel(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CQuickScanPanel::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TScanBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	TScanBase::m_cap_label.Text(
			_T("Scan the most vulnerable places on your PC, such as MS Windows� folder, My Documents and other user data folders")
		);

	CWindow host_ = TScanBase::GetWindow_Ref();

	this->UpdateCtrlState(true);

	return hr_;
}

HRESULT     CQuickScanPanel::Destroy(void)
{
	HRESULT hr_ = TScanBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT     CQuickScanPanel::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM, BOOL& bHandled)
{
	bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			details::CQuickScan_Handler handler_(
						TScanBase::GetWindow_Ref(),
						TScanBase::m_locker
					);
			if (IDC_TMON_SCAN_QUICK_START == ctrlId)
			{
				bHandled = TRUE;
				HRESULT hr_ = global::GetScannerRef().Lock(TScanBase::m_locker);
				if (FAILED(hr_))
				{
					global::SetErrorMessage(_T("Scanner engine is not ready, please try later."));
					return 0;
				}

				details::CQuickScan_Init init_;
				const details::TSpecialFolders spec_ = init_.SpecialFolders();
				for ( details::TSpecialFolders::const_iterator it_ = spec_.begin(); it_!= spec_.end(); ++it_)
				{
					CAtlString cs_target = *it_;
					CAtlString cs_msg;
					cs_msg.Format(
							_T("Preparing file list for folder: %s"),
							cs_target
						);
					TScanBase::m_sta_label.Text(
							cs_msg
						);
					hr_ = global::GetScannerRef().ScanFolderOrFile(cs_target);

					if (FAILED(hr_))
					{
						global::SetErrorMessage(
							global::GetScannerRef().Error()
						);
						global::GetScannerRef().Unlock(TScanBase::m_locker);
						break;
					}
				}
			}
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CQuickScanPanel::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID        CQuickScanPanel::IScannerEvent_OnEngineInitFinish(CScanError _error)
{
	this->UpdateCtrlState(true);
	TScanBase::IScannerEvent_OnEngineInitFinish(_error);
}

VOID        CQuickScanPanel::IScannerEvent_OnLock(const DWORD dwLockOwnerId)
{
	dwLockOwnerId; // doesn't care who is locker; just disables all scan related controls;
	this->UpdateCtrlState(false);
}

VOID        CQuickScanPanel::IScannerEvent_OnScanProcessFinish(VOID)
{
	TScanBase::IScannerEvent_OnScanProcessFinish();
	if (TScanBase::m_locker) // this module is the owner of a scan process;
		global::GetScannerRef().Unlock(TScanBase::m_locker);
	else
		this->UpdateCtrlState(true);
}

VOID        CQuickScanPanel::IScannerEvent_OnUnlock(const DWORD dwLockOwnerId)
{
	if (TScanBase::m_locker == dwLockOwnerId)
		TScanBase::m_locker = 0;
	this->UpdateCtrlState(true); // the scanner has been unlocked; now a scan is enabled
}

/////////////////////////////////////////////////////////////////////////////

VOID        CQuickScanPanel::UpdateCtrlState(const bool bEnabled)
{
	CWindow host_ = TScanBase::GetWindow_Ref();
	CWindow ctrl_ = host_.GetDlgItem(IDC_TMON_SCAN_QUICK_START);
	if (ctrl_)
		ctrl_.EnableWindow(bEnabled && global::GetScannerRef().IsInitialized());
}