#ifndef _TMONSCANCUSTMPANEL_H_3FF4EF8B_B0AB_4493_8940_F3BE2F8872DD_INCLUDED
#define _TMONSCANCUSTMPANEL_H_3FF4EF8B_B0AB_4493_8940_F3BE2F8872DD_INCLUDED
/*
	Created by Tech_dog (VToropov) on 26-Dec-2015 at 8:11:53pm, GMT+8, Kuala Lumpur, Saturday;
	This is Total Monitoring Custom Scan Panel class declaration file.
*/
#include "TMon_Scan_BasePanel.h"

#include "Shared_TskScheduler.h"
#include "Shared_SystemError.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::CLabel;

	using shared::tasks::CCustomTask;
	using shared::tasks::CTaskScheduler;
	using shared::tasks::CTaskSchedulerEvents;
	using shared::tasks::ITaskSchedulerEventSink;

	using shared::lite::common::CSysError;

	class CCustomScanPanel :
		public CBaseScanPanel,
		public ITaskSchedulerEventSink
	{
		typedef CBaseScanPanel TScanBase;
	private:
		CLabel      m_fld_label;
		CTaskScheduler m_scheduler;
	public:
		CCustomScanPanel(IRenderer& _parent);
		~CCustomScanPanel(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IScannerEventSink
		VOID        IScannerEvent_OnEngineInitFinish(CScanError) override sealed;
		VOID        IScannerEvent_OnLock(const DWORD dwLockOwnerId) override sealed;
		VOID        IScannerEvent_OnScanProcessFinish(VOID) override sealed;
		VOID        IScannerEvent_OnUnlock(const DWORD dwLockOwnerId) override sealed;
	private: // ITaskSchedulerEventSink
		VOID        ITaskSchedulerEvent_OnCustomTask(const CCustomTask) override sealed;
		VOID        ITaskSchedulerEvent_OnError(const CSysError) override sealed;
	public:
		VOID        UpdateCtrlState(const bool bEnabled);
	};
}}}

#endif/*_TMONSCANCUSTMPANEL_H_3FF4EF8B_B0AB_4493_8940_F3BE2F8872DD_INCLUDED*/