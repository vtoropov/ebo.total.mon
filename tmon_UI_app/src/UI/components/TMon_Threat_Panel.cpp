/*
	Created by Tech_dog (VToropov) on 21-Dec-2015 at 8:52:03pm, GMT+7, Phuket, Rawai;
	This is Total Monitoring Active threat panel class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Threat_Panel.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon::UI::components;
using namespace ex_ui::controls;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui;
using namespace ex_ui::draw;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"
#include "Shared_FileSystem.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CThreatPanel_Layout
	{
	private:
		enum _e{
			e_label_w = 200,
			e_label_h =  30,
		};
	private:
		struct _lst_col {
			INT     _width;
			LPTSTR  _title;
		};
	private:
		CWindow&     m_page_ref;
		RECT         m_client_area;
	public:
		CThreatPanel_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		HRESULT      CreateList(CWindow& lst_ref)const
		{
			if (lst_ref.IsWindow())
				return (HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS));
			RECT rc_ = CThreatPanel_Layout::GetListRect();
			lst_ref.Create(
					_T("SysListView32"),
					m_page_ref,
					rc_,
					NULL,
					WS_CHILD|WS_VISIBLE|LVS_REPORT|WS_BORDER
				);
			if (!lst_ref.IsWindow())
				return (HRESULT_FROM_WIN32(::GetLastError()));

			ListView_SetExtendedListViewStyle((HWND)lst_ref, LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_LABELTIP);

			const HFONT hFont = m_page_ref.GetFont();
			lst_ref.SetFont(hFont);
			{
				CWindow header_ = ListView_GetHeader(lst_ref);
				if (header_)
					header_.SetFont(hFont);
			}

			CThreatPanel_Layout::_lst_col cols_[] = {
					{ 44,  NULL          },
					{265, _T("File Name")},
					{135, _T("Status"   )},
					{ 65, _T("Result"   )},
				};

			LVCOLUMN lvc ={0};
			lvc.mask     = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
			lvc.fmt      = LVCFMT_LEFT;
			lvc.pszText  = cols_[0]._title;
			lvc.cx       = cols_[0]._width;
			lvc.iSubItem = 0;

			ListView_InsertColumn(lst_ref, 0, &lvc);

			for (INT i_  = 1; i_ < _countof(cols_); i_++)
			{
				lvc.pszText  = cols_[i_]._title;
				lvc.cx       = cols_[i_]._width;
				lvc.iSubItem = i_;

				ListView_InsertColumn(lst_ref, i_, &lvc);
			}

			return S_OK;
		}

		VOID         InitCtrls(void)const
		{
			WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_THREAT_LST_ACT);
			if (cbo_)
			{
				CScanActionEnum enum_;
				const INT count_ = enum_.Count();
				for (INT i_ = 0; i_ < count_; i_++)
					cbo_.AddString(enum_.Item(i_).Name());

				cbo_.SetFont(m_page_ref.GetFont());
				cbo_.SetCurSel(enum_.Default().Index());
			}
			const SIZE sz_ = {-1, +1};

			AlignComboHeightTo(
					m_page_ref,
					IDC_TMON_THREAT_LST_ACT  ,
					22
				);

			AlignCtrlsByHeight(
					m_page_ref,
					IDC_TMON_THREAT_LST_ACT  ,
					IDC_TMON_THREAT_BTN_APPLY,
					sz_
				);
		}

	public:
		RECT         GetListRect(void)const
		{
			RECT rc_ = m_client_area;
			rc_.bottom-= CThreatPanel_Layout::e_label_h;
			return rc_;
		}

		RECT         GetActLabelRect(void)const
		{
			const INT nLeft = 200;
			const INT nTop  = 397;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 55,
					nTop  + 24
				};
			return rc_;
		}
	};

	VOID  CThreatPanel_InsertRow(const CWindow& _list, const CScanFileEx& _file)
	{
		const INT count_ = ListView_GetItemCount(_list);

		LVITEM item_  = {0};
		item_.mask    = LVIF_TEXT | LVIF_IMAGE;
		item_.iItem   = count_;
		item_.pszText = _T("");
		item_.iImage  = _file.Health() == CScanFileHealth::eInfected || true ? 1 : -1;

		if ( -1 == ListView_InsertItem(_list, &item_))
			return;

		CAtlString cs_;
		cs_ = _file.Path();
		ListView_SetItemText(
				_list,
				count_,
				1,
				cs_.GetBuffer()
			);

		cs_ = _file.Virus();
		ListView_SetItemText(
				_list,
				count_,
				2,
				cs_.GetBuffer()
			);

		cs_ = _file.ActionAsText();
		ListView_SetItemText(
				_list,
				count_,
				3,
				cs_.GetBuffer()
			);

		ListView_EnsureVisible(
				_list,
				count_,
				FALSE
			);
	}

	VOID  CThreatPanel_DeleteSelected(const CWindow& _list, CThreatReport& _report)
	{
		const INT cnt_ = ListView_GetItemCount(_list);
		for ( INT i_ = 0; i_ < cnt_; i_++ )
		{
			const BOOL bChecked = ListView_GetCheckState(_list, i_);
			if (!bChecked)
				continue;
			CScanFileEx& file_ = _report.Item(i_);
			if (CScanActionState::ePerformed == file_.Action().State())
				continue;

			file_.Action().Identifier(CScanAction::eActionDelete);

			const BOOL bResult = ::DeleteFile(file_.Path());

			if (!bResult)
				file_.Action().State(CScanActionState::eError);
			else
				file_.Action().State(CScanActionState::ePerformed);

			CAtlString cs_result = file_.ActionAsText();
			ListView_SetItemText(_list, i_, 3, cs_result.GetBuffer());
		}
	}

	bool  CThreatPanel_HasSelected   (const CWindow& _list)
	{
		const INT cnt_ = ListView_GetItemCount(_list);
		for ( INT i_ = 0; i_ < cnt_; i_++ )
		{
			const BOOL bChecked = ListView_GetCheckState(_list, i_);
			if (bChecked)
				return true;
		}
		return false;
	}

	VOID  CThreatPanel_ClearSelection(const CWindow& _list)
	{
		INT cnt_ = ListView_GetItemCount(_list);

		for (INT i_ = 0; i_ < cnt_; i_++)
			ListView_SetCheckState(_list, i_, FALSE);
	}
}}}

/////////////////////////////////////////////////////////////////////////////

CThreatPanel::CThreatPanel(IRenderer& _parent):
	TPageBase  (IDD_TMON_THREAT_PANEL , *this),
	m_bkg_rnd  (IDR_TMON_THREAT_PANEL_BKGND, TPageBase::GetWindow_Ref()),
	m_act_label(CControlCrt(1, m_bkg_rnd , *this)),
	m_images(NULL)
{
	_parent;
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);

	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	CGdiPlusPngLoader::CreateImages(
			IDR_TMON_MAIN_DLG_STAT_STRIP,
			hInstance,
			5,
			m_images
		);
}

CThreatPanel::~CThreatPanel(void)
{
	if (NULL != m_images)
	{
		ImageList_Destroy(m_images);
		m_images = NULL;
	}
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CThreatPanel::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CThreatPanel_Layout layout_(host_);
	hr_ = layout_.CreateList(m_list);
	if (S_OK == hr_)
	{
		ListView_SetImageList(m_list, m_images, LVSIL_SMALL);
	}
	{
		const RECT rc_ = layout_.GetActLabelRect();
		hr_ = m_act_label.Create(
				host_,
				rc_,
				_T("Action:")
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_act_label.FontSize(9);
	}
	layout_.InitCtrls();

	global::GetScannerRef().Events().Subscribe(this);
	return hr_;
}

HRESULT    CThreatPanel::Destroy(void)
{
	global::GetScannerRef().Events().Unsubscribe(this);

	if (m_list)
		m_list.SendMessage(WM_CLOSE);
	m_act_label.Destroy();

	if (global::GetSettings().Protection().Reporting())
	{
		m_report.Save();
		m_report.Close();
	}
	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CThreatPanel::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;

			if (false){}
			else if (IDC_TMON_THREAT_BTN_APPLY == ctrlId)
			{
				bHandled = TRUE;

				if (!details::CThreatPanel_HasSelected(m_list))
					global::SetErrorMessage(
						_T("No item is checked.")
					);
				else
				{
					CScanActionEnum enum_;

					bool bDelete  = false;
					CWindow host_ = TPageBase::GetWindow_Ref();

					WTL::CComboBox combo_ = host_.GetDlgItem(IDC_TMON_THREAT_LST_ACT);
					if (combo_)
						bDelete   = (CScanAction::eActionDelete == enum_.Item(combo_.GetCurSel()).Identifier());
					if (bDelete  == false)
					{
						global::SetInfoMessage(_T("Do nothing action is selected"));
						return 0;
					}

					if (IDCANCEL == ::MessageBox(
											::GetActiveWindow(),
											_T("Do you really want to delete checked item(s)? They will be deleted permanently."),
											global::GetAppObjectRef().GetName(),
											MB_OKCANCEL | MB_ICONEXCLAMATION
										))
						return 0;
					CApplicationCursor wc_;
					details::CThreatPanel_DeleteSelected(m_list, m_report);
					details::CThreatPanel_ClearSelection(m_list);
				}
			}
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CThreatPanel::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID       CThreatPanel::IScannerEvent_OnFileScanFinish(CScanFile _file)
{
	switch (_file.Health())
	{
#if (0) // it's very dangerous: I lost my files
	case CScanFileHealth::eError:
	case CScanFileHealth::eClean:
#endif
	case CScanFileHealth::eInfected:
	case CScanFileHealth::eSuspected:
		{
			CScanFileEx file_ex = _file;
			file_ex.Action() = global::GetSettings().Protection().DefaultAction();

			const bool bAutoDelete = (CScanAction::eActionDelete == file_ex.Action().Identifier());
			if (bAutoDelete)
			{
				CGenericFile  file_(file_ex.Path());
				HRESULT hr_ = file_.Delete();
				if (FAILED(hr_))
					file_ex.Action().State(CScanActionState::eError);
				else
					file_ex.Action().State(CScanActionState::ePerformed);
			}

			details::CThreatPanel_InsertRow(m_list, file_ex);
			m_report.Append(file_ex);
		} break;
	}
}

VOID       CThreatPanel::IScannerEvent_OnScanProcessBegin(VOID)
{
	const INT count_ = ListView_GetItemCount(m_list);
	if (count_ > 0)
		ListView_DeleteAllItems(m_list);

	if (global::GetSettings().Protection().Reporting())
	{
		m_report.Save();
		m_report.Reset();
	}
	else
		m_report.Clear();
}