/*
	Created by Tech_dog (VToropov) on 21-Jan-2016 at 0:22:09am, GMT+7, Phuket, Rawai, Thursday;
	This is Total Monitoring Scan Schedule Settings Tab Page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Setup_JobPage.h"
#include "TMon_Resource.h"

using namespace tmon::common;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "UIX_GdiObject.h"
#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

#include "TMon_Data_Defs.h"

using namespace tmon::data;

#include "Shared_CustomTskMgr.h"

using namespace shared::tasks;

#include "TMon_Task_Dlg.h"

using namespace tmon::UI::dialogs;

#include "Shared_GenericAppObject.h"
#include "Shared_Registry.h"

using namespace shared::lite::common;
using namespace shared::lite::data;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace components { namespace details
{
	CCustomTaskMgr& CJobSetupPage_MgrObject(void)
	{
		static CCustomTaskMgr mgr_;
		return mgr_;
	}

	INT&            CJobSetupPage_LastListIndex(void)
	{
		static INT nIndex = 0;
		return nIndex;
	}

	bool&           CJobSetupPage_EditListMode(void)
	{
		static bool bEdit = false;
		return bEdit;
	}

	class CJobSetupPage_Registry
	{
	private:
		mutable
		CRegistryStorage m_stg;
	public:
		CJobSetupPage_Registry(void) : m_stg(HKEY_CURRENT_USER, true){}
	public:
		CAtlString  LastTaskPath(void)const
		{
			CAtlString cs_path;
			m_stg.Load(
					this->_CommonFolder(),
					this->_NamedValueLastTaskPath(),
					cs_path
				);
			return cs_path;
		}

		HRESULT     LastTaskPath(const CAtlString& _path)
		{
			HRESULT hr_ = m_stg.Save(
					this->_CommonFolder(),
					this->_NamedValueLastTaskPath(),
					_path
				);
			return hr_;
		}
	private:
		CAtlString _CommonFolder(void)            const{ return CAtlString(_T("Settings\\Schedule"));   }
		CAtlString _NamedValueLastTaskPath(void)  const{ return CAtlString(_T("LastTaskPath"));         }
	};

	class CJobSetupPage_Layout
	{
	private:
		RECT        m_client_area;
		CWindow&    m_page_ref;
	public:
		CJobSetupPage_Layout(CWindow& _page) : m_page_ref(_page)
		{
			m_page_ref.GetClientRect(&m_client_area);
		}
	public:
		RECT        GetListRect(void)
		{
			const RECT rc_ = {
					5,
					__H(m_client_area) /  2,
					__W(m_client_area) -107,
					__H(m_client_area) - 20
				};
			return rc_;
		}

		VOID        Update(void)
		{
			::WTL::CEdit edt_;
			edt_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FOLDER);
			if (edt_)
				edt_.SetCueBannerText(_T("Type scan folder or press [Browse] button"), TRUE);

			edt_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_TITLE);
			if (edt_)
				edt_.SetCueBannerText(_T("Type scan task friendly name"), TRUE);

			const SIZE sz_ = {-1, +1};

			AlignCtrlsByHeight(
						m_page_ref,
						IDC_TMON_SETUP_JOB_FOLDER,
						IDC_TMON_SETUP_JOB_BROWSE,
						sz_
					);
		}
	};

	class CJobSetupPage_List
	{
	private:
		struct _lst_col {
			INT     _width;
			LPTSTR  _title;
		};
	private:
		CWindow     m_list;
		CWindow&    m_list_ref;
	public:
		CJobSetupPage_List(CWindow& _parent) : m_list_ref(m_list)
		{
			m_list = _parent.GetDlgItem(IDC_TMON_SETUP_JOB_LIST);
		}
		CJobSetupPage_List(::WTL::CListViewCtrl& _list) : m_list_ref(_list){m_list = m_list_ref;}
	public:
		VOID        AddRow(const CCustomTask& _task, const bool bSelect = false)
		{
			const INT nRow = ListView_GetItemCount(m_list_ref);

			CAtlString cs_buffer = _task.Description();

			LVITEM item_   = {0};
			item_.mask     = LVIF_TEXT|LVIF_PARAM;
			item_.iItem    = nRow;
			item_.pszText  = cs_buffer.GetBuffer();
			item_.lParam   = static_cast<LPARAM>(_task.Timestamp());
			ListView_InsertItem(m_list_ref, &item_);

			cs_buffer = _task.Path();         ListView_SetItemText(m_list_ref, nRow, 1, cs_buffer.GetBuffer());
			cs_buffer = _task.ActiveAsText(); ListView_SetItemText(m_list_ref, nRow, 2, cs_buffer.GetBuffer());

			if (bSelect)
			{
				ListView_SetItemState (m_list_ref,   -1,             0, LVIS_SELECTED);
				ListView_SetItemState (m_list_ref, nRow, LVIS_SELECTED, LVIS_SELECTED);
				ListView_EnsureVisible(m_list_ref, nRow, FALSE);
				m_list_ref.SetFocus();

				CJobSetupPage_LastListIndex() = nRow;
			}
		}

		HRESULT     Create(CWindow& _parent)const
		{
			if (m_list_ref.IsWindow())
				return (HRESULT_FROM_WIN32(ERROR_OBJECT_ALREADY_EXISTS));

			CJobSetupPage_Layout layout_(_parent);

			RECT rc_ = layout_.GetListRect();
			m_list_ref.Create(
					_T("SysListView32"),
					_parent,
					rc_,
					NULL,
					WS_CHILD|WS_VISIBLE|LVS_REPORT|LVS_SHOWSELALWAYS,
					WS_EX_CLIENTEDGE,
					IDC_TMON_SETUP_JOB_LIST
				);
			if (!m_list_ref.IsWindow())
				return (HRESULT_FROM_WIN32(::GetLastError()));

			const DWORD ext_style_ = LVS_EX_FULLROWSELECT
			                        |LVS_EX_LABELTIP;

			ListView_SetExtendedListViewStyle((HWND)m_list_ref, ext_style_);

			const HFONT hFont = _parent.GetFont();
			m_list_ref.SetFont(hFont);
			{
				CWindow header_ = ListView_GetHeader(m_list_ref);
				if (header_)
					header_.SetFont(hFont);
			}

			CJobSetupPage_List::_lst_col cols_[] = {
					{135, _T("Task Name")},
					{190, _T("Path"     )},
					{ 60, _T("Status"   )},
				};

			LVCOLUMN lvc ={0};
			lvc.mask     = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
			lvc.fmt      = LVCFMT_LEFT;
			lvc.pszText  = cols_[0]._title;
			lvc.cx       = cols_[0]._width;
			lvc.iSubItem = 0;

			ListView_InsertColumn(m_list_ref, 0, &lvc);

			for (INT i_  = 1; i_ < _countof(cols_); i_++)
			{
				lvc.pszText  = cols_[i_]._title;
				lvc.cx       = cols_[i_]._width;
				lvc.iSubItem = i_;

				ListView_InsertColumn(m_list_ref, i_, &lvc);
			}
			ListView_SetExtendedListViewStyle(m_list_ref, LVS_EX_FULLROWSELECT);
			return S_OK;
		}

		INT         GetSelectedIndex(void)const
		{
			::WTL::CListViewCtrl ctrl_ = m_list_ref;
			const INT nSelected = ctrl_.GetNextItem(-1, LVNI_SELECTED);
			return nSelected;
		}

		HRESULT     Update(void)
		{
			CCustomTaskMgr& mgr_ = CJobSetupPage_MgrObject();
			HRESULT  hr_  = mgr_.Initialize();
			if (FAILED(hr_))
				global::SetErrorMessage(mgr_.Error());
			else
			{
				::WTL::CListViewCtrl ctrl_ = m_list_ref;
				if (ctrl_.GetItemCount())
					ctrl_.DeleteAllItems();

				const TCustomTasks& tasks_ = mgr_.TaskList();
				for (size_t i_ = 0; i_ < tasks_.size(); i_++)
				{
					const CCustomTask& task_ = tasks_[i_];
					const bool bSelected = (
							CJobSetupPage_LastListIndex() == static_cast<INT>(i_)
						);
					this->AddRow(
							task_,
							bSelected
						);
				}
			}
			return hr_;
		}

		VOID        UpdateRow(const CCustomTask& _task, const INT nRow)
		{
			CAtlString cs_buffer;

			cs_buffer = _task.Description();  ListView_SetItemText(m_list_ref, nRow, 0, cs_buffer.GetBuffer());
			cs_buffer = _task.Path();         ListView_SetItemText(m_list_ref, nRow, 1, cs_buffer.GetBuffer());
			cs_buffer = _task.ActiveAsText(); ListView_SetItemText(m_list_ref, nRow, 2, cs_buffer.GetBuffer());
		};
	};

	class CJobSetupPage_Init
	{
	private:
		CWindow&      m_page_ref;
	public:
		CJobSetupPage_Init(CWindow& _page) : m_page_ref(_page){}
	public:
		VOID          ClearTask(void)
		{
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FOLDER);
			if (ctrl_)
				ctrl_.SetWindowText(NULL);

			ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_TITLE);
			if (ctrl_)
				ctrl_.SetWindowText(NULL);

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FREQ);
			if (cbo_)
				cbo_.SetCurSel(cbo_.GetCount() - 1);

			cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_DAYS);
			if (cbo_)
				cbo_.SetCurSel(CB_ERR);

			::WTL::CDateTimePickerCtrl dt_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_TIME);
			if (dt_)
			{
				SYSTEMTIME st_ = {0};
				::GetLocalTime(&st_);
				dt_.SetSystemTime(GDT_VALID, &st_);
			}

			::WTL::CButton chk_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_ACTIVE);
			if (chk_)
				chk_.SetCheck(static_cast<INT>(false));

			::WTL::CButton btn_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_ADD);
			if (btn_)
				btn_.SetWindowText(_T("Add Scan"));
		}

		VOID          CreateTask(CCustomTask& _task)
		{
			CAtlString cs_value;
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FOLDER);
			if (ctrl_)
				ctrl_.GetWindowText(cs_value);
			_task.Path(cs_value);

			ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_TITLE);
			if (ctrl_)
				ctrl_.GetWindowText(cs_value);
			_task.Description(cs_value);

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FREQ);
			if (cbo_)
				if (CB_ERR != cbo_.GetCurSel())
					_task.Period().Identifier(
						static_cast<CTaskPeriod::_e>(cbo_.GetCurSel())
					);

			cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_DAYS);
			if (cbo_)
				if (CB_ERR != cbo_.GetCurSel())
					_task.Day().Identifier(
						static_cast<CTaskDay::_e>(cbo_.GetCurSel())
					);

			::WTL::CDateTimePickerCtrl dt_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_TIME);
			if (dt_)
			{
				SYSTEMTIME st_ = {0};
				dt_.GetSystemTime(&st_);
				_task.Time(st_);
			}

			bool bActive = false;
			::WTL::CButton chk_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_ACTIVE);
			if (chk_)
				bActive = !!chk_.GetCheck();
			_task.Active(bActive);
		}

		VOID          InitCtrls(void)
		{
			::WTL::CComboBox cbo_;
			cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FREQ);
			if (cbo_)
			{
				CTaskPeriodEnum enum_;
				for (INT i_ = 0; i_ < enum_.Count(); i_++)
					cbo_.AddString(enum_.Item(i_).Title());

				if (cbo_.GetCount())
					cbo_.SetCurSel(static_cast<INT>(enum_.Default().Identifier()));
			}

			cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_DAYS);
			if (cbo_)
			{
				CTaskDayEnum enum_;
				for (INT i_ = 0; i_ < enum_.Count(); i_++)
					cbo_.AddString(enum_.Item(i_).Name());

				cbo_.SetCurSel(CB_ERR);
				cbo_.SetCueBannerText(_T("Not selected"));
			}
		}

		VOID          InitCtrls(const CCustomTask& _task)
		{
			CAtlString cs_value = _task.Path();
			CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FOLDER);
			if (ctrl_)
				ctrl_.SetWindowText(cs_value);

			cs_value = _task.Description();
			ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_TITLE);
			if (ctrl_)
				ctrl_.SetWindowText(cs_value);

			::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FREQ);
			if (cbo_)
				cbo_.SetCurSel(static_cast<INT>(_task.Period().Identifier()));

			cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_DAYS);
			if (cbo_)
				cbo_.SetCurSel(static_cast<INT>(_task.Day().Identifier()));

			::WTL::CDateTimePickerCtrl dt_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_TIME);
			if (dt_)
			{
				SYSTEMTIME st_ = _task.Time();
				dt_.SetSystemTime(GDT_VALID, &st_);
			}

			::WTL::CButton chk_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_ACTIVE);
			if (chk_)
				chk_.SetCheck(static_cast<INT>(_task.Active()));

			::WTL::CButton btn_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_ADD);
			if (btn_)
				btn_.SetWindowText(_T("Update Scan"));
		}
	};

	class CJobSetupPage_Handler
	{
	private:
		CWindow&      m_page_ref;
	public:
		CJobSetupPage_Handler(CWindow& page_ref) : m_page_ref(page_ref){}
	public:
		BOOL          OnCommand(const WORD wCtrlId, const WORD wNotifyCode)
		{
			BOOL bHandled = TRUE; wNotifyCode;
			if (false){}
			else if (IDC_TMON_SETUP_JOB_ADD == wCtrlId)
			{
				CCustomTask task_;
				CJobSetupPage_Init init_(m_page_ref);
				init_.CreateTask(task_);

				const bool bValidated = task_.Validate();
				if (!bValidated)
					global::SetErrorMessage(task_.Error());
				else
				{
					bool& bEditMode = CJobSetupPage_EditListMode();
					CCustomTaskMgr& mgr_ = CJobSetupPage_MgrObject();

					if (bEditMode)
					{
						const INT nSelected = CJobSetupPage_LastListIndex();
						HRESULT hr_ = mgr_.Update(task_, nSelected);
						if (FAILED(hr_))
							global::SetErrorMessage(mgr_.Error());
						else
						{
							global::SetInfoMessage(
									_T("The scheduled scan task is successfully updated.")
								);
							CJobSetupPage_List wrap_(m_page_ref);
							wrap_.UpdateRow(task_, nSelected);
							init_.ClearTask();
						}
					}
					else
					{
						HRESULT hr_ = mgr_.Add(task_);
						if (FAILED(hr_))
							global::SetErrorMessage(mgr_.Error());
						else
						{
							global::SetInfoMessage(
									_T("New scheduled scan task is successfully added.")
								);
							CJobSetupPage_List wrap_(m_page_ref);
							wrap_.AddRow(task_, true);
							init_.ClearTask();
						}
					}
				}
			}
			else if (IDC_TMON_SETUP_JOB_BROWSE == wCtrlId)
			{
				/*CCustomTaskEx task_;
				CTaskDlg dlg_;
				dlg_.DoModal(task_);*/

				CWindow ctrl_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_FOLDER);
				if (ctrl_)
				{
					CAtlString cs_path = this->_SelectTaskPath();
					ctrl_.SetWindowText(cs_path);
				}
			}
			else if (IDC_TMON_SETUP_JOB_FREQ == wCtrlId)
			{
				if (CBN_SELCHANGE == wNotifyCode)
				{
					::WTL::CComboBox cbo_ = m_page_ref.GetDlgItem(wCtrlId);
					const bool bEnabled = CTaskPeriod::eWeekly == cbo_.GetCurSel();
					
					cbo_ = m_page_ref.GetDlgItem(IDC_TMON_SETUP_JOB_DAYS);
					if (cbo_)
					{
						cbo_.EnableWindow(bEnabled);
						if (!bEnabled)
							cbo_.SetCurSel(CB_ERR);
					}
				}
			}
			else if (IDC_TMON_SETUP_JOB_DELETE == wCtrlId)
			{
				CJobSetupPage_List wrap_(m_page_ref);
				const INT nSelected = wrap_.GetSelectedIndex();
				if (nSelected < 0)
					global::SetWarnMessage(_T("No item is selected"));
				else
				{
					CCustomTaskMgr& mgr_ = CJobSetupPage_MgrObject();
					const TCustomTasks& tasks_ = mgr_.TaskList();
					const INT count_ = static_cast<INT>(tasks_.size());
					if (nSelected < count_)
					{
						CJobSetupPage_LastListIndex() = nSelected;
						const CCustomTask& task_ = tasks_[nSelected];

						CAtlString cs_;
						cs_.Format(
								_T("Do you really want to delete '%s' scan task?"),
								task_.Description()
							);
						const INT nResult = ::MessageBox(
								NULL,
								cs_,
								global::GetAppObjectRef().Version().ProductName(),
								MB_OKCANCEL|MB_ICONEXCLAMATION
							);
						if (IDOK == nResult)
						{
							cs_ = task_.Identifier();
							HRESULT hr_ = mgr_.Delete(cs_);
							if (FAILED(hr_))
								global::SetErrorMessage(mgr_.Error());
							else
							{
								wrap_.Update();
								global::SetInfoMessage(
										_T("Scheduled scan task has been deleted")
									);
							}
						}
					}
					else
					{
						global::SetWarnMessage(
								_T("Please refresh the list and try again")
							);
					}
				}
			}
			else if (IDC_TMON_SETUP_JOB_REFRESH == wCtrlId)
			{
				CJobSetupPage_List wrap_(m_page_ref);
				HRESULT hr_ = wrap_.Update();
				if (!FAILED(hr_))
					global::SetInfoMessage(_T("Scheduled scan list is updated"));
			}
			else if (IDC_TMON_SETUP_JOB_EDIT == wCtrlId)
			{
				CJobSetupPage_List wrap_(m_page_ref);
				const INT nSelected = wrap_.GetSelectedIndex();
				if (nSelected < 0)
					global::SetWarnMessage(_T("No item is selected"));
				else
				{
					CCustomTaskMgr& mgr_ = CJobSetupPage_MgrObject();
					const TCustomTasks& tasks_ = mgr_.TaskList();
					const INT count_ = static_cast<INT>(tasks_.size());
					if (nSelected < count_)
					{
						CJobSetupPage_Init init_(m_page_ref);
						const CCustomTask& task_ = tasks_[nSelected];
						init_.InitCtrls(task_);

						CJobSetupPage_EditListMode() = true;
						CJobSetupPage_LastListIndex() = nSelected;
					}
					else
					{
						global::SetWarnMessage(
								_T("Please refresh the list and try again")
							);
					}
				}
			}
			return bHandled;
		}
	private:
		CAtlString   _SelectTaskPath(void)
		{
			CJobSetupPage_Registry reg_;
			CAtlString cs_path = reg_.LastTaskPath();
			if (cs_path.IsEmpty())
			{
				CApplication& the_app = global::GetAppObjectRef();
				the_app.GetPath(cs_path);
			}

			::WTL::CFolderDialog dlg_(
					NULL,
					_T("Choose task path"),
					BIF_RETURNONLYFSDIRS|BIF_USENEWUI
				);

			dlg_.SetInitialFolder(cs_path);

			const INT_PTR nResult = dlg_.DoModal();
			if (IDOK == nResult)
			{
				reg_.LastTaskPath(cs_path); // saves the last selected path
				cs_path = dlg_.GetFolderPath();
			}
			else
				cs_path.Empty();

			return cs_path;
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CJobSetupPage::CJobSetupPage(::WTL::CTabCtrl& tab_ref):
	TBasePage(IDD_TMON_SETUP_JOB_PAGE, tab_ref, *this)
{
}

CJobSetupPage::~CJobSetupPage(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CJobSetupPage::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_NOTIFY:
		{
			const UINT ctrlId = static_cast<UINT>(wParam);
			const LPNMHDR data_ = reinterpret_cast<LPNMHDR>(lParam);

			if (IDC_TMON_SETUP_JOB_LIST == ctrlId)
				if (data_)
					if (data_->code == LVN_ITEMCHANGED)
					{
						bool& bEditMode = details::CJobSetupPage_EditListMode();
						if (bEditMode)
						{
							bEditMode = false;
							details::CJobSetupPage_Init init_(*this);
							init_.ClearTask();
						}
					}
		} break;
	case WM_INITDIALOG:
		{
			if (TBasePage::m_bInitilized)
				break;
			TBasePage::m_bInitilized = true;

			details::CJobSetupPage_Layout layout_(*this);
			layout_.Update();

			details::CJobSetupPage_List list_(m_list);
			list_.Create(*this);
			list_.Update();

			details::CJobSetupPage_Init init_(*this);
			init_.InitCtrls();

			bHandled = TRUE;
		} break;
	case WM_DESTROY:
		{
			if (m_list)
				m_list.SendMessage(WM_CLOSE);
		} break;
	case WM_COMMAND:
		{
			if (!TBasePage::m_bInitilized)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			details::CJobSetupPage_Handler handler_(*this);
			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CAtlString CJobSetupPage::GetPageTitle(void) const
{
	CAtlString cs_title(_T("Scheduled Scans"));
	return cs_title;
}