#ifndef _TMONSPLASHDLG_H_32644073_6947_4dbe_AD38_635F85A75069_INCLUDED
#define _TMONSPLASHDLG_H_32644073_6947_4dbe_AD38_635F85A75069_INCLUDED
/*
	Created by Tech_dog (VToropov) on 18-Dec-2015 at 5:19:47am, GMT+7, Phuket, Rawai, Friday;
	This is Total Monitoring Application Splash Screen class declaration file.
*/
#include "TMon_Splash_Panel.h"

namespace tmon { namespace UI { namespace components
{
	using shared::avs::IScannerEventSink;
	using shared::avs::CScanError;

	class CSplashDlg :
		public  IScannerEventSink
	{
	private:
		class CSplashDlgImpl :
			public  ::ATL::CDialogImpl<CSplashDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CSplashDlgImpl> TDialog;
			friend class CTMonSplashDlg;
		private:
			CSplashPanel       m_panel;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CSplashDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY    ,  OnDestroy )
				MESSAGE_HANDLER     (WM_INITDIALOG ,  OnInitDlg )
			END_MSG_MAP()
		public:
			CSplashDlgImpl(void);
			~CSplashDlgImpl(void);
		private:
			LRESULT OnDestroy  (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnInitDlg  (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
		};
	private:
		CSplashDlg::CSplashDlgImpl  m_dlg;
	public:
		CSplashDlg(void);
		~CSplashDlg(void);
	public:
		HRESULT     Show(void);
	private: // IScannerEventSink
		VOID        IScannerEvent_OnEngineInitFinish(CScanError) override sealed;
	};
}}}

#endif/*_TMONSPLASHDLG_H_32644073_6947_4dbe_AD38_635F85A75069_INCLUDED*/