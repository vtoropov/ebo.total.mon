/*
	Created by Tech_dog (VToropov) on 2-Dec-2015 at 5:55:23pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring application scan tab page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_TabPage_Scan.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

using namespace ex_ui::controls;

#include "UIX_GdiObject.h"

using namespace ex_ui;
using namespace ex_ui::draw::common;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

#include "Shared_AvsDefs.h"

using namespace shared::avs;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CTabPageScan_Layout
	{
		enum _e{
				e_tag_w       = 270,
				e_tag_h       = 136,
				e_tag_h_gap   =   6,
				e_margins_w   =  10,
				e_margins_h   =  15,
				e_panel_w     = 550,
				e_panel_h     = 440,
			};
	private:
		CWindow&      m_page_ref;
		RECT          m_client_area;
	public:
		CTabPageScan_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT     GetScanPanelRect(void)const
		{
			RECT rc_ = this->GetTagQuickRect();

			const INT nLeft = rc_.right;
			const INT nTop  = rc_.top;

			::SetRect(
					&rc_,
					nLeft,
					nTop,
					nLeft + CTabPageScan_Layout::e_panel_w,
					nTop  + CTabPageScan_Layout::e_panel_h
				);
			return rc_;
		}

		RECT     GetTagCustomRect(void)const
		{
			RECT rc_ = this->GetTagQuickRect();
			::OffsetRect(
					&rc_,
					0,
					CTabPageScan_Layout::e_tag_h + CTabPageScan_Layout::e_tag_h_gap
				);
			return rc_;
		}

		RECT     GetTagFullRect (void)const
		{
			RECT rc_ = this->GetTagCustomRect();
			::OffsetRect(
					&rc_,
					0,
					CTabPageScan_Layout::e_tag_h + CTabPageScan_Layout::e_tag_h_gap
				);
			return rc_;
		}

		RECT     GetTagQuickRect(void)const
		{
			const INT nLeft = CTabPageScan_Layout::e_margins_w;
			const INT nTop  = CTabPageScan_Layout::e_margins_h;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CTabPageScan_Layout::e_tag_w,
					nTop  + CTabPageScan_Layout::e_tag_h
				};
			return rc_;
		}
	};

	class CTabPageScan_Init
	{
	private:
		CWindow&      m_page_ref;
	public:
		CTabPageScan_Init(CWindow& page_ref) : m_page_ref(page_ref)
		{
		}
	public:
		VOID    InitCtrls(void)
		{
			const CScannerState::_e state_ = global::GetScannerRef().State();
			switch (state_)
			{
			case CScannerState::eError         :  global::SetErrorMessage(_T("AV engine initialization failed.")); break;
			case CScannerState::eInitialized   :  global::SetInfoMessage (NULL); break;
			case CScannerState::eInitializing  :  global::SetWaitMessage (_T("AV engine initializating...")); break;
			case CScannerState::eNotInitialized:  global::SetWarnMessage (_T("AV engine is not initialized.")); break;
			}
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageScan::CTabPageScan(IRenderer& _parent):
	TPageBase(IDD_TMON_SCAN_TAB_PAGE, *this),
	m_bkg_rnd(IDR_TMON_SCAN_TAB_BKGND, TPageBase::GetWindow_Ref()),
	m_tag_quick(&m_bkg_rnd),
	m_tag_custm(&m_bkg_rnd),
	m_tag_compl(&m_bkg_rnd),
	m_pan_quick( m_bkg_rnd),
	m_pan_custm( m_bkg_rnd),
	m_pan_compl( m_bkg_rnd),
	m_blocked(false),
	m_timer_id(NULL)
{
	_parent;
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);
}

CTabPageScan::~CTabPageScan(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageScan::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CTabPageScan_Layout layout_(host_);
	{
		const RECT rc_ = layout_.GetTagQuickRect();
		hr_ = m_tag_quick.Create(
				host_,
				rc_,
				NULL,
				IDC_TMON_SCAN_TAG_QUICK
			);
		hr_ = m_tag_quick.SetImage(IDR_TMON_SCAN_TAG_QUICK_SEL);
		hr_ = m_tag_quick.UpdateLayout();
		hr_ = m_tag_quick.SetParentNotifyPtr(this);
	}
	{
		const RECT rc_ = layout_.GetTagCustomRect();
		hr_ = m_tag_custm.Create(
				host_,
				rc_,
				NULL,
				IDC_TMON_SCAN_TAG_CUSTM
			);
		hr_ = m_tag_custm.SetImage(IDR_TMON_SCAN_TAG_CUSTM_NOT);
		hr_ = m_tag_custm.UpdateLayout();
		hr_ = m_tag_custm.SetParentNotifyPtr(this);
	}
	{
		const RECT rc_ = layout_.GetTagFullRect();
		hr_ = m_tag_compl.Create(
				host_,
				rc_,
				NULL,
				IDC_TMON_SCAN_TAG_FULL
			);
		hr_ = m_tag_compl.SetImage(IDR_TMON_SCAN_TAG_FULL_NOT);
		hr_ = m_tag_compl.UpdateLayout();
		hr_ = m_tag_compl.SetParentNotifyPtr(this);
	}
	const RECT rcPanel = layout_.GetScanPanelRect();
	{
		hr_ = m_pan_custm.Create(
				host_,
				rcPanel,
				false
			);
	}
	{
		hr_ = m_pan_quick.Create(
				host_,
				rcPanel,
				true
			);
	}
	{
		hr_ = m_pan_compl.Create(
				host_,
				rcPanel,
				false
			);
	}
	global::GetScannerRef().Events().Subscribe(this);

	details::CTabPageScan_Init init_(host_);
	init_.InitCtrls();

	return hr_;
}

HRESULT    CTabPageScan::Destroy(void)
{
	global::GetScannerRef().Events().Unsubscribe(this);

	if (m_timer_id)
	{
		CWindow host_ = TPageBase::GetWindow_Ref();
		host_.KillTimer(m_timer_id);
		m_timer_id = NULL;
	}

	m_pan_compl.Destroy();
	m_pan_custm.Destroy();
	m_pan_quick.Destroy();

	m_tag_compl.Destroy();
	m_tag_quick.Destroy();
	m_tag_custm.Destroy();

	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageScan::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam;
	switch (uMsg)
	{
	case WM_TIMER:
		{
			static size_t ptr_ = 0;

			CAtlString cs_msg;

			if (ptr_ < m_files.size())
			{
				const DWORD nPercent = DWORD((FLOAT(ptr_ + 1) / FLOAT(m_files.size())) * 100);
				cs_msg.Format(
						_T("Loading virus definition: %s (%d%%)"),
						m_files[ptr_].GetString(),
						nPercent
					);
			}
			else
			{
				cs_msg = _T("Optimizing virus database...");
			}

			global::SetWaitMessage(cs_msg);

			ptr_ += 1;
		} break;
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageScan::IControlNotify_OnClick(const UINT ctrlId)
{
	if (m_blocked)
		return S_OK;

	switch (ctrlId)
	{
	case IDC_TMON_SCAN_TAG_QUICK:
		{
			m_tag_quick.SetImage(IDR_TMON_SCAN_TAG_QUICK_SEL); m_tag_quick.UpdateLayout(); m_pan_quick.Show(true);
			m_tag_custm.SetImage(IDR_TMON_SCAN_TAG_CUSTM_NOT); m_tag_custm.UpdateLayout(); m_pan_custm.Show(false);
			m_tag_compl.SetImage(IDR_TMON_SCAN_TAG_FULL_NOT ); m_tag_compl.UpdateLayout(); m_pan_compl.Show(false);
		}break;
	case IDC_TMON_SCAN_TAG_CUSTM:
		{
			m_tag_custm.SetImage(IDR_TMON_SCAN_TAG_CUSTM_SEL); m_tag_custm.UpdateLayout(); m_pan_custm.Show(true);
			m_tag_quick.SetImage(IDR_TMON_SCAN_TAG_QUICK_NOT); m_tag_quick.UpdateLayout(); m_pan_quick.Show(false);
			m_tag_compl.SetImage(IDR_TMON_SCAN_TAG_FULL_NOT ); m_tag_compl.UpdateLayout(); m_pan_compl.Show(false);
		}break;
	case IDC_TMON_SCAN_TAG_FULL:
		{
			m_tag_compl.SetImage(IDR_TMON_SCAN_TAG_FULL_SEL ); m_tag_compl.UpdateLayout(); m_pan_compl.Show(true);
			m_tag_custm.SetImage(IDR_TMON_SCAN_TAG_CUSTM_NOT); m_tag_custm.UpdateLayout(); m_pan_custm.Show(false);
			m_tag_quick.SetImage(IDR_TMON_SCAN_TAG_QUICK_NOT); m_tag_quick.UpdateLayout(); m_pan_quick.Show(false);
		} break;
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

VOID       CTabPageScan::IScannerEvent_OnEngineInitFinish(CScanError _err)
{
	if (m_timer_id)
	{
		CWindow host_ = TPageBase::GetWindow_Ref();
		host_.KillTimer(m_timer_id);
		m_timer_id = NULL;
	}
	if (_err)
		global::SetErrorMessage(_err);
	else
		global::SetInfoMessage(NULL);
}

VOID       CTabPageScan::IScannerEvent_OnScanProcessBegin(VOID)
{
	m_blocked = true;   // locks scan sub-page navigation
}

VOID       CTabPageScan::IScannerEvent_OnScanProcessFinish(VOID)
{
	m_blocked = false;  // release scan sub-page navigation
}

/////////////////////////////////////////////////////////////////////////////

VOID        CTabPageScan::CheckAvsEngineState(void)
{
	const CScannerState::_e state_ = global::GetScannerRef().State();
	if (CScannerState::eNotInitialized == state_
		&& m_timer_id == NULL)
	{
		CWindow host_ = TPageBase::GetWindow_Ref();

		global::GetScannerRef().Initialize();
		m_files = global::GetScannerRef().VirusDatabase().Files();

		DWORD dwPeriod = 500;
		if (m_files.size() > 50)
			dwPeriod = 50 * 1000 / m_files.size();

		m_timer_id = host_.SetTimer(1, dwPeriod);
	}
}