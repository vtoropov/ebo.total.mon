#ifndef _TMONSETUPPROPAGE_H_559ED602_6EDB_43db_8599_BF3939E0B52F_INCLUDED
#define _TMONSETUPPROPAGE_H_559ED602_6EDB_43db_8599_BF3939E0B52F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 20-Jan-2016 at 9:49:26pm, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring Protection Settings Tab Page class declaration file.
*/
#include "TMon_UI_Defs.h"

namespace tmon { namespace UI { namespace components
{
	class CProSetupPage:
		public  CTabPageBase, 
		public  ITabPageCallback
	{
		typedef CTabPageBase TBasePage;
	public:
		CProSetupPage(::WTL::CTabCtrl&);
		~CProSetupPage(void);
	private: // ITabPageCallback
		virtual LRESULT        TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	public:  // TBasePage
		virtual CAtlString     GetPageTitle(void) const override sealed;
	};
}}}

#endif/*_TMONSETUPPROPAGE_H_559ED602_6EDB_43db_8599_BF3939E0B52F_INCLUDED*/