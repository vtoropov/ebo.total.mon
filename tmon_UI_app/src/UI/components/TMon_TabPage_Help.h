#ifndef _TMONTABPAGEHELP_H_5BD0C780_32EB_4d4f_9D2D_06FA05EFEB14_INCLUDED
#define _TMONTABPAGEHELP_H_5BD0C780_32EB_4d4f_9D2D_06FA05EFEB14_INCLUDED
/*
	Created by Tech_dog (VToropov) on 15-Dec-2015 at 11:47:41pm, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Support Tab page class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_CommonDrawDefs.h"
#include "UIX_Button.h"
#include "UIX_Renderer.h"
#include "UIX_Label.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::frames::IMessageHandler;
	using ex_ui::frames::CPanelBase;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CButton;
	using ex_ui::controls::CLabel;

	class CTabPageHelp :
		public  CPanelBase,
		public  IMessageHandler,
		public  IControlNotify
	{
		typedef CPanelBase TPageBase;
	private:
		CBackgroundTileRenderer   m_bkg_rnd;
		CLabel                    m_web_label;
	public:
		CTabPageHelp(IRenderer& _parent);
		~CTabPageHelp(void);
	public:  // CPanelBase
		HRESULT     Create(const HWND hParent, const RECT& rcArea, const bool bVisible) override sealed;
		HRESULT     Destroy(void) override sealed;
	private: // IMessageHandler
		LRESULT     MessageHandler_OnMessage(UINT, WPARAM, LPARAM, BOOL&) override sealed;
	private: // IControlNotify
		HRESULT     IControlNotify_OnClick(const UINT ctrlId) override sealed;
	};
}}}

#endif/*_TMONTABPAGEHELP_H_5BD0C780_32EB_4d4f_9D2D_06FA05EFEB14_INCLUDED*/