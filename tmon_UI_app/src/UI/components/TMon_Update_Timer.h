#ifndef _TMONUPDATETIMER_H_3997EDE2_6551_49c1_9158_1D9C6FD05763_INCLUDED
#define _TMONUPDATETIMER_H_3997EDE2_6551_49c1_9158_1D9C6FD05763_INCLUDED
/*
	Created by Tech_dog (VToropov) on 21-Dec-2015 at 2:59:21pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring Update Timer class declaration file.
*/

namespace tmon { namespace UI { namespace components
{
	class CUpdateTimer
	{
	private:
		class CUpdateTimerWnd :
			public  ::ATL::CWindowImpl<CUpdateTimerWnd>
		{
			friend class CUpdateTimer;
			typedef ::ATL::CWindowImpl<CUpdateTimerWnd> TWindow;
		private:
			UINT_PTR          m_timer;
			time_t            m_updatetime;
			HIMAGELIST        m_digits;
		public:
			CUpdateTimerWnd(void);
			~CUpdateTimerWnd(void);
		public:
			BEGIN_MSG_MAP(CUpdateTimerWnd)
				MESSAGE_HANDLER(WM_CREATE      , OnCreate     )
				MESSAGE_HANDLER(WM_DESTROY     , OnDestroy    )
				MESSAGE_HANDLER(WM_ERASEBKGND  , OnEraseBkgnd )
				MESSAGE_HANDLER(WM_PAINT       , OnPaint      )
				MESSAGE_HANDLER(WM_TIMER       , OnTimer      )
			END_MSG_MAP()
		private:
			LRESULT OnCreate     (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy    (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnEraseBkgnd (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnPaint      (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnTimer      (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		CUpdateTimerWnd       m_wnd;
	public:
		CUpdateTimer(void);
		~CUpdateTimer(void);
	public:
		HRESULT               Create(HWND _parent, const RECT& _area);
		HRESULT               Destroy(void);
	public:
		static SIZE           GetDefaultSize(void);
	};
}}}

#endif/*_TMONUPDATETIMER_H_3997EDE2_6551_49c1_9158_1D9C6FD05763_INCLUDED*/