/*
	Created by Tech_dog (VToropov) on 16-Dec-2015 at 6:59:45am, GMT+7, Phuket, Rawai, Wednesday;
	This is Total Monitoring Support Tab page class implementation file.
*/
#include "StdAfx.h"
#include "TMon_TabPage_Help.h"
#include "TMon_Resource.h"
#include "TMon_UI_Defs.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

using namespace ex_ui::controls;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class CTabPageHelp_Layout
	{
	private:
		CWindow&     m_page_ref;
		RECT         m_client_area;
	public:
		CTabPageHelp_Layout(CWindow& page_ref) : m_page_ref(page_ref)
		{
			if (m_page_ref)
				m_page_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT         GetWebLabelRect(void)const
		{
			const INT nLeft = 300;
			const INT nTop  =  55;

			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + 300,
					nTop  +  30
				};
			return rc_;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CTabPageHelp::CTabPageHelp(IRenderer& _parent):
	TPageBase  (IDD_TMON_HELP_TAB_PAGE , *this),
	m_bkg_rnd  (IDR_TMON_HELP_TAB_BKGND, TPageBase::GetWindow_Ref()),
	m_web_label(CControlCrt(1, m_bkg_rnd , *this))
{
	_parent;
	TPageBase::SetBkgndRenderer(&m_bkg_rnd);
}

CTabPageHelp::~CTabPageHelp(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageHelp::Create(const HWND hParent, const RECT& rcArea, const bool bVisible)
{
	HRESULT hr_ = TPageBase::Create(hParent, rcArea, bVisible);
	if (S_OK != hr_)
		return  hr_;

	CWindow host_ = TPageBase::GetWindow_Ref();

	details::CTabPageHelp_Layout layout_(host_);
	{
		CAtlString url_;
		url_.LoadString(IDS_TMON_HELP_WEB_TITLE);

		const RECT rc_ = layout_.GetWebLabelRect();
		hr_ = m_web_label.Create(
				host_,
				rc_,
				url_
			);
		if (hr_ == S_FALSE)
			hr_  = S_OK;
		m_web_label.ForeColor(RGB(0x0, 0x0, 0xd0));
		m_web_label.FontUnderline(true);
		m_web_label.FontSize(10);
	}

	return hr_;
}

HRESULT    CTabPageHelp::Destroy(void)
{
	m_web_label.Destroy();

	HRESULT hr_ = TPageBase::Destroy();
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CTabPageHelp::MessageHandler_OnMessage(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	bHandled = FALSE; lParam;
	switch (uMsg)
	{
	case WM_COMMAND:
		{
			const WORD wNotify = HIWORD(wParam); wNotify;
			const WORD ctrlId  = LOWORD(wParam); ctrlId;
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTabPageHelp::IControlNotify_OnClick(const UINT ctrlId)
{
	ctrlId;

	static bool bInProgress = false;

	if (!bInProgress)
	{
		bInProgress = true;

		CAtlString url_;
		url_.LoadString(IDS_TMON_HELP_WEB_URL);

		::ShellExecute(
				::GetDesktopWindow(),
				_T("open"),
				url_,
				NULL,
				NULL,
				SW_SHOWNORMAL
			);

		bInProgress = false;
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}