#ifndef _TMONTABSTRIP_H_1B81989F_8E35_43c9_B807_A2A089E8910F_INCLUDED
#define _TMONTABSTRIP_H_1B81989F_8E35_43c9_B807_A2A089E8910F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 24-Nov-2015 at 10:38:56am, GMT+7, Phuket, Rawai, Tuesday;
	This is Total Monitoring Main Form Tab Strip control class declaration file.
*/
#include "UIX_Button.h"
#include "UIX_Renderer.h"

namespace tmon { namespace UI { namespace components
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CButton;

	class CTabIndex
	{
	public:
		enum _e{
			eHome   = 0,
			eScan   = 1,
			eThreat = 2,
			eUpdate = 3,
			eSetup  = 4,
			eHelp   = 5,
		};
	};

	class CTabStrip : public IControlNotify
	{
	private:
		class CTabStripWnd : 
			public  ::ATL::CWindowImpl<CTabStripWnd>
		{
			typedef ::ATL::CWindowImpl<CTabStripWnd> TWindow;
			friend class CTabStrip;
		private:
			CBackgroundTileRenderer m_bkgnd;
			IControlNotify&  m_evt_sink;
			IRenderer&       m_prn_renderer;
			CButton          m_btn_home;
			CButton          m_btn_scan;
			CButton          m_btn_threat;
			CButton          m_btn_update;
			CButton          m_btn_setup;
			CButton          m_btn_help;
		public:
			BEGIN_MSG_MAP(CTabStripWnd)
				MESSAGE_HANDLER(WM_CREATE     ,   OnCreate    )
				MESSAGE_HANDLER(WM_DESTROY    ,   OnDestroy   )
				MESSAGE_HANDLER(WM_ERASEBKGND ,   OnErase     )
			END_MSG_MAP()
		public:
			CTabStripWnd(IRenderer&, IControlNotify&);
		private:
			LRESULT OnCreate    (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnDestroy   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT OnErase     (UINT, WPARAM, LPARAM, BOOL&);
		};
	private:
		IControlNotify&      m_evt_parent;
		CTabStripWnd         m_wnd;
		INT                  m_cur_index;
	public:
		CTabStrip(IRenderer&, IControlNotify&);
		~CTabStrip(void);
	private: // IControlNotify
		virtual   HRESULT    IControlNotify_OnClick(const UINT ctrlId) override sealed;
	public:
		HRESULT              Create(HWND _parent, const RECT& _area);
		HRESULT              Destroy(void);
		SIZE                 GetDefaultSize(void)const;
		INT                  SelectedTab(void)const;
		VOID                 SelectedTab(const INT nIndex);
	private:
		CTabStrip(const CTabStrip&);
		CTabStrip& operator= (const CTabStrip&);
	};
}}}

#endif/*_TMONTABSTRIP_H_1B81989F_8E35_43c9_B807_A2A089E8910F_INCLUDED*/