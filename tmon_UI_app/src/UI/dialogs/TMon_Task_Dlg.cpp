/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 11:52:45pm, GMT+7, Phuket, Rawai, Saturday;
	This is Total Monitoring Scan Task Dialog class implementation file.
*/
#include "StdAfx.h"
#include "TMon_Task_Dlg.h"
#include "TMon_Resource.h"

using namespace tmon::UI::dialogs;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls;

#include "UIX_MouseHandler.h"

using namespace ex_ui::frames;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace dialogs { namespace details
{
	class    CTaskDlg_Layout
	{
	public:
		enum { // from PSD images
			e_frame_h  = 528,
			e_frame_w  = 498,
			e_close_h  =  24,
			e_close_w  =  24,
			e_margin_v =  10,
			e_margin_h =  10,
		};
	private:
		CWindow&      m_frame_ref;
		RECT          m_client_area;
	public:
		CTaskDlg_Layout(CWindow& frame_ref) : m_frame_ref(frame_ref)
		{
			if (m_frame_ref)
				m_frame_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT          GetCloseRect  (void)const
		{
			const INT nLeft = CTaskDlg_Layout::e_frame_w - CTaskDlg_Layout::e_close_w - CTaskDlg_Layout::e_margin_h;
			const INT nTop  = CTaskDlg_Layout::e_margin_v;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CTaskDlg_Layout::e_close_w,
					nTop  + CTaskDlg_Layout::e_close_h
				};
			return rc_;
		}

		RECT          GetDragRect   (void)const
		{
			RECT rc_ = {
					0,
					0,
					CTaskDlg_Layout::e_frame_w - CTaskDlg_Layout::e_close_w * 2 - CTaskDlg_Layout::e_margin_h,
					50
				};
			return rc_;
		}

		RECT          GetFolderLabelRect(void)const
		{
			RECT rc_ = {0};
			return rc_;
		}

		RECT          GetTitleRect(void)const
		{
			RECT rc_ = {
				0, 40, m_client_area.right, 55
			};
			return rc_;
		}

		VOID          RecalcPosition(void)
		{
			const SIZE sz_ = {
				details::CTaskDlg_Layout::e_frame_w,
				details::CTaskDlg_Layout::e_frame_h
			};

			RECT rc_ = {0};
			m_frame_ref.GetWindowRect(&rc_);
			if (sz_.cx > 0)
				rc_.right = rc_.left + sz_.cx;
			if (sz_.cy > 0)
				rc_.bottom = rc_.top + sz_.cy;
			::AdjustWindowRect(
					&rc_,
					(DWORD)m_frame_ref.GetWindowLong(GWL_STYLE),
					FALSE
				);
			{
				m_frame_ref.SetWindowPos(
						NULL, 
						&rc_,
						SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOMOVE
					);
				m_frame_ref.GetClientRect(&m_client_area);
			}
			m_frame_ref.CenterWindow();
		}
	};
}}}}

/////////////////////////////////////////////////////////////////////////////

CTaskDlg::CTaskDlgImpl::CTaskDlgImpl(IControlNotify& _notify): 
	IDD(IDD_TMON_TASK_DLG),
	m_bkgnd  (IDR_TMON_TASK_DLG_BKGND, *this),
	m_close  (IDC_TMON_MAIN_DLG_CLOSE, m_bkgnd, _notify),
	m_caption(CControlCrt(1,  m_bkgnd, _notify)),
	m_lab_folder(CControlCrt(2,  m_bkgnd, _notify))
{
	HRESULT hr_ = S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	{
		hr_ = m_close.SetImage(eControlState::eNormal   , IDR_TMON_MAIN_DLG_CLOSE_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_close.SetImage(eControlState::eDisabled , IDR_TMON_MAIN_DLG_CLOSE_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_close.SetImage(eControlState::ePressed  , IDR_TMON_MAIN_DLG_CLOSE_PRES, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_close.SetImage(eControlState::eHovered  , IDR_TMON_MAIN_DLG_CLOSE_HOT , hInstance); ATLASSERT(S_OK == hr_);
	}
	::SetRectEmpty(&m_drag_rc);
}

CTaskDlg::CTaskDlgImpl::~CTaskDlgImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CTaskDlg::CTaskDlgImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;

	CMouseEvtHandler::GetObjectRef().Unsubscribe(TDialog::m_hWnd);

	m_caption.Destroy();
	m_close.Destroy();

	return 0;
}

LRESULT CTaskDlg::CTaskDlgImpl::OnEraseBg   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;

	RECT rc_ = {0};
	TDialog::GetClientRect(&rc_);

	CZBuffer dc_((HDC)wParam, rc_);

	::OffsetRect(&rc_, -rc_.left, -rc_.top);
	m_bkgnd.Draw(dc_, rc_);

	return 0;
}

LRESULT CTaskDlg::CTaskDlgImpl::OnInitDlg   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	{
		LONG_PTR l_style = TDialog::GetWindowLongPtr(GWL_STYLE);
		l_style &= ~WS_BORDER;
		l_style &= ~WS_DLGFRAME;
		TDialog::SetWindowLongPtr(GWL_STYLE, l_style);
	}
	details::CTaskDlg_Layout layout_(*this);
	layout_.RecalcPosition();
	{
		CApplicationIconLoader loader(IDR_TMON_MAIN_DLG_ICON);
		TDialog::SetIcon(loader.DetachBigIcon(), TRUE);
		TDialog::SetIcon(loader.DetachSmallIcon(), FALSE);
	}
	HRESULT  hr_ = S_OK;
	{
		RECT rc_ = layout_.GetCloseRect();
		hr_ = m_close.Create(
				*this,
				&rc_ ,
				false
			);
	}
	CAtlString cs_caption;
	{
		cs_caption += _T("Schedule Task Properties");
		if (m_task_obj.IsNew())
			cs_caption += _T(" [New]");

		hr_ = m_caption.HorzAlign(DT_CENTER);
		hr_ = m_caption.FontSize (10);
		hr_ = m_caption.FontUnderline(false);
		hr_ = m_caption.ForeColor(RGB(0, 0, 150));

		const RECT rc_ = layout_.GetTitleRect();
		hr_ = m_caption.Create(
				*this,
				rc_,
				cs_caption
			);
	}

	m_drag_rc = layout_.GetDragRect();
	CMouseEvtHandler::GetObjectRef().Subscribe(*this);

	return 0;
}

LRESULT CTaskDlg::CTaskDlgImpl::OnMouseMove (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (m_capture._captured)
	{
		const POINT local_ = {GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)};
		const SIZE  delta_ = {local_.x - m_capture._anchor.x, local_.y - m_capture._anchor.y};
		RECT rc_ = {0};
		TDialog::GetWindowRect(&rc_);
		::OffsetRect(&rc_, delta_.cx, delta_.cy);
		TDialog::SetWindowPos(NULL, &rc_, SWP_NOSIZE|SWP_NOZORDER);
	}
	return 0;
}

LRESULT CTaskDlg::CTaskDlgImpl::OnLButtonDn (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (!m_capture._captured && !::IsRectEmpty(&m_drag_rc))
	{
		POINT local_ = {
				GET_X_LPARAM(lParam),
				GET_Y_LPARAM(lParam)
			};
		::MapWindowPoints(HWND_DESKTOP, TDialog::m_hWnd, &local_, 0x1);
		if (::PtInRect(&m_drag_rc, local_))
		{
			TDialog::SetCapture();
			m_capture._captured = true;
			m_capture._anchor = local_;
			TDialog::SetFocus();
		}
	}
	return 0;
}

LRESULT CTaskDlg::CTaskDlgImpl::OnLButtonUp (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (m_capture._captured)
	{
		m_capture.Reset();
		::ReleaseCapture();
	}
	return 0;
}

LRESULT CTaskDlg::CTaskDlgImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TDialog::ShowWindow(SW_HIDE);
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CTaskDlg::CTaskDlg(void) : m_dlg(*this)
{
}

CTaskDlg::~CTaskDlg(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTaskDlg::IControlNotify_OnClick(const UINT ctrlId)
{
	switch(ctrlId)
	{
	case IDC_TMON_MAIN_DLG_CLOSE:
		{
			m_dlg.ShowWindow(SW_HIDE);
			m_dlg.EndDialog(IDCANCEL);
		} break;
	}
	return S_OK;
}

VOID       CTaskDlg::IControlNotify_OnEvent(const UINT ctrlId)
{
	ctrlId;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CTaskDlg::DoModal(CCustomTaskEx& _task)
{
	m_dlg.m_task_obj = _task;

	CWindow act_ = ::GetActiveWindow();
	const INT_PTR res_ = m_dlg.DoModal();
	{
		act_.SetWindowPos(HWND_TOPMOST  , 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
		act_.SetWindowPos(HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE);
	}
	if (IDOK == res_)
	{
		_task = m_dlg.m_task_obj;
		return S_OK;
	}
	else
		return S_FALSE;
}