#ifndef _TMONTASKDLG_H_2D047366_9399_4cef_8832_1DF4B3BDABA6_INCLUDED
#define _TMONTASKDLG_H_2D047366_9399_4cef_8832_1DF4B3BDABA6_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Jan-2016 at 11:46:44pm, GMT+7, Phuket, Rawai, Saturday;
	This is Total Monitoring Scan Task Dialog class declaration file.
*/
#include "UIX_PanelBase.h"
#include "UIX_Renderer.h"
#include "UIX_Label.h"
#include "UIX_Button.h"

#include "TMon_Data_Defs.h"

namespace tmon { namespace UI { namespace dialogs
{
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CButton;
	using ex_ui::controls::CLabel;

	using tmon::data::CCustomTaskEx;

	class CTaskDlg : private IControlNotify
	{
	private:
		class CTaskDlgImpl :
			public  ::ATL::CDialogImpl<CTaskDlgImpl>
		{
			typedef ::ATL::CDialogImpl<CTaskDlgImpl> TDialog;
			friend class CTaskDlg;
		private:
			struct CMouseCaptureData
			{
				bool     _captured;
				POINT    _anchor;
				CMouseCaptureData(void):_captured(false){_anchor.x = _anchor.y = 0;}
				void     Reset(void){ _captured = false; _anchor.x = _anchor.y = 0;}
			};
		private:
			CMouseCaptureData        m_capture;
			RECT                     m_drag_rc;
			CCustomTaskEx            m_task_obj;
			CBackgroundTileRenderer  m_bkgnd;
			CButton                  m_close;
			CLabel                   m_caption;
			CLabel                   m_lab_folder;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CTaskDlgImpl)
				MESSAGE_HANDLER     (WM_DESTROY     ,  OnDestroy   )
				MESSAGE_HANDLER     (WM_ERASEBKGND  ,  OnEraseBg   )
				MESSAGE_HANDLER     (WM_INITDIALOG  ,  OnInitDlg   )
				MESSAGE_HANDLER     (WM_MOUSEMOVE   ,  OnMouseMove )
				MESSAGE_HANDLER     (WM_LBUTTONDOWN ,  OnLButtonDn )
				MESSAGE_HANDLER     (WM_LBUTTONUP   ,  OnLButtonUp )
				MESSAGE_HANDLER     (WM_SYSCOMMAND  ,  OnSysCommand)
			END_MSG_MAP()
		public:
			CTaskDlgImpl(IControlNotify&);
			~CTaskDlgImpl(void);
		private:
			LRESULT OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnEraseBg   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnInitDlg   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnMouseMove (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnLButtonDn (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnLButtonUp (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
		};
	private:
		CTaskDlg::CTaskDlgImpl  m_dlg;
	public:
		CTaskDlg(void);
		~CTaskDlg(void);
	private: // IControlNotify
		HRESULT    IControlNotify_OnClick(const UINT ctrlId) override sealed;
		VOID       IControlNotify_OnEvent(const UINT ctrlId);
	public:
		HRESULT    DoModal(CCustomTaskEx& _task);
	private:
		CTaskDlg(const CTaskDlg&);
		CTaskDlg& operator= (const CTaskDlg&);
	};
}}}

#endif/*_TMONTASKDLG_H_2D047366_9399_4cef_8832_1DF4B3BDABA6_INCLUDED*/