/*
	Created by Tech_dog (VToropov) on 3-Dec-2015 at 12:57:10am, GMT+7, Phuket, Rawai, Thursday;
	This is Total Monitoring application UI common definition implementation file.
*/
#include "StdAfx.h"
#include "TMon_UI_Defs.h"

using namespace tmon::UI;

////////////////////////////////////////////////////////////////////////////

CTabPageBase::CTabPageBase(const UINT RID, ::WTL::CTabCtrl& ctrl_ref, ITabPageCallback& evt_snk_ref):
	m_ctrl_ref(ctrl_ref),
	m_evt_sink(evt_snk_ref),
	IDD(RID),
	m_bInitilized(false),
	m_nIndex(-1)
{
}

CTabPageBase::~CTabPageBase(void)
{
}

////////////////////////////////////////////////////////////////////////////

LRESULT   CTabPageBase::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	RECT rcArea = {0};
	m_ctrl_ref.GetWindowRect(&rcArea);
	m_ctrl_ref.ScreenToClient(&rcArea);
	m_ctrl_ref.AdjustRect(false, &rcArea);
	TBaseDialog::SetWindowPos(0, rcArea.left, rcArea.top, __W(rcArea), __H(rcArea), SWP_NOZORDER|SWP_NOACTIVATE);
	if (::WTL::CTheme::IsThemingSupported())
	{
		HRESULT hr_ = ::EnableThemeDialogTexture(*this, ETDT_ENABLETAB);
		ATLVERIFY(SUCCEEDED(hr_));
	}
	m_evt_sink.TabPage_OnEvent(uMsg, wParam, lParam, bHandled);
	return 0;
}

LRESULT   CTabPageBase::OnPagePaint(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	::WTL::CPaintDC dc(TBaseDialog::m_hWnd);
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

INT       CTabPageBase::Index(void)const
{
	return m_nIndex;
}

VOID      CTabPageBase::Index(const INT nIndex)
{
	m_nIndex = nIndex;
}

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI
{
	VOID       AlignComboHeightTo(const CWindow& _host, const UINT _combo_id, const UINT _height)
	{
		CWindow ctrl_ = _host.GetDlgItem(_combo_id);
		if (!ctrl_)
			return;

		static const INT _COMBOBOX_EDIT_MARGIN = 3;

		const INT pixHeight = _height - _COMBOBOX_EDIT_MARGIN * 2;
		LRESULT res__ = ERROR_SUCCESS;
		res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM)-1, pixHeight); ATLASSERT(res__ != CB_ERR);
		res__ = ctrl_.SendMessage(CB_SETITEMHEIGHT, (WPARAM) 0, pixHeight); ATLASSERT(res__ != CB_ERR);
	}

	VOID       AlignCtrlsByHeight(const CWindow& _host, const UINT _ctrl_0, const UINT _ctrl_1, const SIZE& _shifts)
	{
		RECT rc_0 = {0};
		CWindow ctrl_;
		ctrl_ = _host.GetDlgItem(_ctrl_0);
		if (ctrl_)
		{
			ctrl_.GetWindowRect(&rc_0);
			::MapWindowPoints(HWND_DESKTOP, _host, (LPPOINT)&rc_0, 0x2);
		}
		ctrl_ = _host.GetDlgItem(_ctrl_1);
		if (ctrl_ && !::IsRectEmpty(&rc_0))
		{
			RECT rc_1 = {0};
			ctrl_.GetWindowRect(&rc_1);
			::MapWindowPoints(HWND_DESKTOP, _host, (LPPOINT)&rc_1, 0x2);
			rc_1.top    = rc_0.top    + _shifts.cx;
			rc_1.bottom = rc_0.bottom + _shifts.cy;
			ctrl_.SetWindowPos(
					NULL,
					&rc_1,
					SWP_NOZORDER|SWP_NOACTIVATE
				);
		}
	}
}}

/////////////////////////////////////////////////////////////////////////////

CCtrlAutoState::CCtrlAutoState(const CWindow& _host, const UINT ctrlId, const bool bEnabled):
m_host(_host), m_ctrlId(ctrlId), m_bEnabled(bEnabled)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
	{
		m_bEnabled = !!ctrl_.IsWindowEnabled();
		if (m_bEnabled != bEnabled)
			ctrl_.EnableWindow(static_cast<BOOL>(bEnabled));
	}
}

CCtrlAutoState::~CCtrlAutoState(void)
{
	CWindow ctrl_ = m_host.GetDlgItem(m_ctrlId);
	if (ctrl_)
		ctrl_.EnableWindow(static_cast<BOOL>(m_bEnabled));
}

/////////////////////////////////////////////////////////////////////////////