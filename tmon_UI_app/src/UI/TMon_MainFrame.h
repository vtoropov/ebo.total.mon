#ifndef _TMONMAINFRAME_H_180C8840_2163_4314_8EFE_08AB50202A7E_INCLUDED
#define _TMONMAINFRAME_H_180C8840_2163_4314_8EFE_08AB50202A7E_INCLUDED
/*
	Created by Tech_dog (VToropov) on 23-Nov-2015 at 7:46:16pm, GMT+7, Phuket, Rawai, Monday,
	This is Total Monitoring Application Main Frame class declaration file.
*/
#include "UIX_Image.h"
#include "UIX_Renderer.h"
#include "UIX_Button.h"

#include "TMon_TabStrip.h"
#include "TMon_TabPage_Home.h"
#include "TMon_TabPage_Scan.h"
#include "TMon_TabPage_Threats.h"
#include "TMon_TabPage_Update.h"
#include "TMon_TabPage_Setup.h"
#include "TMon_TabPage_Help.h"

#include "TMon_StatusBarEx.h"

namespace tmon { namespace UI
{
	using ex_ui::controls::CImage;
	using ex_ui::draw::renderers::CBackgroundTileRenderer;

	using ex_ui::controls::defs::IControlNotify;
	using ex_ui::controls::CButton;

	using tmon::UI::components::CTabStrip;
	using tmon::UI::components::CTabPageHome;
	using tmon::UI::components::CTabPageScan;
	using tmon::UI::components::CTabPageThreats;
	using tmon::UI::components::CTabPageUpdate;
	using tmon::UI::components::CTabPageSetup;
	using tmon::UI::components::CTabPageHelp;

	using tmon::UI::components::CStatusBarEx;

	class CMainFrame : private IControlNotify
	{
	private:
		class CMainDialogImpl :
			public  ::ATL::CDialogImpl<CMainDialogImpl>
		{
			typedef ::ATL::CDialogImpl<CMainDialogImpl> TDialog;
			friend class CMainFrame;
		private:
			struct CMouseCaptureData
			{
				bool     _captured;
				POINT    _anchor;
				CMouseCaptureData(void):_captured(false){_anchor.x = _anchor.y = 0;}
				void     Reset(void){ _captured = false; _anchor.x = _anchor.y = 0;}
			};
		private:
			CMouseCaptureData        m_capture;
			RECT                     m_drag_rc;
			CBackgroundTileRenderer  m_bkgnd;
			CImage                   m_header;
			CStatusBarEx&            m_status;
			CButton                  m_close;
			CButton                  m_mnmze;
			CTabStrip                m_tabs;
			CTabPageHome             m_pg_home;
			CTabPageScan             m_pg_scan;
			CTabPageThreats          m_pg_logs;
			CTabPageUpdate           m_pg_update;
			CTabPageSetup            m_pg_setup;
			CTabPageHelp             m_pg_help;
			bool                     m_hidden;
		public:
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CMainDialogImpl)
				MESSAGE_HANDLER     (WM_DESTROY     ,  OnDestroy   )
				MESSAGE_HANDLER     (WM_ERASEBKGND  ,  OnEraseBknd )
				MESSAGE_HANDLER     (WM_INITDIALOG  ,  OnInitDialog)
				MESSAGE_HANDLER     (WM_MOUSEMOVE   ,  OnMouseMove )
				MESSAGE_HANDLER     (WM_LBUTTONDOWN ,  OnLButtonDn )
				MESSAGE_HANDLER     (WM_LBUTTONUP   ,  OnLButtonUp )
				MESSAGE_HANDLER     (WM_WINDOWPOSCHANGING, OnPosChange)
				MESSAGE_HANDLER     (WM_SHOWMAINFORM,  OnShowDialog)
				MESSAGE_HANDLER     (WM_SYSCOMMAND  ,  OnSysCommand) /*ALT+F4*/
				COMMAND_ID_HANDLER  (IDCANCEL       ,  OnEscapeKey )
			END_MSG_MAP()
		public:
			CMainDialogImpl(IControlNotify&);
			~CMainDialogImpl(void);
		private:
			LRESULT OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnEraseBknd (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnEscapeKey (WORD     , WORD         , HWND         ,  BOOL& bHandled);
			LRESULT OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnMouseMove (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnLButtonDn (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnLButtonUp (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnPosChange (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnShowDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
			LRESULT OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled);
		};
	private:
		CMainFrame::CMainDialogImpl  m_dlg;
	public:
		CMainFrame(void);
		~CMainFrame(void);
	private: // IControlNotify
		HRESULT    IControlNotify_OnClick(const UINT ctrlId) override sealed;
		VOID       IControlNotify_OnEvent(const UINT ctrlId);
	public:
		HRESULT    DoModal(void);
	private:
		CMainFrame(const CMainFrame&);
		CMainFrame& operator= (const CMainFrame&);
	};
}}

#endif/*_TMONMAINFRAME_H_180C8840_2163_4314_8EFE_08AB50202A7E_INCLUDED*/