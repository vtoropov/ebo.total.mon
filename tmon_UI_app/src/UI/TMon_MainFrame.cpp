/*
	Created by Tech_dog (VToropov) on 23-Nov-2015 at 9:23:38pm, GMT+7, Phuket, Rawai, Monday,
	This is Total Monitoring Main Frame class implementation file.
*/
#include "StdAfx.h"
#include "TMon_MainFrame.h"
#include "TMon_Resource.h"

using namespace tmon;
using namespace tmon::UI;
using namespace tmon::UI::components;

#include "Shared_GenericAppObject.h"

using namespace shared::lite::common;

#include "UIX_GdiProvider.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls;

#include "UIX_MouseHandler.h"

using namespace ex_ui::frames;

/////////////////////////////////////////////////////////////////////////////

namespace tmon { namespace UI { namespace details
{
	class    CMainFrame_Layout
	{
	public:
		enum { // from PSD images
			e_frame_h  = 590,
			e_frame_w  = 850,
			e_close_h  =  24,
			e_close_w  =  24,
			e_margin_v =  10,
			e_margin_h =  10,
			e_header_h =  70,
			e_header_w = 850,
			e_status_h =  30,
			e_status_w = 850,
		};
	private:
		CWindow&      m_frame_ref;
		RECT          m_client_area;
	public:
		CMainFrame_Layout(CWindow& frame_ref) : m_frame_ref(frame_ref)
		{
			if (m_frame_ref)
				m_frame_ref.GetClientRect(&m_client_area);
			else
				::SetRectEmpty(&m_client_area);
		}
	public:
		RECT          GetCloseRect  (void)const
		{
			const INT nLeft = CMainFrame_Layout::e_frame_w - CMainFrame_Layout::e_close_w - CMainFrame_Layout::e_margin_h;
			const INT nTop  = CMainFrame_Layout::e_margin_v;
			RECT rc_ = {
					nLeft,
					nTop ,
					nLeft + CMainFrame_Layout::e_close_w,
					nTop  + CMainFrame_Layout::e_close_h
				};
			return rc_;
		}

		RECT          GetDragRect   (void)const
		{
			RECT rc_ = {
					0,
					0,
					CMainFrame_Layout::e_frame_w - CMainFrame_Layout::e_close_w * 2 - CMainFrame_Layout::e_margin_h,
					CMainFrame_Layout::e_header_h
				};
			return rc_;
		}

		RECT          GetHeaderRect (void)const
		{
			RECT rc_ = {
					0,
					0,
					CMainFrame_Layout::e_header_w,
					CMainFrame_Layout::e_header_h
				};
			return rc_;
		}

		RECT          GetMinBtnRect (void)const
		{
			RECT rc_ = this->GetCloseRect();
			::OffsetRect(&rc_, -CMainFrame_Layout::e_close_w, 0);
			return rc_;
		}

		RECT          GetPageRect   (const SIZE& _sz)const
		{
			RECT rc_ = m_client_area;
			::InflateRect(
					&rc_,
					-CMainFrame_Layout::e_margin_h,
					-CMainFrame_Layout::e_status_h
				);
			rc_.top = this->GetTabsRect(_sz).bottom;
			return rc_;
		}

		RECT          GetStatusRect (void)const
		{
			RECT rc_ = {
					0, 
					CMainFrame_Layout::e_frame_h - CMainFrame_Layout::e_status_h,
					CMainFrame_Layout::e_frame_w,
					CMainFrame_Layout::e_frame_h
				};
			return rc_;
		}

		RECT          GetTabsRect   (const SIZE& _sz)const
		{
			RECT hd_ = this->GetHeaderRect();
			RECT rc_ = {
					0,
					hd_.bottom,
					CMainFrame_Layout::e_frame_w,
					hd_.bottom + _sz.cy
				};
			return rc_;
		}

		VOID          RecalcPosition(void)
		{
			const SIZE sz_ = {
				details::CMainFrame_Layout::e_frame_w,
				details::CMainFrame_Layout::e_frame_h
			};

			RECT rc_ = {0};
			m_frame_ref.GetWindowRect(&rc_);
			if (sz_.cx > 0)
				rc_.right = rc_.left + sz_.cx;
			if (sz_.cy > 0)
				rc_.bottom = rc_.top + sz_.cy;
			::AdjustWindowRect(
					&rc_,
					(DWORD)m_frame_ref.GetWindowLong(GWL_STYLE),
					FALSE
				);
			{
				m_frame_ref.SetWindowPos(
						NULL, 
						&rc_,
						SWP_NOZORDER|SWP_NOACTIVATE|SWP_NOMOVE
					);
				m_frame_ref.GetClientRect(&m_client_area);
			}
			m_frame_ref.CenterWindow();
		}
	private:
		static RECT   GetAvailableArea(void)
		{
			const POINT ptZero = {0};
			const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
			MONITORINFO mInfo  = {0};
			mInfo.cbSize = sizeof(MONITORINFO);
			::GetMonitorInfo(hMonitor, &mInfo);
			return mInfo.rcWork;
		}
	};
}}}

/////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainDialogImpl::CMainDialogImpl(IControlNotify& _notify): 
	IDD(IDD_TMON_MAIN_DLG),
	m_header (&m_bkgnd),
	m_bkgnd  (IDR_TMON_MAIN_DLG_BKGND, *this),
	m_close  (IDC_TMON_MAIN_DLG_CLOSE, *(m_header.GetRendererPtr()), _notify),
	m_mnmze  (IDC_TMON_MAIN_DLG_MIN  , *(m_header.GetRendererPtr()), _notify),
	m_tabs   (m_bkgnd , _notify),
	m_pg_home(m_bkgnd , _notify),
	m_pg_scan(m_bkgnd),
	m_pg_logs(m_bkgnd),
	m_pg_help(m_bkgnd),
	m_pg_setup(m_bkgnd),
	m_pg_update(m_bkgnd),
	m_status(global::GetStatusCtrlRef()),
	m_hidden(global::GetSettings().General().StartMinimized())
{
	HRESULT hr_ = S_OK;
	const HINSTANCE hInstance = ::ATL::_AtlBaseModule.GetModuleInstance();
	{
		hr_ = m_close.SetImage(eControlState::eNormal   , IDR_TMON_MAIN_DLG_CLOSE_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_close.SetImage(eControlState::eDisabled , IDR_TMON_MAIN_DLG_CLOSE_NORM, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_close.SetImage(eControlState::ePressed  , IDR_TMON_MAIN_DLG_CLOSE_PRES, hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_close.SetImage(eControlState::eHovered  , IDR_TMON_MAIN_DLG_CLOSE_HOT , hInstance); ATLASSERT(S_OK == hr_);
	}
	{
		hr_ = m_mnmze.SetImage(eControlState::eNormal   , IDR_TMON_MAIN_DLG_MIN_NORM  , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_mnmze.SetImage(eControlState::eDisabled , IDR_TMON_MAIN_DLG_MIN_NORM  , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_mnmze.SetImage(eControlState::ePressed  , IDR_TMON_MAIN_DLG_MIN_PRES  , hInstance); ATLASSERT(S_OK == hr_);
		hr_ = m_mnmze.SetImage(eControlState::eHovered  , IDR_TMON_MAIN_DLG_MIN_HOT   , hInstance); ATLASSERT(S_OK == hr_);
	}
	::SetRectEmpty(&m_drag_rc);
}

CMainFrame::CMainDialogImpl::~CMainDialogImpl(void)
{
}

/////////////////////////////////////////////////////////////////////////////

LRESULT CMainFrame::CMainDialogImpl::OnDestroy   (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;

	global::GetSystemTray().SetMainFrameHandle(NULL);
	global::GetSystemTray().HideIcon();
	global::GetScannerRef().Terminate();

	CMouseEvtHandler::GetObjectRef().Unsubscribe(TDialog::m_hWnd);

	m_pg_update.Destroy();
	m_pg_setup.Destroy();
	m_pg_help.Destroy();
	m_pg_logs.Destroy();
	m_pg_scan.Destroy();
	m_pg_home.Destroy();
	m_tabs.Destroy();
	m_mnmze.Destroy();
	m_close.Destroy();
	m_header.Destroy();
	m_status.Destroy();
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnEraseBknd (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = TRUE;

	RECT rc_ = {0};
	TDialog::GetClientRect(&rc_);

	CZBuffer dc_((HDC)wParam, rc_);

	::OffsetRect(&rc_, -rc_.left, -rc_.top);
	m_bkgnd.Draw(dc_, rc_);

	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnEscapeKey (WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled)
{
	wNotifyCode; wID; hWndCtl; bHandled = TRUE;
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnInitDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	{
		LONG_PTR l_style = TDialog::GetWindowLongPtr(GWL_STYLE);
		l_style &= ~WS_BORDER;
		l_style &= ~WS_DLGFRAME;
		TDialog::SetWindowLongPtr(GWL_STYLE, l_style);
	}
	details::CMainFrame_Layout layout(*this);
	layout.RecalcPosition();
	{
		CApplicationIconLoader loader(IDR_TMON_MAIN_DLG_ICON);
		TDialog::SetIcon(loader.DetachBigIcon(), TRUE);
		TDialog::SetIcon(loader.DetachSmallIcon(), FALSE);
	}
	HRESULT hr_ = S_OK;
	{
		const RECT rc_ = layout.GetHeaderRect();
		hr_ = m_header.Create(
				TDialog::m_hWnd,
				rc_,
				NULL,
				0
			);
		if (!FAILED(hr_))
			hr_ = m_header.SetImage(IDR_TMON_MAIN_DLG_HEADER);
		if (!FAILED(hr_))
			hr_ = m_header.UpdateLayout();
	}
	{
		RECT rc_ = layout.GetCloseRect();
		hr_ = m_close.Create(
				m_header.GetWindow_Ref(),
				&rc_,
				false
			);
	}
	{
		RECT rc_ = layout.GetMinBtnRect();
		hr_ = m_mnmze.Create(
				m_header.GetWindow_Ref(),
				&rc_,
				false
			);
	}
	{
		RECT rc_ = layout.GetStatusRect();
		hr_ = m_status.Create(
				TDialog::m_hWnd,
				rc_
			);
	}
	const SIZE szTabs = m_tabs.GetDefaultSize();
	{
		const RECT rc_ = layout.GetTabsRect(szTabs);
		hr_ = m_tabs.Create(
				TDialog::m_hWnd,
				rc_
			);
	}
	const RECT rcPage = layout.GetPageRect(szTabs);

	hr_ = m_pg_help.Create(*this, rcPage, false);
	if (!FAILED(hr_))
		hr_ = m_pg_help.SetParentRenderer(&m_bkgnd);

	hr_ = m_pg_setup.Create(*this, rcPage, false);
	if (!FAILED(hr_))
		hr_ = m_pg_setup.SetParentRenderer(&m_bkgnd);

	hr_ = m_pg_update.Create(*this, rcPage, false);
	if (!FAILED(hr_))
		hr_ = m_pg_update.SetParentRenderer(&m_bkgnd);

	hr_ = m_pg_logs.Create(*this, rcPage, false);
	if (!FAILED(hr_))
		hr_ = m_pg_logs.SetParentRenderer(&m_bkgnd);

	hr_ = m_pg_scan.Create(*this, rcPage, false);
	if (!FAILED(hr_))
		hr_ = m_pg_scan.SetParentRenderer(&m_bkgnd);

	hr_ = m_pg_home.Create(*this, rcPage, true);
	if (!FAILED(hr_))
		hr_ = m_pg_home.SetParentRenderer(&m_bkgnd);
		

	m_drag_rc = layout.GetDragRect();
	CMouseEvtHandler::GetObjectRef().Subscribe(TDialog::m_hWnd);

	global::GetSystemTray().SetMainFrameHandle(*this);
	// it's necessary to call this method after all pages are created and subscribed to engine events, otherwise, deadlock occurs
#if !defined(_NO_AVS)
	m_pg_scan.CheckAvsEngineState();
#endif
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnMouseMove (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (m_capture._captured)
	{
		const POINT local_ = {GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam)};
		const SIZE  delta_ = {local_.x - m_capture._anchor.x, local_.y - m_capture._anchor.y};
		RECT rc_ = {0};
		TDialog::GetWindowRect(&rc_);
		::OffsetRect(&rc_, delta_.cx, delta_.cy);
		TDialog::SetWindowPos(NULL, &rc_, SWP_NOSIZE|SWP_NOZORDER);
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnLButtonDn (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (!m_capture._captured && !::IsRectEmpty(&m_drag_rc))
	{
		POINT local_ = {
				GET_X_LPARAM(lParam),
				GET_Y_LPARAM(lParam)
			};
		::MapWindowPoints(HWND_DESKTOP, TDialog::m_hWnd, &local_, 0x1);
		if (::PtInRect(&m_drag_rc, local_))
		{
			TDialog::SetCapture();
			m_capture._captured = true;
			m_capture._anchor = local_;
			TDialog::SetFocus();
		}
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnLButtonUp (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	if (m_capture._captured)
	{
		m_capture.Reset();
		::ReleaseCapture();
	}
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnPosChange (UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
#if !defined(_NO_AVS)
	if (m_hidden)
	{
		WINDOWPOS* pos_ = reinterpret_cast<WINDOWPOS*>(lParam);
		if (pos_)
			pos_->flags &= ~SWP_SHOWWINDOW;
	}
#endif
	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnShowDialog(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled;
	m_hidden = false;
	TDialog::ShowWindow(SW_SHOW);

	TDialog::SetWindowPos(
			HWND_TOPMOST  , 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE
		);
	TDialog::SetWindowPos(
			HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE
		);

	global::GetSystemTray().HideIcon();

	return 0;
}

LRESULT CMainFrame::CMainDialogImpl::OnSysCommand(UINT uMsg, WPARAM wParam, LPARAM lParam,  BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam)
	{
	case SC_CLOSE:
		{
			TDialog::ShowWindow(SW_HIDE);
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CMainFrame::CMainFrame(void) : m_dlg(*this)
{
}

CMainFrame::~CMainFrame(void)
{
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFrame::IControlNotify_OnClick(const UINT ctrlId)
{
	switch(ctrlId)
	{
	case IDC_TMON_MAIN_DLG_MIN:
		{
			const bool bStartMinimized = global::GetSettings().General().StartMinimized();
			if (bStartMinimized)
			{
				m_dlg.ShowWindow(SW_HIDE);
				global::GetSystemTray().ShowIcon(
						_T("Total Monitoring protects your system")
					);
			}
			else
			{
				WINDOWPLACEMENT place_ = {0};
				m_dlg.GetWindowPlacement(&place_);
				place_.showCmd = SW_MINIMIZE;
				m_dlg.SetWindowPlacement(&place_);
			}
		} break;
	case IDC_TMON_MAIN_DLG_CLOSE:
		{
			m_dlg.ShowWindow(SW_HIDE);
			m_dlg.EndDialog(IDCANCEL);
		} break;
	case IDC_TMON_HOME_BTN_SCANNER:
		{
			m_dlg.m_tabs.SelectedTab(CTabIndex::eScan);
			this->IControlNotify_OnEvent(ctrlId);
		} break;
	case IDC_TMON_HOME_BTN_THREAT:
		{
			m_dlg.m_tabs.SelectedTab(CTabIndex::eThreat);
			this->IControlNotify_OnEvent(ctrlId);
		} break;
	case IDC_TMON_HOME_BTN_UPDATE:
		{
			m_dlg.m_tabs.SelectedTab(CTabIndex::eUpdate);
			this->IControlNotify_OnEvent(ctrlId);
		} break;
	case IDC_TMON_MAIN_DLG_TABS:
		{
			this->IControlNotify_OnEvent(ctrlId);
		} break;
	}
	return S_OK;
}

VOID       CMainFrame::IControlNotify_OnEvent(const UINT ctrlId)
{
	ctrlId;
	const INT nIndex = m_dlg.m_tabs.SelectedTab();

	m_dlg.m_pg_home.Show(CTabIndex::eHome   == nIndex);
	m_dlg.m_pg_scan.Show(CTabIndex::eScan   == nIndex);
	m_dlg.m_pg_logs.Show(CTabIndex::eThreat == nIndex);
	m_dlg.m_pg_help.Show(CTabIndex::eHelp   == nIndex);
	m_dlg.m_pg_setup.Show(CTabIndex::eSetup  == nIndex);
	m_dlg.m_pg_update.Show(CTabIndex::eUpdate == nIndex);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CMainFrame::DoModal(void)
{
	m_dlg.DoModal();

	HRESULT hr_ = S_OK;
	return  hr_;
}