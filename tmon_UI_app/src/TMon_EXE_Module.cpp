/*
	Created by Tech_dog (VToropov) on 23-Nov-2015 at 6:38:36pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring Application Entry Point implementation file.
*/
#include "StdAfx.h"
#include "UIX_GdiProvider.h"

#include "TMon_MainFrame.h"
#include "TMon_Splash_Dlg.h"

using namespace tmon;
using namespace tmon::UI;

#include "Shared_SystemCore.h"

using namespace shared::lite::sys_core;

#include "TMon_NoUI_Initializer.h"

using namespace tmon::common;

CAppModule _Module;
/////////////////////////////////////////////////////////////////////////////

INT RunModal(VOID)
{
	using tmon::UI::components::CSplashDlg;
	using tmon::common::CSettings;
	using tmon::common::CAvsInitializer;

	INT nRet = 0;
	{
		CSettings& cfg_ = global::GetSettings();
		HRESULT hr_ = cfg_.Load();
#if !defined(_NO_AVS)
		const bool bSilent = cfg_.General().IsSilentStart();// no splash or run minimized: in both cases a splash is hidden, otherwise, it's shown
		if (!bSilent)
		{
			CSplashDlg splash_;
			hr_ = splash_.Show();
		}
		if (cfg_.General().StartMinimized())
		{
			CAvsInitializer avs_init;
			hr_ = avs_init.Start();
		}
#endif		
		CMainFrame frame;
		hr_ = frame.DoModal();

		cfg_.Save();
	}
	return nRet;
}

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, INT nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;

	CCoInitializer com(false);
	HRESULT hr_ = ::CoInitializeSecurity(
						NULL,
						-1,
						NULL,
						NULL,
						RPC_C_AUTHN_LEVEL_NONE,
						RPC_C_IMP_LEVEL_IDENTIFY,
						NULL,
						EOAC_NONE,
						NULL
					);
	ATLASSERT(SUCCEEDED(hr_));
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm:
		________________________________________________
		we need to assign lib id to link ATL DLL statically,
		otherwise we get annoying fucking message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;
	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr_ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr_))
	{
		ATLASSERT(FALSE);
		return 1;
	}

	ex_ui::draw::CGdiPlusLibLoader  gdi_loader;

	nResult = ::RunModal();
	_Module.Term();
	
	return nResult;
}