/*
	Created by Tech_dog (VToropov) on 18-Jan-2016 at 0:19:29am, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring System Tray Wrapper class implementation file.
*/
#include "StdAfx.h"
#include "TMon_SysTray.h"
#include "TMon_Resource.h"

using namespace tmon::common;

#include "TMon_UI_Defs.h"

using namespace tmon::UI;

/////////////////////////////////////////////////////////////////////////////

CSystemTray::CSystemTray(void) : m_tray(*this, 1), m_frame(NULL)
{
	m_tray.Initialize();
}

CSystemTray::~CSystemTray(void)
{
	m_tray.Terminate();
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CSystemTray::HideIcon(void)
{
	if (!m_tray.IsInitialized())
		return OLE_E_BLANK;
	HRESULT hr_ = m_tray.HideIcon();
	return  hr_;
}

VOID     CSystemTray::SetMainFrameHandle(const HWND _frame)
{
	m_frame = _frame;
}

HRESULT  CSystemTray::SetTooltip(LPCTSTR pszTooltip)
{
	if (!m_tray.IsIconShown())
		return S_FALSE;

	HRESULT hr_ = m_tray.SetTooltip(pszTooltip);
	return  hr_;
}

HRESULT  CSystemTray::ShowIcon(LPCTSTR pszTooltip)
{
	if (!m_tray.IsInitialized())
		return OLE_E_BLANK;
	HRESULT hr_ = m_tray.ShowIcon(IDR_TMON_MAIN_DLG_ICON, pszTooltip);
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CSystemTray::NotifyTray_OnClickEvent(const UINT eventId)
{
	eventId;

	CWindow host_ = m_frame;

	if (host_.IsWindow())
	{
		if (host_.IsWindowVisible() == FALSE)
			host_.SendMessage(WM_SHOWMAINFORM);
	}

	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT  CSystemTray::NotifyTray_OnContextMenuEvent(const UINT eventId)
{
	eventId;
	HRESULT hr_ = S_OK;
	return  hr_;
}